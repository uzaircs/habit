#### 1. Generate Keystore using following command

`cd android/app`
`keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000`


#### 2. Replace
`com.flowtech.habit` 
with your company name  

#### Rename paths with com/company/application to your project name

#### 3. Register Icons  
```npm i @ui-kitten/eva-icons react-native-svg```

#### Build APK
`npm run-script build`

#### Install APK to Device / Emulator
`npm run-script install-only`

##### Run in Emulator
`npx react-native run-android`

##### OR

Click *Emulate* icon in **VSCODE** 
Install vscode commandbar to view buttons  
`https://marketplace.visualstudio.com/items?itemName=gsppvo.vscode-commandbar`