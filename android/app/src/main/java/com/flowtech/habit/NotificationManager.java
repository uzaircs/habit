package com.flowtech.habit;

import android.app.Notification;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.util.concurrent.TimeUnit;

public class NotificationManager extends ReactContextBaseJavaModule {
    public static final String CHANNEL_ID = "customNotificationChannel";
    private NotificationManagerCompat notificationManager;
    private static ReactApplicationContext reactContext;
    private String packageName;
    private static final int NOTIFICATION_ID = 24411;

    public String getPackageName() {
        return this.packageName;
    }

    @ReactMethod
    public void show(String time) {
        {
            try {
                long i = Long.parseLong(time);
                new CountDownTimer(i, 1000) {

                    public void onTick(long millisUntilFinished) {

                        showNotification(millisUntilFinished);

                    }

                    public void onFinish() {

                    }
                }.start();


            } catch (NumberFormatException er) {

            }
        }

    }

    public void cancel() {
        this.notificationManager.cancel(this.NOTIFICATION_ID);
    }

    public void showNotification(long remaining) {
        long minutes = TimeUnit.MILLISECONDS.toMinutes(remaining);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(remaining) % 60;

        RemoteViews collapsedView = new RemoteViews(getPackageName(),
                R.layout.notification_collapsed);

        RemoteViews expandedView = new RemoteViews(getPackageName(),
                R.layout.notification_expanded);
        collapsedView.setTextViewText(R.id.remaining_collapsed_minutes, Long.toString(minutes));
        collapsedView.setTextViewText(R.id.remaining_collapsed_seconds, Long.toString(seconds));
        expandedView.setTextViewText(R.id.remaining_expanded_minutes, Long.toString(minutes));
        expandedView.setTextViewText(R.id.remaining_expanded_seconds, Long.toString(seconds));
        Notification notification = new NotificationCompat.Builder(this.getReactApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.android)
                .setCustomContentView(collapsedView)
                .setCustomBigContentView(expandedView)
                .setOnlyAlertOnce(true)
               .setOngoing(true)
                .build();


        notificationManager.notify(this.NOTIFICATION_ID, notification);
    }

    NotificationManager(ReactApplicationContext context, String packageName) {
        super(context);
        reactContext = context;
        this.packageName = packageName;
        this.notificationManager = NotificationManagerCompat.from(this.getReactApplicationContext());

    }

    @NonNull
    @Override
    public String getName() {
        return "NotificationManager";
    }
}
