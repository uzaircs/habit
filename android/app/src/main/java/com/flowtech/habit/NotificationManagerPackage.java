package com.flowtech.habit;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.flowtech.habit.NotificationManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NotificationManagerPackage implements ReactPackage {
    private String packageName;

    public NotificationManagerPackage(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        return Collections.emptyList();
    }

    @Override
    public List<NativeModule> createNativeModules(
            ReactApplicationContext reactContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new NotificationManager(reactContext, packageName));

        return modules;
    }

}
