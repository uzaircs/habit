
// Add your reducers here

import { Platform } from 'react-native';
import { combined } from '../reducers/index'
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
let enhancer = applyMiddleware(thunk)
if (__DEV__) {
    enhancer = composeWithDevTools(
        applyMiddleware(thunk)
    );
}
export default createStore(combined, enhancer)
