import { GetDiaryEntries, RemoveDiaryEntry, AddDiaryEntry, UpdateDiaryEntry } from "../models/api";
import { TimelineActionTypes, TimelineActionObjectType } from "../models/timelinelogs";
import { LogActionAdded } from "./timeline.action";

export enum DiaryActionType {
    LIST_UPDATED = '@diary/list-updated',
    ENTRY_ADDED = '@diary/entry-added',
    ENTRY_REMOVED = '@diary/entry-removed',
    ENTRY_UPDATED = '@diary/entry-updated',
    ENTRY_ID_CHANGED = '@diary/entry-id-changed'
}
export const uuidv4 = () => {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
export type DiaryAction = {
    type: DiaryActionType,
    payload?: any
}
export const DiaryEntryAdded = (entry) => {
    return dispatch => {
        entry.isBusy = true;
        let softID = uuidv4();
        console.log(entry);
        entry.softID = softID;

        AddDiaryEntry(entry).then((added: any) => {
            let action = {
                action: TimelineActionTypes.DIARY_ENTRY,
                subject: added.id,
                subject_type: TimelineActionObjectType.DIARY,
                actionDescription: added.title || 'Diary Entry',

            }
            /*     let savedEntry = {
                    title: added.title,
                    text: added.text,
                    status: added.status,
                    id: added.id,
                    tags: added.tags,
                    createdAt: added.createdAt,
                    updatedAt: added.updatedAt
                } */
            dispatch({ type: DiaryActionType.ENTRY_ADDED, payload: added });
            dispatch(LogActionAdded(action))
            /*    dispatch({ type: DiaryActionType.ENTRY_ID_CHANGED, payload: { softID, entry: added } }) */
        });
    }
}
export const DiaryEntryUpdated = (entry) => {

    return dispatch => {
        dispatch({ type: DiaryActionType.ENTRY_UPDATED, payload: entry });
        UpdateDiaryEntry(entry.id, entry).then(updated => dispatch({ type: DiaryActionType.ENTRY_UPDATED, payload: updated }));
    }
}
export const DiaryEntryRemoved = (id) => {
    RemoveDiaryEntry(id);
    return { type: DiaryActionType.ENTRY_REMOVED, payload: id };
}
export const DiaryListUpdated = (data) => {
    return { type: DiaryActionType.LIST_UPDATED, payload: data };
}
export const RequestDiaryListAsync = () => {
    return (dispatch, getState) => {
        let hasList = getState().diary && getState().diary.entries && getState().diary.entries.length > 0;
        if (!hasList) {
            GetDiaryEntries().then((entries) => {

                dispatch(DiaryListUpdated(entries));
            })
        } else {

        }
    }
}