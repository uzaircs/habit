import { SaveEntry, getEntries, getEntriesByDay, ChangeMood, DeleteEntry, GetEntry } from "../models/api"
export enum EntryActionType {
    LIST_UPDATED = '@entry-list/updated',
    UPDATE_PENDING = '@entry-list/updatePending',
    UPDATE_ERROR = '@entry-list/updateError',
    MOOD_CHANGED = '@entry-action/moodChanged',
    ENTRY_ADDED = '@entry-action/entryAdded'
}
export enum PendingActionTypes {
    SAVE = '@entry-action/save',
    CLEAR = '@entry-action/clear',
    ADD_ENTRY = '@entry-action/add-entry',
    ADD_ACTIVITY = '@entry-action/add-activity',
    REMOVE_ACTIVITY = '@entry-action/removeActivity',
    SAVE_ERROR = '@entry-action/saveError',
    DELETE_ENTRY = '@entry-action/deleteEntry',
    SAVE_SUCCESS = '@entry-action/saveSuccess',
    SAVE_STARTED = '@entry-action/saveStarted',

}
export type EntryAction = {
    type: EntryActionType,
    payload: any
}
export type PendingAction = {
    type: PendingActionTypes,
    payload?: any;
}

export const SaveStarted = () => {
    return { type: PendingActionTypes.SAVE_STARTED }
}
export const MoodChangeSync = async (entry_id, mood_id) => {
    return await ChangeMood(entry_id, mood_id);

}
export const MoodChanged = (entry_id, mood_id) => {
    return (dispatch, getState) => {
        MoodChangeSync(entry_id, mood_id).then((result) => {
            if (result) {
                let moods = getState().moods;
                if (moods && Array.isArray(moods)) {
                    let newMood = moods.filter((mood) => mood.id == mood_id);
                    if (newMood.length === 1) {
                        newMood = newMood[0];
                    }
                    dispatch({
                        type: EntryActionType.MOOD_CHANGED,
                        payload: { entry_id, newMood }
                    })
                }

            }

        })
    }
}
export const ClearPendingItems = (): PendingAction => {
    return {
        type: PendingActionTypes.CLEAR
    }
}
export const RemoveActivity = (activity: string): PendingAction => {
    return {
        type: PendingActionTypes.REMOVE_ACTIVITY,
        payload: { activity }
    }
}
export const AddEntry = (mood: string): PendingAction => {

    return {
        type: PendingActionTypes.ADD_ENTRY,
        payload: { mood }
    }
}
export const AddActivity = (activity: string): PendingAction => {

    return {
        type: PendingActionTypes.ADD_ACTIVITY,
        payload: { activity }
    }
}
export const success = (id) => {
    return dispatch => {
        GetEntry(id).then((entry) => {
            dispatch({ type: PendingActionTypes.SAVE_SUCCESS })
            dispatch({ type: EntryActionType.ENTRY_ADDED, payload: entry })
        })
    }
}
const saveToDb = (mood, activities, notes = null): Promise<string> => {
    return new Promise((resolve, reject) => {

        SaveEntry(mood, activities, { notes }).then((id: any) => {
            resolve(id);
        }).catch(err => reject(err));
    })

}
export const saveError = (): PendingAction => {
    return {
        type: PendingActionTypes.SAVE_ERROR
    }
}
export const RemoveEntry = (id) => {
    return dispatch => {
        DeleteEntry(id).then(() => {
            dispatch(RequestListAsync());
        })
    }

}
export const RequestList = () => {
    return {
        type: EntryActionType.UPDATE_PENDING
    }
}
export const ListReceived = (data: any) => {

    return {
        type: EntryActionType.LIST_UPDATED,
        payload: data
    }
}
export const ListUpdateFailed = (error: any) => {
    return {
        type: EntryActionType.UPDATE_ERROR,
        payload: error
    }
}
export const PopulateEntries = () => {
    return dispatch => {
        getEntries().then((entries) => {
         
            dispatch(ListReceived(entries));
        }).catch((error) => {
            dispatch(ListUpdateFailed(error));
        })
    }
}
export const RequestListAsync = () => {
    return (dispatch, getState) => {
        dispatch(RequestList());

    }
}
export const saveAsync = (notes = null) => {

    return (dispatch, getState) => {
        dispatch(SaveStarted());
        let { pending } = getState();

        if (!pending || !pending.mood) {
            dispatch(saveError);
        } else {
            let { mood, activity } = pending;

            saveToDb(mood, activity, notes).then((id) => {
                dispatch(success(id));
            }).catch(err => {
                dispatch(saveError());
            })
        }

    }
}