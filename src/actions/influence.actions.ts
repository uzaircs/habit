import { CalculateMoodInfluence } from "../components/reports/reports"

export enum InfluenceActionType {
    PENDING = 77874,
    INFLUENCE_UPDATED,
    INFLUENCE_ERROR
}

export type InfluenceAction = {
    type: InfluenceActionType,
    payload: any
}

export const UpdateRequest = () => {
    return { type: InfluenceActionType.PENDING, payload: {} }
}
export const UpdateSuccess = (data: any) => {
    return { type: InfluenceActionType.INFLUENCE_UPDATED, payload: data }
}
export const UpdateFailure = (error: any) => {
    return { type: InfluenceActionType.INFLUENCE_ERROR, payload: error }
}

export const ReqeustInfluenceAsync = (mood_id: string) => {
    return (dispatch, getState) => {
        dispatch(UpdateRequest());
        CalculateMoodInfluence(mood_id).then(result => {
            dispatch(UpdateSuccess(result));
        }).catch(err => {
            dispatch(UpdateFailure(err));
        })

    }
}


