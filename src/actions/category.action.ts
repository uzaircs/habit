import { getCategories, getActivities } from "../models/api"

export enum CategoryActionType {
    CATEGORY_UPDATED = 22,
    CATEGORY_PENDING = 23,
    CATEGORY_FAILED = 24,

}
export type CategoryAction = {
    type: CategoryActionType,
    payload: any
}

export type ActivityDataItem = {
    name: string;
    id: string;
    icon: string;
    icon_type: string;

}
export type CategoryDataItem = {
    name: string;
    id: string;
    icon: string;
    icon_type: string;
    activities: ActivityDataItem[];

}


export const CategoryUpdateStarted = () => {
    return { type: CategoryActionType.CATEGORY_PENDING }
}

export const CategoryUpdateCompleted = (data: CategoryDataItem[]) => {
    return { type: CategoryActionType.CATEGORY_UPDATED, payload: data }
}
const requestAsync = async () => {
    let items: CategoryDataItem[] = [];
    let categories = await getCategories();
    for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        let item: CategoryDataItem = {
            activities: [],
            icon: category.icon,
            icon_type: category.icon_type,
            id: category.id,
            name: category.name
        }
        let activities = await getActivities(category.id);
    
        for (let k = 0; k < activities.length; k++) {
            const activity = activities[k];
            let activityItem: ActivityDataItem = {
                icon: activity.icon, icon_type: activity.icon_type, id: activity.id, name: activity.name
            }
            item.activities.push(activityItem);

        }
        items.push(item);
    }
    return items;
}
export const RequestCategoryAsync = () => {
    return dispatch => {

        dispatch(CategoryUpdateStarted());
        requestAsync().then((data: CategoryDataItem[]) => {
            dispatch(CategoryUpdateCompleted(data));
        }).catch(err => {

        })
    }
}