import { GetRepeatTasks } from "../models/api";
import { TaskItem } from "../models/Tasks"

export enum RepeatActionType {
    REPEAT_ADDED = '@repeat-config/repeat-added',
    REPEAT_MODIFIED = '@repeat-config/repeat-modified',
    REPEAT_REMOVED = '@repeat-config/repeat-removed',
    POPULATED = '@repeat-config/repeat-list-populated',
}

export type RepeatAction = {
    type: RepeatActionType;
    payload?: any;
}
export const Add = (repeatContext: TaskItem) => {
    return dispatch => {
        dispatch({ type: RepeatActionType.REPEAT_ADDED, payload: repeatContext });
    }
}

export const Remove = (id: string) => {
    return dispatch => {
        dispatch({ type: RepeatActionType.REPEAT_REMOVED, payload: id });
    }
}
export const PopulateRepeatItems = () => {
    return dispatch => {
        GetRepeatTasks().then((tasks) => {
            if (tasks && Array.isArray(tasks) && tasks.length) {
                dispatch({ type: RepeatActionType.POPULATED, payload: tasks });
            }
        })
    }
}
export const Modified = (repeatContext: TaskItem) => {
    return dispatch => {
        dispatch({ type: RepeatActionType.REPEAT_MODIFIED, payload: repeatContext });
    }
}