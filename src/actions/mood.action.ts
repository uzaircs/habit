import { getMoods } from "../models/api"
export type MoodAction = {
    type: MoodActionType;
    payload?: any;
}
export enum MoodActionType {
    LIST_UPDATED = '@mood-list/updated',
    UPDATE_PENDING = '@mood-list/updatePending',
    UPDATE_ERROR = '@mood-list/updateError',
}
export const PopulateMoods = () => {
    return dispatch => {
        getMoods().then((moods) => {
            dispatch({ type: MoodActionType.LIST_UPDATED, payload: moods })
        })
    }
}