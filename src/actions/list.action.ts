import { GetListItems } from "../models/api"

export enum TaskListActionType {
    UPDATED = 1441,
    PENDING
}

export const RequestUpdateList = () => {
    return { type: TaskListActionType.PENDING }
}
export const ListUpdateSuccess = (data: any) => {
    return { type: TaskListActionType.UPDATED, payload: data }
}
export const UpdateListAsync = () => {
    return dispatch => {
        dispatch(RequestUpdateList());
        GetListItems().then(items => {
            dispatch(ListUpdateSuccess(items));
        })
    }
}