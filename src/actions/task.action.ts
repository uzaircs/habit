import { TaskItem } from "../models/Tasks"
import { AddTask as Add, GetTasks, ChangeTaskStatus, AttachTaskToList, AddReminder, RemoveReminder, GetListItem, ConfigureRepeat } from '../../src/models/api'
import { uuidv4 } from "./diary.action";
import { TimelineActionTypes, TimelineActionObjectType } from "../models/timelinelogs";
import { LogActionAdded } from "./timeline.action";
import { GetGoalProgress } from "./goal.action";



export enum TaskActionType {
    UPDATED = '@task/list-updated',
    PENDING = '@task/pending',
    BUSY = '@task/busy',
    TASK_ADDED = '@task/added',
    TASK_UPDATED = '@task/updated',
    TASK_COMPLETED = '@task/completed',
    TASK_INCOMPLETED = '@task/incompleted',
    LIST_ATTACHED = '@task/list-attached',
    TASK_DELETE = '@task/deleted',
    TASK_ID_UPDATE = '@task/id-update',
    APPLY_SORT = '@task/apply-sort',
    APPLY_FILTER = '@task/apply-filter',
    REMINDER_ADDED = '@task/reminder-added',
    REMINDER_REMOVED = '@task/reminder-removed',
    CUSTOM_POSITION = '@task/custom-position',
    REPEAT_CONFIG = '@task/repeat-update'

}
export const RandomRange = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
export const ApplySorting = (sortOption, isAscending) => {
    return dispatch => {
        dispatch({
            type: TaskActionType.APPLY_SORT,
            payload: { sortOption, isAscending }
        })
    }
}
export const CustomPosition = () => {
    return {
        type: TaskActionType.CUSTOM_POSITION
    }
}
export const ApplyFilter = (filterOption) => {

    return dispatch => {
        GetTasks().then((tasks) => {
            dispatch({
                type: TaskActionType.APPLY_FILTER,
                payload: { filterOption, tasks }
            })
        })

    }
}
const TaskAttachedToList = (id, list) => {
    return { type: TaskActionType.LIST_ATTACHED, payload: { id, list } };
}
export const AttachAction = (taskId, list) => {

    return async dispatch => {
        AttachTaskToList(taskId, list.id);
        dispatch(TaskAttachedToList(taskId, list));
    }
}
export type TaskAction = {
    type: TaskActionType,
    payload?: any
}
export const AddStarted = (task) => {

    return dispatch => {
        let uid = uuidv4();
        task.softID = uid;

        Add(task).then((t: any) => {
            const bindTask = () => {
                if (task.reminder && !isNaN(task.reminder)) {
                    let notificationId = RandomRange(10, 10 * 100);
                    AddReminder({
                        date: task.reminder, itemId: t.id, notificationId
                    }).then(() => {
                        dispatch({ type: TaskActionType.TASK_ADDED, payload: t });
                        dispatch(LogActionAdded(action))
                        /*    dispatch({ type: TaskActionType.TASK_ID_UPDATE, payload: { softID: uid, task: t } }); */
                        dispatch({ type: TaskActionType.REMINDER_ADDED, payload: { itemId: t.id, notificationId, date: task.reminder } });
                    });

                } else {
                    dispatch({ type: TaskActionType.TASK_ADDED, payload: t });
                    dispatch(GetGoalProgress());
                    dispatch(LogActionAdded(action))
                    /*  dispatch({ type: TaskActionType.TASK_ID_UPDATE, payload: { softID: uid, task: t } }); */
                }
            }
            let action = {
                action: TimelineActionTypes.TASK_CREATED,
                subject: t.id,
                subject_type: TimelineActionObjectType.TASK,
                actionDescription: t.name || 'Task Entry',

            }
            bindTask();


        })
    }
}
export const TaskCompletedStart = (id: any) => {

    return { type: TaskActionType.TASK_COMPLETED, payload: id }
}

export const TaskCompletedAsync = (id: any) => {
    return dispatch => {
        dispatch(TaskCompletedStart(id))
        ChangeTaskStatus(id, 1).then(done => {
            let action = {
                action: TimelineActionTypes.TASK_COMPLETED,
                subject: id,
                subject_type: TimelineActionObjectType.TASK,
                actionDescription: 'Task Completed',

            }
            dispatch(LogActionAdded(action))
            dispatch(GetGoalProgress());
        });
    }
}
export const TaskIncompleteStart = (id) => {

    return { type: TaskActionType.TASK_INCOMPLETED, payload: id }
}
export const AttachReminder = (taskId, date) => {
    let notificationId = RandomRange(10, 10 * 100);
    return dispatch => {
        RemoveReminder(taskId).then((result) => {
            if (result) {
                dispatch({ type: TaskActionType.REMINDER_REMOVED, payload: taskId })
            }
            AddReminder({
                date: date, itemId: taskId, notificationId
            }).then(added => {
                dispatch({ type: TaskActionType.REMINDER_ADDED, payload: { itemId: taskId, notificationId, date: date } });
            })

        });

    }
}
export const RemoveReminderAction = (taskId) => {
    return dispatch => {
        RemoveReminder(taskId).then(() => {
            dispatch({ type: TaskActionType.REMINDER_REMOVED, payload: taskId })
        });

    }
}
export const TaskIncompleteAsync = (id: any) => {
    return dispatch => {
        dispatch(TaskIncompleteStart(id));
        ChangeTaskStatus(id, 0).then(done => {
            let action = {
                action: TimelineActionTypes.TASK_INCOMPLETED,
                subject: id,
                subject_type: TimelineActionObjectType.TASK,
                actionDescription: 'Task Marked Incomplete',

            }
            dispatch(LogActionAdded(action))
            dispatch(GetGoalProgress());
        });
    }
}
export const TaskAdded = (data) => {
    return { type: TaskActionType.TASK_ID_UPDATE, payload: data };
}
export const AddTask = (task: TaskItem) => {
    return (dispatch, getState) => {

        dispatch(AddStarted(task));

    }
}

export const DeleteTaskAction = (id) => {
    return dispatch => {
        dispatch(GetGoalProgress());
        dispatch({ type: TaskActionType.TASK_DELETE, payload: id })
    }
}
export const UpdateRequest = () => {
    return { type: TaskActionType.PENDING }
}
export const ConfigureRepeatAction = (task, repeatConfig) => {
    return dispatch => {
        ConfigureRepeat(task, repeatConfig).then(result => {
            if (result) {
                dispatch({ type: TaskActionType.REPEAT_CONFIG, payload: { task, repeatConfig, context: result } });
            }
        })
    }
}
export const UpdateSuccess = (data: any) => {
    return { type: TaskActionType.UPDATED, payload: data }
}
export const TaskUpdateAction = (props) => {
    return { type: TaskActionType.TASK_UPDATED, payload: props }
}
export const Populate = () => {

    return dispatch => {
        GetTasks().then((tasks) => {
            dispatch(UpdateSuccess(tasks));
        })
    }
}
export const UpdateTaskListAsync = () => {

    return (dispatch, getState) => {
        if (!getState().tasks || !getState().tasks.tasks || getState().tasks.tasks.length <= 0) {
            dispatch(UpdateRequest());
            GetTasks().then((tasks) => {
                dispatch(UpdateSuccess(tasks));
            })
        } else {
            dispatch(UpdateSuccess([...getState().tasks.tasks]));
        }

    }
}