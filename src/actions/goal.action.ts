import { ChangeGoalStatus, DbUnixToDate, DeleteGoal, GetGoals, GetGoalTasksWithSubtasks, UpdateGoal } from "../models/api"
import { AddGoal as SaveGoal } from '../models/api'
export enum GoalActionType {
    UPDATED = '@goal/updated',
    PENDING = '@goal/pending',
    BUSY = '@goal/busy',
    GOAL_ADDED = '@goal/goalAdded',
    GOAL_UPDATED = '@goal/goalUpdated',
    GOAL_REMOVE = '@goal/goal-removed',
    GOAL_PROGRESS_UPDATED = '@goal/progress'
}
export type GoalAction = {
    type: GoalActionType,
    payload?: any
}
export const EditGoal = goal => {
    return dispatch => {

        if (goal.id) {
            UpdateGoal(goal.id, goal).then(() => {
                dispatch({ type: GoalActionType.GOAL_UPDATED, payload: goal });
                dispatch(GetGoalProgress());
            }).catch(err => {

            })
        }
    }
}
export const GetGoalProgress = () => {
    return (dispatch, getState) => {
        let progress = getState().goals.goals.map(goal => { return { goal, tasks: [] } })
        if (progress && progress.length) {
            let goals = progress.map(p => p.goal).slice();
            let goalProgress = [];
            let promises = [];
            for (let i = 0; i < goals.length; i++) {
                let goal = goals[i];
                let promise = GetGoalTasksWithSubtasks(goal.id).then(tasks => {
                    if (tasks && Array.isArray(tasks) && tasks.length) {
                        let progress = { goal, tasks };
                        goalProgress.push(progress);
                    } else {
                        let progress = { goal, tasks: [] };
                        goalProgress.push(progress);
                    }
                })
                promises.push(promise);
            }
            Promise.all(promises).then(() => {
                dispatch({ type: GoalActionType.GOAL_PROGRESS_UPDATED, payload: goalProgress })
            })
            /*  InteractionManager.runAfterInteractions(() => {
                 this.setState(() => {
                     return {
                         ready: true,
                         taskAttached: true,
                         goalProgress: goalProgress
                     }
                 })
             }) */

        }
    }
}
export const RemoveGoal = id => {
    return dispatch => {

        if (id) {
            DeleteGoal(id).then(() => {

                dispatch({ type: GoalActionType.GOAL_REMOVE, payload: id });
                dispatch(GetGoalProgress());
            })
        }
    }
}
export const AddGoalAction = (goal) => {

    return dispatch => {
        SaveGoal(goal).then(saved => {
            if (saved) {
                let savedGoal = [saved].map((g: any) => {
                    return {
                        name: g.name,
                        id: g.id,
                        priority: g.priority,
                        status: g.status,
                        createdAt: g.createdAt,
                        updatedAt: g.updatedAt,
                        DueDate: (() => {
                            if (g.DueDate) {
                                try {
                                    return DbUnixToDate(+g.DueDate)
                                } catch (error) {
                                    return null;
                                }
                            } return null;
                        })()
                    }
                })[0];
                if (saved) {
                    dispatch({ type: GoalActionType.GOAL_ADDED, payload: savedGoal })
                    dispatch(GetGoalProgress());
                }
            }
        })
    }
}
export const UpdateRequest = () => {
    return { type: GoalActionType.PENDING }
}
export const UpdateSuccess = (data: any) => {
    return { type: GoalActionType.UPDATED, payload: data }
}
export const GoalStatus = (id, status) => {
    return dispatch => {
        ChangeGoalStatus(id, status).then((goal) => {
            console.log(goal)
            dispatch({ type: GoalActionType.GOAL_UPDATED, payload: goal })
        })
    }
}
export const PopulateGoals = () => {
    return dispatch => {
        GetGoals().then((goals) => {
            dispatch(UpdateSuccess(goals));
            dispatch(GetGoalProgress());
        })
    }
}
