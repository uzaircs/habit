export enum PomodoroActionType {
    STARTED = 1021,
    PAUSED,
    STOPPED,
    CANCELLED,
    RESUMED
}
export type PomodoroAction = {
    type: PomodoroActionType;
    payload: any;
}
export const StartPomodoro = () => {
    return { type: PomodoroActionType.STARTED }
}
export const StopPomodoro = () => {
    return { type: PomodoroActionType.STOPPED }
}
export const CancelPomodoro = () => {
    return { type: PomodoroActionType.CANCELLED }
}
export const ResumePomodoro = () => {
    return { type: PomodoroActionType.RESUMED }
}