import { CustomPosition } from "./task.action"


export enum PositionActionType {
    CHANGE = '@position/change'
}
export type PositionAction = {
    type: PositionActionType;
    payload: any;
}
export const PositionChanged = (tasks) => {
    return dispatch => {
        if (tasks && Array.isArray(tasks)) {
            let positions = tasks.map((task, index) => {
                return {
                    id: task.id,
                    position: index
                }
            })
            dispatch(CustomPosition())
            dispatch({ type: PositionActionType.CHANGE, payload: [...positions] });

        }
    }
}