import { GetMissions, AddMission } from "src/models/api"
import { uuidv4 } from "./diary.action";

export enum MissionActionType {
    UPDATED = '@missions/list-updated',
    PENDING = '@missions/list-pending',
    BUSY = '@missions/list-busy',
    MISSION_ADD = '@missions/add-mission',
    MISSION_ID_UPDATE = '@missions/id-update'
}
export type MissionAction = {
    type: MissionActionType,
    payload?: any
}

export const AddMissionAsync = (mission) => {
    return dispatch => {
        let softID = uuidv4();
        mission.softID = softID;
        dispatch({
            type: MissionActionType.BUSY,
            payload: mission
        })
        AddMission(mission).then((result) => {
            dispatch({ type: MissionActionType.MISSION_ID_UPDATE, payload: { softID, result } });
        })
    }
}
export const UpdateMissionRequest = () => {
    return { type: MissionActionType.PENDING }
}
export const UpdateMissionSuccess = (data: any) => {
    return { type: MissionActionType.UPDATED, payload: data }
}
export const UpdateMissionListAsync = () => {
    return (dispatch) => {
        dispatch(UpdateMissionRequest());
        GetMissions().then((missions => {
            dispatch(UpdateMissionSuccess(missions));
        }))
    }
}