import moment from "moment"
import { DEFAULT_DATE_FORMAT } from "../common/constants"
import { AddSession, AddSessionEvent, AddSessionTask, GetSessionEvent, GetSessionEvents, GetSessions, GetSingleSession, UpdateSessionEvent, UpdateSessionEventStatus } from "../models/api"
import { SessionActionType } from "../reducers/session.reducer"

export const GenerateSessionName = () => {
    let date = moment().format(DEFAULT_DATE_FORMAT);
    return `Session (${date})`
}

export const UpdateEventStatus = (id, status) => {
    return dispatch => {
        UpdateSessionEventStatus(id, status).then((result) => {
            dispatch({ type: SessionActionType.EVENT_UPDATED, payload: { id, status } })
        }).catch((error) => {

        })

    }
}

export const SaveSessionAction = session => {
    return dispatch => {
        if (!session.name) {
            session.name = GenerateSessionName();
        }
        AddSession(session).then((result) => {
            dispatch({ type: SessionActionType.ADDED, payload: result })
        })
    }
}
export const AddSessionWithTasks = (session, tasks) => {
    return dispatch => {
        AddSession(session).then((result) => {
            if (result.id) {
                AddSessionTask(result.id, tasks).then((taskResult) => {
                    dispatch({ type: SessionActionType.ADDED, payload: result })
                    dispatch({ type: SessionActionType.TASKS_ADDED, payload: { id: result.id, task_ids: tasks } });
                })
            }
            /*   dispatch({ type: SessionActionType.ADDED, payload: result }) */
        })
    }
}


export const AddSessionEventAction = (id, event) => {
    return dispatch => {

        AddSessionEvent(id, event).then((result) => {
            dispatch({ type: SessionActionType.EVENT_ADDED, payload: result })
        })
    }
}
export const PopulateSessionEvents = () => {
    return dispatch => {
        GetSessionEvents().then(events => {
            dispatch({ type: SessionActionType.EVENTS_POPULATED, payload: events })
        })
    }
}
export const AddSessionTaskAction = (id, task_ids) => {
    return dispatch => {
        AddSessionTask(id, task_ids).then((result) => {
            dispatch({ type: SessionActionType.TASKS_ADDED, payload: { id, task_ids } });
        })
    }
}
export const PopulateSession = () => {
    return dispatch => {
        GetSessions().then(sessions => {
            dispatch({ type: SessionActionType.POPULATED, payload: sessions })
        })
    }
}
