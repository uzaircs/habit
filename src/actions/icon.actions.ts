export enum IconActionType {
    ICON_SELECTED = 1,
    CLEAR_SELECTED = 2

}

export type IconAction = {
    type: IconActionType,
    payload: any
}


export const IconSelected = (icon, icon_type) => {
    return {
        type: IconActionType.ICON_SELECTED,
        payload: { icon, icon_type }
    }
}
export const ClearIconSelection = () => {
    return {
        type: IconActionType.CLEAR_SELECTED
    }
}