import { DeleteTasks, BindTaskToParent, GroupTask } from "../models/api"
import { UpdateTaskListAsync } from "./task.action"

export enum BulkActionType {
    BULK_TOGGLE = 42,
    ITEM_ADDED,
    CLEAR_LIST,
    ITEM_REMOVED,
    DELETE_ALL,
    ADD_TO_MY_DAY,
    MOVE,
    GROUP
}
export type BulkAction = {
    type: BulkActionType,
    payload: any
}
export const toggleBulk = () => {
    return { type: BulkActionType.BULK_TOGGLE }
}
export const ItemAdded = id => {
    return { type: BulkActionType.ITEM_ADDED, payload: id }
}
export const ItemRemoved = id => {
    return { type: BulkActionType.ITEM_REMOVED, payload: id }
}
export const ClearBulkList = () => {
    return { type: BulkActionType.CLEAR_LIST };
}
export const BulkDeleteAsync = () => {
    return (dispatch, getState) => {
        dispatch(toggleBulk());
        let ids = getState().bulk.items;
        DeleteTasks(ids).then(res => {
            if (res) {
                dispatch(ClearBulkList());
                dispatch(UpdateTaskListAsync())
            }
        })
    }
}
export const BindTaskToParentAsync = (parent) => {
    return async (dispatch, getState) => {
        dispatch(toggleBulk());
        let ids = getState().bulk.items;
        for (let i = 0; i < ids.length; i++) {
            const id = ids[i];

            await BindTaskToParent(id, parent)

        }
        dispatch(ClearBulkList());
        dispatch(UpdateTaskListAsync());
    }
}
export const GroupAction = (name)=>{
    return { type: BulkActionType.GROUP,payload:{
        name,
    }}
}
export const GroupTaskAsync = (name) => {
    return async (dispatch, getState) => {
        dispatch(toggleBulk());
        let ids = getState().bulk.items;
        await GroupTask(ids, name);
        dispatch(ClearBulkList());
        dispatch(UpdateTaskListAsync());
    }
}
