import TimelineLogs from "../models/timelinelogs";
import { uuidv4 } from "./diary.action";
import { CreateTimelineLog, GetTimelineLogs } from "../models/api";

export enum LogActionType {
    ADDED = '@log/added',
    PENDING = '@log/pending',
    POPULATED = '@log/populated',
    ID_UPDATE = '@log/id-update',
}
export type LogAction = {
    type: LogActionType;
    payload?: any;
}
export const LogActionAdded = (action: any) => {
    return dispatch => {
        let softID = uuidv4();
        action.softID = softID;
        dispatch({ type: LogActionType.ADDED, payload: action });
        CreateTimelineLog(action).then(log => {
            dispatch({ type: LogActionType.ID_UPDATE, payload: { softID: softID, log } });
        })

    }
}
export const RequestLogs = () => {
    return (dispatch, getState) => {
        GetTimelineLogs().then(logs => {
            dispatch({ type: LogActionType.POPULATED, payload: logs });
        })
    }
}