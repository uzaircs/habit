import React from 'react'
import { Image, TouchableNativeFeedback, View } from 'react-native'
import { H2, TextMutedStrong } from '../styles/text'
import { BORDER_RADIUS, FANCY_BOX_HEIGHT } from './constants'
import { default as theme } from '../styles/app-theme.json'
const BoxTitle = ({ title }) => {
    return (
        <H2 text={title} style={{ color: theme["color-primary-500"] }}></H2>
    )
}
export const FancyBox = ({ title, image = null, icon = null, onPress, subtitle = null, }) => {
    if (image) {
        return (
            <TouchableNativeFeedback onPress={onPress} background={TouchableNativeFeedback.Ripple(theme["color-primary-200"], false)}>
                <View style={{ height: FANCY_BOX_HEIGHT }}>
                    <Image source={image}></Image>
                    <BoxTitle title={title}></BoxTitle>
                    {subtitle && <TextMutedStrong text={subtitle}></TextMutedStrong>}
                </View>
            </TouchableNativeFeedback>
        )
    } else {
        return (
            <TouchableNativeFeedback onPress={onPress} background={TouchableNativeFeedback.Ripple(theme["color-primary-200"], false)}>
                <View style={{ height: FANCY_BOX_HEIGHT, backgroundColor: '#fff', borderRadius: BORDER_RADIUS, elevation: 5, justifyContent: 'center', alignItems: 'center' }}>
                    {icon}
                    <BoxTitle title={title}></BoxTitle>
                    {subtitle && <TextMutedStrong style={{ textAlign: 'center', fontSize: 13 }} text={subtitle}></TextMutedStrong>}
                </View>
            </TouchableNativeFeedback>
        )
    }

}
