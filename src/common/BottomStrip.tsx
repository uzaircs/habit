import { View } from 'native-base'
import React from 'react'
import { default as theme } from '../styles/app-theme.json'
import { PADDING } from './constants'
export const BottomStrip = (props) => {

    if (props.children) {
        return (
            <View style={{ height: PADDING * 2, backgroundColor: theme['color-primary-500'], padding: PADDING / 4, borderTopWidth: 1, borderColor: theme['color-font-100'] }}>
                {props.children}
            </View>
        )
    } else return <View />
}
