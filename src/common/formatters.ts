// @flow
export const FormatNumber = number => {
    if (isNaN(number)) return '-';
    if (number.toString().includes('.')) {
        let decimalPart = +number.toString().split('.')[1]
        if (decimalPart > 0) {
            return number.toFixed(1);
        } else {
            return number.toFixed(0);
        }
    } return number;
}