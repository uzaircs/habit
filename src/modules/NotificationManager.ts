import { NativeModules } from 'react-native'
import { default as PushNotifications } from 'react-native-push-notification'
import moment from 'moment';

export type NativeNotificationManager = {
    show: (time: string) => void;
}
class NotificationServiceSingleton {
    private ONGOING_ID = '6546';
    package: NativeNotificationManager;
    constructor() {
        this.package = NativeModules.NotificationManager;

    }
    schedule = (text, title, message, date, id) => {

        let notification = {
            bigText: text,
            title: title,
            soundName: "timer_completed",
            id: id,
            message: message, // (required)
            date: moment(date).toDate(), // in 60 secs
            allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
        };
       
        PushNotifications.localNotificationSchedule(notification);
    }
    cancelScheduled = notificationId => {
        PushNotifications.cancelLocalNotifications({ id: notificationId });
    }
    cancel = () => {
        PushNotifications.cancelAllLocalNotifications();
    }
    ongoing = (till = null) => {
        /*   if (till && moment(till).isValid()) {
              moment(till).diff(moment(), 'milliseconds')
          }
          PushNotifications.cancelAllLocalNotifications();
          PushNotifications.cancelLocalNotifications({ id: this.ONGOING_ID });
          this.package.show("1500000"); */
        PushNotifications.localNotification({
            bigText: "Tap to view",
            title: "Timer is running",
            playSound: false,
            ongoing: true,
            visibility: 'public',
            autoCancel: false,
            priority: 'min',
            when: moment().unix() * 1000, //not on ios
            usesChronometer: true,
            message: "Tap to view",
            id: this.ONGOING_ID,
            timeoutAfter: 1500000 //not on ios
        })
        PushNotifications.localNotificationSchedule({
            bigText: "Time for a break! tap to view progress",
            title: "Time's up",
            soundName: "timer_completed",
            id: this.ONGOING_ID,
            message: "My Notification Message", // (required)
            date: moment().add('25', 'm').toDate(), // in 60 secs
            allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
        });
    }
    show() {
        /* this.package.show("1500000"); */
    }
}
export const NotificationService = new NotificationServiceSingleton();