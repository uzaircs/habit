import Sound from 'react-native-sound'
var complete = require('../../complete.wav')
var incomplete = require('../../incomplete.wav');
var dingSound = require('../../ding.wav');
class SoundManagerSingleton {
    constructor() {

    }
    async ding() {
        try {
            var ding = new Sound(dingSound, (error) => {

                if (error) {
                    console.log(error);
                    return;
                }
                ding.play((success) => {
                    if (success) {
                        ding.release();

                    } else {
                        ding.release();
                    }
                });
            });
        } catch (error) {
            console.log(error);
        }
    }
    async play(type = 0) {
        try {
            var ding = new Sound(type ? incomplete : complete, (error) => {

                if (error) {
                    console.log(error);
                    return;
                }
                ding.play((success) => {
                    if (success) {
                        ding.release();

                    } else {
                        ding.release();
                    }
                });
            });
        } catch (error) {
            console.log(error);
        }
    }
}
export const SoundManager = new SoundManagerSingleton();