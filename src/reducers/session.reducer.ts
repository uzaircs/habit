export enum SessionActionType {
    POPULATED = '@sessions/populated',
    ADDED = '@sessions/added',
    UPDATED = '@sessions/updated',
    TASKS_ADDED = '@sessions/tasksAdded',
    EVENTS_POPULATED = '@sessions/eventsPopulated',
    EVENT_ADDED = '@sessions/eventAdded',
    EVENT_UPDATED = '@sessions/eventUpdated'
}
export interface SessionAction {
    type: SessionActionType;
    payload?: any;
}

const initialState = {
    sessions: [],
    events: [],
}
export const SessionReducer = (state = initialState, action: SessionAction) => {
    switch (action.type) {
        case SessionActionType.EVENT_UPDATED:
            let index = state.events.findIndex(e => e.id == action.payload.id);
            if (index > -1) {
                let session_index = state.sessions.findIndex(s => s.id == state.events[index].session_id);
                if (session_index > -1) {
                    let session_event_index = state.sessions[session_index].events.findIndex(e => e.id == action.payload.id);
                    if (session_event_index > -1) {
                        let newState = {
                            sessions: [
                                ...state.sessions.slice(0, session_index),
                                {
                                    ...state.sessions[session_index],
                                    events: [
                                        ...state.sessions[session_index].events.slice(0, session_event_index),
                                        { ...state.sessions[session_index].events[session_event_index], status: action.payload.status },
                                        ...state.sessions[session_index].events[session_event_index].slice(session_event_index + 1)
                                    ]
                                }
                            ],
                            events: [...state.events.slice(0, index),
                            { ...state.events[index], status: action.payload.status },
                            ...state.events.slice(index + 1)
                            ]
                        }
                        return newState;
                    }


                }
            }
            return state;
        case SessionActionType.EVENTS_POPULATED:
            if (action.payload && Array.isArray(action.payload) && action.payload.length) {
                return { ...state, events: [...action.payload] }
            } else return state;
        case SessionActionType.EVENT_ADDED:
            return { ...state, events: [...state.events, { ...action.payload }] }
        case SessionActionType.POPULATED:
            return { ...state, sessions: [...action.payload] }
        case SessionActionType.ADDED:
            return { ...state, sessions: [...state.sessions, { ...action.payload }] }
        case SessionActionType.TASKS_ADDED:
            try {
                if (action.payload && action.payload.id && action.payload.task_ids) {

                    if (Array.isArray(action.payload.task_ids) && action.payload.task_ids.length) {

                        let index = state.sessions.findIndex(s => s.id === action.payload.id);

                        return {
                            ...state,
                            sessions: [...state.sessions.slice(0, index), { ...state.sessions[index], tasks: [...action.payload.task_ids] }, ...state.sessions.slice(index + 1, state.sessions.length)]
                        }
                    } else return state;
                } else return state
            } catch (error) {
                return state;
            }
        default:
            return state;
    }
}