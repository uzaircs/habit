import { IconAction, IconActionType } from "../actions/icon.actions";

const initialState = {
    icon: null,
    icon_type: null
}
export const IconSelectionReducer = (state = initialState, action: IconAction) => {
    switch (action.type) {
        case IconActionType.ICON_SELECTED:
            return { icon: action.payload.icon, icon_type: action.payload.icon_type }
        case IconActionType.CLEAR_SELECTED:
            return { ...initialState }
        default:
            return state;
    }
}