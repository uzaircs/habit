import { PendingAction, PendingActionTypes, EntryAction, EntryActionType } from "../actions/entry.actions"
const initialState = {
    mood: '',
    activity: [],
}
const initialEntries = {
    entries: [],
    error: {},
    lastUpdated: null
};
export const entriesReducer = (state = initialEntries, action: EntryAction) => {
    switch (action.type) {
        case EntryActionType.MOOD_CHANGED:
            if (action.payload && action.payload.entry_id) {
                let index = state.entries.findIndex(e => e.id === action.payload.entry_id);
                if (index > -1) {
                    if (action.payload.newMood) {

                        return {
                            ...state,
                            entries: [
                                ...state.entries.slice(0, index),
                                { ...state.entries[index], emoji: action.payload.newMood.emoji, mood: action.payload.newMood.title, color: action.payload.newMood.color },
                                ...state.entries.slice(index + 1, state.entries.length)
                            ]
                        }
                    }
                }
            }
        case EntryActionType.ENTRY_ADDED:
            if (action.payload) {
                let newEntries = [{ ...action.payload }, ...state.entries];
                return { ...state, entries: newEntries }
            }
        case EntryActionType.UPDATE_PENDING:

            return { entries: [...state.entries], ready: false, loading: true }
        case EntryActionType.LIST_UPDATED:
            if (action.payload && action.payload.length > 0) {
                return { entries: [...action.payload.reverse()], error: false, lastUpdated: new Date(), ready: true };
            } else {
                return state;
            }

        case EntryActionType.UPDATE_ERROR:
            return { entries: [...state.entries], error: action.payload, ready: true };
        default:
            return state
    }
}

export const pendingReducer = (state = initialState, action: PendingAction) => {
    switch (action.type) {
        case PendingActionTypes.ADD_ENTRY:
            if (action.payload.mood) {
                let newState = { ...state, mood: action.payload.mood };
                return newState;
            }


        case PendingActionTypes.ADD_ACTIVITY:
            if (action.payload.activity) {
                const existingActivity = state.activity.findIndex(a => a == action.payload.activity);
                if (existingActivity == -1) {
                    let newState = { mood: state.mood, activity: [...state.activity, action.payload.activity] }
                    return newState;
                } else {
                    return state;
                }
            } else {
                return state;
            }

        case PendingActionTypes.CLEAR:
            return { ...initialState }
        case PendingActionTypes.SAVE_STARTED:

            return { ...state, started: true }
        case PendingActionTypes.SAVE_SUCCESS:
            return { ...initialState, saved: true };
        case PendingActionTypes.SAVE_ERROR:
            return { ...state, error: true };
        case PendingActionTypes.REMOVE_ACTIVITY:
            if (action.payload.activity) {
                const existingActivity = state.activity.findIndex(a => a == action.payload.activity);
                if (existingActivity > -1) {
                    let newActivities = state.activity.slice();
                    newActivities.splice(existingActivity, 1);
                    let newState = { mood: state.mood, activity: [...newActivities] }
                    return newState;
                } else {
                    return state;
                }
            } else {
                return state;
            }
        default:
            return state;
    }
}