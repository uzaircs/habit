import { TaskAction, TaskActionType } from "../actions/task.action"
import { TaskListActionType } from "../actions/list.action"
import { SortOptions, FilterOptions } from "../components/task/list"
import moment from "moment"
import { RepeatAction, RepeatActionType } from "../actions/task.repeat.action"
export type ListAction = {
    type: TaskListActionType,
    payload: any
}
const initialState = {
    tasks: [],

    filter: {
        filter: []
    },
    sort: {
        sortOption: 2,
        isAscending: false
    },
    isCustomPosition: false
}
const listState = {
    items: []
}
const AutoSortAndFilterState = (state) => {
    if (!state.sort && !state.filter) return state;
    let tasks = [...state.tasks];
    if (state.filter) {

        tasks = FilterTasks(tasks, state.filter)
    }
    if (!state.isCustomPosition && state.sort && !isNaN(state.sort.sortOption)) {
        let sortedTasks = TaskSortPipe(tasks, state.sort.sortOption, state.sort.isAscending);
        tasks = [...sortedTasks];
    }
    return { ...state, tasks: [...tasks], isBusy: false, filter: { ...state.filter }, sort: { ...state.sort } };
}
export const TaskSortPipe = (tasks, sortOption, isAcending): any[] => {
    if (tasks && Array.isArray(tasks)) {
        switch (sortOption) {
            case SortOptions.DUE_DATE:
                if (isAcending) {
                    // nearest on top
                    return [...tasks.sort((a, b) => {

                        if (!a.DueDate && !b.DueDate) return 0;
                        if (!a.DueDate) return 1;
                        if (!b.DueDate) return -1;
                        return parseInt(a.DueDate) < parseInt(b.DueDate) ? -1 : 1;
                    })]
                } else {
                    return [...tasks.sort((a, b) => {

                        return b.DueDate - a.DueDate;
                    })]
                }
            case SortOptions.DATE_COMPLETED:
                if (isAcending) {
                    // nearest on top
                    return [...tasks.sort((a, b) => {

                        if (!a.completedAt && !b.completedAt) return 0;
                        if (!a.completedAt) return 1;
                        if (!b.completedAt) return -1;
                        return parseInt(a.completedAt) < parseInt(b.completedAt) ? -1 : 1;
                    })]
                } else {
                    return [...tasks.sort((a, b) => {

                        return b.completedAt - a.completedAt;
                    })]
                }
            case SortOptions.DATE_CREATED:
                if (isAcending) {
                    return [...tasks.sort((a, b) => {
                        return a.createdAt - b.createdAt;
                    })]
                } else {
                    return [...tasks.sort((a, b) => {
                        return b.createdAt - a.createdAt;
                    })]
                }

            case SortOptions.DATE_MODIFIED:
                if (isAcending) {
                    return [...tasks.sort((a, b) => {
                        return a.createdAt - b.createdAt;
                    })]
                } else {
                    return [...tasks.sort((a, b) => {
                        return b.createdAt - a.createdAt;
                    })]
                }
            case SortOptions.MY_DAY:
                return [...tasks.sort((a, b) => b.myDay - a.myDay)]
            case SortOptions.ALPHATICALLY:
                if (isAcending) {
                    return [...tasks.sort((a, b) => {
                        return a.name.localeCompare(b.name);
                    })]
                } else {
                    return [...tasks.sort((a, b) => {
                        return b.name.localeCompare(a.name);
                    })]
                }
            case SortOptions.STATUS:
                return [...tasks.sort((a, b) => a.status - 1)]
            default: return tasks;
        }
    } return tasks;
}
export const TaskListReducer = (state = listState, action: ListAction) => {
    switch (action.type) {
        case TaskListActionType.UPDATED:
            return { items: [...action.payload] }
        case TaskListActionType.PENDING:
        default: return state

    }
}
export const FilterTasks = (tasks, filterOptions) => {
    if (!filterOptions.filter) {
        filterOptions.filter = [];
    }
    let filteredTask = [...tasks];
    filteredTask.forEach(task => {
        task.is_filtered = false;
        if (filterOptions.filter.indexOf(FilterOptions.SHOW_SUB_TASKS) < 0) {
            if (task.parent && task.parent.id) {
                task.is_filtered = true;
            }
            /* filteredTask = filteredTask.filter(t => (!t.parent || !t.parent.id)); */
        }
        if (filterOptions.filter.indexOf(FilterOptions.SHOW_COMPLETED) < 0) {
            if (task.status > 0) {
                task.is_filtered = true;
            }
        }
        if (filterOptions.goals && filterOptions.goals.length > 0) {
            if (!task.goal || !task.goal.id) {
                task.is_filtered = true;
            }
            if (task.goal && task.goal.id && filterOptions.goals.indexOf(task.goal.id) < 0) {
                task.is_filtered = true;
            }
        }
        if (filterOptions.lists && filterOptions.lists.length > 0) {
            if (!task.list || !task.list.id) {
                task.is_filtered = true;
            }
            if (task.list && task.list.id && filterOptions.lists.indexOf(task.list.id) < 0) {
                task.is_filtered = true;
            }
        }

    })
    return filteredTask;
}
export const TaskReducer = (state = initialState, action: TaskAction) => {
    switch (action.type) {
        case TaskActionType.CUSTOM_POSITION:
            return { ...state, isCustomPosition: true }
        case TaskActionType.REPEAT_CONFIG:
            if (action.payload && action.payload.repeatConfig && action.payload.task && action.payload.task.id && action.payload.context) {
                let index = state.tasks.findIndex(t => t.id === action.payload.task.id);
                let __tasks = [
                    ...state.tasks.slice(0, index),
                    { ...state.tasks[index], repeat: action.payload.repeatConfig, generatedBy: action.payload.context },
                    ...state.tasks.slice(index + 1, state.tasks.length)
                ];
                let s = { ...state, tasks: [...__tasks] }
                return AutoSortAndFilterState(s);
            }
            return state;
        case TaskActionType.REMINDER_ADDED:
            // payload: { itemId: taskId, notificationId, date }
            if (action.payload && action.payload.itemId && action.payload.notificationId) {
                let index = state.tasks.findIndex(t => t.id === action.payload.taskId);
                if (index > -1) {
                    /* notificationId: number | string;
                    itemId: string;
                    itemType?: number;
                    date: Date;
                    createdAt?: Date;
                    updatedAt?: Date; */
                    let __tasks = [...state.tasks.slice(0, index),
                    { ...state.tasks[index], reminders: [{ notificationId: action.payload.notificationId, itemId: action.payload.itemId, date: action.payload.date }] },
                    ...state.tasks.slice(index + 1, state.tasks.length)];
                    let s = { ...state, tasks: [...__tasks] }
                    return AutoSortAndFilterState(s);
                } return state;
            } return state;
        case TaskActionType.REMINDER_REMOVED:
            // payload: taskId
            if (action.payload) {
                let index = state.tasks.findIndex(t => t.id === action.payload);
                if (index > -1) {
                    /* notificationId: number | string;
                    itemId: string;
                    itemType?: number;
                    date: Date;
                    createdAt?: Date;
                    updatedAt?: Date; */
                    let __tasks = [...state.tasks.slice(0, index),
                    { ...state.tasks[index], reminders: [] },
                    ...state.tasks.slice(index + 1, state.tasks.length)];
                    let s = { ...state, tasks: [...__tasks] }
                    return AutoSortAndFilterState(s);
                } return state;
            } return state;
        case TaskActionType.BUSY:
        case TaskActionType.PENDING:
            if (!state.tasks.length) {
                return { ...state, isBusy: true };
            } else {
                return state;
            }
        case TaskActionType.APPLY_FILTER:
            return { ...state, tasks: FilterTasks(action.payload.tasks, action.payload.filterOption), filter: { ...action.payload.filterOption } }
        case TaskActionType.APPLY_SORT:
            if (state.tasks && state.tasks.length) {
                let sortedTasks = TaskSortPipe(state.tasks, action.payload.sortOption, action.payload.isAscending);
                return { ...state, tasks: [...sortedTasks], sort: { ...action.payload }, isCustomPosition: false };
            } else return state;
        case TaskActionType.TASK_DELETE:
            if (action.payload) {
                let delete_index = state.tasks.findIndex(t => t.id === action.payload);
                if (delete_index > -1) {
                    let subtask_indices = state.tasks.filter(t => t.parent && t.parent.id === action.payload).map((t, i) => i);
                    let AfterDelete = state.tasks.filter((t, i) => t.id != action.payload && subtask_indices.indexOf(i) < 0);
                    if (!state.isCustomPosition && state.sort && !isNaN(state.sort.sortOption)) {
                        let sortedTasks = TaskSortPipe(AfterDelete, state.sort.sortOption, state.sort.isAscending);
                        return { ...state, tasks: [...sortedTasks] };
                    }
                    return { ...state, tasks: [...AfterDelete] };

                } else {
                    return state;
                }
            } else {
                return state;
            }
        case TaskActionType.TASK_COMPLETED:
            let id = action.payload;
            let index = state.tasks.findIndex(t => t.id === id);

            if (index > -1) {
                let __tasks = [...state.tasks.slice(0, index), { ...state.tasks[index], status: 1 }, ...state.tasks.slice(index + 1, state.tasks.length)];
                let newState = { ...state, tasks: [...__tasks] };
                return AutoSortAndFilterState(newState);
            }
            else {
                return state
            }
        case TaskActionType.LIST_ATTACHED:
            let taskIndex = state.tasks.findIndex(task => task.id === action.payload.id);
            if (taskIndex > -1) {
                let listAddedState = { ...state, tasks: [...state.tasks.slice(0, taskIndex), { ...state.tasks[taskIndex], list: action.payload.list }, ...state.tasks.slice(taskIndex + 1, state.tasks.length)] };
                return listAddedState;
            } else return state;
        case TaskActionType.TASK_INCOMPLETED:
            let _id = action.payload;
            let _index = state.tasks.findIndex(t => t.id === _id);
            let _newTask = { ...state.tasks[_index] };
            _newTask.status = 0;
            if (_index > -1) {
                let newState = { ...state, tasks: [...state.tasks.slice(0, _index), _newTask, ...state.tasks.slice(_index + 1, state.tasks.length)] };
                return AutoSortAndFilterState(newState);
            }
            else {
                return state
            }

        case TaskActionType.TASK_UPDATED:
            //{id,propType,propValue}
            if (action.payload && action.payload.id) {
                let taskContext = state.tasks.findIndex(t => t.id === action.payload.id);
                if (taskContext > -1) {
                    let { propType } = action.payload;
                    let task = { ...state.tasks[taskContext] };
                    if (task.hasOwnProperty(propType)) {
                        task[propType] = action.payload.propValue;
                    }
                    let newState = { ...state, tasks: [...state.tasks.slice(0, taskContext), task, ...state.tasks.slice(taskContext + 1, state.tasks.length)] };
                    return AutoSortAndFilterState(newState);
                } else { return state; }
            } else {
                return state;
            }
        case TaskActionType.TASK_ADDED:
            if (action.payload) {
                action.payload.createdAt = moment();
                let __state = { ...state, tasks: [...state.tasks, action.payload] }
                return AutoSortAndFilterState(__state);
            } else return state;

        case TaskActionType.TASK_ID_UPDATE:
            if (action.payload && action.payload.softID) {
                let taskContext = state.tasks.findIndex(t => t.softID === action.payload.softID);
                let newState = { ...state, tasks: [...state.tasks.slice(0, taskContext), action.payload.task, ...state.tasks.slice(taskContext + 1, state.tasks.length)] };
                return AutoSortAndFilterState(newState);
            } else return state;

        case TaskActionType.UPDATED:
            let newState = { ...state, tasks: [...action.payload], isBusy: false }

            return AutoSortAndFilterState(newState);

        default:
            return state;

    }
}
const repeatInitial = [];
export const RepeatReducer = (state = repeatInitial, action: RepeatAction) => {
    switch (action.type) {
        case RepeatActionType.REPEAT_ADDED:
            if (action.payload) {
                return [...state, { ...action.payload }];
            }
            return state;
        case RepeatActionType.POPULATED:
            if (action.payload && Array.isArray(action.payload) && action.payload.length > 0) {
                return [...action.payload]
            }
            if (!Array.isArray(state)) {
                return [];
            }
            return state;
        case RepeatActionType.REPEAT_MODIFIED:
            if (+action.payload > -1) {
                let taskContext = state.findIndex(t => t.id === action.payload.id);
                if (taskContext > -1) {
                    /*     let newState = { ...state, tasks: [...state.tasks.slice(0, taskContext), action.payload.task, ...state.tasks.slice(taskContext + 1, state.tasks.length)] }; */
                    let newState = [...state.slice(0, taskContext), { ...action.payload }, ...state.slice(taskContext + 1, state.length)];
                    return newState;
                }
            }
            return state;
        case RepeatActionType.REPEAT_REMOVED:
            if (+action.payload > -1) {
                let removeIndexContext = state.findIndex(t => t.id === action.payload);
                let removedItemState = [...state.slice(0, removeIndexContext), ...state.slice(removeIndexContext + 1, state.length)];
                return removedItemState
            }
            return state;
        default: return state
    }
}   