import { LogAction, LogActionType } from "../actions/timeline.action";

const initialState = [];

export const TimelineLogsReducer = (state = initialState, action: LogAction) => {
    switch (action.type) {
        case LogActionType.PENDING:
            return state;
        case LogActionType.ADDED:
            let newState = [...state, action.payload]
            return newState;
        case LogActionType.ID_UPDATE:
            if (action.payload && action.payload.softID) {
                let index = state.findIndex(e => e.softID === action.payload.softID);
                if (index > -1) {
                    let newState = [...state.slice(0, index), { ...state[index], id: action.payload.log.id }, ...state.slice(index + 1, state.length)];
                    return newState;
                } else return state;
            } else return state;
        case LogActionType.POPULATED:
            if (action.payload && Array.isArray(action.payload)) {
                return [...action.payload];
            } else return state;

        default: return state;
    }
}