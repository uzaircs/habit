import { InfluenceAction, InfluenceActionType } from "../actions/influence.actions"

const initialState = {
    ready: false
}

export const InfluenceReducer = (state = initialState, action: InfluenceAction) => {
    switch (action.type) {
        case InfluenceActionType.PENDING:
            return { ready: false }
        case InfluenceActionType.INFLUENCE_UPDATED:
            return { ...action.payload, ready: true }
        case InfluenceActionType.INFLUENCE_ERROR:
            return { ready: false, error: action.payload }
        default:
            return state;
    }
}