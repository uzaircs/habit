import { MissionAction, MissionActionType } from "../actions/mission.action";


const initialState = {
    missions: []
}

export const GoalsReducer = (state = initialState, action: MissionAction) => {
    switch (action.type) {
        case MissionActionType.BUSY:
        case MissionActionType.PENDING:
            if (!state.missions.length) {
                return { ...state, isBusy: true };
            } else {
                return { ...state }
            }
        case MissionActionType.MISSION_ADD:
            return { missions: [...state.missions, action.payload] }
        case MissionActionType.UPDATED:
            return { missions: [...action.payload] }

        default:
            return state;

    }
}