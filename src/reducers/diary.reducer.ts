import { DiaryAction, DiaryActionType } from "../actions/diary.action"
import moment from 'moment'
const initialState = {
    entries: [],
}
export const SortDiaryEntries = () => {

}
export const DiaryEntryReducer = (state = initialState, action: DiaryAction) => {
    switch (action.type) {
        case DiaryActionType.LIST_UPDATED:
            if (action.payload && Array.isArray(action.payload)) {

                return { entries: [...action.payload] };
            } else return state;
        case DiaryActionType.ENTRY_ADDED:
            if (action.payload) {
                return { entries: [...state.entries, { ...action.payload }] }
            }
        case DiaryActionType.ENTRY_REMOVED:
            if (action.payload) {
                let index = state.entries.findIndex(entry => entry.id === action.payload)
                if (index > -1) {
                    let newState = [...state.entries.slice(0, index), ...state.entries.slice(index + 1, state.entries.length)];
                    return { entries: [...newState] };
                } else return state;
            } else return state;
        case DiaryActionType.ENTRY_UPDATED:
            if (action.payload && action.payload.id) {
                let index = state.entries.findIndex(entry => entry.id === action.payload.id)

                if (index > -1) {
                    let newState = [...state.entries.slice(0, index), action.payload, ...state.entries.slice(index + 1, state.entries.length)];
                    return { entries: [...newState] }
                } else return state
            } else return state
        case DiaryActionType.ENTRY_ID_CHANGED:
            if (action.payload && action.payload.softID && action.payload.entry) {
                let index = state.entries.findIndex(entry => entry.softID === action.payload.softID)
                if (index > -1) {
                    let newState = [...state.entries.slice(0, index), action.payload.entry, ...state.entries.slice(index + 1, state.entries.length)];
                    return { entries: [...newState] }
                } else return state
            } else return state
        default:
            return state;
    }
}