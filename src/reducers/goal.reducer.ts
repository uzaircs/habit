import { GoalAction, GoalActionType } from "../actions/goal.action"

const initialState = {
    goals: [],
    goalsProgress: []
}

export const GoalsReducer = (state = initialState, action: GoalAction) => {
    switch (action.type) {
        case GoalActionType.BUSY:
        case GoalActionType.PENDING:
            if (!state.goals.length) {
                return { ...state, isBusy: true };
            } else {
                return { ...state }
            }
        case GoalActionType.GOAL_UPDATED:
            if (action.payload) {
                let goalContext = state.goals.findIndex(stateGoal => stateGoal.id === action.payload.id);
                let newState = { ...state, goals: [...state.goals.slice(0, goalContext), { ...action.payload }, ...state.goals.slice(goalContext + 1, state.goals.length)] };
                return newState;
            } else return state;
        case GoalActionType.GOAL_REMOVE:
            if (action.payload) {
                let goalContext = state.goals.findIndex(stateGoal => stateGoal.id === action.payload);
                let newState = { ...state, goals: [...state.goals.slice(0, goalContext), ...state.goals.slice(goalContext + 1, state.goals.length)] };
                return newState;
            } else return state;
        case GoalActionType.GOAL_ADDED:
            return { ...state, goals: [...state.goals, action.payload] }
        case GoalActionType.UPDATED:
            return { ...state, goals: [...action.payload] }
        case GoalActionType.GOAL_PROGRESS_UPDATED:
            return { ...state, goalsProgress: [...action.payload] }
        default:
            return state;

    }
}