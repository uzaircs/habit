import { BulkAction, BulkActionType } from "../actions/bulk.actions"

const initialState = {
    status: false,
    items: [],
}
export const BulkReducer = (state = initialState, action: BulkAction) => {
    switch (action.type) {
        case BulkActionType.BULK_TOGGLE:
            if (state.status) {
                return { status: false, items: [...state.items] };
            } else { return { status: true, items: [...state.items] } }
        case BulkActionType.ITEM_ADDED:
            let newState = { items: [...state.items, action.payload], status: state.status };
            return newState;
        case BulkActionType.CLEAR_LIST:
            let clearState = { items: [], status: state.status };
            return clearState;
        case BulkActionType.ITEM_REMOVED:
            let index = state.items.findIndex(item => item == action.payload);
            if (index >= 0) {
                let removeState = { items: [...state.items.splice(index, 1)], status: state.status };
                return removeState;
            } else return state;
        default: return state
    }
}