import { combineReducers } from 'redux'
import { pendingReducer, entriesReducer } from './entry.reducer'
import { InfluenceReducer } from './influence.reducer'
import { IconSelectionReducer } from './icons.reducer'
import { CategoryReducer } from './category.reducer'
import { TaskReducer, TaskListReducer, RepeatReducer } from './task.reducer'
import { GoalsReducer } from './goal.reducer'
import { BulkReducer } from './bulk.reducer'
import { DiaryEntryReducer } from './diary.reducer'
import { TimelineLogsReducer } from './log.reducer'
import { PositionReducer } from './position.reducer'
import { MoodReducer } from './mood.reducer'
import { SessionReducer } from './session.reducer'

export const combined = combineReducers({
    pending: pendingReducer,
    entryStorage: entriesReducer,
    influenceCalculator: InfluenceReducer,
    selectedIcon: IconSelectionReducer,
    categories: CategoryReducer,
    tasks: TaskReducer,
    goals: GoalsReducer,
    bulk: BulkReducer,
    list: TaskListReducer,
    diary: DiaryEntryReducer,
    logs: TimelineLogsReducer,
    positions: PositionReducer,
    moods: MoodReducer,
    repeats: RepeatReducer,
    sessions: SessionReducer
})