import { CategoryAction, CategoryActionType } from "../actions/category.action";

const initialState = {
    items: []
};

export const CategoryReducer = (state = initialState, action: CategoryAction) => {

    switch (action.type) {
        case CategoryActionType.CATEGORY_PENDING:

        case CategoryActionType.CATEGORY_FAILED:
            return { hasItems: false, items: [] };
        case CategoryActionType.CATEGORY_UPDATED:
          
            return { items: [...action.payload], hasItems: true };
        default:
            return state;
    }
}