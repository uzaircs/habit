import { MoodAction, MoodActionType } from "../actions/mood.action"
const initialState = []

export const MoodReducer = (state = initialState, action: MoodAction) => {
    switch (action.type) {
        case MoodActionType.LIST_UPDATED:
            if (action.payload && Array.isArray(action.payload)) {
                return [...action.payload];
            }
        default: return state;
    }
}