import { PositionAction, PositionActionType } from "../actions/position.action"

const initialState = []
export const PositionReducer = (state = initialState, action: PositionAction) => {
    switch (action.type) {
        case PositionActionType.CHANGE:
            if (action.payload && Array.isArray(action.payload) && action.payload.length) {
                return [...action.payload]
            } return state;
        default:
            return state;
    }
}