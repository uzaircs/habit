import { Button, Divider, Layout } from '@ui-kitten/components'
import React from 'react'
import { Image, Keyboard, StyleSheet, Text, TextInput, TouchableNativeFeedback, View } from 'react-native'
import { iOSUIKit } from 'react-native-typography'
import { CONTAINER_PADDING, DEFAULT_MARGIN } from '../common/constants'
import { H1, H1Large, TextMuted, TextMutedStrong, TextSmall } from '../styles/text'
import { default as theme } from '../styles/app-theme.json'
import { InputLabel } from '../components/forms/category'
import { ButtonText, ButtonTextIOS } from '../components/activity'
import { ValidateEmail, ValidatePassword } from '../common/validator'
import { Toast } from 'native-base'
const styles = StyleSheet.create({
    checkimage: {

        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain'
    }
})
export const HyperLink = ({ children, ...props }) => {
    return (
        <TouchableNativeFeedback>
            <View style={{ paddingTop: DEFAULT_MARGIN / 2, paddingBottom: DEFAULT_MARGIN / 2, paddingLeft: DEFAULT_MARGIN / 4, paddingRight: DEFAULT_MARGIN / 4, borderRadius: 10 }}>
                <Text style={[iOSUIKit.footnoteEmphasized, { color: theme["color-primary-500"], ...props.style }]}>{children}</Text>
            </View>
        </TouchableNativeFeedback>
    )
}
const checkImage = require('../images/check-circle.png')
export const Login = (props) => {
    const [isKeyboardVisible, setKeyboardVisible] = React.useState(false);
    const [checked, setChecked] = React.useState(false);
    React.useEffect(() => {
        const keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            () => {
                setKeyboardVisible(true);
            }
        );
        const keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            () => {
                setKeyboardVisible(false);
                setChecked(true);
            }
        );

        return () => {
            keyboardDidHideListener.remove();
            keyboardDidShowListener.remove();
        };
    }, []);
    const [email, setEmail] = React.useState(null);
    const [password, setPassword] = React.useState(null);
    const emailInvalid = () => {

    }
    const passwordInvalid = () => {

    }
    const doValidation = () => {
        let validationResult = true;
        if (!ValidateEmail(email)) {
            emailInvalid();
            validationResult = false;
        }
        if (!ValidatePassword(password)) {
            passwordInvalid();
            validationResult = false;
        }
        return validationResult;
    }
    const doLogin = () => {
        if (doValidation()) {
            //login
            let user = { email, password };
            console.log(user);
            // apiLogin(user);
            Toast.show({
                text: 'Welcome back',
                duration: 2000,
                type: 'success'
            })
            props.navigation.reset({
                routes: [{ name: 'Home' }]
            });
        } else {
            // validation failec
        }

    }
    return (
        <Layout style={{ flex: 1, padding: CONTAINER_PADDING / 2 }}>

            <View style={{ marginTop: DEFAULT_MARGIN * 2, padding: CONTAINER_PADDING }}>
                <View style={{ marginBottom: DEFAULT_MARGIN * 2 }}>
                    <H1Large text="Login First"></H1Large>
                    <TextMuted text="Hello there, sign in to continue"></TextMuted>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN * 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                    <InputLabel label="Username or Email"></InputLabel>
                </View>

                <View style={{ position: 'relative' }}>
                    {checked && <View style={{ height: 20, width: 20, position: 'absolute', right: 16, top: 12, elevation: 4 }}>
                        <Image style={styles.checkimage} source={checkImage}></Image>
                    </View>}
                    <TextInput
                        style={[iOSUIKit.body, { backgroundColor: '#F2F4F7', borderRadius: 15, borderWidth: 0, borderColor: 'rgba(0,0,0,0)', height: 44, paddingLeft: DEFAULT_MARGIN, color: '#002251' }]}
                        value={email}

                        placeholder="Enter your username or email"
                        keyboardType="email-address"
                        /*     textStyle={iOSUIKit.body} */
                        onChangeText={(text) => { setEmail(text); }}
                    /*  label={() => <InputLabel label="Username or Email"></InputLabel>} */
                    ></TextInput>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN }}>
                    <InputLabel label="Password"></InputLabel>
                </View>
                <View style={{ position: 'relative' }}>
                    {checked && <View style={{ height: 20, width: 20, position: 'absolute', right: 16, top: 12, elevation: 4 }}>
                        <Image style={styles.checkimage} source={checkImage}></Image>
                    </View>}
                    <TextInput
                        style={[iOSUIKit.body, { backgroundColor: '#F2F4F7', borderRadius: 15, borderWidth: 0, borderColor: 'rgba(0,0,0,0)', height: 44, paddingLeft: DEFAULT_MARGIN, color: '#002251' }]}
                        value={password}
                        secureTextEntry={true}
                        placeholder="Enter your password"
                        /*    textStyle={iOSUIKit.body} */
                        onChangeText={(text) => { setPassword(text) }}
                    /*   label={() => <InputLabel label="Password"></InputLabel>} */
                    ></TextInput>
                </View>

                <View style={{ marginTop: DEFAULT_MARGIN / 4, flexDirection: 'row' }}>
                    <HyperLink>Forgot Password?</HyperLink>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN }}>
                    <Button appearance="filled" style={{ borderRadius: 10 }} onPress={doLogin} children={(props) => <ButtonTextIOS text="Login" {...props}></ButtonTextIOS>}></Button>
                </View>
            </View>

            {/*   <View style={{ marginTop: DEFAULT_MARGIN * 2, flexDirection: 'row', justifyContent: 'center' }}>
                <Button style={{ width: DEFAULT_MARGIN * 10 }} appearance="outline" onPress={() => { }} children={(props) => <ButtonTextIOS text="Signup" {...props}></ButtonTextIOS>}></Button>
            </View> */}
            {/*       <Divider style={{ marginTop: DEFAULT_MARGIN }}></Divider>
            {!isKeyboardVisible && <View style={{ position: 'absolute', bottom: DEFAULT_MARGIN, left: 0, right: 0, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>

                <Button appearance="ghost" status="basic" onPress={() => { }} children={(props) => <ButtonTextIOS text="Continue without account" {...props}></ButtonTextIOS>}></Button>
                <TextSmall text="*Limited features"></TextSmall>
            </View>} */}
        </Layout>
    )
}
