import React, { Component } from 'react'
import { connect } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native';
import { TabBar, Tab, Layout, Text, Card } from '@ui-kitten/components';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Login } from './login';
import { View } from 'native-base';
import { DEFAULT_MARGIN } from '../common/constants';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { default as theme } from '../styles/app-theme.json'
import { ImageBackground, ScrollView, StyleSheet } from 'react-native';
import { Signup } from './signup';
const image = require('../images/pattern.jpg')
const { Navigator, Screen } = createMaterialTopTabNavigator();
const styles = StyleSheet.create({
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",

    },
})
const TopTabBar = ({ navigation, state }) => (
    <TabBar
        indicatorStyle={{ height: 2 }}
        style={{ borderTopLeftRadius: 15, borderTopRightRadius: 15, height: 56, top: 2 }}
        selectedIndex={state.index}
        onSelect={index => navigation.navigate(state.routeNames[index])}>
        <Tab title='LOG IN' />
        <Tab title='SIGN UP' />
    </TabBar>
);
const TabNavigator = () => (
    <Navigator tabBar={props => <TopTabBar {...props} />}>
        <Screen name='LoginScreen' component={Login} />
        <Screen name='SignupScreen' component={Signup} />
    </Navigator>
);
export class AuthScreen extends Component {
    render() {
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff', }}>
                <Layout style={{ flex: 1, backgroundColor: theme["color-primary-800"], }}>
                    <ImageBackground source={image} style={styles.image}>
                        <View style={{ marginTop: DEFAULT_MARGIN * 4 }}>

                        </View>
                        <View style={{ flex: 1 }}>
                            <TabNavigator></TabNavigator>
                        </View>
                    </ImageBackground>

                </Layout>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen)
