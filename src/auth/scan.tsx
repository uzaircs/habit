import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout } from '@ui-kitten/components'
import { DefaultText } from '../styles/text'
import { FingerPrint } from '../auth/touch.id'
export class Scan extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            ready: false
        }
    }
    componentDidMount() {
        FingerPrint.start().then((ready) => {
            if (ready) {
                this.setState({ ready: true })
                FingerPrint.authenticate().then(result => {
                    if (result.success) {
                        this.props.navigation.pop();
                    }
                })
            } else {
                this.props.navigation.pop();
            }
        })
    }
    render() {
        return (
            <Layout style={{ flex: 1 }}>
                <DefaultText text="Scan your finger to continue"></DefaultText>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Scan)
