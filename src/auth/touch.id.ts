import * as LocalAuthentication from 'expo-local-authentication';

class Fingerprint {
    public IS_AVAILABLE;
    /**
     *
     */
    constructor() {


    }
    async authenticate() {
        if (this.IS_AVAILABLE) {
            return await LocalAuthentication.authenticateAsync();
        }
    }
    async start() {
        try {
            let result = await LocalAuthentication.hasHardwareAsync();
            if (result) {
                let isEnrolled = await LocalAuthentication.isEnrolledAsync();
                let hasFingerprintSensor = (await LocalAuthentication.supportedAuthenticationTypesAsync()).some(s => s == LocalAuthentication.AuthenticationType.FINGERPRINT);
                if (hasFingerprintSensor && isEnrolled) {
                    this.IS_AVAILABLE = true;
                    return true;
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    }
}
export const FingerPrint = new Fingerprint();