import React from 'react'
import { default as OnboardingSwiper } from 'react-native-onboarding-swiper';
import { Text } from 'native-base';
import { default as pages } from './pages.json'
import { Image } from 'react-native';
import { default as theme } from '../styles/app-theme.json'
import { Layout } from '@ui-kitten/components';
import { fontFamily } from '../styles/fonts';
const OnboardingTheme = () => {
    return {
        backgroundColor: theme["color-font-light-100"]
    }
}
const createOnboardingPage = ({ title, subtitle, image }) => {

}
const getPages = () => {
    let onboardingPages = pages.map(page => {
        let img = <Layout style={{ height: 240, justifyContent: 'center' }}>
            <Image source={require('../images/r.png')} style={{ maxHeight: '100%' }} resizeMode="contain" resizeMethod="resize" />
        </Layout>
        return {
            backgroundColor: OnboardingTheme().backgroundColor,
            image: img,
            title: page.title,
            subtitle: page.subtitle,


        }
    })
    return onboardingPages;
}
export const OnBoarding = ({ ondone, onskip }) => {
    return (
        <Layout style={{ flex: 1 }}>
            <OnboardingSwiper
                onDone={ondone}
                onSkip={onskip}
                titleStyles={fontFamily.bold}
                subTitleStyles={fontFamily.semibold}
                pages={getPages()}
            ></OnboardingSwiper>
        </Layout>
    )

}
