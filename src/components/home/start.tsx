import React, { Component } from 'react'
import { Layout, Text } from '@ui-kitten/components'
import { View, StyleSheet, TouchableNativeFeedback } from 'react-native'
import { sizes, fontFamily, fontColors } from '../../styles/fonts'
import { Icon } from 'native-base'
import { Row, Col } from 'react-native-easy-grid'
import { default as theme } from '../../styles/app-theme.json'
import { NavigationContext } from '@react-navigation/native';
import { iOSUIKit } from 'react-native-typography'
const styles = StyleSheet.create({
    centerAll: {
        justifyContent: 'center', alignItems: 'center'
    },
    borderRight: {
        borderRightWidth: 2,
        borderColor: theme["color-font-light-300"]
    },
    borderLeft: {
        borderLeftWidth: 2,
        borderColor: theme["color-font-light-300"]
    },
    borderTop: {
        borderTopWidth: 2,
        borderColor: theme["color-font-light-300"]
    },
    borderBottom: {
        borderBottomWidth: 2,
        borderColor: theme["color-font-light-300"]
    },
    boxIcon: {
        color: theme["color-primary-500"],

        fontSize: 36,
        marginBottom: 16
    }

})
export const Box = ({ icon, title, ...props }) => {
    return (
        /* onPress={props.onPress} */
        <View style={{ justifyContent: 'center', alignItems: 'center', borderColor: '#ddd', padding: 16 }}>
            <Icon type="MaterialCommunityIcons" name={icon} style={styles.boxIcon}></Icon>
            <Text style={[iOSUIKit.bodyEmphasized, fontColors.primary, { textTransform: "uppercase" }]}>{title}</Text>
            <Text style={[iOSUIKit.callout, fontColors.lightDark, { textAlign: 'center', marginTop: 4 }]}>{props.subtitle}</Text>
        </View>
    )
}

export default class StartScreen extends Component {
    static contextType = NavigationContext;
    render() {
        const navigation = this.context;
        return (
            <Layout style={{ flex: 1 }}>
                <Row>
                    <Col style={[styles.centerAll, styles.borderRight, styles.borderBottom, styles.borderTop]}>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])} onPress={() => navigation.navigate('Dashboard')}>

                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="location-enter" title="Start" subtitle="Update and Organize your day"></Box>
                            </View>


                        </TouchableNativeFeedback>
                    </Col>
                    <Col style={[styles.centerAll, styles.borderBottom, styles.borderTop]}>
                        <TouchableNativeFeedback onPress={() => navigation.navigate('PlanPage')} background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])} >
                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="order-bool-ascending-variant" title="Plan" subtitle="Plan your future"></Box>
                            </View>
                        </TouchableNativeFeedback>
                    </Col>
                </Row>
                <Row>
                    <Col style={[styles.centerAll, styles.borderBottom, styles.borderRight]}>
                        <TouchableNativeFeedback onPress={() => navigation.navigate('diaryList')} background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])}>
                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="notebook-outline" title="Diary" subtitle="Keep a log of your thoughts"></Box>
                            </View>
                        </TouchableNativeFeedback>

                    </Col>
                    <Col style={[styles.centerAll, styles.borderBottom]}>
                        <TouchableNativeFeedback onPress={() => navigation.navigate('timelineScreen')} background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])}>
                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="timeline-clock-outline" title="Timeline" subtitle="See what you've been upto"></Box>
                            </View>
                        </TouchableNativeFeedback>

                    </Col>
                </Row>
                <Row>
                    <Col style={[styles.centerAll, styles.borderRight, styles.borderBottom]}>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])}>
                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="file-chart-outline" title="Statistics" subtitle="Analyze your past days for better planning"></Box>
                            </View>
                        </TouchableNativeFeedback>


                    </Col>
                    <Col style={[styles.centerAll, styles.borderBottom]}>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-200"])}>
                            <View style={{ width: '100%', flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                <Box icon="medal-outline" title="Acheivements" subtitle="Your performance awards"></Box>
                            </View>
                        </TouchableNativeFeedback>

                    </Col>
                </Row>
            </Layout>
        )
    }
}
