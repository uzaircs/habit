import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Divider } from '@ui-kitten/components'
import { View } from 'native-base'
import { H2, TextMuted } from '../../styles/text'
import { TouchableNativeFeedback, ScrollView } from 'react-native'
import { Box } from '../home/start'
import { default as theme } from '../../styles/app-theme.json'
import { BackNav } from '../../menu/header'
export class Plan extends Component<any, any>{
    render() {
        return (
            <View style={{ flex: 1 }}>
                <BackNav navigation={this.props.navigation} title="Plan"></BackNav>

                <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 8, paddingTop: 0 }} level="4">

                    <ScrollView>
                        <View style={{ marginBottom: 16, justifyContent: 'center', alignItems: 'center', marginTop: 16 }}>
                            <H2 text="Choose an option"></H2>
                            <TextMuted text="How would you like to plan your future"></TextMuted>
                        </View>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('StartWork') }}>
                            <View style={{ marginBottom: 8, width: '100%', zIndex: 999, backgroundColor: '#fff', }}>
                                <Box icon="calendar-check" title="Create Tasks" subtitle="Simple task, nothing special"></Box>
                            </View>
                        </TouchableNativeFeedback>

                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="calendar-today" title="Add Tasks to My Day" subtitle="Create or Add tasks for your current day"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('deadlineScreen') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="calendar-clock" title="Set Deadlines" subtitle="Assign a due date for your tasks or goals to get notified and stay on top"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="timer-outline" title="Create Work Sessions" subtitle="Lets create some sessions for you to work in the future"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('goalList') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="bullseye-arrow" title="Create Goals" subtitle="What are some of your goals you want to achieve?"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="flag-outline" title="Create Missions" subtitle="Mission gives you a sense of purpose, you should have one atleast"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="swap-horizontal" title="Move Goals" subtitle="Move existing goals to missions"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="swap-horizontal" title="Move Tasks" subtitle="Move existing tasks to goals for better tracking experience"></Box>
                            </View>
                        </TouchableNativeFeedback>
                        <Divider style={{ marginTop: 32, marginBottom: 32 }}></Divider>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('Session') }}>
                            <View style={{ marginBottom: 8, width: '100%', backgroundColor: '#fff', }}>
                                <Box icon="information-outline" title="Why Plan?" subtitle="Planning your life gives you control. If you create a plan then you get to make choices and decisions, rather than leaving things up to chance, or worse yet, letting others make decisions for you. Tap to learn more"></Box>
                            </View>
                        </TouchableNativeFeedback>
                    </ScrollView>
                </Layout>
            </View >
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Plan)
