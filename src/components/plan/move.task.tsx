import { Divider, Layout, List, ListItem } from '@ui-kitten/components'
import { View } from 'native-base';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BackNav } from 'src/menu/header';
import { H4 } from 'src/styles/text';
import { Task } from '../task/task'

export class MoveTask extends Component<any, any>{
    listRef: any;
    setContext = (item) => {
        this.setState({ ...this.state, dateShown: true, context: item });
    }
    renderListItem = ({ item }) => {

        /*   let { navigation } = this.props; */
        return (
            <ListItem disabled={true}>
                <Task readonly onTap={() => this.setContext(item)} task={item} />
            </ListItem>
        )
    }
    ListHeader = (props) => {
        return (
            <BackNav title="Move Tasks" subtitle="Move existing tasks to goals"></BackNav>
        )
    }
    render() {
        return (
            <Layout style={{ flex: 1 }}>
                <List
                    ref={ref => this.listRef = ref}
                    /* ListHeaderComponent={this.sortList.bind(this)} */
                    ItemSeparatorComponent={Divider}
                    data={this.props.tasks}
                    renderItem={this.renderListItem}
                    keyExtractor={(item) => item.id}
                    ListHeaderComponent={this.ListHeader}
                >
                </List>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.filter(t => (!t.goal || !t.goal.id))
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(MoveTask)
