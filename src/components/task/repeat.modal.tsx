import { Button, Card, Layout, ListItem } from '@ui-kitten/components';
import * as React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableNativeFeedback } from 'react-native';
import Animated from 'react-native-reanimated';
import BottomSheet from 'reanimated-bottom-sheet';
import { default as theme } from '../../styles/app-theme.json'
import { DefaultText, H1, H2, H4, TextMutedStrong, TextSubtitle, } from '../../styles/text';
import DateTimePicker from '@react-native-community/datetimepicker'
import { connect } from 'react-redux'
import { DEFAULT_MARGIN } from '../../common/constants';
import { CheckIcon } from './list';
import { BackNav } from '../../menu/header';
import { ButtonText } from '../activity';
import moment from 'moment';
import { Toast } from 'native-base';
import { ConfigureRepeatAction } from '../../actions/task.action';
export const ParseMode = mode => {
    switch (mode) {
        case 1:
            return 'Daily'
        case 2:
            return 'Weekly'
        case 3:
            return 'Monthly'
        case 4:
            return 'Annual'
    }
}
export const MonthDaySelector = ({ onSelect }) => {
    return (
        <View />
    )
}
export const WeekdaySelector = ({ onSelect }) => {
    const [selected, setSelected] = React.useState([]);

    const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'].map(day => day.substr(0, 2));
    const toggleDay = (day) => {
        if (isActive(day)) {
            let index = selected.indexOf(day)
            let days = [...selected.slice(0, index), ...selected.slice(index + 1)];
            setSelected(days)

        } else {
            let days = [...selected, day];
            setSelected(days);
        }
        if (onSelect && typeof onSelect === "function") {
            onSelect(selected)
        }
    }
    const isActive = (day) => {
        return selected.indexOf(day) !== -1;
    }
    return (
        <View style={{ justifyContent: 'space-between', alignItems: 'stretch', flexDirection: 'row' }}>
            {daysOfWeek.map((day, idx) => {
                let weekStyles = [styles.weekDay]
                let active = isActive(day);
                if (active) {
                    weekStyles.pop();
                    weekStyles.push(styles.weekDayActive);
                }
                return (
                    <TouchableNativeFeedback key={idx} onPress={() => toggleDay(day)} background={TouchableNativeFeedback.Ripple(theme["color-font-200"], true)}>
                        <View style={weekStyles}>
                            {!active && <TextMutedStrong text={day} />}
                            {active && <DefaultText style={{ color: 'white' }} text={day} />}
                        </View>
                    </TouchableNativeFeedback>
                )
            })}
        </View>
    )
}
export const MonthView = ({ onSelect }) => {
    const [monthSelected, setMonthSelected] = React.useState([]);
    let days = new Array(31).fill(1, 0, 31).map((d, i) => i + 1);
    const toggle = day => {
        if (monthSelected.indexOf(day) > -1) {
            let selectedDays = [...monthSelected.slice(0, monthSelected.indexOf(day)), ...monthSelected.slice(monthSelected.indexOf(day) + 1, monthSelected.length)]
            setMonthSelected(selectedDays);
            if (onSelect && typeof onSelect === "function") {
                onSelect(selectedDays);
            }
        } else {
            let selectedDays = [...monthSelected, day];
            setMonthSelected(selectedDays);
            if (onSelect && typeof onSelect === "function") {
                onSelect(selectedDays);
            }
        }

    }
    const isActive = (day) => monthSelected.indexOf(day) > -1;
    return (
        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: DEFAULT_MARGIN }}>
            {days.map((day, idx) => {
                let is_active = isActive(day);
                return (
                    <TouchableNativeFeedback key={idx} background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)} onPress={() => toggle(day)}>
                        <View style={{ borderRadius: 30, height: 36, width: '14%', justifyContent: 'center', alignItems: 'center', backgroundColor: is_active ? theme["color-primary-500"] : '#fff' }}>
                            {!is_active && <TextMutedStrong text={day} />}
                            {is_active && <DefaultText style={{ color: '#fff' }} text={day} />}
                        </View>
                    </TouchableNativeFeedback>
                )
            })}
        </View>
    )
}

function RepeatModal(props) {
    //#region hooks
    const [isVisible, setIsVisible] = React.useState(false);
    const [showDatePicker, setShowDatePicker] = React.useState(false);
    const [endsAt, setEndsAt] = React.useState(null);
    const [mode, setMode] = React.useState(1);
    const [daysConfig, setDaysConfig] = React.useState([]);
    let { id } = props.route?.params;
    //#endregion


    let fall = new Animated.Value(1)
    const sheetRef = React.useRef(null);
    const monthRef = React.useRef(null);
    const animatedHeaderContentOpacity = Animated.interpolate(fall, {
        inputRange: [0.75, 1],
        outputRange: [0, 1],
        extrapolate: Animated.Extrapolate.CLAMP,
    })
    const animatedBackgroundOpacity = Animated.sub(
        1,
        animatedHeaderContentOpacity
    )
    const show = () => {
        setIsVisible(true);
        sheetRef.current?.snapTo(0)
    }
    const removeDate = () => {
        setEndsAt(null);
    }
    const setDate = (date) => {
        if (moment(date).isValid()) {
            setEndsAt(date);
            setShowDatePicker(false);
        }
    }
    const hide = () => {
        setIsVisible(false);
        sheetRef.current?.snapTo(300)
    }
    const renderMonthContent = () => {



        return (
            <Card

                style={{
                    elevation: 4,
                    borderRadius: 16,
                    backgroundColor: 'white',
                    height: 366,
                }}
            >
                <View style={styles.handlerBarContainer}>
                    <View style={styles.handlerBar}>

                    </View>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN }}>
                    <H1 text="Days of Month"></H1>
                    <MonthView onSelect={setDaysOfMonth}></MonthView>
                    <View style={{ marginTop: DEFAULT_MARGIN }}>
                        <Button appearance="outline" onPress={() => {
                            monthRef.current?.snapTo(2);
                        }} children={props => <ButtonText text="Okay" {...props} />}></Button>
                    </View>
                </View>
            </Card>
        )
    };
    const renderContent = () => {



        return (
            <Card
                appearance="filled"
                style={{
                    elevation: 4,
                    borderRadius: 16,
                    backgroundColor: 'white',

                    height: 316,

                }}
            >
                <View style={styles.handlerBarContainer}>
                    <View style={styles.handlerBar}>

                    </View>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN }}>
                    <H1 text="Repeat Mode"></H1>

                    <View style={{ marginTop: DEFAULT_MARGIN }}>
                        <ListItem onPress={() => { setMode(1); sheetRef.current?.snapTo(2) }}>
                            <H4 style={{ color: theme["color-font-600"] }} text="Daily"></H4>
                            <View style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                                <CheckIcon color={theme["color-primary-500"]}></CheckIcon>
                            </View>
                        </ListItem>
                        <ListItem onPress={() => { setMode(2); sheetRef.current?.snapTo(2) }}>
                            <H4 style={{ color: theme["color-font-600"] }} text="Weekly"></H4>
                        </ListItem>
                        <ListItem onPress={() => { setMode(3); sheetRef.current?.snapTo(2) }}>
                            <H4 style={{ color: theme["color-font-600"] }} text="Monthly"></H4>
                        </ListItem>
                        <ListItem onPress={() => { setMode(4); sheetRef.current?.snapTo(2) }}>
                            <H4 style={{ color: theme["color-font-600"] }} text="Annual"></H4>
                        </ListItem>

                    </View>
                </View>
            </Card>
        )
    };

    let max = moment().add('25', 'years').toDate();
    let readableMode = ParseMode(mode);
    const setDaysOfWeek = (days: any[]) => {
        setDaysConfig(days);
    }
    const setDaysOfMonth = (days: any[]) => {
        setDaysConfig(days);
    }
    const save = () => {
        let task = null;
        if (props.tasks && props.tasks.length) {
            let filtered = props.tasks.filter(t => t.id == id);
            if (filtered.length === 1) {
                task = filtered[0];
            }

        }
        let repeatConfig = {
            mode, days: daysConfig, endsBefore: endsAt, task: id,
        }
        if (task) {
            if (props.attach && typeof props.attach === 'function') {
                props.attach(task, repeatConfig);
            }
        }
        Toast.show({
            text: 'Repeat setting saved',
            type: 'success',
            buttonText: 'Okay'
        })
        props.navigation.pop();
    }
    return (
        <>

            {showDatePicker && <DateTimePicker maximumDate={max} minimumDate={new Date()} neutralButtonLabel="Remove" onChange={(e, date) => {
                setShowDatePicker(false);
                if (e.type == 'set') {
                    setDate(date);
                }
                if (e.type == 'neutralButtonPressed') {
                    removeDate();
                }

            }} value={new Date()} />}

            <Layout
                level="3"
                style={{
                    flex: 1,

                }}
            >
                <BackNav title="Repeat Task" />
                <View style={{ position: 'relative', flex: 1 }}>
                    <ScrollView>
                        <ListItem onPress={show} style={{}}>
                            <View style={{ flexDirection: 'column', padding: 8 }}>
                                <TextMutedStrong text="Repeat Mode"></TextMutedStrong>
                                <H2 style={{ color: theme["color-primary-500"] }} text={readableMode}></H2>
                            </View>
                        </ListItem>
                        {mode == 2 && <ListItem disabled={true}>
                            <View style={{ flexDirection: 'column', padding: 8 }}>
                                <TextMutedStrong text="Days of Week"></TextMutedStrong>
                                <WeekdaySelector onSelect={setDaysOfWeek} />
                            </View>
                        </ListItem>}
                        {mode == 3 && <ListItem onPress={() => { monthRef.current?.snapTo(0) }}>
                            <View style={{ flexDirection: 'column', padding: 8 }}>
                                <TextMutedStrong text="Select Days of Month"></TextMutedStrong>
                                <H2 style={{ color: theme["color-primary-500"] }} text={"No days selected"}></H2>
                            </View>
                        </ListItem>}
                        {mode == 4 && <ListItem disabled={true}>
                            <View style={{ flexDirection: 'column', padding: 8 }}>
                                <TextMutedStrong text="Days of Week"></TextMutedStrong>
                                <WeekdaySelector onSelect={setDaysOfWeek} />
                            </View>
                        </ListItem>}
                        <ListItem onPress={() => setShowDatePicker(true)}>
                            <View style={{ flexDirection: 'column', padding: 8 }}>
                                <TextMutedStrong text="Ends Before"></TextMutedStrong>
                                <H2 style={{ color: theme["color-primary-500"] }} text={endsAt ? moment(endsAt).format('MMM D, YYYY') : 'Doesnt End'}></H2>
                            </View>
                        </ListItem>
                    </ScrollView>
                    <View style={{ position: 'absolute', bottom: 0, width: '100%', }}>
                        <Button onPress={save} children={props => <ButtonText text="Save" {...props} />} />
                    </View>
                </View>
            </Layout>
            <BottomSheet
                enabledBottomInitialAnimation={true}
                ref={sheetRef}
                snapPoints={[300, 300, 0]}
                initialSnap={1}
                renderContent={renderContent}
            />
            <BottomSheet
                enabledBottomInitialAnimation={true}
                ref={monthRef}
                snapPoints={[350, 350, 0]}
                initialSnap={1}
                renderContent={renderMonthContent}
            />
        </>
    );
}
const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks
})

const mapDispatchToProps = dispatch => {
    return {
        attach: (task, repeatConfig) => dispatch(ConfigureRepeatAction(task, repeatConfig))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(RepeatModal)
const styles = StyleSheet.create({
    handlerBar: {
        position: 'absolute',
        backgroundColor: '#D1D1D6',
        top: 5,
        left: 0,
        right: 0,
        borderRadius: 3,
        height: 5,
        width: 20,
    },
    weekDay: {
        padding: 4,
        margin: 4,
        borderRadius: 20,
        width: 40,
        height: 40,
        borderColor: theme["color-font-200"],
        borderWidth: 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    weekDayActive: {
        padding: 4,
        margin: 4,
        borderRadius: 20,
        width: 40,
        height: 40,
        borderColor: theme["color-primary-100"],
        backgroundColor: theme["color-primary-500"],
        borderWidth: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    handlerBarContainer: {
        position: 'absolute',
        alignSelf: 'center',
        top: 10,

        height: 20,
        width: 20,
    },
})