import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Task, BulbIcon, isToday, SubtaskIcon } from './task'
import { UpdateTaskListAsync, DeleteTaskAction, TaskUpdateAction, ApplySorting, ApplyFilter, PositionChanged } from '../../actions/task.action'
import { Icon as NBIcon, Toast } from 'native-base'
import { Row, Col } from 'react-native-easy-grid'
import { View, Text } from 'native-base'
import { EmptyIcon } from '../day.past'
import { fontFamily, sizes } from '../../styles/fonts'
import ModalBox from 'react-native-modalbox';
import { ListItem, Divider, Icon, Button, Modal, Card, Input, Layout, OverflowMenu, MenuItem, CheckBox, Select, IndexPath, SelectItem, List } from '@ui-kitten/components'
import { H2, TextMuted, DefaultText } from '../../styles/text'
import { CenterLoading } from '../activity.page'
import { SwipeListView } from 'react-native-swipe-list-view';
import { default as theme } from '../../styles/app-theme.json'
import { TrashIcon } from './details'
import { TouchableNativeFeedback, KeyboardAvoidingView, ScrollView, TouchableWithoutFeedback, TouchableOpacity, InteractionManager } from 'react-native'
import DraggableFlatList from "react-native-draggable-flatlist";
import { RemoveMyDay, AddMyDay, DeleteTask, GroupTask } from '../../models/api'
import { BulkDeleteAsync, BindTaskToParentAsync, GroupTaskAsync } from '../../actions/bulk.actions'
import { TaskPicker } from './picker'
import { InputLabel, CardHeader } from '../forms/category'
import { PlusOutlineIcon } from '../activity'
import { genTitle, TopNav } from '../../menu/header'
import {
    SelectMultipleButton,
    SelectMultipleGroupButton
} from "react-native-selectmultiple-button";
import { UppercaseHeading } from '../entry/past'
import { iOSUIKit } from 'react-native-typography'
import { useAndroidBackHandler, AndroidBackHandler } from 'react-navigation-backhandler'
import moment from 'moment'
import { AddButton } from '../goals/list'
import { AddTaskModal } from './add'
import { BORDER_RADIUS, DEFAULT_MARGIN, MENU_HEIGHT_COLLAPSED, MENU_HEIGHT_EXPANED } from '../../common/constants'
import { useIsFocused } from '@react-navigation/native';
export const CustomSeperator = () => {
    return (
        <View style={{ height: DEFAULT_MARGIN / 4 }}></View>
    )
}
export const FilterIcon = props => {
    return (
        <Icon name="funnel-outline" {...props}></Icon>
    )
}
export const MenuIcon = props => {
    return (
        <Icon name="more-vertical-outline" {...props}></Icon>
    )
}
export const MenuIconHorizontal = props => {
    return (
        <Icon name="more-horizontal-outline" {...props}></Icon>
    )
}
export const ShowIcon = props => {
    return (
        <Icon name="eye-outline" {...props}></Icon>
    )
}
export const SyncIcon = props => {
    return (
        <Icon name="sync-outline" {...props}></Icon>
    )
}
export const HideIcon = props => {
    return (
        <Icon name="eye-off-outline" {...props}></Icon>
    )
}
export const CloseIcon = props => {
    return (
        <Icon name="close-outline" {...props}></Icon>
    )
}
export const QuestionIcon = props => {
    return (
        <Icon name="question-mark-circle-outline" {...props}></Icon>
    )
}
export const GoalLabelSelector = ({ goals, onChange, selected = [] }) => {
    const [selectedGoals, setSelectedGoals] = React.useState([...selected]);
    const toggleGoal = goal => {
        try {
            const exists = selectedGoals.findIndex(g => g === goal);
            if (exists > -1) {
                let newState = [...selectedGoals.slice(0, exists), ...selectedGoals.slice(exists + 1)];
                setSelectedGoals(newState);
            } else {
                let newState = [...selectedGoals, goal];
                setSelectedGoals(newState);
            }
        } catch (error) {

        }
    }
    if (onChange && typeof onChange === "function") {
        onChange(selectedGoals);
    }
    if (!goals || !Array.isArray(goals) || goals.length < 1) {
        return <View />
    }
    return (
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {goals.map(goal => {
                return <SelectMultipleButton
                    key={goal.id}
                    buttonViewStyle={{
                        borderRadius: 4,
                        borderWidth: 2,
                        height: 32
                    }}
                    textStyle={iOSUIKit.subheadEmphasized}
                    highLightStyle={{
                        borderColor: theme["color-primary-100"],

                        backgroundColor: "transparent",

                        textColor: theme["color-font-400"],


                        borderTintColor: theme["color-primary-300"],

                        backgroundTintColor: theme["color-primary-500"],

                        textTintColor: "white"
                    }}
                    multiple={true}
                    value={goal.id}
                    displayValue={goal.name}

                    selected={selectedGoals.indexOf(goal.id) !== -1}
                    singleTap={valueTap => toggleGoal(valueTap)}

                />
            })}
        </View>
    )
}
export const ListLabelSelector = ({ listItems, onChange, selected = [] }) => {
    const [selectedLists, setSelectedLists] = React.useState([...selected]);
    const toggleGoal = goal => {
        try {
            const exists = selectedLists.findIndex(g => g === goal);
            if (exists > -1) {
                let newState = [...selectedLists.slice(0, exists), ...selectedLists.slice(exists + 1)];
                setSelectedLists(newState);
            } else {
                let newState = [...selectedLists, goal];
                setSelectedLists(newState);
            }
        } catch (error) {

        }
    }
    if (onChange && typeof onChange === "function") {
        onChange(selectedLists);
    }
    if (!listItems || !Array.isArray(listItems) || listItems.length < 1) {
        return <View />
    }
    return (
        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            {listItems.map(list => {
                return <SelectMultipleButton
                    key={list.id}
                    buttonViewStyle={{
                        borderRadius: 4,
                        borderWidth: 2,
                        height: 32
                    }}
                    textStyle={iOSUIKit.subheadEmphasized}
                    highLightStyle={{
                        borderColor: theme["color-primary-100"],

                        backgroundColor: "transparent",

                        textColor: theme["color-font-400"],


                        borderTintColor: theme["color-primary-300"],

                        backgroundTintColor: theme["color-primary-500"],

                        textTintColor: "white"
                    }}
                    multiple={true}
                    value={list.id}
                    displayValue={list.name}

                    selected={selectedLists.indexOf(list.id) !== -1}
                    singleTap={valueTap => toggleGoal(valueTap)}

                />
            })}
        </View>
    )
}
export const TaskFilterSettings = ({ onApply, onClose, goals, listItems, filters }) => {

    let existingGoals = filters.goals ? [...filters.goals] : [];
    let existingList = filters.lists ? [...filters.lists] : [];
    const [showCompleted, setShowCompleted] = React.useState(filters.filter.indexOf(FilterOptions.SHOW_COMPLETED) > -1);
    const [showSubTasks, setShowSubTasks] = React.useState(filters.filter.indexOf(FilterOptions.SHOW_SUB_TASKS) > -1);
    const [showMyday, setShowMyday] = React.useState(false);
    const [selectedGoals, setSelectedGoals] = React.useState(existingGoals);
    const [selectedLists, setSelectedLists] = React.useState(existingList);
    const applyFilter = () => {
        if (onApply && typeof onApply === "function") {
            let filterOptions = [];
            if (showCompleted) {
                filterOptions.push(FilterOptions.SHOW_COMPLETED)
            }
            if (showSubTasks) {
                filterOptions.push(FilterOptions.SHOW_SUB_TASKS);
            }
            if (selectedGoals.length) {
                filterOptions.push(FilterOptions.BY_GOAL);
            }
            if (selectedLists.length) {
                filterOptions.push(FilterOptions.BY_LIST);
            }

            onApply({ filter: filterOptions, goals: selectedGoals, lists: selectedLists });
        }

    }
    return (
        <Layout style={{ flex: 1, padding: 16 }}>
            <Row style={{ maxHeight: 56 }}>
                <Col style={{ justifyContent: 'center' }}>
                    <Row style={{ alignItems: 'center' }}>
                        <FilterIcon width={16} height={16} fill={theme["color-font-300"]} style={{ marginRight: 8 }} />
                        <H2 text="Filter Tasks" style={{ color: theme["color-font-500"] }}></H2>
                    </Row>
                </Col>
                <Col style={{ justifyContent: 'center' }}>
                    <View style={{ alignItems: 'flex-end' }}>
                        <TouchableNativeFeedback onPress={onClose}>
                            <CloseIcon width={24} height={24} fill={theme["color-font-300"]} />
                        </TouchableNativeFeedback>
                    </View></Col>
            </Row>
            <Divider style={{ marginBottom: 16 }}></Divider>
            <UppercaseHeading heading="General"></UppercaseHeading>
            <View style={{ marginTop: 16 }}>
                <ScrollView>
                    <Row size={12} style={{ marginBottom: 16 }}>
                        <Col size={1}>
                            <CheckBox onChange={checked => setShowCompleted(checked)} checked={showCompleted}></CheckBox>
                        </Col>
                        <Col size={11}>
                            <TouchableWithoutFeedback onPress={() => setShowCompleted(!showCompleted)}>
                                <View>
                                    <DefaultText text="Show Completed Tasks" style={{ color: theme["color-font-400"] }}></DefaultText>
                                </View>
                            </TouchableWithoutFeedback>

                        </Col>
                    </Row>
                    <Row size={12} style={{ marginBottom: 16 }}>
                        <Col size={1}>
                            <CheckBox onChange={checked => setShowSubTasks(checked)} checked={showSubTasks}></CheckBox>
                        </Col>
                        <Col size={11}>
                            <TouchableWithoutFeedback onPress={() => setShowSubTasks(!showSubTasks)}>
                                <View>
                                    <DefaultText text="Show Subtasks" style={{ color: theme["color-font-400"] }}></DefaultText>
                                </View>
                            </TouchableWithoutFeedback>

                        </Col>
                    </Row>
                    <Row size={12} style={{ marginBottom: 16 }}>
                        <Col size={1}>
                            <CheckBox onChange={checked => setShowMyday(checked)} checked={showMyday}></CheckBox>
                        </Col>
                        <Col size={11}>
                            <TouchableWithoutFeedback onPress={() => setShowMyday(!showMyday)}>
                                <View>
                                    <DefaultText text="Show only my day tasks" style={{ color: theme["color-font-400"] }}></DefaultText>
                                </View>
                            </TouchableWithoutFeedback>

                        </Col>
                    </Row>
                    <Divider style={{ marginBottom: 16 }}></Divider>
                    <UppercaseHeading heading="Goals"></UppercaseHeading>
                    <Row size={12}>
                        <View style={{ marginBottom: 16 }}>
                            <GoalLabelSelector selected={selectedGoals} onChange={__goals => setSelectedGoals(__goals)} goals={goals}></GoalLabelSelector>
                        </View>
                    </Row>
                    <Divider style={{ marginBottom: 16 }}></Divider>
                    <UppercaseHeading heading="List"></UppercaseHeading>
                    <Row size={12}>
                        <View style={{ marginBottom: 16 }}>
                            <ListLabelSelector selected={selectedLists} onChange={__lists => setSelectedLists(__lists)} listItems={listItems}></ListLabelSelector>
                        </View>
                    </Row>

                </ScrollView>
            </View>
            <View>
                <Button onPress={applyFilter}>Apply</Button>
            </View>
        </Layout >

    )
}
export enum SortOptions {
    DUE_DATE = 0,
    DUE_DATE_GOAL = 1,
    DATE_CREATED = 2,
    DATE_MODIFIED = 3,
    ALPHATICALLY,
    MY_DAY,
    STATUS,
    DATE_COMPLETED
}
export enum FilterOptions {
    SHOW_SUB_TASKS = 1,
    SHOW_COMPLETED = 2,
    BY_GOAL,
    BY_LIST
}
export const TaskSortSettings = ({ onApply, onClose, isAscending = false, selectedOption = 0 }) => {
    console.log(isAscending)
    const applySort = () => {
        if (onApply && typeof onApply === 'function') {
            onApply(selectedIndex.row, ascending);
        }
    }
    let group = [
        { value: "asc", displayValue: 'Ascending' },
        { value: "desc", displayValue: 'Descending' }

    ];
    const setOptionText = option => {
        switch (option) {
            case 0:
                return 'Due Date';

            case 1:
                return 'Due Date (Goal)';

            case 2:
                return 'Date Created';

            case 3:
                return 'Date Modified';

            case 4:
                return 'Alphabetically';

            case 5:
                return 'My Day';

            default: break;
        }
    }
    const [ascending, setAscending] = React.useState(isAscending);
    const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(selectedOption));
    const defaultIndex = ascending ? 0 : 1;
    const [option, setOption] = React.useState(setOptionText(selectedOption));
    switch (selectedIndex.row) {
        case 0:
        case 1:
            group = [
                { value: "asc", displayValue: 'Nearest on Top' },
                { value: "desc", displayValue: 'Farthest on Top' }

            ];

            break;
        case 2:
        case 3:
            group = [
                { value: "asc", displayValue: 'Recent on Top' },
                { value: "desc", displayValue: 'Oldest on Top' }

            ];
            break;
        case 4:
            group = [
                { value: "asc", displayValue: 'A to Z' },
                { value: "desc", displayValue: 'Z to A' }

            ];
        default:
            break;
    }
    return (
        <Layout style={{ flex: 1, padding: 16 }}>
            <Row style={{ maxHeight: 56 }}>
                <Col style={{ justifyContent: 'center' }}>
                    <Row style={{ alignItems: 'center' }}>
                        <SortIcon style={{ tintColor: theme["color-font-300"] }} extraStyles={{ marginRight: 8 }}></SortIcon>
                        <H2 text="Sort Tasks" style={{ color: theme["color-font-500"] }}></H2>
                    </Row>
                </Col>
                <Col style={{ justifyContent: 'center' }}>
                    <View style={{ alignItems: 'flex-end' }}>
                        <TouchableNativeFeedback onPress={onClose}>
                            <CloseIcon width={24} height={24} fill={theme["color-font-300"]} />
                        </TouchableNativeFeedback>
                    </View></Col>
            </Row>
            <View >
                <ScrollView>
                    <Divider style={{ marginBottom: 16, marginTop: 16 }}></Divider>
                    <UppercaseHeading heading="Sort By"></UppercaseHeading>
                    <Row>
                        <Col>
                            <Select
                                value={option}
                                selectedIndex={selectedIndex}
                                onSelect={(indexPath: any) => {
                                    setSelectedIndex(indexPath)
                                    setOption(setOptionText(indexPath.row));
                                }}>
                                <SelectItem title="Due Date"></SelectItem>
                                <SelectItem title="Due Date (Goal)"></SelectItem>
                                <SelectItem title="Date Created"></SelectItem>
                                <SelectItem title="Date Modified"></SelectItem>
                                <SelectItem title="Alphatically"></SelectItem>
                                <SelectItem title="My Day"></SelectItem>
                            </Select>
                        </Col>
                    </Row>
                    {selectedIndex.row <= 4 && <Divider style={{ marginBottom: 16, marginTop: 16 }}></Divider>}
                    {selectedIndex.row <= 4 && <UppercaseHeading heading="Sort Order"></UppercaseHeading>}
                    {selectedIndex.row <= 4 && <Row size={12}>
                        <Col size={10}>
                            <SelectMultipleGroupButton
                                defaultSelectedIndexes={[defaultIndex]}
                                multiple={false}
                                containerViewStyle={{
                                    justifyContent: "flex-start"
                                }}
                                highLightStyle={{
                                    borderColor: theme["color-primary-100"],

                                    backgroundColor: "transparent",

                                    textColor: theme["color-font-400"],


                                    borderTintColor: theme["color-primary-300"],

                                    backgroundTintColor: theme["color-primary-500"],

                                    textTintColor: "white"
                                }}
                                onSelectedValuesChange={selectedValues => setAscending((selectedValues[0] && selectedValues[0] == 'asc'))}

                                group={group}
                            />

                        </Col>
                        <Col size={2} style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                            {ascending && <AscendingIcon style={{ fontSize: 24, color: theme["color-primary-500"] }}></AscendingIcon>}
                            {!ascending && <DescendingIcon style={{ fontSize: 24, color: theme["color-primary-500"] }}></DescendingIcon>}
                        </Col>
                    </Row>}
                    <Divider style={{ marginBottom: 16, marginTop: 16 }}></Divider>
                </ScrollView>
            </View>
            <View>
                <Button onPress={applySort}>Apply</Button>
            </View>
        </Layout >

    )
}
export const SortIcon = props => {
    return (<NBIcon type="MaterialCommunityIcons" name="sort" style={{ color: props.style.tintColor, fontSize: 16, ...props.extraStyles }} />)
}
export const AscendingIcon = props => {
    return (<NBIcon type="FontAwesome5" name="sort-amount-up" style={{ fontSize: 16, ...props.style }} />)
}
export const DescendingIcon = props => {
    return (<NBIcon type="FontAwesome5" name="sort-amount-down-alt" style={{ fontSize: 16, ...props.style }} />)
}
export const BulkIcon = props => {
    return (<NBIcon type="FontAwesome" name="edit" style={{ color: props.style.tintColor, fontSize: 16 }} />)
}
export const MergIcon = props => {
    return (<NBIcon type="MaterialCommunityIcons" name="merge" style={{ color: props.style.tintColor, fontSize: 24 }} />)
}
export const SwapIcon = props => {
    return (<NBIcon type="MaterialCommunityIcons" name="swap-horizontal" style={{ color: props.style.tintColor, fontSize: 24 }} />)
}
export const MoveDownIcon = props => {
    return (<NBIcon type="MaterialCommunityIcons" name="chevron-down" style={{ fontSize: 24, color: theme["color-font-300"] }} />)
}
export const MoveUpIcon = props => {
    return (<NBIcon type="MaterialCommunityIcons" name="chevron-up" style={{ fontSize: 24, color: theme["color-font-300"] }} />)
}
export const MoveDownButton = props => {
    return (
        <Button status="basic" size="small" accessoryRight={MoveDownIcon} appearance="ghost" ></Button>
    )
}
export const MoveUpButton = props => {
    return (
        <Button status="basic" size="small" accessoryRight={MoveUpIcon} appearance="ghost" ></Button>
    )
}
export const SortButton = ({ onPress, status = "basic" }) => {
    return (
        <Button onPress={onPress} status={status} size="small" accessoryRight={SortIcon} appearance="ghost" style={{ width: 24 }}></Button>
    )
}
export const TrashButton = ({ onPress }) => {
    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={() => <TrashIcon color={theme["color-danger-500"]}></TrashIcon>} appearance="ghost" style={{ width: 24 }}></Button>
    )
}
export const MydayButton = () => {
    return (
        <Button status="basic" size="small" accessoryRight={() => <BulbIcon style={{ width: 24, height: 24 }} fill={theme["color-primary-500"]}></BulbIcon>} appearance="ghost" style={{ width: 24 }}></Button>
    )
}
export const MergeButton = ({ onPress }) => {

    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={MergIcon} appearance="ghost" ></Button >
    )
}
export const SwapButton = ({ onPress }) => {

    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={SwapIcon} appearance="ghost" ></Button >
    )
}

export const FilterButton = ({ onPress }) => {
    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={FilterIcon} appearance="ghost" style={{ width: 24 }}></Button>
    )
}
export const MenuButton = ({ onPress }) => {
    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={MenuIcon} appearance="ghost" style={{ width: 24 }}></Button>
    )
}
export const CheckIcon = ({ color = theme["color-font-100"], ...otherProps }) => (
    <Icon name='checkmark' fill={color} style={{ width: 24, height: 24, ...otherProps.style }} otherProps />
);
class _TaskList extends React.Component<any, any> {
    listRef: any;
    load() {
        this.setState((state, props) => { return { isBusy: true } })
    }
    setFromProps = () => {
        if (this.props.task) {
            this.setState({ tasks: this.props.tasks.filter(t => !t.is_filtered), isBusy: false });
        }
    }
    /**
     *
     */
    constructor(props: any) {
        super(props);
        this.state = {
            selectedIndex: 0,
            menuVisible: false,
            totalTasks: 0,
            filteredTask: 0
        };

    }

    renderMenuToggleButton = () => {
        return (
            <Button onPress={() => this.setState({ menuVisible: true })} status="basic" size="small" accessoryRight={MenuIcon} appearance="ghost" style={{ width: 24 }}></Button>
        )
    }
    OverflowMenu() {
        return (
            <OverflowMenu
                backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.2)', }}
                anchor={this.renderMenuToggleButton}
                style={{ width: 256 }}
                visible={this.state.menuVisible}
                onSelect={() => this.setState({ menuVisible: false })}
                onBackdropPress={() => this.setState({ menuVisible: false })}>
                <MenuItem accessoryLeft={(props) => <View style={{ marginRight: 8, marginLeft: 8 }}><BulkIcon {...props}></BulkIcon></View>} title={props => genTitle('Bulk Edit', props)} />
                <MenuItem accessoryLeft={(props) => <ShowIcon {...props}></ShowIcon>} title={props => genTitle('Show Subtasks', props)} />
                <MenuItem accessoryLeft={(props) => <ShowIcon {...props}></ShowIcon>} title={props => genTitle('Show Completed Tasks', props)} />
                <MenuItem accessoryLeft={(props) => <SyncIcon {...props}></SyncIcon>} title={props => genTitle('Sync', props)} />
            </OverflowMenu>
        )
    }
    headerRight = () => {
        return (
            <View style={{ backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'center', flexDirection: 'row', flex: 1 }}>
                <SortButton status={this.props.hasSort ? "primary" : "basic"} onPress={() => this.refs.sortModal.open()}></SortButton>
                <FilterButton onPress={() => this.refs.filterModal.open()}></FilterButton>
                {/*  {this.OverflowMenu()} */}
            </View>
        )
    }
    sortList() {

        return (
            <Row>
                <Col>
                    <View style={{ backgroundColor: 'white', flex: 1 }}>
                        <H2 text="Tasks"></H2>
                    </View>
                </Col>
                <Col>
                    {this.headerRight()}
                </Col>
            </Row>
        )
    }
    onTaskSelected(t) {
        if (t.Id) {
            if (this.props.bindTasks && typeof this.props.bindTasks === 'function') {
                this.props.bindTasks(t.Id);
            }
            Toast.show({
                text: "Task moved",
                textStyle: {
                    fontFamily: fontFamily.semibold.fontFamily
                },
                duration: 1000,
                type: 'success'
            })
        }

    }
    /**
     * @return True if both are equal
     * @param context Task to compare with in current context
     * @param newTask Incoming task for comparison
     */
    compare = (context, newTask) => {
        if (context.softID) {
            return true;
        }
        if ((!context.reminders || !context.reminders.length) && (newTask.reminders && newTask.reminders.length)) {
            return true;
        }
        if (newTask?.name != context?.name) {
            return false;
        }
        if (newTask?.status != context?.status) {
            return false;
        }
        if (newTask.DueDate != context.DueDate) {
            return false;
        }
        if (newTask.estimatedTime != context.estimatedTime) {
            return false;
        }
        if (newTask.myDay != context.myDay) {
            return false;
        }
        return true;
    }


    bulkHeader() {

        return (
            <Row size={12}>
                <Col size={4}>
                    <View style={{ backgroundColor: 'white', padding: 16, flex: 1 }}>
                        <H2 text="Tasks"></H2>
                    </View>
                </Col>
                <Col size={8}>
                    <View style={{ padding: 16, backgroundColor: 'white', justifyContent: 'flex-end', flexDirection: 'row' }}>
                        <View style={{ marginRight: 8 }}>
                            <TrashButton onPress={() => this.props.deleteBulk()}></TrashButton>
                        </View>
                        <View style={{ marginRight: 8 }}>
                            <MydayButton></MydayButton>
                        </View>
                        <View style={{ marginRight: 8 }}>
                            <MergeButton onPress={() => {
                                this.setState({
                                    groupVisible: true
                                })
                            }}></MergeButton>
                        </View>
                        <View style={{ marginRight: 8 }}>
                            <TaskPicker onSelected={(selected) => this.onTaskSelected(selected)} selectView={({ showModal }) => <SwapButton onPress={showModal}></SwapButton>}></TaskPicker>

                        </View>

                    </View></Col>
            </Row>
        )
    }
    componentDidMount() {

        InteractionManager.runAfterInteractions(() => {
            this.load();
        })

    }


    groupCardHeader = (props) => {
        return (<CardHeader subtitle="Enter a task name to group all 7 tasks under this new task" {...props} title="Group into new task"></CardHeader>)
    }
    groupItems = () => {
        this.setState({ groupVisible: false })

    }

    toggleMyday = (task) => {
        if (task.myDay && isToday(task.myDay)) {
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update(task.id, 'myDay', null);
            }
            RemoveMyDay(task.id).then((result) => {
                if (result) {
                    if (this.props.request && typeof this.props.request === 'function') {
                        this.props.request();

                    }
                }
            })
        } else {
            //propType = myDay
            //propValue = moment().unix() * 1000;
            //id = task.id
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update(task.id, 'myDay', moment().unix() * 1000);
            }
            AddMyDay(task.id).then((added) => {
                if (added) {

                    if (this.props.request && typeof this.props.request === 'function') {
                        this.props.request();

                    }
                }
            }).catch(() => {

            })
        }

    }

    renderListItem({ item, drag, isActive }) {

        /*   let { navigation } = this.props; */
        return (

            <ListItem disabled={true} style={{ borderRadius: BORDER_RADIUS, elevation: isActive ? 25 : 0 }}>
                <Task drag={drag} active={isActive} task={item} ></Task>
            </ListItem>
        )
    }
    add(key) {
        if (this.props.tasks && this.props.tasks.length > 0) {
            let task = this.props.tasks.filter(t => t.id == key);
            if (task && task.length) {
                task = task[0];
                this.toggleMyday(task);

            }
        }
    }

    delete(key) {
        this.props.deleteTask(key);
        DeleteTask(key).catch(err => console.log(err));
    }
    onClose = () => {
        this.setState({
            modalOpen: false
        })
    }

    onOpen = () => {
        this.setState({
            modalOpen: true
        })
    }
    onClosingState(state) {

    }
    applyFilter = (filter) => {

        this.setState(() => { return { updateRequired: true } })

        if (this.props.filter && typeof this.props.filter === 'function') {
            this.props.filter(filter);
        }
        this.refs.filterModal.close();
    }
    filterSettings = () => {
        return (
            <TaskFilterSettings filters={this.props.filters} onApply={(filter) => {
                this.applyFilter(filter);
            }} listItems={this.props.listItems} goals={this.props.goals} onClose={() => { this.refs.filterModal.close(); }} onSelect={() => { this.refs.filterModal.close(); }}></TaskFilterSettings>
        )
    }
    sortSettings = () => {
        return (
            <TaskSortSettings selectedOption={this.props.sortSettings.sortOption} isAscending={this.props.sortSettings.isAscending} onClose={() => { this.refs.sortModal.close(); }} onApply={(sortOption, isAscending) => {
                this.setState((state, props) => {
                    return {
                        updateRequired: true
                    }
                })

                this.refs.sortModal.close();
                if (this.props.sort && typeof this.props.sort === 'function') {
                    this.props.sort(sortOption, isAscending);
                }

            }}  ></TaskSortSettings>
        )
    }
    render() {

        if (this.props.isBusy) { return <CenterLoading></CenterLoading> }
        if (!this.props.tasks || !this.props.tasks.length) {
            if (!this.props.isBusy) {
                return (
                    <AndroidBackHandler onBackPress={() => {
                        if (this.state.modalOpen) {

                            this.setState({ modalOpen: false })
                            this.refs.filterModal.close();
                            this.refs.sortModal.close();
                            return true;
                        } else {
                            return false;
                        }
                    }}>
                        <TopNav accessoryRight={() => this.headerRight()} title="Task List" height={MENU_HEIGHT_COLLAPSED} navigation={this.props.navigation}>

                        </TopNav>
                        <ModalBox
                            useNativeDriver={true}
                            ref={"filterModal"}
                            swipeToClose={true}
                            onClosed={this.onClose}
                            onOpened={this.onOpen}
                            onClosingState={this.onClosingState}>

                            <this.filterSettings></this.filterSettings>
                        </ModalBox>
                        <ModalBox
                            useNativeDriver={true}
                            ref={"sortModal"}
                            swipeToClose={true}
                            onClosed={this.onClose}
                            onOpened={this.onOpen}
                            onClosingState={this.onClosingState}>
                            <this.sortSettings></this.sortSettings>
                        </ModalBox>
                        <View style={{ height: 200, justifyContent: 'center', alignItems: 'center' }}>
                            <EmptyIcon></EmptyIcon>
                            <Text style={[fontFamily.semibold, sizes.medium, { color: '#aaa' }]}>Nothing here</Text>
                            <Text style={[fontFamily.semibold, sizes.normal, { color: '#666666', marginTop: 2 }]}>Your tasks will be shown here</Text>
                        </View>
                    </AndroidBackHandler>
                )
            }
        }

        return (
            <AndroidBackHandler onBackPress={() => {
                if (this.state.modalOpen) {

                    this.setState({ modalOpen: false })
                    this.refs.filterModal.close();
                    this.refs.sortModal.close();
                    return true;
                } else {
                    return false;
                }
            }}>
                <TopNav accessoryRight={() => this.headerRight()} title="Task List" height={MENU_HEIGHT_COLLAPSED} navigation={this.props.navigation}>

                </TopNav>

                <Layout style={{ flex: 1, }} level="2">


                    <ModalBox
                        useNativeDriver={true}
                        ref={"filterModal"}
                        swipeToClose={true}
                        onClosed={this.onClose}
                        onOpened={this.onOpen}
                        onClosingState={this.onClosingState}>

                        <this.filterSettings></this.filterSettings>
                    </ModalBox>
                    <ModalBox
                        useNativeDriver={true}
                        ref={"sortModal"}
                        swipeToClose={true}
                        onClosed={this.onClose}
                        onOpened={this.onOpen}
                        onClosingState={this.onClosingState}>
                        <this.sortSettings></this.sortSettings>
                    </ModalBox>

                    <DraggableFlatList

                        ref={ref => this.listRef = ref}
                        keyboardShouldPersistTaps='always'
                        ItemSeparatorComponent={CustomSeperator}
                        data={this.props.tasks}
                        renderItem={this.renderListItem}
                        autoscrollThreshold={96}
                        autoscrollSpeed={256}
                        keyExtractor={(item) => item.id}
                        onDragEnd={({ data, from, to }) => {
                            let tasks = [...data];
                            tasks[to].position = to;
                            tasks[from].position = from;
                            if (this.props.reposition && typeof this.props.reposition === 'function') {
                                /*    this.props.reposition(from, to); */
                            }
                            this.setState((state, props) => {
                                return {
                                    tasks: data
                                }
                            })
                            this.forceUpdate();
                        }}
                    >
                    </DraggableFlatList>
                </Layout>
            </AndroidBackHandler >
        )

    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.filter(t => !t.is_filtered),
    filters: state.tasks.filter,
    goals: state.goals.goals,
    isBusy: state.tasks.isBusy,
    sortSettings: state.tasks.sort,
    listItems: state.list.items,
    bulk: state.bulk.status,
    hasSort: (() => {
        try {
            return !isNaN(state.tasks.sort.sortOption)
        } catch (error) {
            return false;
        }
    })()
})
const FocusEffectTaskList = (props) => {
    const isFocused = useIsFocused();
    return (
        <_TaskList isFocused={isFocused}  {...props}></_TaskList>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync()),
        deleteBulk: () => dispatch(BulkDeleteAsync()),
        bindTasks: (parent) => dispatch(BindTaskToParentAsync(parent)),
        groupTasks: (name) => dispatch(GroupTaskAsync(name)),
        deleteTask: (id) => dispatch(DeleteTaskAction(id)),
        sort: (option, isAscending) => dispatch(ApplySorting(option, isAscending)),
        update: (id, propType, propValue) => dispatch(TaskUpdateAction({ id, propType, propValue })),
        filter: filterOption => dispatch(ApplyFilter(filterOption)),
        reposition: (from, to) => dispatch(PositionChanged(from, to))
    }
}
export const CustomTaskList = _TaskList;
export const TaskList = connect(mapStateToProps, mapDispatchToProps)(FocusEffectTaskList)
