import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, ListItem } from '@ui-kitten/components'
import { BulbIcon, BulbOffIcon, isToday, Task } from './task'
import { TaskUpdateAction } from '../../actions/task.action';
import { EmptyListPlaceholder } from '../day.past';
import { DEFAULT_MARGIN } from '../../common/constants';
import { View } from 'react-native';
import { H4, TextMuted } from '../../styles/text';
import { default as theme } from '../../styles/app-theme.json'
export class MyDayTasks extends Component<any, any>{
    /**
     *
     */
    noTasks = () => {
        if (this.props.empty && typeof this.props.empty === 'function') {
            this.props.empty(true);
        }
        return (
            <View />
        )
        return (
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <BulbOffIcon style={{ fontSize: 24, color: theme["color-font-300"], marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}></BulbOffIcon>
                <TextMuted text="No tasks for current day."></TextMuted>
            </View>
        )
    }
    constructor(props) {
        super(props);

    }
    listRef: any;

    render() {
        if (!this.props.tasks || !this.props.tasks.length) {
            return this.noTasks();
        } else {
            if (this.props.empty && typeof this.props.empty === 'function') {
                this.props.empty(false);
            }
        }
        return (
            <View>
                {this.props.tasks.map(task => {
                    return (
                        <ListItem style={{ marginBottom: DEFAULT_MARGIN / 4 }}>
                            <Task key={task.id} task={task}></Task>
                        </ListItem>
                    )
                })}

            </View>

        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.filter(t => isToday(t.myDay))
})

const mapDispatchToProps = dispatch => {
    return {
        update: props => dispatch(TaskUpdateAction(props))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyDayTasks)
