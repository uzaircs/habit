import React, { Component } from 'react'
import { connect } from 'react-redux'
import { List, Divider, ListItem, Layout } from '@ui-kitten/components'
import { Task } from './task'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import { ChangeTaskDueDate } from '../../models/api';
import { TaskUpdateAction } from '../../actions/task.action';
export class TaskDeadline extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            dateShown: false,
            context: null,
            selectedDate: moment().toDate()
        }
    }
    listRef: any;
    applyDate = () => {
       
        if (this.state.context) {
            ChangeTaskDueDate(this.state.context.id, this.state.selectedDate);
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update({
                    id: this.state.context.id,
                    propType: 'DueDate',
                    propValue: this.state.selectedDate
                })
            }
        }
    }
    openPicker = () => {
        this.setState({ ...this.state, dateShown: true });
    }
    closePicker = () => {
        this.setState({ ...this.state, dateShown: false });
    }
    setContext = (item) => {
        this.setState({ ...this.state, dateShown: true, context: item });
    }
    renderListItem = ({ item }) => {

        /*   let { navigation } = this.props; */
        return (
            <ListItem disabled={true}>
                <Task readonly onTap={() => this.setContext(item)} task={item} />
            </ListItem>
        )
    }
    render() {

        return (
            <Layout>
                {this.state.dateShown && <DateTimePicker
                    onChange={(e, date) => {

                        if (e.type == 'set') {
                            this.setState({
                                ...this.state,
                                dateShown: false,
                                selectedDate: (moment(date).unix() * 1000).toString()
                            })
                            this.applyDate();
                        }
                    }}
                    value={moment().toDate()}
                    mode="date"
                    is24Hour={true}

                />}
                <List
                    ref={ref => this.listRef = ref}
                    /* ListHeaderComponent={this.sortList.bind(this)} */
                    ItemSeparatorComponent={Divider}
                    data={this.props.tasks}
                    renderItem={this.renderListItem}
                    keyExtractor={(item) => item.id}
                >
                </List>
            </Layout>

        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.filter(t => t.status < 1 && !t.DueDate)
})

const mapDispatchToProps = dispatch => {
    return {
        update: props => dispatch(TaskUpdateAction(props))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskDeadline)
