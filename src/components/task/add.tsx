import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Text, Input, Card, Button, Icon, OverflowMenu, MenuItem, Datepicker } from '@ui-kitten/components'
import { InputLabel } from '../forms/category'
import { DefaultText, TextMuted, TextMutedStrong, TextSmall } from '../../styles/text'
import { Row, Col } from 'react-native-easy-grid'
import { default as theme } from '../../styles/app-theme.json'
import { Dimensions, KeyboardAvoidingView, ScrollView, StatusBar, StyleSheet, TouchableNativeFeedback, TouchableOpacity, View } from 'react-native'
import { AddTask } from '../../actions/task.action'
import { ButtonText } from '../activity'
import { TaskItem } from '../../models/Tasks'
import { Icon as NBIcon, Toast } from 'native-base'
import { fontFamily, fontColors } from '../../styles/fonts'
import moment from 'moment'
import DateTimePicker from '@react-native-community/datetimepicker';
import { useKeyboard } from '../../common/useKeyboard'

import Modal from 'react-native-modal';
import { BORDER_RADIUS, CONTENT_START, DEFAULT_MARGIN, MENU_HEIGHT_COLLAPSED, MENU_HEIGHT_EXPANED } from '../../common/constants'
import { ClockIcon, ListIcon } from './details'
import { DateToDbUnix } from '../../models/api'
import { TaskListPicker } from '../tasklist/picker'
import { GoalPicker } from '../goals/picker'
const styles = StyleSheet.create({
    container: {
        minHeight: 144,
    },
    backdrop: {
        backgroundColor: 'rgba(0, 0, 0, 0.2)',
    },
})

export const DateSelectionCalendar = ({ onSelect }) => {

}
export const CalendarIcon = ({ fill = theme["color-primary-200"], width = 20, height = 20 }) => {
    return (

        <Icon name='calendar-outline' fill={fill} style={{ width, height }} />
    )
}

export const TodayIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="calendar-today" style={{ fontSize: 20, color: theme["color-primary-200"] }} />
    )
}
export const GoalIcon = ({ fontsize = 20, color = theme["color-primary-200"] }) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="bullseye-arrow" style={{ fontSize: fontsize, color: color }} />
    )
}
export const NoGoalIcon = ({ fontsize = 20, color = theme["color-primary-200"], ...props }) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="folder-alert-outline" style={{ fontSize: fontsize, color: color, ...props.style }} />
    )
}
export const TommorowIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="calendar-arrow-right" style={{ fontSize: 20, color: theme["color-primary-200"] }} />
    )
}
export const NextWeekIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="calendar-week-begin" style={{ fontSize: 20, color: theme["color-primary-200"] }} />
    )
}
export const SaveIcon = (props) => {
    return (
        <Icon name='chevron-right-outline' style={{ width: 24, height: 24 }} {...props} />
    )
}
export const SaveIcon2 = (props) => {
    return (
        <Icon name='arrow-circle-up' style={{ width: 24, height: 24 }} {...props} />
    )
}


export const ReminderSelectionDropdown = ({ onShown = null, onSelect, selectedText = null, ...props }) => {
    const [visible, setVisible] = React.useState(false);

    let time = moment().format('h:MM A');
    return (
        <TouchableOpacity onPress={() => onSelect(4)} >
            <View style={{ height: 36 }}>
                <Row size={12} style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Col size={2}>
                        {props.darkIcon && <ClockIcon fill={theme["color-font-400"]} width={20} height={20} ></ClockIcon>}
                        {!props.darkIcon && <ClockIcon fill={theme["color-primary-300"]} width={20} height={20} ></ClockIcon>}
                    </Col>
                    <Col size={10} style={{ justifyContent: 'center' }}>

                        {(!selectedText && !props.hideLabel) && <TextMuted text="Set a reminder" />}
                        {selectedText && <TextMutedStrong style={{ fontSize: 14, marginLeft: 4 }} text={selectedText} />}
                    </Col>
                </Row>
            </View>
        </TouchableOpacity>
    )
    /*   const toggleButton = () => (
  
          <TouchableOpacity onPress={() => setVisible(!visible)} >
              <View style={{ height: 36 }}>
                  <Row size={12} style={{ alignItems: 'center', justifyContent: 'center' }}>
                      <Col size={2}>
                          {props.darkIcon && <ClockIcon fill={theme["color-font-400"]} width={20} height={20} ></ClockIcon>}
                          {!props.darkIcon && <ClockIcon fill={theme["color-primary-300"]} width={20} height={20} ></ClockIcon>}
                      </Col>
                      <Col size={10} style={{ justifyContent: 'center' }}>
  
                          {(!selectedText && !props.hideLabel) && <TextMuted text="Set a reminder" />}
                          {selectedText && <TextMutedStrong style={{ fontSize: 14, marginLeft: 4 }} text={selectedText} />}
                      </Col>
                  </Row>
              </View>
          </TouchableOpacity>
  
      ) */
    /*   return (
          <View>
  
              <OverflowMenu
                  placement="top"
               
                  anchor={toggleButton}
                  visible={visible}
                  onSelect={(indexPath) => {
                      setVisible(false);
                      onSelect(indexPath.row)
                  }}
  
                  onBackdropPress={() => setVisible(false)}>
                  <MenuItem accessoryLeft={TodayIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="1 hour later"></TextSmall>} />
                  <MenuItem accessoryLeft={TommorowIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="2 hours"></TextSmall>} />
                  <MenuItem accessoryLeft={NextWeekIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="6 hours later"></TextSmall>} />
                  <MenuItem accessoryLeft={CalendarIcon} title={(props) => <TextSmall style={{ flex: 1, marginLeft: 4 }} text={'Tommorow (' + time + ')'}></TextSmall>} />
                  <MenuItem accessoryLeft={CalendarIcon} title={(props) => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="Pick a date and time"></TextSmall>} />
              </OverflowMenu>
          </View>
  
      ) */

}
export const DateSelectionDropdown = ({ onShown = null, onSelect, selectedText = null, ...props }) => {
    const [visible, setVisible] = React.useState(false);
    return (
        <TouchableOpacity onPress={() => onSelect(3)}>
            <View style={{ height: 36 }}>
                <Row size={12} style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Col size={2}>
                        {props.darkIcon && <CalendarIcon fill={theme["color-font-400"]}></CalendarIcon>}
                        {!props.darkIcon && <CalendarIcon></CalendarIcon>}
                    </Col>
                    <Col size={10} style={{ justifyContent: 'center' }}>
                        {(!selectedText && !props.hideLabel) && <TextMuted text="Set a due date" />}
                        {selectedText && <TextMutedStrong style={{ fontSize: 14, marginLeft: 4 }} text={selectedText} />}
                    </Col>
                </Row>
            </View>
        </TouchableOpacity>
    )
    const toggleVisibility = () => {


    }
    /*     const toggleButton = () => (
    
            <TouchableOpacity onPress={() => setVisible(!visible)}>
                <View style={{ height: 36 }}>
                    <Row size={12} style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Col size={2}>
                            {props.darkIcon && <CalendarIcon fill={theme["color-font-400"]}></CalendarIcon>}
                            {!props.darkIcon && <CalendarIcon></CalendarIcon>}
                        </Col>
                        <Col size={10} style={{ justifyContent: 'center' }}>
                            {(!selectedText && !props.hideLabel) && <TextMuted text="Set a due date" />}
                            {selectedText && <TextMutedStrong style={{ fontSize: 14, marginLeft: 4 }} text={selectedText} />}
                        </Col>
                    </Row>
                </View>
            </TouchableOpacity>
    
        ) */
    /*  return <toggleButton></toggleButton>; */
    /* return (

        <Layout level='1' >
            <OverflowMenu
                placement="top"
                anchor={toggleButton}
                visible={visible}
                onSelect={(indexPath) => {
                    setVisible(false);
                    onSelect(indexPath.row)
                }}

                onBackdropPress={() => setVisible(false)}>
                <MenuItem accessoryLeft={TodayIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="Today"></TextSmall>} />
                <MenuItem accessoryLeft={TommorowIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="Tommorow"></TextSmall>} />
                <MenuItem accessoryLeft={NextWeekIcon} title={() => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="Next Week"></TextSmall>} />
                <MenuItem accessoryLeft={CalendarIcon} title={(props) => <TextSmall style={{ flex: 1, marginLeft: 4 }} text="Pick a date"></TextSmall>} />
            </OverflowMenu>
        </Layout >

    ) */

}
const _AddTaskModal = ({ visible, onToggle, add, ...props }) => {
    const [name, setName] = React.useState(null);
    const [dueDate, setDueDate] = React.useState(null);
    const [selectedDate, setSelectedDate] = React.useState(null);
    const [showDatePicker, setShowDatePicker] = React.useState(false);
    const [reminder, setReminder] = React.useState(null);
    const [showReminderPicker, setShowReminderPicker] = React.useState(false);
    const [selectedReminder, setSelectedReminder] = React.useState(null);
    const [showReminderTime, setShowReminderTime] = React.useState(false);
    const [selectedList, setSelectedList] = React.useState(null);
    const [listDisplay, setListDisplay] = React.useState(null);
    const [goalDisplay, setGoalDisplay] = React.useState(null);
    const [selectedGoal, setSelectedGoal] = React.useState(null);
    if (props.goal) {
        try {
            let index = props.goals.findIndex(goal => goal.id == props.goal);
            if (index > -1) {
                setGoalDisplay(props.goals[index].name);
                setSelectedGoal({ id: props.goals[index].id, name: props.goals[index].name });
            }
        } catch (error) {

        }
    }
    const addTask = (hide) => {
        /*    if (hide) {
               if (onToggle && typeof onToggle === 'function') {
                   onToggle();
               }
           } */
        let task: any = {
            name: name,
            DueDate: dueDate, reminder, list: selectedList, list_id: selectedList?.id, goal: selectedGoal, goal_id: selectedGoal?.id
        }; 21
        if (props.parent) {
            task.parent_id = props.parent;
        }
        add(task);

        if (props.onAdd && props.onAdd === 'function') {
            props.onAdd(task);
        }
        Toast.show({
            text: 'Task added',
            duration: 1500,
            type: 'success',
            position: 'center',
            style: {
                elevation: 210,
                zIndex: 999
            },


        })
        reset();

        onToggle();
    }
    const reset = () => {
        setDueDate(null);
        setName(null);
        setReminder(null);
        setSelectedReminder(null);
        setSelectedDate(null);
        setListDisplay(null);
        setSelectedList(null);
        setGoalDisplay(null);
        setSelectedGoal(null);
      
    }
    const dueDateSelected = (index: number) => {
        switch (index) {
            case 0:
                setSelectedDate(moment(new Date()).format('MMM D'));
                let dt = moment(new Date()).unix() * 1000;
                setDueDate(dt.toString());
                break;
            case 1:
                setSelectedDate(moment().add(1, 'days').format('MMM D'));

                setDueDate((moment(new Date()).add(1, 'days').unix() * 1000).toString());
                break;
            case 2:
                setSelectedDate(moment().add(1, 'week').format('ddd, MMM D'));
                setDueDate((moment(new Date()).add(1, 'week').unix() * 1000).toString());
                break;
            case 3:
                setShowDatePicker(true);
                break;
            default:
                break;
        }
    }
    let statusBarHeight = StatusBar.currentHeight;
    let width = Dimensions.get('screen').width;
    let ref;
    const reminderSelected = (index) => {
        let date;
        switch (index) {
            case 0:
                //@ts-ignore
                date = moment().add(1, 'hours')
                setReminder(date);
                setSelectedReminder(moment(date).format('D MMM @ h:mm A'));
                break;
            case 1:
                //@ts-ignore
                date = moment().add(2, 'hours')
                setReminder(date);
                setSelectedReminder(moment(date).format('D MMM @ h:mm A'));
                break;
            case 2:
                //@ts-ignore
                date = moment().add(6, 'hours')
                setReminder(date);
                setSelectedReminder(moment(date).format('D MMM @ h:mm A'));
                break;
            case 3:
                //@ts-ignore
                date = moment().add(1, 'day')
                setReminder(date);
                setSelectedReminder(moment(date).format('D MMM @ h:mm A'));
                break;
            case 4:
                setShowReminderPicker(true);
                break;

        }

    }
    return (
        <Modal coverScreen={false} onBackdropPress={onToggle} style={{ width: width, left: -DEFAULT_MARGIN - 4, position: 'absolute', right: 0, bottom: 0 - statusBarHeight }} hasBackdrop={true} propagateSwipe onSwipeComplete={onToggle} swipeDirection="down" animationIn="slideInUp" animationOut="slideOutDown" hideModalContentWhileAnimating={true} useNativeDriver={true} isVisible={visible}   >

            <KeyboardAvoidingView behavior="padding" style={{ width: '100%' }}>

                <View style={{ borderRadius: BORDER_RADIUS, padding: CONTENT_START, flexDirection: 'column', height: 148, backgroundColor: '#fff', width: '100%', elevation: 1 }}>
                    <View style={{ height: 56 }}>
                        <Input blurOnSubmit={false} ref={r => ref = r} onSubmitEditing={() => addTask(true)} value={name} accessoryRight={() => <Button style={{ maxHeight: 16, maxWidth: 22 }} accessoryRight={SaveIcon2} onPress={addTask} status="primary" appearance="ghost" />} onChangeText={t => setName(t)} autoFocus={true} style={{ flex: 1 }} label={() => <InputLabel label="Task Name"></InputLabel>}></Input>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode="on-drag" horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>

                                <View style={{ height: 24, marginRight: 16, marginTop: 8 }}>
                                    <DateSelectionDropdown darkIcon hideLabel selectedText={selectedDate} onSelect={dueDateSelected}></DateSelectionDropdown>

                                    {showDatePicker && <DateTimePicker
                                        value={(dueDate && moment.unix(+dueDate / 1000).toDate()) || new Date()}
                                        mode="date"
                                        minimumDate={new Date()}
                                        is24Hour={true}
                                        display="default"
                                        onChange={(e, date) => {
                                            setShowDatePicker(false);
                                            if (e.type == 'set') {

                                                setTimeout(() => {
                                                    setDueDate((moment(date).unix() * 1000).toString());
                                                    setSelectedDate(moment(date).format('ddd, MMM D'));
                                                }, 150);
                                            }

                                        }}
                                    />}
                                </View>
                                <View style={{ height: 24, marginRight: 16, marginTop: 8, }}>
                                    <ReminderSelectionDropdown darkIcon hideLabel selectedText={selectedReminder} onSelect={reminderSelected}></ReminderSelectionDropdown>
                                    {showReminderPicker && <DateTimePicker
                                        value={new Date()}
                                        mode="date"
                                        minimumDate={new Date()}
                                        display="default"
                                        onChange={(e, date) => {
                                            setShowReminderPicker(false);
                                            if (e.type == 'set') {

                                                setReminder(moment(date));
                                                setTimeout(() => {
                                                    setShowReminderTime(true);
                                                }, 150)
                                                setSelectedReminder(moment(date).format('D MMM @ h:mm A'));
                                            }

                                        }}
                                    />}
                                    {showReminderTime && <DateTimePicker
                                        value={new Date()}
                                        mode="time"
                                        minimumDate={new Date()}
                                        display="clock"
                                        onChange={(e, date) => {
                                            setShowReminderPicker(false);
                                            setShowReminderTime(false);
                                            if (e.type == 'set') {
                                                setTimeout(() => {
                                                    let selectedTime = moment(date);
                                                    let setTime = moment(reminder).set('hour', selectedTime.hour()).set('minute', selectedTime.minute());
                                                    setReminder((moment(setTime)));
                                                    setSelectedReminder(moment(setTime).format('D MMM @ h:mm A'));
                                                }, 200);
                                            }

                                        }}
                                    />}

                                </View>
                                <View style={{ height: 24, marginRight: 16, marginTop: 24 }}>
                                    <TaskListPicker onSelected={(selected) => {
                                        setListDisplay(selected.name);
                                        setSelectedList(selected);


                                    }} selectView={({ showModal }) => {
                                        return (
                                            <TouchableOpacity onPress={showModal}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <ListIcon fill={theme["color-font-400"]} width={20} height={20}></ListIcon>
                                                    <TextMuted style={{ marginLeft: DEFAULT_MARGIN / 4 }} text={listDisplay} />
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    }} />


                                </View>
                                <View style={{ height: 24, marginRight: 16, marginTop: 24 }}>
                                    <GoalPicker manual onSelected={(selected) => {
                                        setGoalDisplay(selected.name);
                                        setSelectedGoal(selected);


                                    }} selectView={({ showModal }) => {
                                        return (
                                            <TouchableOpacity onPress={showModal}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <GoalIcon color={theme["color-font-400"]}></GoalIcon>
                                                    <TextMuted style={{ marginLeft: DEFAULT_MARGIN / 4 }} text={goalDisplay} />
                                                </View>
                                            </TouchableOpacity>
                                        )
                                    }} />

                                </View>

                            </View>
                        </ScrollView>
                    </View>

                </View>

            </KeyboardAvoidingView >


        </Modal >
    )
}


const mapStateToProps = (state) => ({
    goals: state.goals.goals
})
const mapDispatchToProps = dispatch => {
    return {
        add: item => dispatch(AddTask(item))
    }
}

export const AddTaskModal = connect(mapStateToProps, mapDispatchToProps)(_AddTaskModal)
