import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, ListItem, Icon, OverflowMenu, MenuItem, Divider, Button, Input, Card } from '@ui-kitten/components'
import { Row, Col, Grid } from 'react-native-easy-grid';
import { H2, TextMuted, DefaultText, TextSmall, H4 } from '../../styles/text';
import { UppercaseHeading } from '../entry/past';
import InputSpinner from "react-native-input-spinner";
import Modal from 'react-native-modal';
import { GetTask, DeleteTask, ChangeTaskDueDate, RemoveMyDay, AddMyDay, SetTaskEstimatedTime, AddTaskNotes, DbUnixToDate, ChangeTaskName } from '../../models/api';
import { BackButton, BackNav } from '../../menu/header';
import { Toast, View } from 'native-base';
import moment from 'moment'
import { CalendarIcon, TommorowIcon, TodayIcon, NextWeekIcon, AddTaskModal, GoalIcon, ReminderSelectionDropdown } from './add';
import { default as theme } from '../../styles/app-theme.json'
import { fontFamily } from '../../styles/fonts';
import { TouchableOpacity, TouchableNativeFeedback, ScrollView, StyleSheet } from 'react-native';
import { UpdateTaskListAsync, TaskCompletedAsync, TaskIncompleteAsync, DeleteTaskAction, TaskUpdateAction, AttachReminder } from '../../actions/task.action';
import { PlusOutlineIcon, MinusOutlineIcon, ButtonText, AttachClipIcon, ImageIcon, EditIcon } from '../activity';
import { CloseIcon, CustomTaskList } from './list';
import CheckBox from '@react-native-community/checkbox';
import { Task, BulbIcon, isToday } from './task';
import { GoalPicker } from '../goals/picker';

import { CenterLoading } from '../activity.page';
import { TaskListPicker, ColorIndicator } from '../tasklist/picker';
import UIStepper from 'react-native-ui-stepper';
import DateTimePicker from '@react-native-community/datetimepicker';
import { InteractionManager } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { DEFAULT_MARGIN, MENU_HEIGHT_COLLAPSED } from '../../common/constants';
import TaskFacts, { ExtractTaskFactsSync } from './facts.group';
import { InputLabel } from '../forms/category';

const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {

        margin: 2,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
});
export const ClockIcon = ({ width = 24, height = 24, fill = theme["color-font-300"], ...props }) => {
    return (
        <Icon name='clock-outline' fill={fill} style={{ width, height, ...props.style }} />
    )
}
export const RepeatIcon = () => {
    return (
        <Icon name='repeat-outline' fill={theme["color-font-300"]} style={{ width: 24, height: 24 }} />
    )
}
export const NotesIcon = () => {
    return (
        <Icon name='file-text-outline' fill={theme["color-font-300"]} style={{ width: 24, height: 24 }} />
    )
}

export const TrashIconProps = (props) => {
    return <Icon name='trash-outline' {...props} />
}
export const TrashIcon = ({ color = theme["color-font-300"], width = 24, height = 24 }) => {
    return <Icon name='trash-outline' fill={color} style={{ width: width, height: height }} />
}
export const ListIcon = ({ fill = theme["color-font-300"], width = 24, height = 24 }) => {
    return (
        <Icon name='list-outline' fill={fill} style={{ width: width, height: height }} />
    )
}
export const ToggleOverflowIcon = () => {
    return (

        <Icon name='arrow-down' fill={theme["color-font-300"]} style={{ width: 24, height: 24 }} />
    )
}
export const DueDateDropdown = ({ onSelect, onBackdropPress, dueDate, toggle = false }) => {

    const [visible, setVisible] = React.useState(toggle);
    const DueDate = () => {
        if (dueDate) {
            return (

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TextMuted text={moment.unix(+dueDate / 1000).format('MMMM Do YYYY')}></TextMuted>
                    <TouchableOpacity onPress={() => setVisible(!visible)}>
                        <ToggleOverflowIcon></ToggleOverflowIcon>
                    </TouchableOpacity>
                </View>


            )
        } else {
            return (<TouchableOpacity onPress={() => setVisible(!visible)}>
                <TextMuted text="Not Set"></TextMuted>
            </TouchableOpacity>)
        }

    }
    return (
        <Layout level='1' >
            <OverflowMenu
                backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.4)', }}
                anchor={DueDate}
                visible={visible}
                onSelect={(indexPath) => {
                    setVisible(false);
                    onSelect(indexPath.row)
                }}

                onBackdropPress={() => setVisible(false)}>
                <MenuItem accessoryLeft={TodayIcon} title={() => <DefaultText style={{ flex: 1, marginLeft: 8, fontFamily: fontFamily.semibold.fontFamily }} text="Today"></DefaultText>} />
                <MenuItem accessoryLeft={TommorowIcon} title={() => <DefaultText style={{ flex: 1, marginLeft: 8, fontFamily: fontFamily.semibold.fontFamily }} text="Tommorow"></DefaultText>} />
                <MenuItem accessoryLeft={NextWeekIcon} title={() => <DefaultText style={{ flex: 1, marginLeft: 8, fontFamily: fontFamily.semibold.fontFamily }} text="Next Week"></DefaultText>} />
                <MenuItem accessoryLeft={CalendarIcon} title={(props) => <DefaultText style={{ flex: 1, marginLeft: 8, fontFamily: fontFamily.semibold.fontFamily }} text="Pick a date"></DefaultText>} />
            </OverflowMenu>
        </Layout>
    )
}
export const GoalSelectView = ({ showModal, selected }) => {
    let isSelected = false;
    try {
        if (selected && selected.Id) {
            isSelected = true;
        }
    } catch (error) {
        isSelected = false;
    }
    return (
        <ListItem onPress={() => showModal()}>
            <View style={{ padding: 8, flexDirection: 'row' }}>
                <Row size={12}>
                    <Col>
                        <Row size={12} style={{ alignItems: 'center' }}>
                            <Col size={2}>
                                <GoalIcon fontsize={24} color={theme["color-font-300"]}></GoalIcon>

                            </Col>
                            <Col size={10} >
                                <TextMuted text="Goal"></TextMuted>
                            </Col>
                        </Row>
                    </Col>
                    <Col style={{ alignItems: 'flex-end' }}>
                        {isSelected && <TextMuted text={selected.Name}></TextMuted>}
                    </Col>
                </Row>
            </View>
        </ListItem >
    )
}
class _TaskDetails extends Component<any, any>{

    ChangeDueDate(date) {

    }
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            task: {},
            visible: false,
            DueDateVisible: false,
            estimatedTime: 0,
            selectedReminder: null,
            reminder: null,
            ready: false
        };

    }
    componentDidMount() {
        this.load();

    }
    nameModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { nameVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>
                <Button
                    onPress={() => {
                        this.changeName();
                        this.setState(() => { return { nameVisible: false }; })

                    }}
                    style={styles.footerControl}
                    size='small'
                    status='primary'>
                    SAVE
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Edit Name"></H4>
                <Input
                    placeholder="Task name"
                    autoFocus={true}
                    accessoryRight={() => {
                        return (
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-font-300"], true)} onPress={() => {
                                this.setState(() => { return { _name: '' } })

                            }}>
                                <View>
                                    <CloseIcon fill={theme["color-font-200"]} width={20} height={20}></CloseIcon>
                                </View>
                            </TouchableNativeFeedback>
                        )
                    }}
                    value={this.state._name}
                    textStyle={iOSUIKit.body}
                    onChangeText={(text) => {
                        this.setState(() => { return { _name: text }; })

                    }}
                    label={() => <InputLabel label="Task Name"></InputLabel>}
                ></Input>
            </Card>
        )
    }
    toggleMyday = (task) => {
        if (task.myDay && isToday(task.myDay)) {

            this.props.update(task.id, 'myDay', null);
            this.setState({

                myDay: false
            })
            RemoveMyDay(task.id);

        } else {
            this.props.update(task.id, 'myDay', moment().unix() * 1000);
            this.setState({
                myDay: true
            })
            AddMyDay(task.id);
        }

    }
    changeDueDate = date => {
        InteractionManager.runAfterInteractions(() => {
            let dueDateParsed = (moment(date).unix() * 1000).toString();
            this.props.update(this.props.task.id, 'DueDate', dueDateParsed);
            ChangeTaskDueDate(this.props.task.id, dueDateParsed);
        })
    }
    load() {
        InteractionManager.runAfterInteractions(() => {
            let isMyDay = isToday(this.props.task.myDay);
            let facts = ExtractTaskFactsSync(this.props.task);
            this.setState({ DueDateVisible: false, myDay: isMyDay, ready: true, estimatedTime: this.props.task.estimatedTime, notes: this.props.task.notes, facts });
        });

    }
    componentDidUpdate() {
        let oldFacts = JSON.stringify(this.state.facts);
        let newFacts = JSON.stringify(ExtractTaskFactsSync(this.props.task));
        if (oldFacts !== newFacts) {
            this.setState(() => {
                return {
                    facts: ExtractTaskFactsSync(this.props.task)
                }
            })

        }
    }
    onSelect(index) {
        switch (index) {
            case 0:
                this.changeDueDate(moment());
                break;
            case 1:
                this.changeDueDate(moment().add(1, 'days'));
                break;
            case 2:
                this.changeDueDate(moment().add(1, 'week'));
                break;
            case 3:
                this.setState(() => { return { dueDateVisible: true } })
                break;
            default:
                break;
        }
    }
    estimatedTimeChange = async (value) => {
        this.setState((state, props) => {
            return {
                estimatedTime: value
            }
        })

    }
    Confirm() {
        this.setState((state, props) => {
            return {
                deleteVisible: true
            }
        })

    }
    DeleteTask() {
        this.setState({
            ready: false
        })
        DeleteTask(this.props.task.id)
        this.props.deleteTask(this.props.task.id);

    }
    attachReminder = () => {
        if (this.props.attachReminder && typeof this.props.attachReminder === 'function') {

            if (this.state.reminder) {
                this.props.attachReminder(this.props.task.id, this.state.reminder);
            }
        }
    }
    reminderSelected = (index) => {
        let date;
        switch (index) {
            case 0:
                //@ts-ignore
                date = moment().add(1, 'hours');
                this.setState((state, props) => {

                    return {
                        reminder: date,
                        selectedReminder: moment(date).format('D MMM @ h:mm A')
                    }
                }, () => {
                    this.attachReminder();
                })



                break;
            case 1:
                //@ts-ignore
                date = moment().add(2, 'hours')
                this.setState(() => {
                    return {
                        reminder: date,
                        selectedReminder: moment(date).format('D MMM @ h:mm A')
                    }
                }, () => {
                    this.attachReminder();
                })
                break;
            case 2:
                //@ts-ignore
                date = moment().add(6, 'hours')
                this.setState(() => {
                    return {
                        reminder: date,
                        selectedReminder: moment(date).format('D MMM @ h:mm A')
                    }
                }, () => {
                    this.attachReminder();
                })
                break;
            case 3:
                //@ts-ignore
                date = moment().add(1, 'day')
                this.setState(() => {
                    return {
                        reminder: date,
                        selectedReminder: moment(date).format('D MMM @ h:mm A')
                    }
                }, () => {
                    this.attachReminder();
                })
                break;
            case 4:
                this.setState(() => {
                    return {
                        showReminderPicker: true,
                    }
                })

                break;

        }
    }
    saveNotes = () => {
        if (this.state.estimatedTime) {
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update(this.props.task.id, 'estimatedTime', this.state.estimatedTime);
            }
            SetTaskEstimatedTime(this.props.task.id, this.state.estimatedTime);
        }
        if (this.state.save_required) {
            AddTaskNotes(this.props.task.id, this.state.notes)
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update(this.props.task.id, 'notes', this.state.notes);
            }

        }
        this.props.navigation.pop();
    }
    updateText = text => {
        this.setState(() => {
            return {
                notes: text,
                save_required: true
            }
        })

    }
    onToggle = () => {
        this.setState(() => { return { visible: !this.state.visible } })
    }
    deleteModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { deleteVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>
                <Button
                    onPress={() => {
                        this.DeleteTask();
                        Toast.show({
                            text: 'Task deleted',
                            textStyle: iOSUIKit.subheadShortWhite,
                            duration: 2000,
                            type: 'success'
                        })
                        this.setState(() => { return { deleteVisible: false }; })
                        setTimeout(() => {
                            if (this.props.navigation) {
                                this.props.navigation.pop();
                            }
                        })
                    }}
                    style={styles.footerControl}
                    size='small'
                    status='danger'>
                    DELETE
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Confirm Delete"></H4>
                <DefaultText text="Are you sure you want to delete this task? This action cannot be undone"></DefaultText>
            </Card>
        )
    }
    changeName = () => {
        InteractionManager.runAfterInteractions(() => {
            this.props.update(this.props.task.id, 'name', this.state._name);
            ChangeTaskName(this.props.task.id, this.state._name).then((result) => {
                if (result) {
                    Toast.show({
                        type: 'success',
                        text: 'Task Updated',
                        duration: 1000
                    })
                } else {
                    Toast.show({
                        type: 'danger',
                        text: 'Something went wrong',
                        duration: 1000
                    })
                }
            }).catch((error) => {
                Toast.show({
                    type: 'danger',
                    text: 'Something went wrong',
                    duration: 1000
                })
            }).finally(() => {
                this.setState(() => { return { _name: this.props.task.name } })

            })
        })
    }
    render() {
        let dueDate;
        if (this.props.task.DueDate) {
            dueDate = DbUnixToDate(this.props.task.DueDate).toDate();
        } else dueDate = new Date();
        if (!this.state || !this.state.task || !this.state.ready) {
            return <CenterLoading></CenterLoading>;
        }
        return (
            <Layout style={{ flex: 1 }} level="3">
                {this.state.dueDateVisible && <DateTimePicker
                    value={dueDate}
                    mode="date"
                    minimumDate={new Date()}
                    is24Hour={true}
                    display="default"
                    onChange={(e, date) => {
                        this.setState(() => {
                            return {
                                dueDateVisible: false
                            }
                        })
                        this.changeDueDate(date);

                    }}
                />}
                <Modal isVisible={this.state.nameVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.nameModal></this.nameModal>
                </Modal>
                <Modal isVisible={this.state.deleteVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.deleteModal></this.deleteModal>
                </Modal>
                <AddTaskModal onToggle={this.onToggle} parent={this.props.task.id} visible={this.state.visible}></AddTaskModal>

                <ScrollView style={{ flex: 1 }} >
                    <View style={{ flex: 1 }}>

                        <View style={{ height: MENU_HEIGHT_COLLAPSED * 2, marginBottom: 16, padding: 16, backgroundColor: '#fff', }}>

                            <Row size={12} style={{ alignItems: 'center' }} >
                                <Col size={1}>
                                    <CheckBox
                                        style={{ left: -8, top: 3 }}
                                        tintColors={{ true: theme["color-font-200"], false: theme["color-primary-300"] }}
                                        onChange={() => this.props.task.status ? this.props.incomplete(this.props.task.id) : this.props.completed(this.props.task.id)}
                                        value={this.props.task.status === 1} />
                                </Col>
                                <Col size={11}>
                                    <Row style={{ alignItems: 'center' }} size={12}>
                                        <Col size={11}>
                                            <H2 text={this.props.task.name} />

                                        </Col>
                                        <Col size={1}>
                                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)} onPress={() => {

                                                this.setState(() => { return { nameVisible: true, _name: this.props.task.name } })

                                            }}>
                                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>

                                                    <EditIcon width={24} height={24}></EditIcon>
                                                </View>
                                            </TouchableNativeFeedback>

                                        </Col>
                                    </Row>

                                </Col>
                            </Row>
                            {/*     <Row>
                                <Col>
                                    <TaskFacts facts={this.state.facts}></TaskFacts>
                                </Col>
                            </Row> */}
                        </View>

                        <View style={{ flex: 1, paddingBottom: 0 }}>
                            <View style={{ padding: 8, paddingTop: 0 }}>
                                <UppercaseHeading heading="Sub Tasks"></UppercaseHeading>
                            </View>

                            {this.props.subtasks.map((task) => {
                                return (<ListItem key={task.id}>
                                    <Task task={task}></Task>
                                </ListItem>)
                            })}
                            <ListItem onPress={() => this.setState({ visible: true })}>
                                <View style={{ padding: 8, flexDirection: 'row' }}>
                                    <Row size={12}>
                                        <Col>
                                            <Row size={12} style={{ alignItems: 'center' }}>
                                                <Col size={2}>
                                                    <PlusOutlineIcon></PlusOutlineIcon>

                                                </Col>
                                                <Col size={10} >
                                                    <TextMuted text="Add sub tasks" style={{ color: theme["color-primary-500"] }}></TextMuted>
                                                </Col>
                                            </Row>

                                        </Col>
                                        <Col style={{ alignItems: 'flex-end' }}>
                                            <TextMuted text=""></TextMuted>
                                        </Col>
                                    </Row>

                                </View>
                            </ListItem>

                        </View>
                        <View style={{ padding: 8 }}>
                            <UppercaseHeading heading="Settings"></UppercaseHeading>
                        </View>
                        <ListItem disabled={true}>
                            <View style={{ padding: 8, flexDirection: 'row' }}>
                                <Row size={12}>
                                    <Col>
                                        <Row size={12} style={{ alignItems: 'center' }}>
                                            <Col size={2}>
                                                <CalendarIcon fill={theme["color-font-300"]} width={24} height={24}></CalendarIcon>

                                            </Col>
                                            <Col size={10} >

                                                <TextMuted text="Due Date"></TextMuted>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col style={{ alignItems: 'flex-end' }}>
                                        <DueDateDropdown dueDate={this.props.task.DueDate} onBackdropPress={() => this.setState({ DueDateVisible: false })} onSelect={(index) => this.onSelect(index)} ></DueDateDropdown>
                                    </Col>
                                </Row>
                            </View>
                        </ListItem>
                        <ListItem disabled={true}>

                            <Grid style={{ padding: 8, flexDirection: 'row' }}>
                                <Row size={12}>
                                    <Col size={6}>
                                        <Row size={12} style={{ alignItems: 'center' }}>
                                            <Col size={2}>
                                                <CalendarIcon fill={theme["color-font-300"]} width={24} height={24}></CalendarIcon>

                                            </Col>
                                            <Col size={10} >

                                                <TextMuted text="Reminder"></TextMuted>

                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col size={6} >
                                        <ReminderSelectionDropdown selectedText={this.state.selectedReminder} onSelect={this.reminderSelected}></ReminderSelectionDropdown>
                                        {this.state.showReminderPicker && <DateTimePicker
                                            value={new Date()}
                                            mode="date"
                                            minimumDate={new Date()}
                                            display="default"
                                            onChange={(e, date) => {
                                                /*    setShowReminderPicker(false);
                                                   setReminder(moment(date)); */
                                                this.setState(() => {
                                                    return {
                                                        showReminderPicker: false,
                                                        reminder: moment(date)
                                                    }
                                                })

                                                setTimeout(() => {
                                                    this.setState(() => {
                                                        return {
                                                            showReminderTime: true,
                                                        }
                                                    })

                                                }, 150)
                                                /* setSelectedReminder(); */
                                                this.setState((state, props) => {
                                                    return {
                                                        selectedReminder: moment(date).format('D MMM @ h:mm A')
                                                    }
                                                })

                                            }}
                                        />}
                                        {this.state.showReminderTime && <DateTimePicker
                                            value={new Date()}
                                            mode="time"
                                            minimumDate={new Date()}
                                            display="clock"
                                            onChange={(e, date) => {
                                                let selectedTime = moment(date);
                                                let setTime = moment(this.state.reminder).set('hour', selectedTime.hour()).set('minute', selectedTime.minute());
                                                this.setState((state, props) => {
                                                    return {
                                                        showReminderPicker: false,
                                                        showReminderTime: false,
                                                        reminder: moment(setTime),
                                                        selectedReminder: moment(setTime).format('D MMM @ h:mm A')
                                                    }
                                                })
                                                this.attachReminder();
                                            }}
                                        ></DateTimePicker>}
                                    </Col>
                                </Row>
                            </Grid>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.navigate('RepeatModal', { id: this.props.task.id })}>
                            <View style={{ padding: 8, flexDirection: 'row' }}>
                                <Row size={12}>
                                    <Col>
                                        <Row size={12} style={{ alignItems: 'center' }}>
                                            <Col size={2}>
                                                <RepeatIcon></RepeatIcon>

                                            </Col>
                                            <Col size={10} >
                                                <TextMuted text="Repeat"></TextMuted>
                                            </Col>
                                        </Row>

                                    </Col>
                                    <Col style={{ alignItems: 'flex-end' }}>
                                        <TextMuted text="No Repeat"></TextMuted>
                                    </Col>
                                </Row>

                            </View>

                        </ListItem>
                        <ListItem>
                            <View style={{ padding: 8, flexDirection: 'row' }}>
                                <Row size={12}>
                                    <Col>
                                        <Row size={12} style={{ alignItems: 'center' }}>
                                            <Col size={2}>
                                                <ClockIcon></ClockIcon>


                                            </Col>
                                            <Col size={10} >
                                                {!this.state.estimatedTime && <TextMuted text="Estimated Time"></TextMuted>}
                                                {(this.state.estimatedTime > 0) && <TextMuted text={this.state.estimatedTime + ' minutes'}></TextMuted>}
                                                {this.state.estimatedTime > 0 && <TextSmall text="Estimated Time"></TextSmall>}
                                            </Col>
                                        </Row>

                                    </Col>
                                    <Col style={{ alignItems: 'flex-end' }}>
                                        <InputSpinner
                                            width={128}
                                            height={42}
                                            inputStyle={{ fontFamily: iOSUIKit.body.fontFamily, fontSize: 16, color: theme["color-font-300"] }}
                                            max={360}
                                            min={0}
                                            color={theme["color-font-300"]}
                                            initialValue={this.state.estimatedTime}
                                            step={5}
                                            buttonStyle={{ width: 36, height: 36 }}
                                            colorMax={theme["color-font-500"]}
                                            colorMin={theme["color-font-200"]}
                                            value={this.state.number}
                                            onChange={(num) => {
                                                this.estimatedTimeChange(num)
                                            }}
                                        />
                                        {/*   <UIStepper
                                            width={72}
                                            height={28}
                                            borderRadius={9}
                                            tintColor={theme["color-primary-400"]}
                                            borderColor={theme["color-primary-400"]}
                                            maximumValue={360}
                                            minimumValue={0}
                                            steps={5}
                                            initialValue={this.state.estimatedTime}
                                            onValueChange={this.estimatedTimeChange}
                                        /> */}
                                    </Col>
                                </Row>

                            </View>

                        </ListItem>
                        <TaskListPicker onSelected={() => this.load()} task={this.props.task} selectView={({ showModal }) => {
                            return (<ListItem onPress={showModal}>
                                <View style={{ padding: 8, flexDirection: 'row' }}>
                                    <Row size={12}>
                                        <Col>
                                            <Row size={12} style={{ alignItems: 'center' }}>
                                                <Col size={2}>
                                                    <ListIcon></ListIcon>

                                                </Col>
                                                <Col size={10} >
                                                    <TextMuted text="List"></TextMuted>
                                                </Col>
                                            </Row>

                                        </Col>
                                        <Col style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                            {
                                                (() => {
                                                    if (this.props.task.list && this.props.task.list.name) {
                                                        return (

                                                            <View style={{ flexDirection: 'row' }}>
                                                                <View style={{ width: 12, marginRight: 8, justifyContent: 'center', alignItems: 'center' }}>
                                                                    <ColorIndicator color={this.props.task.list.color}></ColorIndicator>
                                                                </View>
                                                                <TextMuted text={this.props.task.list.name}></TextMuted>

                                                            </View>
                                                        )
                                                    } else {
                                                        return (
                                                            <TextMuted text="No list"></TextMuted>
                                                        )
                                                    }
                                                })()
                                            }
                                        </Col>
                                    </Row>

                                </View>
                            </ListItem>)
                        }}></TaskListPicker>

                        <GoalPicker task={this.props.task} selectView={({ disabled, selected, showModal }) => <GoalSelectView selected={this.props.selectedGoal} showModal={showModal}></GoalSelectView>} selectedItem={this.props.selectedGoal}></GoalPicker>
                        <ListItem onPress={() => this.toggleMyday(this.props.task)}>

                            <Row size={12} style={{ padding: 8 }}>
                                <Col size={11}>
                                    <Row size={12} >
                                        <Col size={1}>
                                            {!this.state.myDay && <PlusOutlineIcon></PlusOutlineIcon>}
                                            {this.state.myDay && <MinusOutlineIcon style={{ width: 24, height: 24 }}></MinusOutlineIcon>}


                                        </Col>
                                        <Col size={10} >
                                            {!this.state.myDay && <TextMuted text="Add to my day" ></TextMuted>}
                                            {this.state.myDay && <TextMuted text="Remove from my day" ></TextMuted>}
                                        </Col>
                                    </Row>

                                </Col>
                                <Col style={{ alignItems: 'flex-end' }} size={1}>
                                    {this.state.myDay && <BulbIcon fill={theme["color-primary-500"]} style={{ width: 20, height: 20 }}></BulbIcon>}
                                </Col>
                            </Row>
                        </ListItem>

                    </View>
                    <Divider></Divider>
                    <View style={{ padding: 8 }}>
                        <UppercaseHeading heading="Task Notes"></UppercaseHeading>
                        <Input
                            multiline={true}
                            numberOfLines={10}
                            textStyle={[iOSUIKit.body, { textAlignVertical: 'top', paddingTop: 8 }]}
                            style={{ textAlignVertical: 'top' }}
                            onChangeText={(text) => {
                                this.updateText(text);
                            }}
                            value={this.state.notes}
                            placeholder='Write something...' />

                    </View>
                    <View>
                        <View style={{ padding: 8 }}>
                            <UppercaseHeading heading="Attachments"></UppercaseHeading>
                        </View>
                        <ListItem>
                            <Row size={12} style={{ padding: 8 }}>
                                <Col size={11}>
                                    <Row size={12} >
                                        <Col size={1}>
                                            <ImageIcon></ImageIcon>

                                        </Col>
                                        <Col size={10} >
                                            <TextMuted text="Attach Image" ></TextMuted>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{ alignItems: 'flex-end' }} size={1}>
                                    {this.state.myDay && <BulbIcon fill={theme["color-primary-500"]} style={{ width: 20, height: 20 }}></BulbIcon>}
                                </Col>
                            </Row>
                        </ListItem>
                        <ListItem>
                            <Row size={12} style={{ padding: 8 }}>
                                <Col size={11}>
                                    <Row size={12} >
                                        <Col size={1}>
                                            <AttachClipIcon></AttachClipIcon>

                                        </Col>
                                        <Col size={10} >
                                            <TextMuted text="Attach Files" ></TextMuted>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col style={{ alignItems: 'flex-end' }} size={1}>
                                    {this.state.myDay && <BulbIcon fill={theme["color-primary-500"]} style={{ width: 20, height: 20 }}></BulbIcon>}
                                </Col>
                            </Row>
                        </ListItem>

                    </View>
                    <View style={{ padding: 8 }}>
                        <Button onPress={this.saveNotes} style={{ marginTop: DEFAULT_MARGIN }} children={props => <ButtonText text="Save" {...props}></ButtonText>} />
                    </View>
                    <View style={{ width: '100%', padding: 8, marginTop: 16, alignItems: 'center', flexDirection: 'row', backgroundColor: '#fff', }}>
                        <Row size={12}>
                            <Col size={6}>
                                <Row style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <TextSmall text="Created On"></TextSmall>
                                    <TextSmall style={{ marginLeft: 8 }} text={moment.unix(+this.props.task.createdAt / 1000).format('ddd, DD MMM YYYY')}></TextSmall>
                                </Row>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <TouchableNativeFeedback onPress={() => this.Confirm()} background={TouchableNativeFeedback.Ripple(theme["color-danger-200"], true)}>
                                    <View>
                                        <TrashIcon color={theme["color-danger-400"]}></TrashIcon>
                                    </View>
                                </TouchableNativeFeedback></Col>
                        </Row>
                    </View>
                </ScrollView>
            </Layout>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    task: state.tasks.tasks.filter(t => t.id === ownProps.route.params.id)[0],
    subtasks: state.tasks.tasks.filter(t => t.parent && t.parent.id == ownProps.route.params.id),
    selectedGoal: (() => {
        try {
            let goal_id = state.tasks.tasks.filter(t => t.id == ownProps.route.params.id)[0].goal.id;

            if (goal_id) {
                let goal = state.goals.goals.filter(g => g.id == goal_id)[0];
                if (goal) {
                    return {
                        Id: goal.id,
                        Name: goal.name,
                        Value: goal.value,
                        key: goal.id
                    }
                }
            } else {
                return null;
            }
        } catch (error) {
            return null;
        }

    })()
})

const mapDispatchToProps = dispatch => {
    return {
        updateList: () => dispatch(UpdateTaskListAsync()),
        completed: id => dispatch(TaskCompletedAsync(id)),
        incomplete: id => dispatch(TaskIncompleteAsync(id)),

        deleteTask: id => dispatch(DeleteTaskAction(id)),
        attachReminder: (id, date) => dispatch(AttachReminder(id, date)),
        update: (id, propType, propValue) => dispatch(TaskUpdateAction({ id, propType, propValue }))

    }
}

export const TaskDetails = connect(mapStateToProps, mapDispatchToProps)(_TaskDetails)
