import { Button, Icon, Layout, ListItem, Text } from '@ui-kitten/components'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { UpdateTaskListAsync } from '../../actions/task.action'
import BottomSheet from 'reanimated-bottom-sheet';
import { Dimensions, FlatList, ScrollView, View } from 'react-native';
import { ModalHandleBar } from '../day/day.tabs';
import { DEFAULT_MARGIN, PADDING } from '../../common/constants';
import { FontFamilies } from '../../styles/text';
import { default as theme } from '../../styles/app-theme.json'
import { CheckBox, Fab } from 'native-base';
import CircleCheckBox from 'react-native-circle-checkbox';
import { CenterLoading } from '../activity.page';
const DefaultSelectButton = ({ onPress }) => {
    return (
        <Button onPress={onPress} children={props => <Text {...props}>Select Tasks</Text>}></Button>
    )
}
export class TaskSelectorV2 extends Component<any, any>{
    modalCloseStart = () => {
        if (this.props.onClose && typeof this.props.onClose === 'function') {
            this.props.onClose();
            this.close();

        }
    };
    modalOpenCallback: () => void;
    SCREEN_HEIGHT = (Dimensions.get('window').height) - 72 - DEFAULT_MARGIN;
    sheetRef: any;
    header = () => {
        return (
            <View style={{
                backgroundColor: 'white',
                padding: 16,
                paddingBottom: 0,
                borderTopLeftRadius: 9,
                borderTopRightRadius: 9

            }}>
                <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                    <ModalHandleBar></ModalHandleBar>

                </View>
                <View style={{ flexDirection: 'row', marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2, justifyContent: 'center', alignItems: 'center' }}>
                    <Text category="h6" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-400'] }}>Select Tasks</Text>
                </View>
            </View>
        )
    };
    apply = () => {
        try {
            let ids = Object.keys(this.state.selected).filter(id => this.state.selected[id]);
            if (ids && Array.isArray(ids)) {
                if (this.props.onSelect && typeof this.props.onSelect === "function") {
                    this.props.onSelect(ids)
                }
            }
        } catch (error) {

        }
        this.close();
    }
    renderContent = () => {
        if (!this.state.open) return <View style={{
            backgroundColor: 'white',
            padding: 16,
            height: this.SCREEN_HEIGHT,
            elevation: 20
        }} />
        return (
            <View
                style={{
                    backgroundColor: 'white',
                    padding: 16,
                    height: this.SCREEN_HEIGHT,
                    elevation: 20
                }}
            >

                <View style={{ position: "relative", flex: 1 }}>

                    <ScrollView>
                        {this.props.tasks.map((task, index) => {
                            return this.RenderItem({ item: task });
                        })}
                    </ScrollView>
                    <Fab
                        /*   active={this.state.active} */
                        direction="up"
                        containerStyle={{ marginBottom: DEFAULT_MARGIN * 4 }}
                        style={{ backgroundColor: theme['color-primary-500'] }}
                        position="bottomRight"
                        onPress={() => this.apply()}>
                        <Icon name="checkmark-outline" fill={theme['color-font-300']} width={24} height={24} />

                    </Fab>
                </View>
            </View>
        )
        /*   return (

) */
    }
    selected = (id) => {
        let newSelected = { ...this.state.selected };
        if (this.state.selected[id]) {
            newSelected[id] = false;
        } else {

            newSelected[id] = true;
        }
        this.setState(() => { return { selected: newSelected } })


    }
    RenderItem = ({ item }) => {
        let goal = null;
        if (item.goal && item.goal.id) {
            try {
                let index = this.props.goals.findIndex(g => g.id === item.goal.id);
                if (index > -1) {
                    goal = this.props.goals[index];

                }
            } catch (error) {
                console.log(error);
            }
        }
        return (
            <ListItem onPress={() => this.selected(item.id)}>
                <View style={{ padding: PADDING / 2, justifyContent: 'space-between', flexDirection: 'row', flex: 1, alignItems: 'center' }}>
                    <View>
                        {goal && <Text numberOfLines={1} category="label" appearance="hint" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{goal.name}</Text>}
                        <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{item.name}</Text>
                    </View>
                    <View>
                        <CircleCheckBox
                            innerSize={32}
                            outerSize={32}
                            filterSize={28}
                            innerColor={theme["color-success-400"]}
                            outerColor={theme["color-font-300"]}
                            onToggle={() => this.selected(item.id)}
                            checked={this.state.selected[item.id]} />
                    </View>
                </View>
            </ListItem>
        )
    }
    componentDidMount() {
        setTimeout(() => {
            this.setState((state, props) => { return { ready: true } })

        }, 300);
    }
    componentDidUpdate = (nextProps) => {

        if (this.props.open && !this.state.open) {

            this.open();

        }
    }
    open = () => {
        if (this.state.ready) {
            this.setState((state, props) => { return { open: true } })

            this.sheetRef?.current.snapTo(1);
        }
    }
    close = () => {
        this.setState((state, props) => { return { open: false } })
        this.sheetRef?.current.snapTo(2);
    }
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            selected: {}
        };
        this.sheetRef = React.createRef();
    }
    render() {

        return (
            <View style={{ height: 1 }} >

                <BottomSheet
                    enabledContentGestureInteraction={false}
                    onCloseEnd={this.modalCloseStart}
                    /*  onCloseStart={this.modalCloseStart} */
                    onOpenEnd={this.modalOpenCallback}
                    renderHeader={this.header}
                    onOpenStart={this.modalOpenCallback}
                    ref={this.sheetRef}
                    snapPoints={[this.SCREEN_HEIGHT, this.SCREEN_HEIGHT, 0]}
                    initialSnap={2}
                    borderRadius={0}
                    renderContent={this.renderContent}
                />

            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks,
    goals: state.goals.goals
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskSelectorV2)
