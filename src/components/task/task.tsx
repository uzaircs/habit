import React, { Component } from 'react'
import { connect } from 'react-redux'
import DateTimePicker from '@react-native-community/datetimepicker';
import { StyleSheet, TouchableOpacity, TouchableNativeFeedback, InteractionManager } from 'react-native'
import { Text, Card, Button, Icon } from '@ui-kitten/components'
import { Row, Col } from 'react-native-easy-grid'
import { sizes } from '../../styles/fonts'
import { View, Icon as NBIcon, ActionSheet, Toast } from 'native-base'
import CircleCheckBox from 'react-native-circle-checkbox';
import { ButtonText } from '../activity'
import { default as theme } from '../../styles/app-theme.json'
import { DeleteTaskAction, TaskCompletedAsync, TaskIncompleteAsync, TaskUpdateAction, UpdateTaskListAsync } from '../../actions/task.action'
import { TextMuted, TextDanger, TextWarning, TextSmall, DefaultText, H4, FontFamilies } from '../../styles/text'
import { SoundManager } from "../../modules/SoundManager";
import moment from 'moment'
import { toggleBulk, ItemAdded, ItemRemoved } from '../../actions/bulk.actions'
import { useAndroidBackHandler } from "react-navigation-backhandler";
import { CheckIcon, MenuIcon } from './list'
import Modal from 'react-native-modal';
import { NavigationContext } from '@react-navigation/native';
import {
    Placeholder,
    PlaceholderMedia,
    PlaceholderLine,
    Fade,

} from "rn-placeholder";
import { iOSUIKit } from 'react-native-typography'
import { ClockIcon } from './details'
import { ExtractTaskFacts, ExtractTaskFactsSync, TaskFacts } from './facts.group'
import { BORDER_RADIUS, CHECKBOX_BORDER, DEFAULT_MARGIN, TASK_CARD_HEIGHT } from '../../common/constants'
import { ChangeTaskDueDate, DeleteTask } from '../../models/api'

const styles = StyleSheet.create({
    card: {

        margin: 2,
    },
    taskCard: {
        height: 64,
        width: '100%',
        flexDirection: 'column', justifyContent: 'center'
    },
    lineThrough: {
        textDecorationLine: 'line-through', textDecorationStyle: 'solid'
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },

})
export const TaskActionCard = ({ onToggle }) => {
    return (
        <Card style={styles.taskCard}>
            <Button onPress={onToggle} appearance="ghost" children={(props) => <ButtonText text="Add New" {...props}></ButtonText>}></Button>
        </Card>
    )
}
export const isOverDue = (date) => {
    return moment.unix(+date / 1000).endOf('day').isBefore(moment(new Date()))
}
export const isToday = (date) => {
    return moment.unix(+date / 1000).startOf('day').isSame(moment(new Date()), 'day')
}
export const DueDate = ({ date }) => {

    if (date && moment.unix(+date / 1000).isValid() && isOverDue(date)) {
        return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <CalendarAlertIcon style={{ fontSize: 16, color: theme["color-danger-500"], marginRight: 4 }}></CalendarAlertIcon>
            <TextDanger text="Overdue" style={{ fontSize: sizes.small.fontSize }}></TextDanger>
        </View>)

    }
    if (date && moment.unix(+date / 1000).isValid() && isToday(date)) {
        return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name='calendar-outline' fill={theme["color-warning-600"]} style={{ width: 16, height: 16 }} />
            <TextWarning style={{ marginLeft: 4, fontSize: sizes.small.fontSize }} text={"Today"}></TextWarning>
        </View>)
    }
    if (date && moment.unix(+date / 1000).isValid()) {
        return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name='calendar-outline' fill={theme["color-font-300"]} style={{ width: 16, height: 16 }} />
            <TextMuted style={{ marginLeft: 4, fontSize: sizes.small.fontSize }} text={moment.unix(+date / 1000).format('MMM D')}></TextMuted>
        </View>)
    } else {
        return (
            <></>

        )
    }
}
export const BulbIcon = (props) => {
    return (
        <Icon name="bulb-outline" {...props}></Icon>
    )
}
export const BulbOffIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="lightbulb-off-outline" {...props} />
    )
}
export const CalendarAlertIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="calendar-alert" {...props} />
    )
}
export const EstimatedTimeIndicator = ({ time }) => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 4 }}>
                <ClockIcon width={16} height={16}></ClockIcon>
            </View>
            <TextSmall text={time + ' minutes'}></TextSmall>
        </View>
    )
}
export const NoDateCalendarIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="calendar-remove-outline" {...props} />
    )
}

export const SubtaskIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="file-tree" {...props} />
    )
}
export const SubtaskIndicatorIcon = ({ count, remaining }) => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <SubtaskIcon style={{ fontSize: 16, color: theme["color-font-300"], marginRight: 4 }}></SubtaskIcon>
            <TextMuted style={{ marginLeft: 4, fontSize: sizes.small.fontSize }} text={remaining}></TextMuted>
            <TextMuted style={{ marginLeft: 2, fontSize: sizes.small.fontSize }} text="/"></TextMuted>
            <TextMuted style={{ marginLeft: 2, fontSize: sizes.small.fontSize }} text={count}></TextMuted>
        </View>
    )
}
export const MydayIndicator = () => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <BulbIcon style={{ width: 20, height: 20 }} fill={theme["color-primary-500"]}></BulbIcon>
            <TextMuted text="My Day" style={{ color: theme["color-primary-500"], marginLeft: 4, fontSize: sizes.small.fontSize }}></TextMuted>
        </View>
    )
}
export const CircleCheckBoxWrapper = ({ initialValue, onToggle }) => {
    const [active, setActive] = React.useState(initialValue);
    const toggle = () => {
        setActive(!active);

        InteractionManager.runAfterInteractions(() => {
            onToggle();
        })
    }
    return (
        <CircleCheckBox
            innerSize={32}
            outerSize={32}
            filterSize={28}
            innerColor={theme["color-success-400"]}
            outerColor={theme["color-font-300"]}
            onToggle={toggle}
            checked={active} />
    )
}
class _Task extends React.Component<any, any>{
    static contextType = NavigationContext;
    constructor(props) {
        super(props);
        this.state = {
            deleteVisible: false,
            dateShown: false,
        }
    }
    setFromProps = () => {
        if (this.props.task) {
            let facts = ExtractTaskFactsSync(this.props.task);
            this.setState(() => { return { task: this.props.task, facts } })

            /*    ExtractTaskFacts(this.props.task).then((facts) => {
                   this.setState({ facts: facts });
               }) */
        }
    }
    componentDidMount() {
        this.setFromProps();
    }
    compare = (newTask) => {
        /* if(newTask?.list?.id && !this.state.task?.list?.id){
            return true;
        } */

        if (newTask?.list?.id != this.state.task?.list?.id) {
            console.log(`list changed`)
            return false;
        }
        if (newTask?.status != this.state.task?.status) {
            return false;
        }
        if (newTask?.name != this.state.task?.name) {
            return false;
        }
        if (newTask.DueDate != this.state.task.DueDate) {
            return false;
        }
        if (newTask.estimatedTime != this.state.task.estimatedTime) {
            return false;
        }
        if (newTask.myDay != this.state.task.myDay) {
            return false;
        }
        return true;
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.dateShown != nextState.dateShown) return true;
        if (!this.state.facts && nextState.facts) { return true }
        if (this.state.deleteVisible !== nextState.deleteVisible) return true;
        return (!this.compare(nextProps.task));
    }
    componentDidUpdate() {
        InteractionManager.runAfterInteractions(() => {
            this.setFromProps();
        })
    }
    applyDate = () => {

        if (this.state.task) {
            ChangeTaskDueDate(this.state.task.id, this.state.selectedDate);
            if (this.props.update && typeof this.props.update === 'function') {
                this.props.update({
                    id: this.state.task.id,
                    propType: 'DueDate',
                    propValue: this.state.selectedDate
                })
            }
        }
    }
    render() {

        const navigation = this.context;
        if (!this.state.task) {
            return this.taskLoading();
        }

        const textStyle = [];

        textStyle.push(sizes.normal)


        if (this.state.task.status) {
            textStyle.push(iOSUIKit.body)
            textStyle.push(FontFamilies.quicksand)
            textStyle.push({ textDecorationLine: 'line-through', textDecorationStyle: 'dashed' })
            textStyle.push({ color: theme["color-font-300"] });

        } else {
            textStyle.push(iOSUIKit.subheadEmphasized)
            textStyle.push(FontFamilies.quicksandsemibold)
        }
        if (!this.state.facts || !Array.isArray(this.state.facts) || !this.state.facts.length) {
            textStyle.push({ marginTop: DEFAULT_MARGIN })

        }

        return (
            <View style={{ flex: 1, minHeight: TASK_CARD_HEIGHT - 16, borderRadius: BORDER_RADIUS }}>
                {this.state.dateShown && <DateTimePicker
                    onChange={(e, date) => {

                        if (e.type == 'set') {
                            this.setState({
                                ...this.state,
                                dateShown: false,
                                selectedDate: (moment(date).unix() * 1000).toString()
                            })
                            this.applyDate();
                        }
                    }}
                    value={moment().toDate()}
                    mode="date"
                    is24Hour={true}

                />}
                <Modal hardwareAccelerated={true} onBackdropPress={() => this.setState((state, props) => { return { deleteVisible: false } })
                } coverScreen={true} isVisible={this.state.deleteVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.deleteModal></this.deleteModal>
                </Modal>
                <Row size={12} style={{ alignItems: "center" }}>
                    {!this.props.readonly && <Col size={1} style={{ marginRight: DEFAULT_MARGIN }} >

                        {!this.props.isBusy && this.state.task.status == 0 && <CircleCheckBoxWrapper onToggle={this.toggleStatus} initialValue={this.state.task.status == 1}></CircleCheckBoxWrapper>}
                        {!this.props.isBusy && this.state.task.status == 1 &&
                            <TouchableNativeFeedback onPress={this.toggleStatus} >
                                <View>
                                    <CheckIcon color={theme["color-success-500"]}></CheckIcon>
                                </View>
                            </TouchableNativeFeedback>
                        }
                    </Col>}

                    <Col size={10} style={{ justifyContent: 'center' }} >
                        <TouchableOpacity onLongPress={() => {
                            if (this.props.drag) {
                                this.props.drag();
                            }
                        }} style={{ flex: 1 }} onPress={() => (this.props.onTap && typeof this.props.onTap === 'function') ? this.props.onTap() : navigation.navigate('taskDetail', { id: this.state.task.id })}>
                            {/*  {task.parent && task.parent.id && <TextMuted text={parentName} style={{ fontSize: 12 }}></TextMuted>} */}
                            <Text style={textStyle}>{this.state.task.name}</Text>
                            <Row style={{ top: -DEFAULT_MARGIN / 2 }}>
                                {(this.state.task.status == 0) && <TaskFacts facts={this.state.facts}></TaskFacts>}
                                {!this.props.isBusy && this.state.task.status == 1 &&
                                    <TextSmall text={moment(this.state.task.completedAt).fromNow()} style={{ marginTop: DEFAULT_MARGIN / 2 }}></TextSmall>
                                }
                            </Row>
                        </TouchableOpacity>
                    </Col>
                    <Col size={1}>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)} onPress={() => this.showTaskActions()}>
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <MenuIcon fill={theme["color-font-300"]} height={16} width={16}></MenuIcon>
                            </View>
                        </TouchableNativeFeedback>
                    </Col>
                </Row>
            </View >
        )
    }

    taskLoading = () => {
        return (
            <Placeholder
                Animation={Fade}
                Left={(props) => <PlaceholderMedia {...props} style={{ width: 24, height: 24, borderRadius: 12, marginRight: 8, marginTop: 8 }}></PlaceholderMedia>}

            >
                <PlaceholderLine />
                <PlaceholderLine width={30} />
            </Placeholder>
        )
    }
    startWorking = () => {
        let { task } = this.state;
        const navigation = this.context;

        navigation.navigate('StartWork', { tasks: [task.id] });
    }
    view = () => {
        const navigation = this.context;
        navigation.navigate('taskDetail', { id: this.state.task.id });
    }
    changeDueDate = () => {
        this.setState(() => { return { dateShown: true } })

    }
    deleteConfirm = () => {
        this.setState(() => { return { deleteVisible: true } })

    }
    delete = () => {
        DeleteTask(this.state.task.id);
        this.props.deleteTask(this.state.task.id);
    }
    deleteModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { deleteVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>
                <Button
                    onPress={() => {
                        this.delete();
                        Toast.show({
                            text: 'Task deleted',
                            textStyle: iOSUIKit.subheadShortWhite,
                            duration: 2000,
                            type: 'success'
                        })
                        this.setState(() => { return { deleteVisible: false }; })

                    }}
                    style={styles.footerControl}
                    size='small'
                    status='danger'>
                    DELETE
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Confirm Delete"></H4>
                <DefaultText text="Are you sure you want to delete this task? This action cannot be undone"></DefaultText>
            </Card>
        )
    }
    showTaskActions = () => {
        var BUTTONS = ["Start Working", "View Task", "Change Due Date", "Delete Task", "Cancel"];
        ActionSheet.show(
            {
                options: BUTTONS,
                cancelButtonIndex: 4,
                destructiveButtonIndex: 4,
                title: "Choose an action"
            },
            buttonIndex => {
                switch (buttonIndex) {
                    case 0:
                        this.startWorking();
                        break;
                    case 1:
                        this.view();
                        break;
                    case 2:
                        this.changeDueDate();
                        break;;
                    case 3:
                        this.deleteConfirm();
                        break;
                    default: break;
                }
            }
        )
    }
    toggleStatus = () => {

        if (this.state.task.status) {
            SoundManager.play(1);
            let { task } = this.state;
            this.setState({
                task: { ...task, status: 0 }
            })

            /* status ? props.incomplete(task.id) : props.completed(task.id) */
            InteractionManager.runAfterInteractions(() => {
                this.props.incomplete(this.state.task.id);
            })
        } else {
            SoundManager.play();
            let { task } = this.state;
            this.setState({
                task: { ...task, status: 1 }
            })

            InteractionManager.runAfterInteractions(() => {
                this.props.completed(this.state.task.id);
            })

        }
    }

}





const mapDispatchToProps = dispatch => {
    return {
        deleteTask: id => dispatch(DeleteTaskAction(id)),
        completed: id => dispatch(TaskCompletedAsync(id)),
        incomplete: id => dispatch(TaskIncompleteAsync(id)),
        request: () => dispatch(UpdateTaskListAsync()),
        toggle: () => dispatch(toggleBulk()),
        markSelection: id => dispatch(ItemAdded(id)),
        removeSelection: id => dispatch(ItemRemoved(id)),
        update: props => dispatch(TaskUpdateAction(props))
    }
}

export const Task = connect(null, mapDispatchToProps)(_Task)
