import React, { Component } from 'react'
import { connect } from 'react-redux'
import PickerModal from 'react-native-picker-modal-view';
import { ListItem, Button, Divider } from '@ui-kitten/components';
import { DefaultText } from '../../styles/text';

import { UpdateTaskListAsync } from '../../actions/task.action';
import { View } from 'native-base';


class _TaskPicker extends Component<any, any>{
    constructor(props) {
        super(props);

    }
    private onClosed(): void {

    }

    private onSelected(selected: any): void {
        this.setState({ selectedItem: selected });
        if (this.props.onSelected && typeof this.props.onSelected === 'function') {
            this.props.onSelected(selected);
        }
        return selected;
    }

    private onBackButtonPressed(): void {

    }
    private renderItem = (selectedItem, item) => {

        return (
            <ListItem disabled={true} >
                <View>
                    <View style={{ padding: 8, flexDirection: 'row', alignItems: 'flex-start' }}>
                        <DefaultText text={item.Name}></DefaultText>

                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Divider style={{ width: '100%', marginTop: 8 }}></Divider>
                    </View>
                </View>
            </ListItem>
        )
    }
    render() {
        return (

            <PickerModal
                renderSelectView={(disabled, selected, showModal) =>
                    this.props.selectView({ disabled, selected, showModal })
                }
                renderListItem={this.renderItem}
                onSelected={this.onSelected.bind(this)}
                onClosed={this.onClosed.bind(this)}
                onBackButtonPressed={this.onBackButtonPressed.bind(this)}
                items={this.props.tasks}
                sortingLanguage={'en'}
                showToTopButton={true}
                selected={this.props.selectedItem}
                
                autoGenerateAlphabeticalIndex={true}
                selectPlaceholderText={'Choose a task'}
                onEndReached={() => console.log('list ended...')
                }
                searchPlaceholderText={'Search tasks...'}
                requireSelection={false}
                autoSort={false}
            />

        );
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.map(selectItem => {
        return {
            Id: selectItem.id,
            Name: selectItem.name,
            Value: selectItem.id,
            key: selectItem.id
        }
    })
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync())
    }
}

export const TaskPicker = connect(mapStateToProps, mapDispatchToProps)(_TaskPicker)
