import { Icon } from '@ui-kitten/components'
import { View } from 'native-base'
import React from 'react'
import { TextSmall } from '../../styles/text'
import { DotCircleIcon } from '../../main'
import { default as theme } from '../../styles/app-theme.json'
import { CalendarIcon } from './add'
import { ClockIcon } from './details'
import { SubtaskIcon } from './task'
import moment from 'moment'
import { GetSubtaskCount } from '../../models/api'

import { DEFAULT_MARGIN } from '../../common/constants'
import { Text } from 'react-native'
const DEFAULT_ICON_SIZE = 16;
export const ReminderIcon = (props) => {
    return (
        <Icon name='bell-outline' fill={theme["color-font-300"]} style={{ width: DEFAULT_ICON_SIZE, height: DEFAULT_ICON_SIZE }} {...props} />
    )
}
export enum ETaskFact {
    DUE_DATE = 0,
    ESTIMATED_TIME,
    REMINDER,
    SUBTASK,
    LIST,
    NOTES
}
export const ReminderIndicator = ({ fact }) => {
    let factInstance: TaskFact = fact;
    let date = moment(factInstance.value);
    if (!date.isValid() || date.isBefore(moment())) {
        return <View />
    }
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {factInstance.color ? <ReminderIcon fill={factInstance.color}></ReminderIcon> : <ReminderIcon></ReminderIcon>}
            {factInstance.color && <TextSmall style={{ color: factInstance.color }} text={date.format('h:mm A')}></TextSmall>}
            {!factInstance.color && <TextSmall text={date.format('h:mm A')}></TextSmall>}
        </View>
    )
}
export const ListIndicator = ({ fact }) => {
    let factInstance: TaskFact = fact;
    if (!factInstance.value) {
        return <View />
    } else {

        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                {factInstance.color ? <DotCircleIcon style={{ fontSize: 8, color: factInstance.color, marginRight: 4 }}></DotCircleIcon> : <DotCircleIcon style={{ fontSize: 6, color: theme["color-font-200"], marginRight: 4 }}></DotCircleIcon>}
                {factInstance.color && <TextSmall text={factInstance.value}></TextSmall>}
                {!factInstance.color && <TextSmall text={factInstance.value}></TextSmall>}
            </View>
        )
    }
}
export const DueDateIndicator = ({ fact }) => {
    let factInstance: TaskFact = fact;
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center', }}>
            <View style={{ marginRight: DEFAULT_MARGIN / 4 }}>
                {

                    factInstance.color ?
                        <CalendarIcon width={DEFAULT_ICON_SIZE} height={DEFAULT_ICON_SIZE} fill={factInstance.color}></CalendarIcon> :
                        <CalendarIcon width={DEFAULT_ICON_SIZE} height={DEFAULT_ICON_SIZE}></CalendarIcon>
                }
            </View>
            {factInstance.color && <TextSmall style={{ color: factInstance.color }} text={factInstance.value}></TextSmall>}
            {!factInstance.color && <TextSmall text={factInstance.value}></TextSmall>}
        </View>
    )
}
export const SubtaskIndicator = ({ fact }) => {
    let factInstance: TaskFact = fact;
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {
                factInstance.color ? <SubtaskIcon
                    style={{ fontSize: DEFAULT_ICON_SIZE, color: factInstance.color }}></SubtaskIcon> :
                    <SubtaskIcon style={{ fontSize: DEFAULT_ICON_SIZE, color: theme["color-font-300"] }}></SubtaskIcon>
            }
            {factInstance.color && <TextSmall style={{ color: factInstance.color }} text={factInstance.value}></TextSmall>}
            {!factInstance.color && <TextSmall text={factInstance.value}></TextSmall>}
        </View>
    )
}
export const EstimatedTimeIndicator = ({ fact }) => {
    let factInstance: TaskFact = fact;
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View style={{ marginRight: 4 }}>
                {
                    factInstance.color ? <ClockIcon width={DEFAULT_ICON_SIZE} height={DEFAULT_ICON_SIZE}
                    ></ClockIcon> :
                        <ClockIcon width={DEFAULT_ICON_SIZE} height={DEFAULT_ICON_SIZE}></ClockIcon>
                }
            </View>
            {factInstance.color && <TextSmall style={{ color: factInstance.color }} text={factInstance.value + 'm'}></TextSmall>}
            {!factInstance.color && <TextSmall text={factInstance.value + 'm'}></TextSmall>}
        </View>
    )
}
export type TaskFact = {
    type: ETaskFact;
    value?: any;
    color?: any;
}
export const NotesIndicator = () => {

    return (
        <Icon name="file-text-outline" fill={theme['color-font-300']} width={DEFAULT_ICON_SIZE} height={DEFAULT_ICON_SIZE} />

    )
}
export const GetFactIndicator = ({ fact }) => {

    switch (fact.type) {
        case ETaskFact.DUE_DATE:
            return <DueDateIndicator fact={fact} />;
        case ETaskFact.ESTIMATED_TIME:
            return <EstimatedTimeIndicator fact={fact} />
        case ETaskFact.REMINDER:
            return <ReminderIndicator fact={fact} />
        case ETaskFact.SUBTASK:
            return <SubtaskIndicator fact={fact} />
        case ETaskFact.LIST:
            return <ListIndicator fact={fact} />
        case ETaskFact.NOTES:

            return <NotesIndicator />
        default: return <View />
    }
}
const ParseTaskFact = (propertyType: ETaskFact, propertyValue): TaskFact => {
    switch (propertyType) {
        case ETaskFact.DUE_DATE:
            //TODO confirm if unix
            if (propertyValue && !isNaN(parseInt(propertyValue)) && moment.unix(+propertyValue / 1000).isValid()) {
                let date = moment.unix(+propertyValue / 1000);
                let fact: TaskFact = {
                    type: ETaskFact.DUE_DATE,
                    value: date.format('D MMM')
                }
                if (date.startOf('day').isBefore(moment().startOf('day'), 'second')) {
                    fact.color = theme["color-danger-500"]
                }
                return fact;
            } else {
                return {
                    type: ETaskFact.DUE_DATE,
                    value: 'None'
                }
            }
        case ETaskFact.ESTIMATED_TIME:
            return {
                type: ETaskFact.ESTIMATED_TIME,
                value: propertyValue
            }
        case ETaskFact.SUBTASK:
            let fact: TaskFact = {
                type: ETaskFact.SUBTASK,
                value: propertyValue
            }
            return fact;
        case ETaskFact.REMINDER:
            return {
                type: ETaskFact.REMINDER,
                value: propertyValue[0]?.date
            }
        case ETaskFact.LIST:
            return {
                type: ETaskFact.LIST,
                value: propertyValue.name,
                color: propertyValue.color
            }
        case ETaskFact.NOTES:
            return {
                type: ETaskFact.NOTES,
            }


    }
}
export const ExtractTaskFactsSync = (task) => {
    let facts: TaskFact[] = [];
    Object.keys(task).forEach(key => {
        if (task.hasOwnProperty(key)) {
            switch (key) {
                case 'DueDate':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.DUE_DATE, task[key]));
                    }
                    break;
                case 'estimatedTime':
                case 'estimated_time':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.ESTIMATED_TIME, task[key]));
                    }
                    break;
                case 'reminders':
                    if (task[key] && Array.isArray(task[key]) && task[key].length) {
                        facts.push(ParseTaskFact(ETaskFact.REMINDER, task[key]));
                    }
                    break;
                case 'list':
                    if (task[key] && task[key].id) {
                        facts.push(ParseTaskFact(ETaskFact.LIST, task[key]));
                    }
                    break;
                case 'notes':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.NOTES, task[key]));

                    }
                    break;

            }
        }
    })

    return facts.sort((a, b) => a.type - b.type);
}
export const ExtractTaskFacts = async (task): Promise<TaskFact[]> => {
    let facts: TaskFact[] = [];
    Object.keys(task).forEach(key => {
        if (task.hasOwnProperty(key)) {
            switch (key) {
                case 'DueDate':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.DUE_DATE, task[key]));
                    }
                    break;
                case 'estimatedTime':
                case 'estimated_time':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.ESTIMATED_TIME, task[key]));
                    }
                    break;
                case 'reminders':
                    if (task[key] && Array.isArray(task[key]) && task[key].length) {
                        facts.push(ParseTaskFact(ETaskFact.REMINDER, task[key]));
                    }
                    break;
                case 'list':
                    if (task[key]) {
                        facts.push(ParseTaskFact(ETaskFact.LIST, task[key]));
                    }
                    break;

            }
        }
    })
    try {
        let totalSubtasks = await GetSubtaskCount(task.id);
        if (totalSubtasks) {
            facts.push(ParseTaskFact(ETaskFact.SUBTASK, totalSubtasks));
        }
    } catch (error) {

    }
    return facts.sort((a, b) => a.type - b.type);
}
export class TaskFacts extends React.PureComponent<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            facts: []
        }
    }


    render() {
        if (!this.props.facts?.length) {
            return <View />;
        }

        return (<View style={{ flexDirection: 'row' }}>
            {this.props.facts.map((fact, index) => {
                return (
                    <View key={index} style={{ flexDirection: 'row', alignItems: 'center', paddingRight: 8 }}>
                        {index > 0 && <DotCircleIcon style={{ fontSize: 6, color: theme["color-font-200"], marginRight: 4 }}></DotCircleIcon>}
                        <GetFactIndicator fact={fact}></GetFactIndicator>
                    </View>
                )
            })}
        </View>)
    }
}
export default TaskFacts;