import { Button, Card, Layout, ListItem, Text } from '@ui-kitten/components'
import { View } from 'native-base'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { FontFamilies } from '../../styles/text'
import { AddSessionEventAction, AddSessionTaskAction, PopulateSession, SaveSessionAction } from '../../actions/session.action'
import moment from 'moment'
import { DEFAULT_MARGIN, PADDING } from '../../common/constants'
import { StyleSheet } from 'react-native'
import { DotCircleIcon, DotFilledIcon, DotIcon, PlayIcon } from '../../main'
import { default as theme } from '../../styles/app-theme.json'
import { useNavigation } from '@react-navigation/native'
const textStyle = StyleSheet.create({
    lineThrough: {
        textDecorationLine: 'line-through', textDecorationStyle: 'solid', color: theme['color-font-300']
    },
})
export const SessionProgressIndicator = ({ total, completed }) => {
    let indicators = []
    for (let i = 0; i < total; i++) {
        if (i < completed) {
            indicators[i] = 1;
        } else indicators[i] = 0;

    }
    return (
        <View style={{ flexDirection: 'row' }}>
            {indicators.map(indicator => {
                if (indicator == 1) {
                    return <DotCircleIcon style={{ color: theme['color-primary-500'], fontSize: 12, marginRight: DEFAULT_MARGIN / 4 }}></DotCircleIcon>
                } else return <DotIcon style={{ color: theme['color-font-300'], fontSize: 12, marginRight: DEFAULT_MARGIN / 4 }}></DotIcon>

            })}
        </View>
    )
}
const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {
        flex: 1,
        margin: 2,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    footerControl: {
        marginHorizontal: 2,
    },
});
//#region SessionCard
const _SessionCardV2 = ({ session, ...props }) => {
    const navigation = useNavigation();
    const Footer = (props) => (
        <View {...props} style={[props.style, styles.footerContainer]}>
            <SessionProgressIndicator total={session.limit} completed={2}></SessionProgressIndicator>
            <Button
                onPress={() => navigation.navigate('TimerV2', { id: session.id })}
                style={styles.footerControl}
                appearance="ghost"
                size="small"
                accessoryRight={PlayIcon}
                children={props => <Text {...props}>START WORKING</Text>}
                status='primary' />
        </View>
    );
    const Header = (props) => (
        <View {...props}>
            <View style={{ flexDirection: 'row' }}>
                <View style={{ flexDirection: 'column' }}>
                    <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category='h6'>{session.name}</Text>
                    {(Array.isArray(session.tasks) && session.tasks.length > 0) && <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category='label' appearance='hint'>{session.tasks.length + ' tasks'}</Text>}
                </View>
                <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end', flex: 1 }}>
                    <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category='h6'>{session.time + ' min'}</Text>
                    <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category='label' appearance='hint'>{session.limit + ' sessions'}</Text>
                </View>
            </View>

        </View>
    );
    return (
        <Card footer={Footer} header={Header} style={{ marginTop: DEFAULT_MARGIN, flex: 1 }} disabled={true}>
            {(!Array.isArray(session.tasks) || session.tasks.length == 0) && <View style={{ justifyContent: 'center', alignItems: 'center', padding: PADDING }}>
                <Text category="s1" numberOfLines={1} appearance="hint" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>No tasks</Text>
            </View>}
            {session.tasks?.map(task_id => {
                try {
                    let index = props.tasks.findIndex(t => t.id === task_id);
                    let goal_index = -1;
                    let goal = null;
                    try {
                        goal_index = props.goals.findIndex(g => g.id === props.tasks[index].goal.id);
                        if (goal_index > -1) {
                            goal = props.goals[goal_index];
                        }
                    } catch (error) {

                    }
                    if (index > -1) {
                        return (
                            <View style={{ flex: 1 }}>
                                <ListItem>
                                    <View style={{ flexDirection: 'column' }}>
                                        {goal && <Text numberOfLines={1} category="label" appearance="hint" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{goal.name}</Text>}
                                        {props.tasks[index].status == 0 && <Text category="p1" numberOfLines={1} style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily, }}>{props.tasks[index].name}</Text>}
                                        {props.tasks[index].status > 0 && <Text category="p1" numberOfLines={1} style={[{ fontFamily: FontFamilies.quicksandmedium.fontFamily }, textStyle.lineThrough]}>{props.tasks[index].name}</Text>}
                                    </View>
                                </ListItem>
                            </View>
                        )
                    } else throw new Error('no task');

                } catch (error) {
                    return <Text category="p1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>No Tasks</Text>
                }
            })}
        </Card>
    )
}

const mapStateToPropsV2 = (state) => ({
    tasks: state.tasks.tasks,
    goals: state.goals.goals
})

const mapDispatchToPropsV2 = dispatch => {
    return {

    }
}
export const SessionCardV2 = connect(mapStateToPropsV2, mapDispatchToPropsV2)(_SessionCardV2);

//#endregion

//#region Session List
export class SessionList extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {

        }

    }
    componentDidMount() {

    }
    render() {
        return (
            <Layout style={{ flex: 1 }} level="4">
                {this.props.sessions.map(session => (
                    <SessionCardV2 session={session}></SessionCardV2>
                ))}
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    sessions: state.sessions.sessions
})

const mapDispatchToProps = dispatch => {
    return {
        save: session => dispatch(SaveSessionAction(session)),
        load: () => dispatch(PopulateSession()),
        add_task: (id, task) => dispatch(AddSessionTaskAction(id, task)),
        add_event: (id, event) => dispatch(AddSessionEventAction(id, event))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SessionList)
//#endregion
