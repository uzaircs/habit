import React from 'react'

import { DefaultText, H4 } from '../../styles/text'
import { Card, ListItem, Button } from '@ui-kitten/components';
import { View } from 'native-base';
import { EstimatedTimeIndicator, DueDate } from '../task/task';

export default function SessionCard({ items }) {
    const Header = (props) => {
        return (
            <View {...props}>
                <H4 text="Suggested Work Session"></H4>
            </View>
        );
    }
    const Footer = (props) => {
        return (
            <View {...props}>
                <Button appearance="ghost">Start Working</Button>
            </View>
        );
    }
   
    if (!items || !Array.isArray(items) || items.length === 0) {
        return <View />
    }
    return (
        <Card disabled={true} style={{ width: '100%' }} header={Header} footer={Footer}>
            {items.map(item => {
                return (
                    <ListItem>
                        <View>
                            <View style={{ marginBottom: 4 }}>
                                <DefaultText text={item.name}></DefaultText>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: 128 }}>
                                    {item.estimatedTime && <EstimatedTimeIndicator time={item.estimatedTime}></EstimatedTimeIndicator>}
                                </View>
                                <View style={{ width: 128 }}>
                                    {item.status != 1 && <DueDate date={item.DueDate}></DueDate>}
                                </View>
                            </View>
                        </View>
                    </ListItem>
                )
            })}
        </Card>
    )
}
