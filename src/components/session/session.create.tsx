import { Button, Icon, Input, Layout, ListItem, Text } from '@ui-kitten/components'
import React, { Component } from 'react'
import { Animated, InteractionManager, ScrollView, View } from 'react-native'
import { Col, Row } from 'react-native-easy-grid'
import { connect } from 'react-redux'
import { FontFamilies, TextMuted, TextMutedStrong, TextSmall } from '../../styles/text'
import { ClockIcon } from '../task/details'
import InputSpinner from "react-native-input-spinner";
import { default as theme } from '../../styles/app-theme.json'
import { BackNav } from '../../menu/header'
import Collapsible from 'react-native-collapsible';
import { DEFAULT_MARGIN, PADDING } from '../../common/constants'
import { PlusOutlineIcon } from '../activity'
import TaskSelectorV2 from '../task/selector.v2'
import { CenterLoading } from '../activity.page'
import { Toast } from 'native-base'
import { AddSessionWithTasks, SaveSessionAction } from '../../actions/session.action'
export const ChevronDown = (props) => {
    return (
        <Icon width={24} height={24} fill={theme['color-font-300']} name="chevron-down-outline" {...props} />
    )
}
export const ChevronUp = (props) => {
    return (
        <Icon width={24} height={24} fill={theme['color-font-300']} name="chevron-up-outline" {...props} />
    )
}
export class CreateSession extends Component<any, any>{
    selected = (ids) => {
        try {
            let selectedTasks = [];
            ids.forEach(id => {
                let index = this.props.tasks.findIndex(task => task.id === id);
                if (index > -1) {
                    selectedTasks.push(this.props.tasks[index]);
                }
            })
            this.setState((state, props) => { return { selectedTasks } })

        } catch (error) {

        }
    }
    create = () => {
        this.props.navigation.pop();
        let { session } = this.state;
        if (this.state.selectedTasks && this.state.selectedTasks.length) {

            this.props.add(session, this.state.selectedTasks.map(t => t.id));
        } else {
            this.props.save(session);
        }
        Toast.show({
            text: 'Session saved',
            type: 'success',
        })
    }
    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState((state, props) => { return { ready: true } })
        })

    }
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            settingsCollapsed: true,
            taskCollapsed: false,
            selectedTasks: [],
            session: {
                time: 25,
                shortBreak: 5,
                longBreak: 15,
                limit: 4,
                name: 'Pomodoro',
                longBreakAfter: 2,

            }
        };
    }
    //mins
    timeChange = (value) => {

    }
    render() {
        if (!this.state.ready) {
            return <CenterLoading></CenterLoading>
        }
        return (
            <Layout style={{ flex: 1 }} level="4">
                <BackNav title="Create Session"></BackNav>
                <ScrollView>
                    <View style={{ padding: PADDING / 2 }}>
                        <View style={{ marginBottom: DEFAULT_MARGIN / 2 }}>
                            <ListItem disabled={true}>
                                <View style={{ flex: 1, padding: PADDING / 2 }}>
                                    <Input
                                        textStyle={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }}
                                        value={this.state.session.name}
                                        onChangeText={(text) => this.setState((state, props) => { return { session: { ...state.session, name: text } } })
                                        }
                                        label={props => <Text  {...props} appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>Session Name</Text>}
                                        caption={props => <Text appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginTop: DEFAULT_MARGIN / 4 }}>A name to distinguish this session from the rest</Text>}
                                    ></Input>
                                </View>

                            </ListItem>
                        </View>
                        <ListItem onPress={() => {
                            this.setState((state, props) => { return { settingsCollapsed: !this.state.settingsCollapsed } })
                        }}>
                            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', flex: 1, padding: PADDING / 4 }}>
                                <View>
                                    <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category="h6">Timer Settings</Text>
                                    <Text appearance="hint" category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} >Pomodoro</Text>
                                </View>
                                {this.state.settingsCollapsed && <ChevronDown></ChevronDown>}
                                {!this.state.settingsCollapsed && <ChevronUp></ChevronUp>}
                            </View>

                        </ListItem>

                        <Collapsible collapsed={this.state.settingsCollapsed}>
                            <Animated.View>
                                <ListItem>
                                    <View style={{ padding: 8, flexDirection: 'row' }}>
                                        <Row size={12}>
                                            <Col>
                                                <Row size={12} style={{ alignItems: 'center' }}>

                                                    <Col size={10} >
                                                        {!this.state.session.time && <TextMutedStrong text="Time Per Session"></TextMutedStrong>}
                                                        {(this.state.session.time > 0) && <TextMutedStrong text={this.state.session.time + ' minutes'}></TextMutedStrong>}
                                                        {this.state.session.time > 0 && <TextSmall text="Time Per Session"></TextSmall>}
                                                    </Col>
                                                </Row>

                                            </Col>
                                            <Col style={{ alignItems: 'flex-end' }}>
                                                <InputSpinner
                                                    width={128}
                                                    height={42}
                                                    inputStyle={{ fontFamily: FontFamilies.quicksandbold.fontFamily, fontSize: 16, color: theme["color-font-300"] }}
                                                    max={360}
                                                    min={0}
                                                    color={theme["color-font-300"]}
                                                    initialValue={this.state.session.time}
                                                    step={5}
                                                    buttonStyle={{ width: 36, height: 36 }}
                                                    colorMax={theme["color-font-500"]}
                                                    colorMin={theme["color-font-200"]}
                                                    value={this.state.session.time}
                                                    onChange={(num) => {
                                                        this.setState((state, props) => { return { session: { ...state.session, time: num } } })
                                                    }}
                                                />

                                            </Col>
                                        </Row>

                                    </View>

                                </ListItem>
                                <ListItem>
                                    <View style={{ padding: 8, flexDirection: 'row' }}>
                                        <Row size={12}>
                                            <Col>
                                                <Row size={12} style={{ alignItems: 'center' }}>

                                                    <Col size={10} >
                                                        {!this.state.session.limit && <TextMutedStrong text="Total Sessions"></TextMutedStrong>}
                                                        {(this.state.session.limit > 0) && <TextMutedStrong text={this.state.session.limit + ' sessions'}></TextMutedStrong>}
                                                        {this.state.session.limit > 0 && <TextSmall text="Total Sessions"></TextSmall>}
                                                    </Col>
                                                </Row>

                                            </Col>
                                            <Col style={{ alignItems: 'flex-end' }}>
                                                <InputSpinner
                                                    width={128}
                                                    height={42}
                                                    inputStyle={{ fontFamily: FontFamilies.quicksandbold.fontFamily, fontSize: 16, color: theme["color-font-300"] }}
                                                    max={10}
                                                    min={1}
                                                    color={theme["color-font-300"]}
                                                    initialValue={this.state.session.limit}
                                                    step={1}
                                                    buttonStyle={{ width: 36, height: 36 }}
                                                    colorMax={theme["color-font-500"]}
                                                    colorMin={theme["color-font-200"]}
                                                    value={this.state.session.limit}
                                                    onChange={(num) => {
                                                        this.setState((state, props) => { return { session: { ...state.session, limit: num } } })
                                                    }}
                                                />

                                            </Col>
                                        </Row>

                                    </View>

                                </ListItem>
                                <ListItem>
                                    <View style={{ padding: 8, flexDirection: 'row' }}>
                                        <Row size={12}>
                                            <Col>
                                                <Row size={12} style={{ alignItems: 'center' }}>

                                                    <Col size={10} >
                                                        {!this.state.session.shortBreak && <TextMutedStrong text="Time for short break"></TextMutedStrong>}
                                                        {(this.state.session.shortBreak > 0) && <TextMutedStrong text={this.state.session.shortBreak + ' minutes'}></TextMutedStrong>}
                                                        {this.state.session.shortBreak > 0 && <TextSmall text="Time for long break"></TextSmall>}
                                                    </Col>
                                                </Row>

                                            </Col>
                                            <Col style={{ alignItems: 'flex-end' }}>
                                                <InputSpinner
                                                    width={128}
                                                    height={42}
                                                    inputStyle={{ fontFamily: FontFamilies.quicksandbold.fontFamily, fontSize: 16, color: theme["color-font-300"] }}
                                                    max={360}
                                                    min={0}
                                                    color={theme["color-font-300"]}
                                                    initialValue={this.state.session.shortBreak}
                                                    step={5}
                                                    buttonStyle={{ width: 36, height: 36 }}
                                                    colorMax={theme["color-font-500"]}
                                                    colorMin={theme["color-font-200"]}
                                                    value={this.state.session.shortBreak}
                                                    onChange={(num) => {
                                                        this.setState((state, props) => { return { session: { ...state.session, shortBreak: num } } })
                                                    }}
                                                />

                                            </Col>
                                        </Row>

                                    </View>

                                </ListItem>
                                <ListItem>
                                    <View style={{ padding: 8, flexDirection: 'row' }}>
                                        <Row size={12}>
                                            <Col>
                                                <Row size={12} style={{ alignItems: 'center' }}>

                                                    <Col size={10} >
                                                        {!this.state.session.longBreak && <TextMutedStrong text="Time for long break"></TextMutedStrong>}
                                                        {(this.state.session.longBreak > 0) && <TextMutedStrong text={this.state.session.longBreak + ' minutes'}></TextMutedStrong>}
                                                        {this.state.session.longBreak > 0 && <TextSmall text="Time for long break"></TextSmall>}
                                                    </Col>
                                                </Row>

                                            </Col>
                                            <Col style={{ alignItems: 'flex-end' }}>
                                                <InputSpinner
                                                    width={128}
                                                    height={42}
                                                    inputStyle={{ fontFamily: FontFamilies.quicksandbold.fontFamily, fontSize: 16, color: theme["color-font-300"] }}
                                                    max={360}
                                                    min={0}
                                                    color={theme["color-font-300"]}
                                                    initialValue={this.state.session.longBreak}
                                                    step={5}
                                                    buttonStyle={{ width: 36, height: 36 }}
                                                    colorMax={theme["color-font-500"]}
                                                    colorMin={theme["color-font-200"]}
                                                    value={this.state.session.longBreak}
                                                    onChange={(num) => {
                                                        this.setState((state, props) => { return { session: { ...state.session, longBreak: num } } })
                                                    }}
                                                />

                                            </Col>
                                        </Row>

                                    </View>

                                </ListItem>
                            </Animated.View>
                        </Collapsible>
                        <View style={{ marginTop: DEFAULT_MARGIN / 2 }}></View>
                        <ListItem onPress={() => {
                            this.setState((state, props) => { return { taskCollapsed: !this.state.taskCollapsed } })
                        }}>
                            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', flex: 1, padding: PADDING / 4 }}>
                                <View>
                                    <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category="h6">Tasks</Text>
                                    <Text appearance="hint" category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} >{this.state.selectedTasks.length} selected</Text>
                                </View>
                                {this.state.taskCollapsed && <ChevronDown></ChevronDown>}
                                {!this.state.taskCollapsed && <ChevronUp></ChevronUp>}
                            </View>

                        </ListItem>

                        <Collapsible collapsed={this.state.taskCollapsed}>
                            <Animated.View>
                                {(Array.isArray(this.state.selectedTasks)) && this.state.selectedTasks.map((task, index) => (
                                    <ListItem key={`task-item-${index}`} >
                                        <View style={{ padding: PADDING / 4, flexDirection: 'row', alignItems: 'center' }}>
                                            <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: DEFAULT_MARGIN / 4, flexDirection: 'row', position: 'relative', top: 4 }}>
                                                <ClockIcon width={20} height={20}></ClockIcon>
                                            </View>
                                            <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{task.name}</Text>
                                        </View>
                                    </ListItem>
                                ))}
                                <ListItem>
                                    <Button onPress={() => this.setState((state, props) => { return { open: !state.open } })
                                    } accessoryLeft={PlusOutlineIcon} appearance="ghost" children={props => <Text {...props}>Select Tasks</Text>}></Button>
                                </ListItem>

                            </Animated.View>
                        </Collapsible>
                    </View>
                </ScrollView>
                <View style={{ margin: PADDING / 2 }}>
                    <Button onPress={this.create} children={props => <Text {...props}>Create</Text>}></Button>
                </View>
                <TaskSelectorV2 onSelect={this.selected} onClose={() => this.setState(() => { return { open: false } })
                } open={this.state.open}></TaskSelectorV2>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks,
    goals: state.goals.goals
})

const mapDispatchToProps = dispatch => {
    return {
        add: (s, t) => dispatch(AddSessionWithTasks(s, t)),
        save: (s) => dispatch(SaveSessionAction(s))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSession)
