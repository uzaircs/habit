import React, { Component } from 'react'
import { Layout, Text, Spinner, Card } from '@ui-kitten/components'
import { View } from 'native-base'
import { ActivityIcon } from '../activity'
import { default as theme } from '../../styles/app-theme.json'
import { default as icons } from './icons.json'
import { ScrollView, TouchableOpacity, TouchableNativeFeedback } from 'react-native'
import { fontFamily, sizes, fontColors } from '../../styles/fonts'
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack'
import { connect } from 'react-redux'
import { IconSelected } from '../../actions/icon.actions'
import { BackNav } from '../../menu/header'
import { UppercaseHeading } from '../entry/past'
import { Col, Grid, Row } from 'react-native-easy-grid'
import { DEFAULT_MARGIN } from '../../common/constants'
const Stack = createStackNavigator();

export const SelectableIcon = (props) => {
    return (
        <View style={{ padding: 12 }}>
            <ActivityIcon defaultColor={theme["color-primary-600"]} isActive={false} activity={{ icon: props.icon, icon_type: props.iconType }}></ActivityIcon>
        </View>
    )
}

export const NoIcon = () => {
    return (
        <ActivityIcon isActive={false} activity={{ icon: 'question-mark-circle-outline', icon_type: 'eva' }} ></ActivityIcon>
    )
}

export const IconSelectionInput = ({ selected }) => {
    return (

        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 16, marginBottom: 16 }}>
            <View>
                <Text style={[fontFamily.semibold, sizes.normal, fontColors.primary]}>Select an Icon</Text>
            </View>
            <View>
                {(selected && selected.icon && selected.icon_type) ? <ActivityIcon isActive={false} activity={{ icon: selected.icon, icon_type: selected.icon_type }}></ActivityIcon> : <NoIcon></NoIcon>}
            </View>
        </View>

    )
}

export const IconSelectionNav = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false,
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid

        }}>
            <Stack.Screen name="selectionScreen" children={(props) => <IconScreens navigation={props.navigation}></IconScreens>} ></Stack.Screen>


        </Stack.Navigator>
    )
}



class _IconScreens extends Component<any, any> {
    /**
     *
     */

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }

    }
    selected = (icon, icon_type) => {
        this.props.onSelect(icon, icon_type);
        this.props.navigation.pop();
    }

    componentDidMount() {
        let categories = icons.map(c => c.category).filter((item, index, arr) => {
            return arr.indexOf(item) === index;
        })

        if (this.state.loading) {
            setTimeout(() => {
                this.setState({ loading: false, categories });
            }, 200);
        }
    }
    render() {
        if (this.state.loading) {
            return (
                <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <Spinner />
                </Layout>
            )
        }
        return (
            <Layout style={{ flex: 1 }} level="4">
                <BackNav navigation={this.props.navigation} title="Select an icon"></BackNav>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{ padding: 8, }}>



                        {
                            this.state.categories.map((category) => {
                                return (
                                    <Card style={{ marginBottom: DEFAULT_MARGIN / 2 }}>
                                        <Grid>
                                            <Row size={12}>
                                                <Col size={12}>
                                                    <Row>
                                                        <UppercaseHeading heading={category} />
                                                    </Row>
                                                    <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
                                                        {icons.filter(icon => icon.category == category).map((icon) => {
                                                            return (
                                                                <TouchableOpacity onPress={() => this.selected(icon.icon, icon.icon_type)}>
                                                                    <SelectableIcon icon={icon.icon} iconType={icon.icon_type}></SelectableIcon>
                                                                </TouchableOpacity>
                                                            )
                                                        })}
                                                    </View>
                                                </Col>
                                            </Row>
                                        </Grid>
                                    </Card>

                                )
                            })
                        }

                        {/*    {
   <Row>
                                                    
                                                </Row>
                                    icons.sort((a, b) => a.category.localeCompare(b.category)).map(icon => {
                                        return (
                                            <TouchableOpacity onPress={() => this.selected(icon.icon, icon.icon_type)}>
                                                <SelectableIcon icon={icon.icon} iconType={icon.icon_type}></SelectableIcon>
                                            </TouchableOpacity>
                                        )
                                    })} */}
                    </View>


                </ScrollView>
            </Layout >
        )
    }
}





const mapDispatchToProps = dispatch => {
    return {
        onSelect: (icon, icon_type) => dispatch(IconSelected(icon, icon_type))
    }
}

export const IconScreens = connect(null, mapDispatchToProps)(_IconScreens);
class _IconSelector extends Component<any, any> {

    constructor(props) {
        super(props);


    }
    selected = () => {

    }
    startSelection = () => {
        this.props.navigation.navigate('IconSelectionNav');
    }
    render() {
        return (
            <Layout >
                <TouchableOpacity onPress={this.startSelection}>
                    <IconSelectionInput selected={this.props.selected}></IconSelectionInput>
                </TouchableOpacity>
            </Layout>
        )
    }
}
const mapStateToProps = (state) => ({
    selected: {
        icon: state.selectedIcon.icon, icon_type: state.selectedIcon.icon_type
    }
})
export const IconSelector = connect(mapStateToProps, null)(_IconSelector);


