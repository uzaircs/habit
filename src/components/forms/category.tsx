import React, { Component } from 'react'
import { Layout, Card, Text, Input, Button } from '@ui-kitten/components'
import { StyleSheet } from 'react-native'
import { fontFamily, sizes, fontColors } from '../../styles/fonts'
import { View, Toast } from 'native-base'
import { IconSelector } from './icon.selector'
import { connect } from 'react-redux'
import { CreateCategory } from '../../models/api'
import { RequestCategoryAsync } from '../../actions/category.action'
import { BackNav } from '../../menu/header'
import { iOSUIKit } from 'react-native-typography'
import { FontFamilies } from '../../styles/text'

const styles = StyleSheet.create({
    container: {
        padding: 4,
        flex: 1
    }
})

export const CardHeader = (props) => {
    return (
        <View {...props}>
            <Text style={[fontFamily.bold, sizes.normal]}>{props.title}</Text>
            {props.subtitle && <Text style={[fontFamily.semibold, sizes.small, fontColors.lightDark]}>{props.subtitle}</Text>}
        </View>
    )
}

export const InputLabel = ({ label, style = {} }) => {
    return (
        <Text style={[{ fontFamily: FontFamilies.quicksandbold.fontFamily }, sizes.small, fontColors.muted, { marginBottom: 4, ...style }]}>{label}</Text>
    )
}
class _CategoryForm extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {}
    }
    save = () => {
        if (this.state.isBusy) {
            return false;
        }
        if (!this.state.text || !this.props.selectedIcon || !this.props.selectedIcon.icon) {
            Toast.show({
                text: 'Please enter name for this category',
                type: 'danger'
            })
        } else {
            this.setState({ isBusy: true });
            CreateCategory({ icon: this.props.selectedIcon.icon, name: this.state.text, icon_type: this.props.selectedIcon.icon_type }).then((category) => {
                this.setState({ isBusy: false });
                this.props.reload();
                this.props.navigation.pop();
                Toast.show({
                    text: 'Category saved',
                    duration: 1200,
                    type: 'success',
                })
            })
        }
    }
    CategoryCardFooter = () => {
        return (
            <View style={{ justifyContent: 'center', padding: 16 }}>
                <Button appearance={this.state.isBusy ? 'ghost' : 'filled'} onPress={this.save}>{this.state.isBusy ? 'Saving...' : 'Save'}</Button>
            </View>
        )
    }
    setText(text: string) {
        this.setState({ text })

    }
    render() {

        return (
            <Layout style={{ flex: 1 }} level="4">
                <BackNav navigation={this.props.navigation} title="Create new group"></BackNav>
                <Layout style={styles.container} level="4" >

                    <Card footer={this.CategoryCardFooter} header={(props) => <CardHeader {...props} title="Add new category"></CardHeader>} style={{ elevation: 20 }}>

                        <View>
                            <Input onChangeText={(text) => this.setText(text)} caption="Enter a name for this category, example : Productivity" label={() => <InputLabel label="Category Name"></InputLabel>}></Input>
                            <IconSelector {...this.props}></IconSelector>


                        </View>
                    </Card>

                </Layout>
            </Layout>

        )
    }
}
const mapStateToProps = (state) => ({
    selectedIcon: state.selectedIcon
})
const mapDispatchToProps = (dispatch) => {
    return {
        reload: () => dispatch(RequestCategoryAsync())
    }
}

export const CategoryForm = connect(mapStateToProps, mapDispatchToProps)(_CategoryForm);
