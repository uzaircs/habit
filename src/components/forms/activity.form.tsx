import React, { Component } from 'react'
import { View } from 'react-native';
import { Select, SelectItem, Input, Card, Layout, Button } from '@ui-kitten/components';
import { InputLabel } from './category';
import { connect } from 'react-redux'
import { IconSelector } from './icon.selector';
import { getCategories, CreateActivity } from '../../models/api';
import { BackNav } from '../../menu/header';
import { Toast, Text } from 'native-base';
import { default as theme } from '../../styles/app-theme.json'
import { RequestCategoryAsync } from '../../actions/category.action';
import { fontFamily, fontColors, sizes } from '../../styles/fonts';
import { CenterLoading } from '../activity.page';
class _CreateActivityForm extends Component<any, any>{
    constructor(props) {
        super(props);
        this.state = {
            categories: [],
            selectedIndex: 0,
            activity_name: ''
        };

    }
    validate() {
        if (!this.props.categories.length || this.state.selectedIndex < 0 || !this.props.categories[this.state.selectedIndex - 1] || !this.props.categories[this.state.selectedIndex - 1].id) {
            Toast.show({
                text: 'Please select a category for this activity',
                duration: 1200,
                type: 'danger'
            })
            return false;
        }
        if (!this.state.activity_name) {
            Toast.show({
                text: 'Please enter an activity name',
                duration: 1200,
                type: 'danger'
            })
            return false;
        }
        if (!this.props.selectedIcon || !this.props.selectedIcon.icon || !this.props.selectedIcon.icon_type) {
            Toast.show({
                text: 'Please choose an icon for this activity',
                duration: 1200,
                type: 'danger'
            })
            return false;
        }
        return true;
    }
    save() {
        this.setState({ saving: true });
        try {
            if (this.validate()) {
                let id = this.props.categories[this.state.selectedIndex - 1].id;
                /* let title = this.state.categories[this.state.selectedIndex - 1].title; */
                let icon = this.props.selectedIcon.icon;
                let icon_type = this.props.selectedIcon.icon_type;
                let activity_name = this.state.activity_name;
                CreateActivity({
                    category_id: id,
                    icon: icon, icon_type, name: activity_name
                }).then((result) => {
                    Toast.show({
                        text: 'Activity saved successfully',
                        type: 'success',
                        duration: 1000
                    })
                    if (this.props.updateList && typeof this.props.updateList === 'function') {

                    }
                    this.props.updateList();
                    this.setState({ saving: false });
                    this.props.navigation.pop();
                })
            } else {
                this.setState({ saving: false });
            }

        } catch (error) {
         
            this.setState({ saving: false });

        }

    }

    categorySelected = index => {
        let selectedCategory = this.props.categories[index - 1].title;
        this.setState({ selectedIndex: index, selectedCategory });
    }
    componentDidMount() {
        try {
            const { id } = this.props.route.params;
            if (id) {
                let index = this.props.categories.findIndex(category => category.id === id);
                if (index > -1) {
                    this.categorySelected(index + 1)
                }
            }
        } catch (error) {

        }
    }
    componentDidUpdate() {

    }
    CardFooter = ({ saving }) => {
        if (!saving) {
            return (
                <View style={{ padding: 16 }}>
                    <Button appearance="outline" onPress={() => this.save()}>Save</Button>
                </View>
            )
        } else {
            return (
                <View style={{ padding: 16 }}>
                    <Button appearance="ghost" status="basic">Saving...</Button>
                </View>
            )
        }
    }
    render() {
        if (!this.props.hasItems) {
            return <CenterLoading></CenterLoading>
        }
        return (
            <Layout style={{ flex: 1 }} level="4">
                <BackNav navigation={this.props.navigation} title="Create new activity"></BackNav>
                <View style={{ padding: 16 }}>
                    <View style={{ justifyContent: 'flex-end', width: '100%', flexDirection: 'row' }}>
                        <View style={{ width: 148 }}>
                            <Button size="small" appearance="ghost" onPress={() => this.props.navigation.navigate('addCategory')} children={(props) => <Text {...props} style={[fontFamily.semibold, sizes.small, { textTransform: 'uppercase', color: theme["color-primary-600"] }]}>Add new group</Text>} />
                        </View>
                    </View>
                    <Card footer={() => this.CardFooter({ saving: this.state.saving })}>
                        <Select
                            style={{ marginBottom: 16 }}
                            value={this.state.selectedCategory}
                            label={() => <InputLabel label="Select Category"></InputLabel>}
                            caption="A category or group is where this new activity will belong"
                            selectedIndex={this.state.selectedIndex}
                            onSelect={this.categorySelected}>
                            {this.props.categories.map(category => {
                                return (
                                    <SelectItem title={category.title}></SelectItem>
                                )
                            })}
                        </Select>
                        <Input
                            onChangeText={(text) => this.setState({ activity_name: text })}
                            label={() => <InputLabel label="Activity Name"></InputLabel>}
                        >
                        </Input>
                        <IconSelector {...this.props}></IconSelector>
                    </Card>
                </View>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    selectedIcon: state.selectedIcon,
    categories: state.categories.items.map(category => {
        return {
            title: category.name,
            id: category.id
        }
    }),
    hasItems: state.categories.hasItems
})
const mapDispatchToProps = (dispatch) => {
    return {
        updateList: () => dispatch(RequestCategoryAsync())
    }
}

export const CreateActivityForm = connect(mapStateToProps, mapDispatchToProps)(_CreateActivityForm);
