import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Divider, Layout, ListItem } from '@ui-kitten/components'
import { SafeAreaView, FlatList, StyleSheet, StatusBar } from 'react-native'
import DiaryEntry from './diary.entry';
import { EmptyListPlaceholder } from '../day.past';
import { Row, Col } from 'react-native-easy-grid';
import { View } from 'native-base';
import { H2 } from '../../styles/text';
import { AddButton } from '../goals/list';
import { SortButton, FilterButton } from '../task/list';
import { NavigationContext, useIsFocused } from '@react-navigation/native';
import { RequestDiaryListAsync } from '../../actions/diary.action';
import { FingerPrint } from '../../auth/touch.id';
import { CenterLoading } from '../activity.page';
import { DEFAULT_MARGIN } from '../../common/constants';

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 32,
    },
});

export class DiaryList extends Component<any, any>{
    /**
     *
     */
    static contextType = NavigationContext;
    constructor(props) {
        super(props);
        this.state = {
            auth: true,
        }
    }
    renderItem = ({ item }) => {
        return (
            <DiaryEntry item={item}></DiaryEntry>
        );
    }
    diaryHeader = () => {
        const navigation = this.context;
        return (
            <Row>
                <Col>
                    <View style={{ backgroundColor: 'white', padding: 16, flex: 1 }}>
                        <H2 text="Entries List"></H2>
                    </View>
                </Col>
                <Col>
                    <View style={{ padding: 16, backgroundColor: 'white', justifyContent: 'flex-end', flexDirection: 'row' }}>
                        <AddButton onPress={() => { navigation.navigate('addDiaryEntry') }}></AddButton>
                        <SortButton></SortButton>
                        <FilterButton></FilterButton>
                    </View></Col>
            </Row>
        );
    }
    componentDidMount() {
        /*  const navigation = this.context;
         FingerPrint.start().then((ready) => {
             if (ready) {
 
                 FingerPrint.authenticate().then(result => {
                     if (result.success) {
                         this.setState({ auth: true })
                     }
                 }).catch(error => {
                     navigation.pop();
                 })
             } else {
                 this.setState({ auth: true })
             }
         }).catch(err => {
             this.setState({ auth: true })
         }) */
        if (this.props.update && typeof this.props.update === 'function') {
            this.props.update();
        }
    }
    render() {
        const separator = () => {
            return (
                <Divider style={{ marginTop: DEFAULT_MARGIN / 4, marginBottom: DEFAULT_MARGIN / 4 }}></Divider>
            )
        }
        if (!this.state.auth) { return <CenterLoading></CenterLoading> };
        return (
            <Layout style={{ flex: 1 }} level="3">
                <SafeAreaView style={styles.container}>
                    <FlatList
                        keyboardShouldPersistTaps='always'
                        ListHeaderComponent={this.diaryHeader}
                        ListEmptyComponent={EmptyListPlaceholder}
                        data={this.props.entries}
                        renderItem={this.renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        stickyHeaderIndices={[0]}

                        ItemSeparatorComponent={separator}
                    />
                </SafeAreaView>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    entries: state.diary.entries
})/*  */

const mapDispatchToProps = dispatch => {
    return {
        update: dispatch(RequestDiaryListAsync())
    }
}
const DiaryListFocusRenderOnly = (props) => {
    return <DiaryList {...props}></DiaryList>
    /*     const isFocused = useIsFocused();
        if (!isFocused) {
            return <View />
        } */
}
/* export default connect(mapStateToProps, mapDispatchToProps)(DiaryList) */
export default connect(mapStateToProps, mapDispatchToProps)(DiaryListFocusRenderOnly)
