import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Input, Card, Button, Select, SelectItem, Text, ListItem } from '@ui-kitten/components'
import { DiaryEntryAdded, DiaryEntryUpdated } from '../../actions/diary.action'
import { BackNav } from '../../menu/header'
import { ScrollView, TouchableNativeFeedback, TouchableOpacity } from 'react-native'
import { InputLabel } from '../forms/category'
import { CapitalDayFromDate, WeekdayFromDate, FullMonthAndYearFromDate, TimeFromDate } from './diary.entry'
import { View } from 'native-base'
import { Row, Col } from 'react-native-easy-grid'
import { PlusOutlineIcon, ButtonText, AutoApplyIcon } from '../activity'
import { CenterLoading } from '../activity.page'
import moment from 'moment'
import { iOSUIKit } from 'react-native-typography'
import { getMoods } from '../../models/api'
import { DEFAULT_MARGIN, PADDING } from '../../common/constants'
import { FontFamilies, H4, TextMuted } from '../../styles/text'
import { EmojiActions, EmojiItem } from '../day'
import { ColorIndicator, TaskListPicker } from '../tasklist/picker'
import { ListIcon } from '../task/details'
import { default as theme } from '../../styles/app-theme.json'
export const EntryHeader = ({ date }) => {
    return (
        <Row size={12}>
            <Col size={1} style={{ justifyContent: 'center' }}>
                <CapitalDayFromDate date={date}></CapitalDayFromDate>
            </Col>
            <Col size={8} style={{ justifyContent: 'center', marginLeft: 8 }}>
                <Row>
                    <WeekdayFromDate muted date={date} full={true}></WeekdayFromDate>
                </Row>
                <Row>
                    <FullMonthAndYearFromDate muted date={date}></FullMonthAndYearFromDate>
                </Row>
            </Col>
            <Col size={4} style={{ justifyContent: 'center', alignItems: 'flex-end' }}>
                <TimeFromDate date={date}></TimeFromDate>
            </Col>
        </Row>
    )
}
export class AddDiaryEntry extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            initiatedAt: new Date(),
            ready: false
        }
    }
    componentDidMount() {

        setTimeout(() => {
            let editMode = false;
            let title = '';
            let text = '';
            let _id = '';
            let initiatedAt = new Date();
            let item = null;
            try {
                let { id } = this.props.route.params;

                if (id) {
                    editMode = true;
                    _id = id;
                    let index = this.props.entries.findIndex(entry => entry.id === id);
                    if (index > -1) {
                        item = this.props.entries[index];
                        if (item.initiatedAt && !isNaN(item.initiatedAt)) {
                            let unix = moment.unix(parseInt(item.initiatedAt) / 1000).toDate();
                            initiatedAt = unix;
                        }
                        title = item.title;
                        text = item.text;
                    }
                }
            } catch (error) {

            }
            getMoods().then(moods => {
                this.setState(() => {
                    return {
                        moods, ready: true, item: {
                            title, text, initiatedAt, id: _id
                        }, editMode
                    }
                })

            })


        }, 200);
    }
    save() {
        let mood_id = null;
        if (!this.state.editMode) {
            this.props.add({
                title: this.state.item.title,
                text: this.state.item.text,
                initiatedAt: this.state.item.initiatedAt,
                mood: { id: this.state.selectedMoodId },
                list: { id: this.state.selectedList?.id },
            })
        } else {
            this.props.update({
                id: this.state.item.id,
                status: this.state.item.status,
                mood: { id: this.state.selectedMoodId },
                title: this.state.item.title,
                text: this.state.item.text,
                initiatedAt: this.state.item.initiatedAt,
                list: { id: this.state.selectedList?.id },
            })
        }
        this.props.navigation.pop();
    }
    MoodSelected = mood_id => {
        if (this.props.moods && Array.isArray(this.props.moods) && this.props.moods.length) {
            let index = this.props.moods.findIndex(m => m.id === mood_id);
            if (index > -1) {
                let emoji = this.props.moods[index].emoji;
                this.setState(() => { return { showMoods: false, selectedMoodId: mood_id, emoji } })

            }
        }


    }
    listSelected = (item) => {
        this.setState((state, props) => { return { listDisplay: item.name, selectedList: item } })

    }
    render() {
        if (!this.state.ready) {
            return <CenterLoading></CenterLoading>
        }
        return (
            <Layout style={{ flex: 1 }} >
                <BackNav navigation={this.props.navigation} title={this.state.editMode ? 'Entry' : 'Add Entry'}></BackNav>
                <ScrollView>
                    <View >
                        <TouchableNativeFeedback>
                            <View style={{ flex: 1, padding: 16 }}>
                                <EntryHeader date={this.state.item.initiatedAt}></EntryHeader>
                            </View>
                        </TouchableNativeFeedback>
                    </View>
                    <View style={{ padding: 8 }}>

                        <Input
                            textStyle={iOSUIKit.body}
                            autoFocus={!this.state.editMode}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, title: text } })}
                            value={this.state.item.title}
                            label={() => <InputLabel label="Entry Title"></InputLabel>}
                        ></Input>
                        <Input
                            multiline={true}
                            numberOfLines={10}
                            textStyle={[iOSUIKit.body, { textAlignVertical: 'top', paddingTop: 8 }]}
                            style={{ textAlignVertical: 'top' }}
                            value={this.state.item.text}
                            onChangeText={(text) => this.setState({ item: { ...this.state.item, text } })}
                            label={() => <InputLabel label="Entry text"></InputLabel>}
                        ></Input>
                        <Text appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, }}>Mood</Text>
                        <View style={{ padding: PADDING / 2, paddingTop: 0 }}>

                            {(!this.state.showMoods && !this.state.selectedMoodId) && <ListItem onPress={() => this.setState((state, props) => { return { showMoods: true } })
                            }>
                                <Text
                                    appearance="hint"
                                    category="p1"
                                    style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>
                                    Tap to select a mood (Optional)
                            </Text>
                            </ListItem>}
                            {this.state.showMoods && <EmojiActions onSelect={this.MoodSelected}></EmojiActions>}
                            {(!this.state.showMoods && this.state.selectedMoodId) && <ListItem
                                onPress={() => this.setState((state, props) => { return { showMoods: true } })
                                }><EmojiItem emoji={this.state.emoji}></EmojiItem></ListItem>}
                        </View>



                        <View style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                            <Text appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>List</Text>
                            <TaskListPicker onSelected={(selected) => {

                                this.listSelected(selected);



                            }} selectView={({ showModal }) => {
                                if (this.state.listDisplay) {
                                    return (
                                        <ListItem onPress={showModal}>
                                            <View style={{ marginRight: 16 }}>
                                                <ColorIndicator color={this.state.selectedList.color}></ColorIndicator>
                                            </View>
                                            <View style={{ marginRight: 8 }}>
                                                <AutoApplyIcon small={true} defaultColor={this.state.selectedList.color} item={this.state.selectedList}></AutoApplyIcon>

                                            </View>
                                            <View>
                                                <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>{this.state.listDisplay}</Text>
                                            </View>
                                        </ListItem >
                                    )
                                }
                                return (
                                    <ListItem onPress={showModal}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <PlusOutlineIcon width={24} height={24}></PlusOutlineIcon>
                                            {this.state.listDisplay && <Text

                                                category="p1"
                                                style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>
                                                {this.state.listDisplay}
                                            </Text>}
                                            {!this.state.listDisplay && <Text
                                                appearance="hint"
                                                category="p1"
                                                style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>
                                                Tap to select a list (Optional)
                                            </Text>}
                                        </View>
                                    </ListItem>
                                )
                            }} />
                        </View>
                    </View>
                </ScrollView>
                <View style={{ flex: 1, flexDirection: 'column', width: '100%', position: 'absolute', bottom: 0 }}>
                    <Button style={{ borderRadius: 0 }} onPress={() => this.save()} accessoryLeft={PlusOutlineIcon} children={(props) => <ButtonText text="Save" {...props}></ButtonText>}></Button>
                </View>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    entries: state.diary.entries,
    moods: state.moods
})

const mapDispatchToProps = dispatch => {
    return {
        add: item => dispatch(DiaryEntryAdded(item)),
        update: item => dispatch(DiaryEntryUpdated(item))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDiaryEntry)
