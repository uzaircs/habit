import React, { Component } from 'react'
import { EmptyListPlaceholder } from '../day.past'
import { DiaryEntry } from './diary.entry'
import DiaryList from './diary.list'

export default class DiaryDay extends Component {
    render() {
        return (
            <DiaryList></DiaryList>
        )
    }
}
