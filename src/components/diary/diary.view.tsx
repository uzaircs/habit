import { Button, Card, Icon, Layout, Text } from '@ui-kitten/components/ui';
import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux'
import { DefaultText, FontFamilies, TextMutedStrong } from '../../styles/text';
import { BackNav } from '../../menu/header';
import { CenterLoading } from '../activity.page';
import { DEFAULT_DATE_FORMAT, PADDING } from '../../common/constants';
import { ButtonText } from '../activity';
import moment from 'moment';
import { Emoji } from '../../emoji/emoji';
import { TrashIcon, TrashIconProps } from '../task/details';
import { ListDisplay } from './diary.entry';
const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {
        flex: 1,
        margin: 2,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
});
export const ShareIcon = props => {
    return (
        <Icon  {...props} name="share-outline"></Icon>
    )
}
export const StarIcon = props => {
    return (
        <Icon  {...props} name="star-outline"></Icon>
    )
}
export class DiaryView extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {

    }

    render() {
        let { id } = this.props.route.params;

        const EditButton = () => {
            return (
                <Button onPress={() => this.props.navigation.navigate('addDiaryEntry', { id })} appearance="ghost" children={props => <ButtonText text="Edit" {...props} />}></Button>
            )
        }
        if (!this.props.entries) {
            return <CenterLoading></CenterLoading>;
        }

        try {

            let index = this.props.entries.findIndex(entry => entry.id === id);

            if (index > -1) {
                let entry = this.props.entries[index];
                let list = null;
                if (entry.list && entry.list.id) {
                    let listIndex = this.props.lists.findIndex(l => l.id === entry.list.id);
                    if (listIndex > -1) {
                        list = this.props.lists[listIndex];
                    }
                }
                const Header = (props) => (
                    <View {...props}>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flexDirection: 'column' }}>
                                <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category='h6'>{entry.title}</Text>
                                <Text style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }} category='s2'>{moment(entry.createdAt).format(DEFAULT_DATE_FORMAT)}</Text>
                            </View>
                            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'flex-end', flex: 1 }}>
                                {list && <ListDisplay list={list}></ListDisplay>}
                                {/*  <Emoji size={{ width: 24, height: 24 }} name="good"></Emoji> */}
                            </View>
                        </View>

                    </View>
                );
                const Footer = (props) => (
                    <View {...props} style={[props.style, styles.footerContainer]}>
                        <Button
                            style={styles.footerControl}

                            appearance="ghost"
                            accessoryLeft={ShareIcon}
                            status='basic' />
                        <Button
                            style={styles.footerControl}

                            appearance="ghost"
                            accessoryLeft={TrashIconProps}
                            status='basic' />
                        <Button
                            style={styles.footerControl}

                            appearance="ghost"
                            accessoryLeft={StarIcon}
                            status='basic' />

                    </View>
                );
                return (
                    <Layout style={{ flex: 1 }} level="4" >
                        <BackNav ac_right={EditButton} title={"View Entry"}></BackNav>
                        <View style={{ flex: 1, padding: PADDING / 2 }}>
                            <Card footer={Footer} header={Header}>
                                <View style={{ flexDirection: 'row' }}>

                                </View>
                                <Text style={{ fontFamily: FontFamilies.quicksandmedium.fontFamily }} category="p1">{entry.text}</Text>
                            </Card>
                        </View>
                    </Layout>
                )

            }

        } catch (error) {
            console.log(error);
            this.props.navigation.pop();
            return <></>;
        }
        return <CenterLoading></CenterLoading>
    }
}

const mapStateToProps = (state) => ({
    entries: state.diary.entries,
    lists: state.list.items,
    moods: state.moods
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(DiaryView)
