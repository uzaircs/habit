import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'native-base'
import { Row, Col } from 'react-native-easy-grid'
import { DefaultText, H2, H4, TextMuted, TextSubtitle, TextSmall, FontFamilies, TextMutedStrong } from '../../styles/text'
import moment from 'moment'
import { TouchableNativeFeedback } from 'react-native'
import { CenterLoading } from '../activity.page'
import { NavigationContext } from '@react-navigation/native';
import { Icon, Text } from '@ui-kitten/components'
import { AutoApplyIcon } from '../activity'
import { DEFAULT_MARGIN } from '../../common/constants'
import list from '../goals/list'
import { default as theme } from '../../styles/app-theme.json'
export const MoodMaterialIcon = ({emoji})=>{
    switch(emoji){
        
    }
}
export const WeekdayFromDate = ({ date, full = false, muted = false }) => {
    if (moment(date).isValid()) {
        let day = moment(date).format('ddd');
        if (full) {
            day = moment(date).format('dddd');
        }
        if (muted) {
            return <TextSmall text={day}></TextSmall>
        }
        return <DefaultText text={day} style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}></DefaultText>
    } else {
        return <DefaultText text="INV"></DefaultText>
    }
}
export const FullMonthAndYearFromDate = ({ date, muted = false }) => {
    if (moment(date).isValid()) {
        let day = moment(date).format('MMMM YYYY').toUpperCase();

        if (muted) {
            return <TextMuted text={day}></TextMuted>
        }
        return <DefaultText text={day}></DefaultText>
    } else {
        return <DefaultText text="INV"></DefaultText>
    }
}
export const TimeFromDate = ({ date }) => {
    if (moment(date).isValid()) {
        let day = moment(date).format('hh:mm A');
        return <TextMutedStrong style={{ fontSize: 12 }} text={day}></TextMutedStrong>
    } else {
        return <TextMutedStrong text="INV"></TextMutedStrong>
    }
}
export const MoodDisplay = ({ mood }) => {
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon style={{ marginRight: DEFAULT_MARGIN / 4 }} name="smiling-face-outline" width={16} height={16} fill={theme['color-font-300']} />
            <Text appearance="hint" category="s2" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{mood.title}</Text>
        </View>
    )
}
export const ListDisplay = ({ list }) => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={{ marginRight: DEFAULT_MARGIN / 4 }}>
                <AutoApplyIcon defaultColor={list.color} small item={{ icon: list.icon, icon_type: list.icon_type }}></AutoApplyIcon>
            </View>
            <Text appearance="hint" category="s2" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{list.name}</Text>
        </View>
    )
}
export const CapitalDayFromDate = ({ date }) => {
    if (moment(date).isValid()) {
        let day = moment(date).format('DD').toUpperCase();
        return <H2 text={day}></H2>
    } else {
        return <H2 text="INV"></H2>
    }
}
/**
 * Date
 * Title
 * Text
 * mood
 * tags
 * 
 */
export class DiaryEntry extends Component<any, any>{
    static contextType = NavigationContext;
    constructor(props) {
        super(props);
        this.state = {

        }

    }
    componentDidMount() {
        if (this.props.item) {
            let list = null;
            let mood = null;
            if (this.props.item.list && this.props.item.list.id) {
                if (this.props.lists && Array.isArray(this.props.lists)) {
                    let index = this.props.lists.findIndex(l => l.id === this.props.item.list.id);
                    if (index > -1) {
                        list = this.props.lists[index];
                    }
                }
            }
            if (this.props.item.mood && this.props.item.mood.id) {
                if (this.props.moods && Array.isArray(this.props.moods)) {
                    let index = this.props.moods.findIndex(m => m.id === this.props.item.mood.id);
                    if (index > -1) {
                        mood = this.props.moods[index];
                    }
                }
            }
            this.setState({ ready: true, mood, list });

        }
    }
    render() {
        const navigation = this.context;
        if (!this.state.ready) {
            return <CenterLoading></CenterLoading>;
        }
        let date = this.props.item.initiatedAt;
        if (date && !isNaN(date)) {
            date = moment.unix(date / 1000).toDate();
        }
        return (
            <TouchableNativeFeedback onPress={() => navigation.navigate('diaryView', { id: this.props.item.id })}>
                <View style={{ height: 96, padding: 8, backgroundColor: '#fff', position: 'relative' }}>
                    {this.state.list && <View style={{ position: 'absolute', left: 0, top: 0, height: 96, width: 4, backgroundColor: this.state.list.color, }}>

                    </View>}
                    <Row size={5}>
                        <Col size={1} style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <WeekdayFromDate date={date}></WeekdayFromDate>
                            <CapitalDayFromDate date={date} />
                            <TimeFromDate date={date} />
                        </Col>
                        <Col size={4}>
                            <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} category="s1" numberOfLines={1}>{this.props.item.title}</Text>
                            <Text appearance="hint" category="p2" style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }} numberOfLines={2}>{this.props.item.text}</Text>
                            <View style={{ flexDirection: 'row', marginTop: DEFAULT_MARGIN / 2, alignItems: 'center' }}>
                                {this.state.list && <ListDisplay list={this.state.list}></ListDisplay>}
                                {this.state.mood && <Text appearance="hint" category="label"> | </Text>}
                                {this.state.mood && <MoodDisplay mood={this.state.mood} />}
                            </View>
                        </Col>
                    </Row>

                </View>
            </TouchableNativeFeedback>
        )
    }
}

const mapStateToProps = (state) => ({
    moods: state.moods,
    lists: state.list.items
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(DiaryEntry)
