import React, { Component } from 'react'
import { connect } from 'react-redux'
import PickerModal from 'react-native-picker-modal-view';
import { ListItem, Button } from '@ui-kitten/components';
import { H4 } from '../../styles/text';
import { UpdateListAsync } from '../../actions/list.action';
import { AutoApplyIcon, ButtonText, PlusOutlineIcon } from '../activity';
import { View } from 'native-base';
import { AttachAction } from '../../actions/task.action';
import { DotCircleIcon } from '../../main';
export const ColorIndicator = ({ color }) => {
    return (

        <DotCircleIcon style={{ fontSize: 10, color: color }} />
    )
}
class _TaskListPicker extends Component<any, any>{
    constructor(props) {
        super(props);

    }
    private onClosed(): void {

    }
    componentDidMount() {
        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request();
        }
    }
    ListHeaderComponent = () => {
        return (
            <View style={{ height: 56 }}>
                <Button accessoryLeft={PlusOutlineIcon} appearance="ghost" children={(props) => <ButtonText text="Add New" {...props}></ButtonText>}></Button>
            </View>
        )
    }
    private onSelected(selected: any): void {
        this.setState({ selectedItem: selected });
        const selectedListEntry = {
            name: selected.Name,
            id: selected.Id,
            icon: selected.icon,
            icon_type: selected.icon_type,
            color: selected.color
        }
        if (this.props.task && this.props.task.id) {
            this.props.attach(this.props.task.id, {
                name: selected.Name,
                id: selected.Id,
                icon: selected.icon,
                icon_type: selected.icon_type,
                color: selected.color
            });
        }

        this.props.onSelected(selectedListEntry);
        return selected;
    }

    private onBackButtonPressed(): void {

    }

    private renderItem = (selectedItem, item) => {

        return (
            <ListItem disabled={true}>
                <View style={{ marginRight: 16 }}>
                    <ColorIndicator color={item.color}></ColorIndicator>
                </View>
                <View style={{ marginRight: 8 }}>
                    <AutoApplyIcon small={true} defaultColor={item.color} item={item}></AutoApplyIcon>

                </View>
                <View>
                    <H4 text={item.Name}></H4>
                </View>
            </ListItem >
        )
    }
    render() {
        return (

            <PickerModal
                renderSelectView={(disabled, selected, showModal) =>
                    this.props.selectView({ disabled, selected, showModal })
                }
                FlatListProps={
                    {
                        ListHeaderComponent: this.ListHeaderComponent(),
                        stickyHeaderIndices: [0]

                    }
                }
                renderListItem={this.renderItem}
                onSelected={this.onSelected.bind(this)}
                onClosed={this.onClosed.bind(this)}
                onBackButtonPressed={this.onBackButtonPressed.bind(this)}
                items={this.props.list}
                sortingLanguage={'en'}
                showToTopButton={true}
                selected={this.props.selectedItem}

                autoGenerateAlphabeticalIndex={true}
                selectPlaceholderText={'Choose one...'}
                onEndReached={() => console.log('list ended...')
                }
                searchPlaceholderText={'Search...'}
                requireSelection={false}
                autoSort={false}
            />

        );
    }
}

const mapStateToProps = (state) => ({
    list: state.list.items.map(selectItem => {
        return {
            Id: selectItem.id,
            Name: selectItem.name,
            Value: selectItem.id,
            key: selectItem.id,
            icon: selectItem.icon,
            icon_type: selectItem.icon_type,
            color: selectItem.color,
        }
    })
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateListAsync()),
        attach: (id, list) => dispatch(AttachAction(id, list))
    }
}

export const TaskListPicker = connect(mapStateToProps, mapDispatchToProps)(_TaskListPicker)
