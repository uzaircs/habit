import React, { Component } from 'react'
import { connect } from 'react-redux'
import Timeline from 'react-native-timeline-flatlist'
import { Layout, Icon } from '@ui-kitten/components';
import { BackNav } from '../../menu/header';
import { View, TouchableNativeFeedback } from 'react-native';
import { iOSUIKit } from 'react-native-typography';
import { fontColors } from '../../styles/fonts';
import { RequestLogs } from '../../actions/timeline.action';
import moment from 'moment'
import { default as theme } from '../../styles/app-theme.json'
import { Placeholder, Shine, PlaceholderMedia, PlaceholderLine } from 'rn-placeholder';
import { DefaultText, H4, TextMuted, TextSmall } from '../../styles/text';
import { ParseAction, TimelineActionTypes } from '../../models/timelinelogs';
import { Icon as NBIcon } from 'native-base'
const LogActionIcon = ({ action }) => {
    switch (action) {
        case TimelineActionTypes.TASK_CREATED:
        case TimelineActionTypes.DIARY_ENTRY:

            return (
                <Icon name="plus-circle-outline" width={20} height={20} fill={fontColors.lightDark.color} />

            )
        case TimelineActionTypes.MOOD_ADDED:
            return (
                <Icon name="smiling-face-outline" width={20} height={20} fill={fontColors.lightDark.color} />

            )
        case TimelineActionTypes.TASK_COMPLETED:
            return (
                <Icon name="checkmark-circle-outline" width={20} height={20} fill={theme["color-success-500"]} />

            )
        case TimelineActionTypes.TASK_INCOMPLETED:
            return (
                <Icon name="close-square-outline" width={20} height={20} fill={fontColors.lightDark.color} />

            )
        default:
            return (
                <Icon name="clock-outline" width={16} height={16} fill={fontColors.muted.color} />
            )

    }

}
export class TimelineList extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {};

    }
    componentDidMount() {
        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request();
        }
    }
    loadingState = () => {
        return (
            <Placeholder
                Animation={Shine}
                Left={PlaceholderMedia}

            >
                <PlaceholderLine />
                <PlaceholderLine width={30} />
            </Placeholder>
        )
    }
    renderDetail(rowData, sectionID, rowID) {
        let actionType = ParseAction(rowData.title);

        let title = <H4 text={actionType}></H4>
        let time = moment(rowData.__time).format('h:mm A');
        let timeDisplay = <TextSmall text={time}></TextSmall>
        var desc = <TextMuted text={rowData.description} style={{ marginTop: 4 }}></TextMuted>
        return (
            <TouchableNativeFeedback>
                <View style={{ flex: 1, minHeight: 78 }}>

                    <View style={{ top: -12 }}>
                        {timeDisplay}
                        {title}
                        {desc}
                    </View>
                </View>
            </TouchableNativeFeedback>
        )

    }
    render() {
        if (!this.props.data) {
            return this.loadingState();
        }
        return (
            <Layout style={{ flex: 1 }}>
                <BackNav navigation={this.props.navigation} title="Your Timeline"></BackNav>
                <View style={{ padding: 8, flex: 1, paddingTop: 16 }}>
                    <Timeline
                        renderDetail={this.renderDetail}
                        lineColor={theme["color-primary-100"]}
                        dotColor={theme["color-primary-100"]}
                        circleColor="white"

                        innerCircle="icon"

                        titleStyle={iOSUIKit.bodyEmphasized}
                        timeStyle={[iOSUIKit.subheadEmphasized, { color: fontColors.muted.color }]}
                        descriptionStyle={iOSUIKit.callout}
                        data={this.props.data}
                    />
                </View>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.logs.map(log => {
        return {
            time: moment(log.createdAt).format('MMM D'),
            title: log.action,
            __time: log.createdAt,
            description: log.actionDescription,
            icon: <LogActionIcon action={log.action}></LogActionIcon>
        }
    })
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(RequestLogs())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineList)
