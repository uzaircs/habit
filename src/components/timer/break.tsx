import { Button, Layout } from '@ui-kitten/components'
import { View } from 'native-base'
import React from 'react'
import { connect } from 'react-redux'
import { DEFAULT_MARGIN } from '../../common/constants'
import { H1Large } from '../../styles/text'
import { ButtonText } from '../activity'
import LottieView from 'lottie-react-native';
export const TimerCompleted = (props) => {
    return (
        <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
            <LottieView style={{ width: 256 }} renderMode="HARDWARE" source={require('../../images/stopwatch.json')} autoPlay loop={true} />
            <View style={{ marginBottom: 32, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: DEFAULT_MARGIN * 2 }}>
                <H1Large text="Timer Completed"></H1Large>
                <Button onPress={props.onPress} style={{ marginTop: DEFAULT_MARGIN }} status="primary" appearance="ghost" children={props => <ButtonText {...props} text="Start Break" />} />
            </View>
        </Layout>
    )
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(TimerCompleted)
