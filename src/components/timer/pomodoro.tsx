import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, } from 'native-base'
import { H2, H4, DefaultText, TextMuted, H1Large, FontFamilies } from '../../styles/text'
import { Layout, Button, Icon, Card, Modal, ListItem, Divider, Text } from '@ui-kitten/components'
import { fontFamily, sizes, } from '../../styles/fonts';
import { default as theme } from '../../styles/app-theme.json'
import { SessionManager } from './session'
import { Ticker } from './ticker'
import { Dimensions, FlatList, ScrollView, StatusBar, TouchableNativeFeedback } from 'react-native'
import { Row, Col } from 'react-native-easy-grid'
import { ButtonText } from '../activity'
import { NotificationService } from '../../modules/NotificationManager'
import { CircularProgress } from './circular';
import { activateKeepAwake, deactivateKeepAwake } from 'expo-keep-awake';
import { SaveTimer, ClearTicker, RetreiveTimer, SaveSession, SaveEvents, RetrieveSession, RemoveDuplicateTasks } from '../../models/api'
import { DEFAULT_MARGIN, MENU_HEIGHT_COLLAPSED, PADDING } from '../../common/constants'
import { BackNav } from '../../menu/header'
import ModalBox from 'react-native-modalbox';
import { CheckIcon, CloseIcon } from '../task/list'
import LottieView from 'lottie-react-native';
import { SoundManager } from '../../modules/SoundManager'
import { CountdownCircleTimer } from 'react-native-countdown-circle-timer'
export const Timer = ({ isAbstract = false, hours = null, minutes, seconds, small = false, started = false, initial, onCompleted }) => {
    if (minutes < 0 || initial < 0) {
        if (onCompleted && typeof onCompleted === "function") {
            onCompleted();
        }
        return <View />
    }
    let initialRemainingTime = minutes * 60;
    console.log(`initial = ${initial * 60}`)
    const children = ({ remainingTime }) => {

        const __minutes = Math.floor(remainingTime / 60)
        const seconds = remainingTime % 60
        let text = `${__minutes}:${seconds}`;
        return <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-500'] }}>Time remaining</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text category="h1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{__minutes}</Text>
                <Text category="h4" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-500'] }}>:</Text>
                <Text appearance="hint" category="h4" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{seconds}</Text>
            </View>
            {/*    <Text category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-500'] }}>Minutes</Text> */}
        </View>
    }
    return (
        <CountdownCircleTimer
            onComplete={() => onCompleted ? onCompleted() : (() => { })()}
            key={initialRemainingTime}
            strokeWidth={8}
            size={200}
            initialRemainingTime={initialRemainingTime}
            isPlaying={started}
            children={children}
            duration={initial * 60}
            colors={[
                [theme['color-success-500'], 0.25],
                [theme['color-primary-500'], 0.50],
                [theme['color-warning-500'], 0.15],
                [theme['color-danger-500'], 0.10],

            ]}></CountdownCircleTimer>

    )
}
const PauseIcon = (props) => (
    <Icon name='pause-circle'  {...props} />
);
const PlayIcon = (props) => (
    <Icon name='play-circle'  {...props} />
);
const StopIcon = (props) => (
    <Icon name='stop-circle'  {...props} />
);
export const TimerTaskSelector = ({ tasks, onSelect, list, onRemove, abstract }) => {
    let [show, setShow] = React.useState(false);
    const [selected, setSelected] = React.useState([]);
    const toggle = (item) => {
        let index = selected.findIndex(i => i.id === item.id);
        if (index !== -1) {
            let _selected = selected.filter(t => t.id != item.id);
            setSelected([..._selected])
        } else {
            let _selected = [...selected, { ...item }];
            setSelected([..._selected]);
        }
    }
    const add = () => {
        if (onSelect && typeof onSelect === "function") {
            onSelect(selected);
            onClose();
        }
    }
    const isSelected = id => {
        return selected.findIndex(i => i.id === id) > -1;
    }
    let modalRef = null;
    const onClose = () => {
        modalRef?.close();
    }
    const open = () => {
        modalRef?.open();
    }
    const onOpen = () => {

    }
    const ModalHeader = () => {
        return (
            <View style={{ height: MENU_HEIGHT_COLLAPSED, backgroundColor: '#fff', padding: DEFAULT_MARGIN / 2, justifyContent: 'center' }}>
                <Row style={{ maxHeight: 56 }}>
                    <Col style={{ justifyContent: 'center' }}>
                        <Row style={{ alignItems: 'center' }}>

                            <H2 text="Select Task(s)" style={{ color: theme["color-font-500"] }}></H2>
                        </Row>
                    </Col>
                    <Col style={{ justifyContent: 'center' }}>
                        <View style={{ alignItems: 'flex-end' }}>
                            <TouchableNativeFeedback onPress={onClose}>
                                <CloseIcon width={24} height={24} fill={theme["color-font-300"]} />
                            </TouchableNativeFeedback>
                        </View></Col>
                </Row>
            </View>
        )
    }
    if (!tasks || !tasks.length) {
        return (
            <View style={{ padding: DEFAULT_MARGIN }}>
                <ModalBox visible={show}
                    coverScreen={true}
                    style={{ zIndex: 999 }}
                    useNativeDriver={true}
                    ref={ref => { modalRef = ref }}
                    swipeToClose={true}
                    onClosed={onClose}
                    onOpened={onOpen}>

                    <Layout style={{ flex: 1 }} level="4">
                        <ModalHeader></ModalHeader>
                        <ScrollView style={{ padding: DEFAULT_MARGIN / 2 }}>
                            {list.filter(t => t.status == 0).map(item => {
                                return (
                                    <Card onPress={() => toggle(item)} key={item.id} style={{ marginBottom: DEFAULT_MARGIN / 4, minHeight: 64 }}>
                                        <Row>
                                            <Col>
                                                <DefaultText text={item.name} />
                                            </Col>
                                            <Col style={{ alignItems: 'flex-end' }}>
                                                {isSelected(item.id) && <CheckIcon color={theme["color-success-500"]}></CheckIcon>}
                                            </Col>
                                        </Row>
                                    </Card>
                                )
                            })}
                            <View style={{ height: DEFAULT_MARGIN * 2, justifyContent: 'center', alignItems: 'center', padding: DEFAULT_MARGIN }}>
                                <TextMuted text="End of list" />
                            </View>
                        </ScrollView>
                        <View style={{ padding: DEFAULT_MARGIN / 4 }}>
                            <Button onPress={add} children={props => <ButtonText {...props} text="Add" />} />
                        </View>
                    </Layout>
                </ModalBox>
                <Button onPress={() => open()} appearance="ghost" children={props => <ButtonText {...props} text="Select Tasks" />}></Button>
            </View>
        )
    }
    return (
        <TimerTaskScroller abstract={abstract} onRemove={onRemove} tasks={tasks}></TimerTaskScroller>
    )
}
export const PauseButton = ({ onPause }) => {
    return (
        <Button onPress={onPause} style={{ marginRight: 8, marginLeft: 8 }} appearance="ghost" status="primary" children={props => <ButtonText {...props} text="Pause"></ButtonText>} />
    )
}
export const PlayButton = ({ onPlay, text = "Resume" }) => {
    return (
        <Button onPress={onPlay} style={{ marginRight: 8, marginLeft: 8 }} appearance="ghost" status="primary" children={props => <ButtonText {...props} text={text} />} />
    )
}
export const StopButton = ({ onStop }) => {
    return (
        <Button onPress={onStop} style={{ marginRight: 8, marginLeft: 8 }} appearance="ghost" status="danger" children={props => <ButtonText {...props} text={'Cancel'} />} />
    )
}

const Spacer = () => {
    return (
        <View style={{ marginLeft: DEFAULT_MARGIN }}>

        </View>
    )
}
const _TimerTaskScroller = ({ tasks, onRemove, ...props }) => {
    const RenderTimerTaskItem = ({ item }) => {
        let goal_id = item.goal.id;
        let goal = null;
        if (goal_id) {
            try {
                let index = props.goals.findIndex(g => g.id === goal_id);
                if (index > -1) {
                    goal = props.goals[index];
                }
            } catch (error) {

            }
        }

        return (
            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-font-100"])} onPress={() => { }}>
                <View style={{ width: '100%', marginTop: DEFAULT_MARGIN / 4, borderRadius: 9, elevation: 0, backgroundColor: props.abstract ? '#000' : '#fff' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: PADDING }}>
                        <View style={{ flexDirection: 'column' }}>
                            {goal && <Text numberOfLines={1} category="label" appearance="hint" style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }}>{goal.name}</Text>}
                            <Text category="p1" appearance={props.abstract ? 'hint' : ''} style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{item.name}</Text>
                        </View>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => {
                            if (onRemove && typeof onRemove === "function") {
                                onRemove(item.id);
                            }
                        }}>
                            <View>
                                <CloseIcon fill={theme["color-font-400"]} width={20} height={20}></CloseIcon>
                            </View>
                        </TouchableNativeFeedback>
                    </View>



                </View >
            </TouchableNativeFeedback>
        )
    }
    if (tasks && Array.isArray(tasks) && tasks.length) {
        let height = Dimensions.get('window').height;
        return (
            <View style={{ width: '100%', paddingBottom: DEFAULT_MARGIN, height: height / 3 }}>
                <FlatList
                    data={tasks}
                    renderItem={RenderTimerTaskItem}
                    /*  horizontal={true} */
                    ItemSeparatorComponent={Spacer}
                    bounces={true}
                    alwaysBounceHorizontal={true}
                    showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}
                >

                </FlatList>
            </View>
        )
    } else {
        return <View />
    }
}
const mapStateToPropsTimerTasks = (state) => ({
    goals: state.goals.goals,
    lists: state.list.items
})

const mapDispatchToPropsTimerTasks = {

}

export const TimerTaskScroller = connect(mapStateToPropsTimerTasks, mapDispatchToPropsTimerTasks)(_TimerTaskScroller)
class _PomodoroTimer extends Component<any, any>{
    notificationService: any;
    constructor(props) {
        super(props);
        this.notificationService = NotificationService;
        let session = {
            config: {
                id: 'abc',
                longBreak: 15 * 60,
                longBreakAfter: 4,
                shortBreak: 5 * 60,
                time: 25 * 60,
            },
            limit: 4,
            tasks: []
        }
        let manager = new SessionManager(session);
        let next = manager.next();

        this.state = {
            ready: false,
            events: [next],
            abstract: false, session
        }
    }
    componentWillUnmount() {
        /*   StatusBar.setBackgroundColor(theme["color-primary-300"]) */
        deactivateKeepAwake();

        this.clearInterval();
    }
    checkFinished() {
        return (this.state.minutes < 1 && this.state.seconds < 1) || this.state.minutes < 0;
    }
    stop() {
        this.notificationService.cancel();
        this.clearInterval();
        ClearTicker().then(() => {
            this.setState({ finished: true });
        });
    }
    /**
     * Clear ticker from local storage and stop the timer.
     */
    finish(auto = 0) {
        if (auto) {
            SoundManager.ding();
            this.setState(() => { return { isAutoFinished: 1, finished: true } })

        } else {
            this.setState({ finished: true, isAutoFinished: 0 });
        }
        this.clearInterval();
        ClearTicker();
        /* this.notificationService.show(); */
    }
    /**
     * @description
     * Start the ticker and update minutes and seconds if not paused    
     * Uses the ticker in state instanceof Ticker
     * @see Ticker
     */
    start = () => {

        if (!this.state.started) {
            this.state.ticker.start();
            this.notificationService.cancel();
            this.notificationService.ongoing();
            activateKeepAwake();
        }

        let interval = setInterval(() => {

            this.state.ticker.tick();
            this.save();
            let minutes = Math.floor((this.state.ticker.remaining / 60));
            let seconds = (this.state.ticker.remaining - (minutes * 60))
            if (!this.state.ticker.isPaused) {
                this.setState(() => { return { minutes, seconds } })
            }
            if (this.checkFinished()) {
                this.finish(1);
            }
        }, 600)
        this.setState(() => { return { interval: interval, started: true } }, () => {
            this.saveSession();
        })



    }
    componentDidMount() {

        RetreiveTimer().then(t => {
            if (t) {
                RetrieveSession().then(session => {
                    if (session) {
                        if (session.tasks.length && this.state.session?.tasks?.length) {
                            let tasks = session.tasks.concat(this.state.session.tasks);
                            session.tasks = tasks;
                            this.setState(() => {
                                return { session, ticker: t, ready: true, started: true }
                            }, () => {
                                this.start();
                            })
                        } else {
                            this.setState(() => {
                                return { session, ticker: t, ready: true, started: true }
                            }, () => {
                                this.start();
                            })

                        }

                    }
                    else {
                        this.setState(() => { return { ticker: t, ready: true, started: true } }, () => {
                            this.start();
                        });
                    }
                }).catch(error => {

                    this.setState(() => { return { ticker: t, ready: true, started: true } }, () => {
                        this.start();
                    });
                })


            } else {
                this.prepareSession();



            }
        }).catch(e => {

            this.prepareSession();
        })

    }

    removeTaskFromSession = id => {
        if (this.state.session && this.state.session.tasks && Array.isArray(this.state.session.tasks) && this.state.session.tasks.length) {
            let filteredTasks = this.state.session.tasks.filter((task) => task.id !== id);
            this.setState(() => { return { session: { ...this.state.session, tasks: filteredTasks } } }, () => {
                this.saveSession();
            })
        }
    }
    prepareSession = async () => {

        let session = { ...this.state.session };
        try {
            let RouteTasks = this.props.route?.params?.tasks;
            let storageSession = await RetrieveSession()

            if (storageSession) {
                if (session.tasks?.length && storageSession.tasks?.length) {
                    storageSession.tasks = storageSession.tasks.concat(session.tasks);
                    session = { ...storageSession };
                } else {
                    session = { ...storageSession }
                }
            }
            if (RouteTasks && Array.isArray(RouteTasks) && RouteTasks.length) {
                let RouteTasksFetched = this.props.tasks.filter(sourceTask => RouteTasks.some(routeTaskId => sourceTask.id === routeTaskId));
                if (Array.isArray(session.tasks) && session.tasks.length) {
                    let merged = [...session.tasks, ...RouteTasksFetched];
                    session.tasks = merged;
                } else {
                    session.tasks = [...RouteTasksFetched];
                }
            }
        } catch (error) {
            console.log(error);
        }
        let minutes = Math.floor((this.state.session.config.time / 60));
        let seconds = (this.state.session.config.time - (minutes * 60))
        let ticker = new Ticker(this.state.events[0]);
        let allTasks = session.tasks;
        if (allTasks && Array.isArray(allTasks) && allTasks.length) {
            let uniqueTasks = RemoveDuplicateTasks(allTasks);
            session.tasks = uniqueTasks;
        }
        this.setState(() => { return { minutes, seconds, ready: true, ticker, session } }, () => {
            this.saveSession();
            console.log(this.state);
        })
    }
    /**
     * Save or update current ticker in storage
     */
    save() {
        try {
            SaveTimer(this.state.ticker);

        } catch (error) {

        }
    }
    /**
     * Pause or resume current ticker
     */
    TogglePause() {
        this.state.ticker.toggle();
        this.setState({ isPaused: !this.state.ticker.isPaused });
        this.save();
    }
    clearInterval() {
        if (this.state.interval) {
            clearInterval(this.state.interval);
        }
    }
    reset() {

    }
    render() {
        if (!this.state.ready) {
            return <View />;
        }
        if (this.state.clicked >= 0 && !this.state.finished) {
            this.finish();

        }
        let progress = this.state.minutes / (this.state.ticker.event.time / 60) * 100;
        if (isNaN(progress)) {
            progress = 100;
        }
        if (this.state.finished) {
            if (this.state.isAutoFinished) {
                return (
                    <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
                        <LottieView style={{ width: 256 }} renderMode="HARDWARE" source={require('../../images/stopwatch.json')} autoPlay loop={true} />
                        <View style={{ marginBottom: 32, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: DEFAULT_MARGIN * 2 }}>

                            <H1Large text="Timer Completed"></H1Large>
                            <Button style={{ marginTop: DEFAULT_MARGIN }} status="primary" appearance="ghost" children={props => <ButtonText {...props} text="Start Break" />} />
                        </View>
                    </Layout>
                )
            }
            return (
                <Layout style={{ flex: 1, position: 'relative' }}>
                    <BackNav title="Pomodoro Timer" />
                    {/*    <LottieView style={{ width: 196 }} renderMode="HARDWARE" source={require('../../images/error.json')} autoPlay loop={false} /> */}
                    <View style={{ marginBottom: 32, justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: DEFAULT_MARGIN * 2 }}>

                        <H1Large text="Timer Cancelled"></H1Large>
                        <View style={{ flexDirection: 'row' }}>
                            <Button onPress={() => { this.reset(); }} style={{ marginTop: DEFAULT_MARGIN }} status="primary" appearance="ghost" children={props => <ButtonText {...props} text="Start Break" />} />
                            <Button onPress={() => { this.reset(); }} style={{ marginTop: DEFAULT_MARGIN }} status="primary" appearance="ghost" children={props => <ButtonText {...props} text="Reset Timer" />} />
                        </View>
                    </View>
                </Layout>
            )
        }
        if (this.state.abstract && !this.state.finished) {
            return (
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000' }}>
                    <StatusBar backgroundColor='#000'></StatusBar>
                    <TimerTaskSelector abstract={this.state.abstract} onRemove={id => {
                        this.removeTaskFromSession(id);

                    }} list={this.props.tasks} tasks={this.state.session.tasks} onSelect={(tasks) => {
                        this.setState((state, props) => {
                            return {
                                session: {
                                    ...this.state.session,
                                    tasks: tasks
                                }
                            }
                        }, () => {
                            this.saveSession();
                        })


                    }}></TimerTaskSelector>
                    <View style={{ marginBottom: 32 }}>
                        <CircularProgress isAbstract={this.state.abstract} onActivate={() => {
                            this.setState((state, props) => { return { abstract: !this.state.abstract } })

                        }} max={this.state.ticker.event.time / 60} minutes={this.state.minutes} seconds={this.state.seconds} progress={progress}></CircularProgress>
                    </View>
                </View>
            )
        }
        return (
            <View style={{ flex: 1, position: 'relative' }}>
                <BackNav title="Work" ></BackNav>
                <Layout style={{ backgroundColor: '#F7F9FC' }} >

                    <TimerTaskSelector abstract={this.state.abstract} onRemove={id => {
                        this.removeTaskFromSession(id);

                    }} list={this.props.tasks} tasks={this.state.session.tasks} onSelect={(tasks) => {
                        this.setState((state, props) => {
                            return {
                                session: {
                                    ...this.state.session,
                                    tasks: tasks
                                }
                            }
                        }, () => {
                            this.saveSession();
                        })


                    }}></TimerTaskSelector>
                </Layout>

                <Divider></Divider>
                <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#F7F9FC' }}>
                    <Layout style={{ backgroundColor: '#F7F9FC', position: 'absolute', bottom: 0, left: 0, right: 0, width: '100%', height: DEFAULT_MARGIN * 4, padding: DEFAULT_MARGIN }} >
                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row' }}>
                            <TextMuted text="Pomodoro 1 of 4"></TextMuted>
                            <TextMuted text="|" style={{ marginLeft: DEFAULT_MARGIN }}></TextMuted>
                            <TextMuted text="25m" style={{ marginLeft: DEFAULT_MARGIN }}></TextMuted>
                            <TextMuted text="|" style={{ marginLeft: DEFAULT_MARGIN }}></TextMuted>
                            <TextMuted text="3 Tasks" style={{ marginLeft: DEFAULT_MARGIN }}></TextMuted>

                        </View>
                    </Layout>

                    <View style={{ marginBottom: 32 }}>
                        <CircularProgress isAbstract={this.state.abstract} onActivate={() => {
                            this.setState((state, props) => { return { abstract: !this.state.abstract } })

                        }} max={this.state.ticker.event.time / 60} minutes={this.state.minutes} seconds={this.state.seconds} progress={progress}></CircularProgress>
                    </View>
                    <Modal
                        visible={this.state.modalVisibility}
                        backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}
                        onBackdropPress={() => this.setState({ modalVisibility: false })}>
                        <Card disabled={true}>
                            <Text style={[fontFamily.semibold, sizes.normal, { color: theme["color-font-400"], marginBottom: 16 }]}>Are you sure you want to finish this pomodoro?</Text>
                            <Row size={12}>
                                <Col size={6} style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                                    <Button children={props => <ButtonText {...props} text="No" />} accessoryLeft={(props) => <Icon {...props} name="chevron-left-outline"></Icon>} appearance="ghost" status="primary" onPress={() => this.setState({ modalVisibility: false })} />

                                </Col>
                                <Col size={6} style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                    <Button children={props => <ButtonText {...props} text="Finish" />} size="small" appearance="outline" status="danger" onPress={() => this.setState({ modalVisibility: false })} />


                                </Col>
                            </Row>
                        </Card>
                    </Modal>

                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {!this.state.started && <PlayButton text="Start" onPlay={() => { this.start() }}></PlayButton>}
                        {(this.state.started && !this.state.ticker.isPaused) && <PauseButton onPause={() => { this.TogglePause() }}></PauseButton>}
                        {(this.state.started && this.state.ticker.isPaused) && <PlayButton onPlay={() => { this.TogglePause() }}></PlayButton>}

                        <StopButton onStop={() => {
                            this.stop();
                        }}></StopButton>
                    </View>
                </Layout>
            </View>
        )
    }
    saveSession() {
        SaveSession(this.state.session);
        SaveEvents(this.state.events);
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks,
    config: state.next?.config
})

const mapDispatchToProps = dispatch => {
    return {

    }
}

export const PomodoroTimer = connect(mapStateToProps, mapDispatchToProps)(_PomodoroTimer)
