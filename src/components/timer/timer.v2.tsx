import { Button, Divider, Layout, Text } from '@ui-kitten/components'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BackNav, styles } from '../../menu/header';
import { AddSessionEventAction, UpdateEventStatus } from '../../actions/session.action';
import { CenterLoading } from '../activity.page';
import { InteractionManager, ScrollView, StatusBar, TouchableNativeFeedback, View } from 'react-native';
import { SessionEventStatus, SessionEventType, SessionManager } from './session';
import { CONTAINER_PADDING, PADDING } from '../../common/constants';
import { Ticker } from './ticker';
import { FontFamilies } from '../../styles/text';
import { CircularProgress } from './circular';
import { Timer, TimerTaskScroller } from './pomodoro';
import { UppercaseHeading } from '../entry/past';
import { default as theme } from '../../styles/app-theme.json'
import { SessionEvents } from '../../models/Session';
import { TimerCompleted } from './break';
export class TimerV2 extends Component<any, any>{
    /**
     *
     */
    manager: SessionManager = null;
    constructor(props) {
        super(props);
        this.state = {
            minutes: 0,
            seconds: 0
        };

    }
    next = () => {
        if (this.manager) {
            let next = this.manager.next();
            this.setState(() => { return { next, ready: true } })
        }
    }
    handler = (event) => {
        if (event == 1) {
            // completed
            let next = this.manager.next();
            console.log(next);
            this.setState((state, props) => { return { completed: true, next } })

        }
        if (event == 2) {
            //break
            this.setState(() => { return { _started: false } }, () => {
                console.log(this.state);
                this.start();
            });


        }
    }
    start = (endAt = null) => {

        if (this.state._started) {
            return;
        }

        let ticker = new Ticker(this.state.next);
        if (endAt) {
            ticker.start(endAt);
            ticker.tick();

        } else {
            ticker.start();
        }

        this.setState(() => { return { ticker, _started: true } }, () => {
            let remainingMinutes = (this.state.ticker.remaining / 60);
            let totalMinutes = Math.floor((this.state.next.time / 60));

            let seconds = (this.state.ticker.remaining - (remainingMinutes * 60))
            this.setState(() => { return { minutes: totalMinutes, seconds, started: true } }, () => {
                if (this.props.add_event && typeof this.props.add_event === 'function' && endAt == null) {
                    //@ts-ignore
                    let event: SessionEvents = {
                        startAt: new Date(),
                        endAt: ticker.endAt,
                        status: SessionEventStatus.ACTIVE,
                        eventType: this.state.next.type,
                    }
                    this.props.add_event(this.state.session.id, event);
                    this.setState(() => { return { completed: false } });


                }
            })
        });




    }
    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            let { id } = this.props.route.params;
            try {
                let index = -1;
                index = this.props.sessions.findIndex(s => s.id === id);
                if (index > -1) {
                    let is_active = false;
                    let started = false;

                    if ((this.props.active_event && Array.isArray(this.props.active_event) && this.props.active_event.length)) {
                        is_active = true;

                        if (this.props.active_event[this.props.active_event.length - 1].status == SessionEventStatus.ACTIVE) {
                            started = true;
                        }
                    }
                    this.setState(() => { return { session: this.props.sessions[index] } }, () => {
                        let events = this.props.sessions[index].events;

                        this.manager = new SessionManager({
                            config: this.state.session, tasks: this.state.session.tasks, limit: this.state.session.limit
                        });
                        let tasks = this.props.tasks.filter(t => this.state.session.tasks.findIndex(i => t.id === i) > -1);
                        if (this.manager) {
                            let next = this.manager.next();

                            this.setState(() => { return { next, ready: true, minutes: next.time, tasks, started, events, isActive: is_active } }, () => {
                                if (this.state.started) {

                                    this.start(this.props.active_event[0].end_at);

                                }
                            })
                        }

                    })

                }

            } catch (error) {

            }
        })
    }

    render() {

        if (!this.state.ready) return <CenterLoading></CenterLoading>;
        if (this.state.completed) {
            return <TimerCompleted onPress={() => this.handler(2)}></TimerCompleted>
        }
        return (
            <Layout style={{ flex: 1 }} level="1">

                {/*         <BackNav title="Work"></BackNav> */}
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
                    <View style={{ padding: CONTAINER_PADDING, }}>

                        <Timer onCompleted={() => this.handler(1)} initial={this.state.minutes} started={this.state.started} minutes={(this.state.ticker?.remaining / 60) || this.state.minutes} seconds={this.state.seconds}></Timer>
                    </View>

                </View>
                <Divider></Divider>
                <View style={{ flex: 1 }}>
                    <View style={{ padding: PADDING / 2, }}>
                        <UppercaseHeading heading="Tasks"></UppercaseHeading>
                    </View>
                    <TimerTaskScroller abstract={false} tasks={this.state.tasks}></TimerTaskScroller>
                </View>
                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => this.start()}>
                    <View style={{ position: "absolute", bottom: 0, left: 0, width: '100%', height: 56, backgroundColor: theme['color-primary-500'], justifyContent: 'center', alignItems: 'center' }}>
                        <Text category="h6" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: '#fff', textTransform: 'uppercase' }}>Start</Text>
                    </View>
                </TouchableNativeFeedback>
            </Layout>
        )
    }
}

const mapStateToProps = (state, props) => ({

    sessions: state.sessions.sessions,
    active_event: state.sessions.sessions.filter(s => s.id === props.route?.params?.id)[0].events.filter(s => s.status == SessionEventStatus.ACTIVE || s.status == SessionEventStatus.PAUSED),
    tasks: state.tasks.tasks
})

const mapDispatchToProps = dispatch => {
    return {
        add_event: (id, event) => dispatch(AddSessionEventAction(id, event)),
        update_status: (id, status) => dispatch(UpdateEventStatus(id, status))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TimerV2)
