import moment from "moment";
import { TaskItem } from "../../models/Tasks"
import { TaskSortPipe } from "../../reducers/task.reducer";
import { SortOptions } from "../task/list";
import { Task } from "../task/task";
import { SecondsToMinutesAndSeconds } from "./ticker";

export type SessionConfig = {
    id: string;
    time: number;
    shortBreak: number;
    longBreak: number;
    longBreakAfter: number;
}
export type Session = {
    config: SessionConfig;
    limit: number;
    tasks: TaskItem[];
}
export enum SessionEventType {
    POMODORO = 1,
    SHORTBREAK = 2,
    LONGBREAK,
}
export enum SessionEventStatus {
    ACTIVE = 1,
    CANCELLED,
    COMPLETED,
    PAUSED,

}
export type SessionEvent = {
    type: SessionEventType,
    time: number
    status
}
export class SessionManager {
    Session: Session;
    events: SessionEvent[] = [];
    public evaluateNext = (): SessionEvent => {
        if (!this.events.length) {
            return {
                status: SessionEventStatus.PAUSED,
                time: this.Session.config.time,
                type: SessionEventType.POMODORO
            }
        } else {
            let last = this.events[this.events.length - 1];
            switch (last.type) {
                case SessionEventType.POMODORO:
                    if (this.events.filter(e => e.type === SessionEventType.POMODORO).length % this.Session.config.longBreakAfter == 0) {
                        // long break
                        return {
                            status: SessionEventStatus.PAUSED,
                            time: this.Session.config.longBreak,
                            type: SessionEventType.LONGBREAK
                        }
                    } else {
                        return {
                            status: SessionEventStatus.PAUSED,
                            time: this.Session.config.shortBreak,
                            type: SessionEventType.SHORTBREAK
                        }
                    }
                case SessionEventType.LONGBREAK:
                    return {
                        status: SessionEventStatus.PAUSED,
                        time: this.Session.config.longBreakAfter,
                        type: SessionEventType.LONGBREAK
                    }
                case SessionEventType.SHORTBREAK:
                    return {
                        status: SessionEventStatus.PAUSED,
                        time: this.Session.config.time,
                        type: SessionEventType.POMODORO
                    }

                default:
                    return {
                        status: SessionEventStatus.PAUSED,
                        time: this.Session.config.time,
                        type: SessionEventType.POMODORO
                    }
            }
        }

    }
    // TODO : Mission and Goal due fetching
    /**
     * @description
     * Retrieve a list of applicable tasks for creating a new work session containing a set of tasks
     * @param tasks List of tasks to
     * @param count Max nummber of tasks to retrieve
     * @summary
     * Includes tasks that are :
     * 1. Due Soon
     * 2. That have a goal that is due soon 
     * 3. That have a mission that is due soon 
     * 4. Tasks that are overdue
     * 5. Oldest created tasks that are not yet completed
     * 6. High priority tasks
     * 7. Tasks that take small amount of time
     */
    public AutoRetrieveFromList = async (tasks: TaskItem[] | any[], count = 5) => {
        let eligbleTasks: any[] = [];
        let soonDue = TaskSortPipe(tasks, SortOptions.DUE_DATE, true).slice(0, count).filter(t => t.DueDate);
        let lateIncomplete = TaskSortPipe(tasks, SortOptions.DATE_CREATED, true).slice(0, count).reverse();
        eligbleTasks.push(...soonDue, ...lateIncomplete);
        return eligbleTasks.slice(0, count);
    }
    public next = (): SessionEvent => {
        let next = this.evaluateNext();
        this.events.push(next);
        return next;
    }
   
    constructor(Session: Session, events = []) {
        this.Session = Session;
        if (events.length) {
            this.events = [...events];
        }
    }
}