import React from 'react'
import AnimatedCircularProgress from 'react-native-conical-gradient-progress';
import { View, Text } from 'native-base';
import { StyleSheet, TouchableNativeFeedback } from 'react-native';
import { default as theme } from '../../styles/app-theme.json'
import { Timer } from './pomodoro';
import { TextMuted } from '../../styles/text';
const style = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#4FC87A',
    },
    titleBox: {
        ...StyleSheet.absoluteFillObject,
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontSize: 10,
        color: 'white',
        textAlign: 'center',
    },
    box: {
        margin: 10,
    },
});

/**
 * 
 * @param param0 Minutes = Total Minutes
 */
export const CircularProgress = ({ isAbstract = false, onActivate = null, backgroundColor = theme["color-font-700"], small = false, minutes, seconds, size = 204, width = 10, beginColor = theme["color-primary-500"], endColor = theme["color-primary-900"] }) => {
    // actiavtion login , double tap, interval, ??
    const [tap, setTap] = React.useState(0);
    const HandleTap = () => {

        if (tap == 1) {
            if (onActivate && typeof onActivate === "function") {
                onActivate();
                return setTap(0);

            }
        } else {
            setTap(1);
            setTimeout(() => {
                setTap(0);
            }, 1000);
        }

    }
    return (
        <TouchableNativeFeedback onPress={HandleTap} background={TouchableNativeFeedback.Ripple(theme["color-font-100"], true)}>
            <View >
                <Timer isAbstract={isAbstract} small={small} minutes={minutes} seconds={seconds}></Timer>
              {/*   {!isAbstract && <TextMuted text="Double tap to enter abstract mode"></TextMuted>}
                {isAbstract && <TextMuted style={{ color: theme["color-font-600"] }} text="Double tap to exit abstract mode"></TextMuted>} */}
            </View>
        </TouchableNativeFeedback>
        /*   <AnimatedCircularProgress
              size={size}
              width={width}
              fill={progress}
              prefill={100}
              beginColor={beginColor}
              endColor={endColor}
              segments={max}
              backgroundColor={backgroundColor}
              linecap="round"
          >
              {progress => (
                  <View style={style.titleBox}>
                      <Timer small={small} minutes={minutes} seconds={seconds}></Timer>
                  </View>
              )}
          </AnimatedCircularProgress> */
    )
}
