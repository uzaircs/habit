import { SessionEvent } from "./session";
import moment from 'moment'
export const SecondsToMinutesAndSeconds = seconds => {
    /*  const format = val => `0${Math.floor(val)}`.slice(-2) */
    const hours = seconds / 3600
    const minutes = (seconds % 3600) / 60
    return [hours, minutes, seconds % 60];
}
export class Ticker {
    event: SessionEvent;
    timeKeeper: Date;
    remaining: number;
    isPaused: boolean = false;
    pausedAt: Date;
    endAt: Date;
    timeDifference: number = 0;
    constructor(event: SessionEvent) {
        this.event = event;
        this.event.time = this.event.time * 60;
    }

    public toggle = () => {
        if (this.pausedAt) {
            this.pausedAt = null;
            this.endAt = moment(this.endAt).add(this.timeDifference, 'seconds').toDate();
            this.timeDifference = 0;
            this.isPaused = false;
        } else {
            this.isPaused = true;
            this.pausedAt = new Date();
        }

    }

    private adjust = () => {
        if (!this.pausedAt) return;
        let difference = moment().diff(moment(this.pausedAt), 'seconds');
        this.timeDifference = difference;

    }
    tick() {

        this.adjust();
        this.remaining = this.evaluateElapsed();
    }
    private evaluateElapsed = () => {
        let difference = moment(this.endAt).diff(moment(), 'seconds');
        return difference;
    }

    public start = (endAt = null) => {
        if (endAt) {
            this.endAt = endAt;
        } else {
            this.endAt = moment().add(this.event.time, 'second').toDate();
        }
    }
}