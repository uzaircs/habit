import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Card } from '@ui-kitten/components'
import { RetreiveTimer, SaveTimer } from '../../models/api';
import { CircularProgress } from './circular';
import { View } from 'native-base';
import { default as theme } from '../../styles/app-theme.json'
import { H2, DefaultText } from '../../styles/text';
import { Row } from 'react-native-easy-grid';
import Draggable from 'react-native-draggable';
export class ActiveSessionCard extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {}
    }
    save() {
        try {
            SaveTimer(this.state.ticker);
        } catch (error) {

        }
    }
    componentDidMount() {
        RetreiveTimer().then((timer) => {
            if (timer && timer.event) {
                this.setState({
                    ticker: timer, ready: true
                })
                this.state.ticker.start();
                let interval = setInterval(() => {
                    this.state.ticker.tick();
                    this.save();
                    let minutes = Math.floor((this.state.ticker.remaining / 60));
                    let seconds = (this.state.ticker.remaining - (minutes * 60))
                    if (!this.state.ticker.isPaused) {
                        this.setState({ minutes, seconds });

                    }
                    /*  if (this.checkFinished()) {
                         this.finish();
                     } */
                }, 300)
                this.setState({ interval: interval });
            }
        })
    }
    render() {

        if (!this.state.ready) {
            return <></>;
        } else {
            let progress = this.state.minutes / (this.state.ticker.event.time / 60) * 100;
            if (isNaN(progress)) {
                progress = 100;
            }
            return (
                <Card appearance="filled" style={{ backgroundColor: theme["color-primary-500"], justifyContent: 'center', alignItems: 'center', borderRadius: 92 / 2, width: 92, height: 92 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                        <H2 style={{ color: theme["color-font-100"] }} text={this.state.minutes}></H2>
                        <DefaultText style={{ color: theme["color-font-100"] }} text=":"></DefaultText>
                        <DefaultText style={{ color: theme["color-font-100"] }} text={this.state.seconds}></DefaultText>
                    </View>
                </Card>


            )
        }

    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveSessionCard)
