import React, { Component } from 'react'
import { connect } from 'react-redux'
import PickerModal from 'react-native-picker-modal-view';
import { ListItem, Button, Text } from '@ui-kitten/components';
import { DefaultText, FontFamilies } from '../../styles/text';

import { UpdateTaskListAsync, TaskUpdateAction } from '../../actions/task.action';
import { AttachTaskToGoal } from '../../models/api';

class _GoalPicker extends Component<any, any>{
    constructor(props) {
        super(props);

    }
    private onClosed(): void {

    }

    private onSelected(selected: any): void {

        if (this.props.manual) {
            if (this.props.onSelected && typeof this.props.onSelected === 'function') {
                this.props.onSelected({
                    name: selected.Name,
                    id: selected.Id,
                });
            }
        } else {
            this.setState({ selectedItem: selected });
            this.props.update(this.props.task.id, 'goal', {
                id: selected.Id,
                name: selected.Name
            });
            AttachTaskToGoal(this.props.task.id, selected.Id);
            return selected;
        }
    }

    private onBackButtonPressed(): void {

    }
    private renderItem = (selectedItem, item) => {

        return (
            <ListItem disabled={true}>
                <Text category="p1" style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }}>{item.Name}</Text>
            </ListItem>
        )
    }
    render() {
        return (

            <PickerModal
                renderSelectView={(disabled, selected, showModal) =>
                    this.props.selectView({ disabled, selected, showModal })
                }
                renderListItem={this.renderItem}
                onSelected={this.onSelected.bind(this)}
                onClosed={this.onClosed.bind(this)}
                onBackButtonPressed={this.onBackButtonPressed.bind(this)}
                items={this.props.goals}
                sortingLanguage={'en'}
                showToTopButton={true}
                selected={this.props.selectedItem}

                autoGenerateAlphabeticalIndex={true}
                selectPlaceholderText={'Choose one...'}
                onEndReached={() => console.log('list ended...')
                }
                searchPlaceholderText={'Search...'}
                requireSelection={false}
                autoSort={false}
            />

        );
    }
}

const mapStateToProps = (state) => ({
    goals: state.goals.goals.map(selectItem => {
        return {
            Id: selectItem.id,
            Name: selectItem.name,
            Value: selectItem.id,
            key: selectItem.id
        }
    })
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync()),
        update: (id, propType, propValue) => dispatch(TaskUpdateAction({ id, propType, propValue }))
    }
}

export const GoalPicker = connect(mapStateToProps, mapDispatchToProps)(_GoalPicker)
