import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, List, ListItem, Button } from '@ui-kitten/components/ui'
import { Row, Col } from 'react-native-easy-grid';
import { View } from 'native-base';
import { H2, DefaultText } from '../../styles/text';
import { SortButton, FilterButton } from '../task/list';
import { PlusOutlineIcon } from '../activity';
import { EmptyListPlaceholder } from '../day.past';

import { DueDate } from '../task/task';
import moment from 'moment'

export const AddButton = ({ onPress }) => {
    return (
        <Button onPress={onPress} status="basic" size="small" accessoryRight={PlusOutlineIcon} appearance="ghost" style={{ width: 24 }}></Button>

    )
}
export const PriorityMeter = ({ priority }) => {
    return (
        <DefaultText text={priority}></DefaultText>
    )
}
export class GoalsList extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request();
        }
    }
    renderItem = ({ item }) => {
        return (
            <ListItem onPress={() => this.props.navigation.navigate('goalItem', { id: item.id })}>
                <View style={{ padding: 4, width: '100%' }}>
                    <Row>
                        <DefaultText text={item.name}></DefaultText>
                    </Row>

                    <Row style={{ marginTop: 4 }} size={12}>
                        <Col size={4}>
                            <DueDate date={item.DueDate}></DueDate>
                        </Col>
                    </Row>
                </View>
            </ListItem>
        );
    }
    listEmpty = () => {
        return (
            <EmptyListPlaceholder text="You don't have any goals'" subtext="Add some goals to see the list"></EmptyListPlaceholder>
        )
    }
    listHeader = () => {
        return (
            <Row>
                <Col>
                    <View style={{ backgroundColor: 'white', padding: 16, flex: 1 }}>
                        <H2 text="Goals"></H2>
                    </View>
                </Col>
                <Col>
                    <View style={{ padding: 16, backgroundColor: 'white', justifyContent: 'flex-end', flexDirection: 'row' }}>
                        <AddButton onPress={() => { this.props.navigation.navigate('addGoal') }}></AddButton>
                        <SortButton></SortButton>
                        <FilterButton></FilterButton>
                    </View></Col>
            </Row>
        );
    }
    render() {
       
        return (
            <Layout style={{ flex: 1 }}>
                <List
                    ListEmptyComponent={this.listEmpty}
                    ListHeaderComponent={this.listHeader}
                    data={this.props.goals}
                    keyExtractor={item => item.id}
                    renderItem={this.renderItem}
                >

                </List>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    goals: state.goals.goals
})

const mapDispatchToProps = dispatch => {
    return {

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoalsList)
