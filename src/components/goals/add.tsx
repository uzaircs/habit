import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, ListItem, Text, List, Input, Select, SelectItem, Button, IndexPath } from '@ui-kitten/components'
import { DefaultText, TextMuted } from '../../styles/text'
import { BackNav } from '../../menu/header'
import { Toast, View } from 'native-base'
import { InputLabel } from '../forms/category'
import DateTimePicker from '@react-native-community/datetimepicker';
import { CalendarIcon } from '../task/add'
import moment from 'moment'
import { Alert, TouchableNativeFeedback } from 'react-native'
import { default as theme } from '../../styles/app-theme.json'
import { ButtonText, PlusOutlineIcon } from '../activity'

import { CenterLoading } from '../activity.page'
import { AddGoalAction, EditGoal } from '../../actions/goal.action'
export class AddGoal extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            dueDate: new Date(),
            _dueDate: null,
            selectedDate: 'Select a date',
            priority: 2,
            priorities: ['High', 'Normal', 'Low']
        }
    }
    setDate = (date) => {
        this.setState({
            showDatePicker: false,
            dueDate: date,
            _dueDate: date,
            selectedDate: moment(date).format('ddd Do MMM, YYYY')
        })
    }
    save() {
        this.setState({
            saving: true
        })
        if (!this.state.edit) {
            if (this.props.add && typeof this.props.add === 'function') {
                this.props.add({
                    DueDate: this.state._dueDate ? (moment(this.state.dueDate).unix() * 1000).toString() : null,
                    name: this.state.name,
                    priority: this.state.priority,
                    status: 0
                })
            }
        } else {
            if (this.props.edit && typeof this.props.edit === 'function') {
                let { goal } = this.state;
                let goalUnref = { ...goal };
                if (this.state._dueDate) {
                    goalUnref.DueDate = (moment(this.state.dueDate).unix() * 1000).toString();
                }
                goalUnref.name = this.state.name;
                goalUnref.priority = this.state.priority;
                goalUnref.status = this.state.status;
                this.props.edit(goalUnref)
            }
        }
        this.props.navigation.pop();

    }
    componentDidCatch() {

    }
    componentDidMount() {
        try {

            let { id } = this.props.route.params;
            if (id) {
                let goal = this.props.goals.filter(g => g.id === id)[0];
                this.setState(() => { return { edit: true, id, goal, name: goal.name, priority: goal.priority || 2, status: goal.status } })
                if (goal.DueDate) {
                    let dueDate = moment(goal.DueDate);
                    let selectedDate = dueDate.format('ddd Do MMM, YYYY')
                    this.setState(() => { return { dueDate: moment(dueDate).toDate(), selectedDate, _dueDate: dueDate } })
                }
            }
        } catch (error) {

        }
    }
    render() {
        if (this.state.saving) {
            return (
                <CenterLoading></CenterLoading>
            )
        }
        return (
            <Layout style={{ flex: 1 }} level="1">
                <BackNav title={this.state.edit ? 'Edit Goal' : 'Add Goal'} navigation={this.props.navigation}></BackNav>
                <View style={{ padding: 8, flex: 1 }}>
                    <View style={{ marginTop: 8 }}>
                        <View style={{ padding: 8 }}>
                            <Input value={this.state.name} onChangeText={text => this.setState({ name: text })} caption="Enter a name for this goal, example : Loose 10 KG" label={() => <InputLabel label="Goal Name"></InputLabel>}></Input>
                        </View>

                    </View>
                    <View style={{ marginTop: 8 }}>
                        <View style={{ padding: 8 }}>
                            <Select onSelect={(indexPath: IndexPath) => this.setState({ priority: indexPath.row + 1 })} value={() => <DefaultText text={this.state.priorities[this.state.priority - 1]}></DefaultText>} selectedIndex={new IndexPath(this.state.priority - 1)} label={() => <InputLabel label="Priority"></InputLabel>}>
                                <SelectItem title="High"></SelectItem>
                                <SelectItem title="Normal"></SelectItem>
                                <SelectItem title="Low"></SelectItem>

                            </Select>
                        </View>
                    </View>
                    <View style={{ marginTop: 8 }}>
                        <View >
                            <View style={{ padding: 8 }}>
                                <InputLabel label="Due On" style={{ marginBottom: 8 }}></InputLabel>
                            </View>
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-300"])} onPress={() => this.setState({ showDatePicker: true })}>
                                <View style={{ width: '100%', padding: 8, flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ marginRight: 4 }}>
                                        <CalendarIcon ></CalendarIcon>
                                    </View>
                                    <TextMuted text={this.state.selectedDate}></TextMuted>
                                </View>
                            </TouchableNativeFeedback>

                            {this.state.showDatePicker && <DateTimePicker
                                onChange={(e, date) => {
                                    if (e.type == 'set') {
                                        this.setDate(moment(date).unix() * 1000)
                                    }

                                }}
                                value={this.state.dueDate}
                                mode="date"
                                is24Hour={true}
                                display="default"

                            />}
                        </View>
                    </View>

                </View>
                <View style={{ flex: 1, flexDirection: 'column', width: '100%', position: 'absolute', bottom: 0 }}>
                    <Button onPress={() => this.save()} accessoryLeft={PlusOutlineIcon} children={(props) => <ButtonText text="Add" {...props}></ButtonText>}></Button>
                </View>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    goals: state.goals.goals
})

const mapDispatchToProps = dispatch => {
    return {

        add: goal => dispatch(AddGoalAction(goal)),
        edit: goal => dispatch(EditGoal(goal))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddGoal)
