import { Card, Layout, Text } from '@ui-kitten/components'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as Progress from 'react-native-progress';
import { default as theme } from '../../styles/app-theme.json'
import { DefaultText, FontFamilies, H4, TextSmall } from '../../styles/text';
import { DEFAULT_MARGIN } from '../../common/constants';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { DueDateIndicator, ETaskFact, SubtaskIndicator, TaskFact } from '../task/facts.group';
import moment from 'moment';
import { useNavigation } from '@react-navigation/native';
import { InteractionManager, View } from 'react-native';
import { CheckIcon } from '../task/list';
export const GoalCard = ({ goal, tasks }) => {
    const navigation = useNavigation();
    let completed = 0;
    let remaining = 0;
    let total = 0;
    let hasTask = false;
    let isCompleted = false;

    if (tasks && Array.isArray(tasks) && tasks.length) {
        completed = tasks.filter(t => t.status == 1).length;
        total = tasks.length;
        remaining = total - completed;
        hasTask = true;
    }
    let factInstance: TaskFact = {
        type: ETaskFact.SUBTASK,
        value: `${completed}/${total}`
    };
    let dueDate: TaskFact = null;
    if (goal.DueDate) {
        dueDate = {
            type: ETaskFact.DUE_DATE,
            value: moment(goal.DueDate).format('Do MMM')
        };
    }
    let progressBorder = theme["color-primary-200"];
    let progressColor = theme["color-primary-500"];
    if (total > 0 && remaining == 0) {
        isCompleted = true;
        progressBorder = theme['color-success-200'];
        progressColor = theme['color-success-500'];
    }
    let isMarkedCompleted = goal.status === 2;
    return (
        <Card  appearance="filled" onPress={() => {
            InteractionManager.runAfterInteractions(() => {
                navigation.navigate('goalItem', { id: goal.id })
            })
        }}>
            <Text numberOfLines={1} category="p1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 2 }}>{goal.name}</Text>
            {/* <H4 text={goal.name} style={{ marginBottom: DEFAULT_MARGIN / 2 }} /> */}
            {!isMarkedCompleted && <Progress.Bar useNativeDriver={true} progress={total == 0 ? total : completed / total} borderWidth={2} width={null} borderColor={progressBorder} color={progressColor} />}
            <Grid style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                <Row>
                    <Col>
                        <SubtaskIndicator fact={factInstance}></SubtaskIndicator>

                    </Col>
                    <Col>
                        {dueDate && <DueDateIndicator fact={dueDate}></DueDateIndicator>}
                    </Col>
                    <Col>
                        {isMarkedCompleted && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <CheckIcon color={theme['color-success-500']}></CheckIcon>
                            <Text appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, marginBottom: DEFAULT_MARGIN / 4 }}>Completed</Text>
                        </View>}
                    </Col>
                </Row>
            </Grid>

        </Card>
    )
}
/*
const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(GoalCard) */
