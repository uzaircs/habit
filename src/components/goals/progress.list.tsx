import { View } from 'native-base';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { DEFAULT_MARGIN } from '../../common/constants';

import { GoalCard } from './card';
import { InteractionManager } from 'react-native';
import { GetGoalTasks, GetGoalTasksWithSubtasks } from '../../models/api';
import { TextMuted } from '../../styles/text';
import { GoalIcon, NoGoalIcon } from '../task/add';
import { default as theme } from '../../styles/app-theme.json'
import { GetGoalProgress } from '../../actions/goal.action';

export class GoalProgressList extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = { ready: false, goalProgress: [], taskAttached: false }
    }
    AttachTaskToGoal = async () => {

    }


    componentDidUpdate() {
        /*    this.fromProps(); */

    }
    componentDidMount() {

        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request();
        }
    }
    render() {


        if (this.state.no_goals) {
            return (
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <NoGoalIcon color={theme["color-font-300"]} style={{ marginBottom: DEFAULT_MARGIN / 2, marginTop: DEFAULT_MARGIN / 2 }}></NoGoalIcon>
                    <TextMuted text="You don't have any goals"></TextMuted>
                </View>
            )
        }
        /*   if (!goalProgress || !goalProgress.length) { return <View /> } */
        return (
            <View>
                {this.props.goalProgress.map((progress) => {
                    return (
                        <View key={progress.goal.id} style={{ marginBottom: DEFAULT_MARGIN / 2 }}>
                            <GoalCard goal={progress.goal} tasks={progress.tasks}></GoalCard>
                        </View>
                    )
                })}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    goalProgress: state.goals.goalsProgress
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(GetGoalProgress())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoalProgressList)
