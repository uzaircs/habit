import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Button, Card, Divider, Icon, Layout, ListItem, Text } from '@ui-kitten/components'
import { BackNav, TopNav } from '../../menu/header'
import { Toast, View } from 'native-base'
import LottieView from 'lottie-react-native'
import { DefaultText, H1, H1Large, H4, TextDanger, TextMuted, TextMutedStrong, TextSuccess } from '../../styles/text'
import { InputLabel } from '../forms/category'
import { CenterLoading } from '../activity.page'
import { default as theme } from '../../styles/app-theme.json'
import { ClockIcon, TrashIcon } from '../task/details'
import { Dimensions, InteractionManager, ScrollView, StyleSheet, TouchableHighlight, TouchableNativeFeedback, TouchableOpacity } from 'react-native'
import { DeleteGoal, GetGoalTasksWithSubtasks, GetSubtask, GOAL_STATUS } from '../../models/api'
import Modal from 'react-native-modal';
import { BORDER_RADIUS, DEFAULT_MARGIN, MENU_HEIGHT_COLLAPSED, MENU_HEIGHT_EXPANED, PADDING } from '../../common/constants'
import { UppercaseHeading } from '../entry/past'
import { Task } from '../task/task'
import * as Progress from 'react-native-progress';
import Animated, { Extrapolate } from 'react-native-reanimated';
import { Col, Grid, Row } from 'react-native-easy-grid'
import { DueDateIndicator, ETaskFact, SubtaskIndicator, TaskFact } from '../task/facts.group'
import moment from 'moment'

import { GoalStatistics, GoalStats } from '../reports/reports'
import { GoalProgressChart, LineChartView } from './charts'
import { window } from 'rxjs/operators'
import { ButtonText, EditIcon, PlusOutlineIcon } from '../activity'
import { iOSUIKit } from 'react-native-typography'
import { AddTaskModal } from '../task/add'
import { GoalStatus, RemoveGoal } from '../../actions/goal.action'
import { BottomStrip } from '../../common/BottomStrip'
import { CheckIcon, FilterIcon, SortIcon } from '../task/list'

const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 2,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
});
export const RateIndicator = ({ rate }) => {
    if (rate && !isNaN(rate)) {
        if (+rate < 0) {
            return (
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                    <Row>
                        <Col size={2}>
                            <Icon name="arrow-downward-outline" height={20} width={20} fill={theme["color-danger-500"]}></Icon>
                        </Col>
                        <Col size={10}>
                            <TextDanger text={rate + '%'}></TextDanger>
                        </Col>
                    </Row>
                </View>
            )
        } if (+rate > 0) {
            return (
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
                    <Row>
                        <Col size={2}>
                            <Icon name="arrow-upward-outline" height={20} width={20} fill={theme["color-success-500"]}></Icon>
                        </Col>
                        <Col size={10}>
                            <TextSuccess text={rate + '%'}></TextSuccess>
                        </Col>
                    </Row>
                </View>
            )
        }
    }
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start' }}>
            <Row>
                <Col size={2}>

                </Col>
                <Col size={10}>
                    <TextMuted text="-" />
                </Col>
            </Row>
        </View>
    )
}
export const CompletedModalContent = modalProps => {
    const Footer = (props) => (
        <View {...props} style={[props.style, styles.footerContainer]}>
            <Button
                onPress={modalProps.onCancel}
                style={styles.footerControl}
                size='small'
                status='basic'>
                NO
          </Button>
            <Button
                onPress={modalProps.onSelect}
                style={styles.footerControl}
                size='small'
                status='success'>
                YES
          </Button>

        </View>
    );

    return (
        <Card style={styles.card} footer={Footer} disabled={true}>
            {/* <H4 style={{ marginBottom: DEFAULT_MARGIN }} text=""></H4> */}
            <DefaultText text="Do you want to mark this goal as completed?"></DefaultText>
        </Card>
    )
}
export const TimeIndicator = ({ time }) => {
    if (time && !isNaN(time)) {
        if (+time > 60) {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginRight: DEFAULT_MARGIN / 4 }}>
                        <ClockIcon width={20} height={20}></ClockIcon>
                    </View>
                    <View>
                        <TextMuted text={(time / 60).toFixed(0) + ' hrs'} />
                    </View>

                </View>
            )
        } else {
            return (
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginRight: DEFAULT_MARGIN / 4 }}>
                        <ClockIcon width={20} height={20}></ClockIcon>
                    </View>
                    <View>
                        <TextMuted text={(time).toFixed(0) + ' min'} />
                    </View>

                </View>
            )
        }
    }
    return (
        <View>
            <TextMuted text="-" />
        </View>
    )
}
export const GoalStatisticsRow = ({ statistics }) => {
    if (!statistics) {
        return <View />
    }
    let stats: GoalStats = statistics;
    return (
        <Grid>
            <Row size={12}>
                <Col size={4}>
                    <UppercaseHeading margin={0} heading="Average Time"></UppercaseHeading>

                    <TimeIndicator time={stats.AverageTimePerTask} />
                </Col>
                <Col size={4}>
                    <UppercaseHeading margin={0} heading="Progress Rate"></UppercaseHeading>

                    <RateIndicator rate={(stats.PercentChange * 100).toFixed(0)} />
                </Col>
                <Col size={4}>
                    <UppercaseHeading margin={0} heading="Total Est Time"></UppercaseHeading>

                    <TimeIndicator time={stats.TotalEstimatedTime} />
                </Col>

            </Row>
        </Grid>
    )
}
export class GoalItem extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            ready: false,
            scrollY: new Animated.Value(0),
            deleted: false,
            addModalOpen: false
        }

    }

    load() {
        setTimeout(() => {
            InteractionManager.runAfterInteractions(() => {
                try {
                    const { id } = this.props.route.params;
                    let goal = this.props.goals.filter(g => g.id === id);
                    GetGoalTasksWithSubtasks(id).then(tasks => {
                        if (goal && goal.length) {
                            if (tasks && tasks.length) {
                                GoalStatistics(id).then((stats) => {

                                    this.setState({
                                        goal: goal[0], ready: true,
                                        tasks, statistics: stats
                                    })
                                })
                            } else {
                                this.setState({
                                    goal: goal[0], ready: true,
                                    tasks,
                                })
                            }
                        }
                    })

                } catch (error) {

                }
            })
        }, 300)
    }
    DidStatusChange(old, recent) {
        return JSON.stringify(old.map(t => t.status)) !== JSON.stringify(recent.map(t => t.status));
    }
    componentDidUpdate() {
        const { id } = this.props.route.params;
        /*   GoalStatistics(id).then((stats) => {
              this.setState(() => { return { statistics: stats } });
          }) */
    }
    shouldComponentUpdate(nextProps, nextState) {

        let should = false;

        if (this.state.addModalOpen && !nextState.addModalOpen) {
            should = true;
        }
        if (!this.state.addModalOpen && nextState.addModalOpen) {
            should = true;
        }
        /*   if (JSON.stringify(this.state.statistics) !== JSON.stringify(nextState.statistics)) {
              should = true;
          } */
        if (this.props.tasks && Array.isArray(this.props.tasks) && this.props.tasks.length && nextProps.tasks && Array.isArray(nextProps.tasks)) {
            if (this.DidStatusChange) {
                should = true;
            }
        }
        try {
            if ((!this.state.tasks || !this.state.tasks.length) && (nextState.tasks && nextState.tasks.length)) {
                should = true;
            }
        } catch (error) {

        }
        try {
            if (this.state.tasks.length != nextState.tasks.length) {
                should = true;
            }
        } catch (error) {

        }
        try {
            GetGoalTasksWithSubtasks(this.state.goal.id).then(tasks => {
                if (tasks && Array.isArray(tasks) && tasks.length) {
                    if (!this.state.tasks || !this.state.tasks.length || (this.state.tasks.length !== tasks.length)) {
                        this.setState(() => { return { tasks } });

                    }
                }
            })
        } catch (error) {

        }
        if (!this.state.ready && nextState.ready) {
            should = true;
        }
        return should;
    }
    componentDidMount() {
        this.load();
    }
    deleteModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { deleteVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>
                <Button
                    onPress={() => {
                        //delete Goal
                        if (this.props.deleteGoal && typeof this.props.deleteGoal === 'function') {
                            this.props.deleteGoal(this.state.goal.id);
                        }
                        Toast.show({
                            text: 'Goal deleted',
                            textStyle: iOSUIKit.subheadShortWhite,
                            duration: 2000,
                            type: 'success'
                        })
                        this.setState(() => { return { deleteVisible: false }; })
                        setTimeout(() => {
                            if (this.props.navigation) {
                                this.props.navigation.pop();
                            }
                        })
                    }}
                    style={styles.footerControl}
                    size='small'
                    status='danger'>
                    DELETE
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Confirm Delete"></H4>
                <DefaultText text="Are you sure you want to delete this goal? This action cannot be undone"></DefaultText>
            </Card>
        )
    }
    delete() {
        try {
            DeleteGoal(this.state.goal.id).then(() => {
                if (this.props.request && typeof this.props.request === 'function') {
                    this.props.request();
                }
                this.props.navigation.pop();
            })
        } catch (error) {

        }
    }
    FullReportCard = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>


            </View>
        );
        return (
            <Card style={styles.card} disabled={true}>
                {/*    <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="How is your progress?"></H4> */}
                <DefaultText text="Check out your detailed progress for this goal"></DefaultText>
                <View>
                    <Button
                        onPress={() => {
                            this.props.navigation.navigate('goalProgress', { id: this.state.goal.id });
                        }}
                        appearance="ghost"
                        style={styles.footerControl}
                        size='small'
                        status='primary'>
                        VIEW PROGRESS
              </Button>
                </View>
            </Card>
        )
    }
    onCompleted = () => {
        this.setState(() => { return { completeVisible: false } })
        this.props.changeStatus(this.state.goal.id, GOAL_STATUS.COMPLETED);
    }
    onCancel = () => {
        this.setState((state, props) => { return { completeVisible: false } })

    }
    render() {
        if (!this.state.ready) {
            return <CenterLoading></CenterLoading>
        }
        const headerHeight = this.state.scrollY.interpolate({
            inputRange: [0, MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED],
            outputRange: [MENU_HEIGHT_EXPANED, MENU_HEIGHT_COLLAPSED],
            extrapolate: Extrapolate.CLAMP
        })
        const fontSize = this.state.scrollY.interpolate({
            inputRange: [0, MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED],
            outputRange: [24, 20],
            extrapolate: Extrapolate.CLAMP
        })
        const opacity = this.state.scrollY.interpolate({
            inputRange: [0, (MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED) / 2],
            outputRange: [1, 0],
            extrapolate: Extrapolate.CLAMP
        });
        let completed = this.state.tasks?.filter(t => t.status == 1).length;
        let total = this.state.tasks.length;
        let percentComplete = completed / total;
        let remaining = null;
        if (this.state.goal.DueDate) {
            let endAt = moment(this.state.goal.DueDate);
            remaining = endAt.diff(moment(), 'd');
            if (remaining < 0) {
                remaining = 0;
            }
        }
        let factInstance: TaskFact = {
            type: ETaskFact.SUBTASK,
            value: `${completed}/${total}`
        };
        let dueDate: TaskFact = {
            type: ETaskFact.DUE_DATE,
            value: moment().format('Do MMM')
        };
        let isCompleted = false;
        try {
            isCompleted = this.state.tasks.every(task => task.status);
        } catch (error) {

        }
        const editButton = () => {
            return (
                <View style={{ alignItems: 'flex-end', flexDirection: 'row' }}>
                    <TouchableOpacity style={{ marginRight: DEFAULT_MARGIN }} onPress={() => this.setState(() => { return { addModalOpen: true } })}>
                        <TextMutedStrong text="Add Task" style={{ color: theme['color-primary-500'] }}></TextMutedStrong>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('addGoal', { id: this.state.goal.id })}>
                        <TextMutedStrong text="Edit" style={{ color: theme['color-primary-500'] }}></TextMutedStrong>
                    </TouchableOpacity>
                </View>
            )
        }
        if (isNaN(total) || total === 0) {
            return (
                <Layout style={{ flex: 1 }} level="2">
                    <AddTaskModal goal={this.state.goal.id} visible={this.state.addModalOpen} onToggle={() => this.setState((state, props) => { return { addModalOpen: !state.addModalOpen } })}></AddTaskModal>
                    <TopNav accessoryRight={editButton} title={this.state.goal.name} navigation={this.props.navigation}>

                    </TopNav>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                        <LottieView style={{ width: 256 }} renderMode="HARDWARE" source={require('../../images/empty.json')} autoPlay loop={true} />
                        <TextMutedStrong text="You have no tasks for this goal"></TextMutedStrong>
                        <Button onPress={() => this.setState((state, props) => { return { addModalOpen: !state.addModalOpen } })
                        } appearance="ghost" accessoryRight={props => <View style={{ top: 1 }}><PlusOutlineIcon {...props} /></View>} children={props => <ButtonText text="Add Task" {...props} />}></Button>
                    </View>
                </Layout >
            )
        }
        return (
            <Layout style={{ flex: 1, position: 'relative' }} level="1">
                <AddTaskModal goal={this.state.goal.id} visible={this.state.addModalOpen} onToggle={() => this.setState((state, props) => { return { addModalOpen: !state.addModalOpen } })}></AddTaskModal>
                <TopNav accessoryRight={editButton} title={this.state.goal.name} navigation={this.props.navigation}>

                </TopNav>
                <BottomStrip>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                        <TouchableNativeFeedback onPress={() => this.deleteConfirm()} background={TouchableNativeFeedback.Ripple(theme["color-danger-300"], true)}>
                            <View>
                                <TrashIcon width={20} height={20} color={theme['color-font-light-100']}></TrashIcon>
                            </View>
                        </TouchableNativeFeedback>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            {this.state.goal.status != 2 && <TouchableNativeFeedback onPress={() => this.setState((state, props) => { return { completeVisible: true } })
                            } background={TouchableNativeFeedback.Ripple(theme["color-success-300"], true)}>
                                <View style={{ marginRight: DEFAULT_MARGIN }}>
                                    <CheckIcon color={theme['color-font-light-100']} width={24} height={24}></CheckIcon>
                                </View>
                            </TouchableNativeFeedback>}
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)} onPress={() => { }}>
                                <View style={{ marginRight: DEFAULT_MARGIN }}>
                                    <SortIcon style={{ tintColor: theme['color-font-light-100'], marginRight: DEFAULT_MARGIN }}></SortIcon>
                                </View>
                            </TouchableNativeFeedback>
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)} onPress={() => { }}>
                                <View style={{ marginRight: DEFAULT_MARGIN }}>
                                    <FilterIcon width={20} height={20} fill={theme['color-font-light-100']}></FilterIcon>
                                </View>
                            </TouchableNativeFeedback>
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)} onPress={() => this.setState(() => { return { addModalOpen: true } })}>
                                <View style={{ marginRight: DEFAULT_MARGIN }}>
                                    <PlusOutlineIcon width={20} height={20} fill={theme["color-font-light-100"]}></PlusOutlineIcon>
                                </View>
                            </TouchableNativeFeedback>

                        </View>
                    </View>
                </BottomStrip>
                <Modal isVisible={this.state.deleteVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.deleteModal></this.deleteModal>
                </Modal>
                <Modal isVisible={this.state.completeVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <CompletedModalContent onSelect={this.onCompleted} onCancel={this.onCancel}></CompletedModalContent>
                </Modal>
                <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>

                    <View >


                        <ScrollView style={{ padding: PADDING }} horizontal={true} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                            <Card style={{ borderRadius: BORDER_RADIUS, width: 160, height: 128, justifyContent: 'center', alignItems: 'center', marginRight: DEFAULT_MARGIN, }}>
                                <H1Large text={((isNaN(percentComplete) ? 0 : percentComplete) * 100).toFixed(0) + '%'} style={[{ color: theme['color-primary-500'], textAlign: 'center' }]}></H1Large>
                                <TextMutedStrong text="Overall Progress" style={{ fontSize: 12, textAlign: 'center', marginBottom: DEFAULT_MARGIN / 2 }} />
                                <View style={{ width: 132 }}>
                                    <Progress.Bar useNativeDriver={true} progress={isNaN(percentComplete) ? 0 : percentComplete} borderWidth={2} width={null} borderColor={theme["color-primary-200"]} color={theme["color-primary-500"]} />
                                </View>
                            </Card>
                            <Card style={{ borderRadius: BORDER_RADIUS, width: 160, height: 128, justifyContent: 'center', alignItems: 'center', marginRight: DEFAULT_MARGIN, }}>
                                <H1Large text={this.state.statistics?.progressRing?.completedTasks || '-'} style={[{ color: theme['color-primary-500'], textAlign: 'center' }]}></H1Large>
                                <TextMutedStrong text="Tasks Completed" style={{ fontSize: 12, textAlign: 'center' }} />

                            </Card>
                            <Card style={{ borderRadius: BORDER_RADIUS, width: 160, height: 128, justifyContent: 'center', alignItems: 'center', marginRight: DEFAULT_MARGIN, }}>
                                <H1Large text={remaining} style={[{ color: theme['color-primary-500'], textAlign: 'center' }]}></H1Large>
                                <TextMutedStrong text="Days remaining" style={{ fontSize: 12, textAlign: 'center' }} />

                            </Card>
                            <Card style={{ borderRadius: BORDER_RADIUS, width: 160, height: 128, justifyContent: 'center', alignItems: 'center', marginRight: DEFAULT_MARGIN, }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <Icon name="arrow-forward-outline" fill={theme['color-primary-500']} width={32} height={32}></Icon>
                                </View>
                                <TextMutedStrong text="View more..." style={{ fontSize: 12, textAlign: 'center' }} />

                            </Card>


                        </ScrollView>
                        {(this.state.tasks.filter(t => !t.status).length > 0) && <View>
                            <View style={{ marginLeft: DEFAULT_MARGIN, marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                                <UppercaseHeading heading="Active Tasks"></UppercaseHeading>
                            </View>
                            <View >
                                {this.state.tasks.filter(t => !t.status).map(task => {
                                    return (
                                        <View key={task.id} style={{ marginBottom: DEFAULT_MARGIN / 4 }}>
                                            <ListItem>
                                                <Task task={task}></Task>
                                            </ListItem>
                                        </View>
                                    )
                                })}
                            </View>

                        </View>}
                        {(this.state.tasks.filter(t => t.status).length > 0) && <View>
                            <View style={{ marginLeft: DEFAULT_MARGIN, marginBottom: DEFAULT_MARGIN / 2 }}>
                                <UppercaseHeading heading="Completed Tasks"></UppercaseHeading>
                            </View>
                            <View style={{ marginBottom: DEFAULT_MARGIN * 4 }}>
                                {this.state.tasks.filter(t => t.status).map(task => {
                                    return (
                                        <View key={task.id} style={{ marginBottom: DEFAULT_MARGIN / 4 }}>
                                            <ListItem>
                                                <Task task={task}></Task>
                                            </ListItem>
                                        </View>
                                    )
                                })}
                            </View>

                        </View>}
                    </View>
                </ScrollView>

            </Layout>
        )
    }
    deleteConfirm(): void {
        this.setState(() => { return { deleteVisible: true } })

    }
}

const mapStateToProps = (state) => ({
    goals: state.goals.goals,
    tasks: state.tasks.tasks
})

const mapDispatchToProps = dispatch => {
    return {
        deleteGoal: (id) => dispatch(RemoveGoal(id)),
        changeStatus: (id, status) => dispatch(GoalStatus(id, status))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GoalItem)
