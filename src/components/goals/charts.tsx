import { View, Text } from 'native-base'
import React from 'react'
import { Dimensions } from 'react-native'
import {
    LineChart, ProgressChart,

} from "react-native-chart-kit";
import { default as theme } from '../../styles/app-theme.json'
import { CONTAINER_PADDING, DEFAULT_MARGIN } from '../../common/constants';
import { DefaultText, H2, TextMuted } from '../../styles/text';
import { iOSUIKit, material } from 'react-native-typography';
import { GoalProgressRing } from '../reports/reports';
import { DotCircleIcon } from '../../main';
import { CheckIcon } from '../task/list';
export const LineChartView = ({ data }) => {

    let hiddenPoints = data?.datasets[0].data.map((item, index) => item === 0 ? index : -1).filter(item => item > -1);
    return (
        <View style={{ backgroundColor: 'white', paddingTop: DEFAULT_MARGIN / 2, paddingBottom: DEFAULT_MARGIN / 2, borderRadius: 16, elevation: 4 }}>
            {/*    <H2 text="Bezier Line Chart" style={{ marginBottom: DEFAULT_MARGIN / 2 }}></H2> */}
            <LineChart
                data={data}
                withInnerLines={false}
                width={Dimensions.get("window").width - DEFAULT_MARGIN / 2} // from react-native
                height={260}
                hidePointsAtIndex={hiddenPoints}
                transparent={true}
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{

                    backgroundGradientFrom: '#fff',
                    backgroundGradientTo: '#fff',
                    color: (opacity = 1) => `rgba(${51}, ${102}, ${255}, ${opacity})`,

                    decimalPlaces: 0, // optional, defaults to 2dp
                    /*   color: (opacity = 1) => `rgba(${255}, ${255}, ${255}, ${opacity})`, */
                    labelColor: (opacity = 1) => theme["color-font-300"],
                    style: {
                        borderRadius: 16,

                    },
                    propsForDots: {
                        r: "5",
                        strokeWidth: "1",
                        stroke: theme["color-primary-300"]
                    },
                    propsForLabels: {
                        fontFamily: material.body1.fontFamily,
                        fontSize: iOSUIKit.footnote.fontSize,
                        fontWeight: iOSUIKit.footnote.fontWeight
                    }
                }}

                bezier
                style={{
                    marginVertical: 8,
                    borderRadius: 16
                }}
            />
        </View>
    )
}

export const CheckList = ({ items }) => {
    return (
        <View style={{ flexDirection: 'column' }}>
            {items.map(item => {
                return (
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <CheckIcon style={{ marginRight: DEFAULT_MARGIN / 4 }} color={theme["color-success-500"]}></CheckIcon>
                        <DefaultText style={{ color: theme["color-font-400"] }} text={item} />
                    </View>
                )
            })}
        </View>
    )
}
export const GoalProgressChart = ({ data }) => {
    let ring: GoalProgressRing = data;
    let labels = ['Progress', 'Progress Per Day', 'Days Remaining']
    let total = 0;
    if (ring.completedTasks && ring.totalTasks) {
        total = ring.completedTasks / ring.totalTasks
    }
    let ring1 = total;
    let ring2 = 0;
    let ring3 = 0;
    if (ring.tasksPerDay && +ring.tasksPerDayRecommended) {
        ring2 = ring.tasksPerDay / ring.tasksPerDayRecommended;
        if (ring2 > 1) ring2 = 1;
    }
    if (ring.hasDueDate && ring.totalDays && ring.daysElapsed) {
        ring3 = ring.daysElapsed / ring.totalDays;
    }
    let items = []
    if (ring2 >= 1) {
        items.push('Your daily progress is good!')
    }

    if (ring3 - ring1 < 0.15) {
        items.push('Your overall progress is on track!')
    }
    if (ring2 >= 0.95) {
        items.push(`Acheiving recommended daily task (~${Math.round(ring.tasksPerDayRecommended)} per day)`)
    }
    let _data = { labels, data: [ring3, ring2, ring1] };
    ;
    /*     const data = {
            labels: ["Swim", "Bike", "Run"], // optional
            data: [0.4, 0.6, 0.8]
        }; */
    let info = `Outer circle of following display your total progress.\nThe middle circle indicates how is your progress every day\nWhile the inner most circle indicates total elapsed days since the goal was started uptil the due date (0 if there is no due date)`
    let colors = [`rgba(${51}, ${102}, ${255}, ${0.3})`, `rgba(${51}, ${102}, ${255}, ${0.6})`, `rgba(${51}, ${102}, ${255}, ${0.9})`].reverse();
    return (
        <View style={{ backgroundColor: '#fff', borderRadius: 15, paddingTop: DEFAULT_MARGIN }}>
            <View style={{ padding: CONTAINER_PADDING }}>
                <TextMuted text={info} />
            </View>
            <ProgressChart
                hideLegend={true}
                data={_data}
                width={Dimensions.get("window").width - (DEFAULT_MARGIN / 2)}
                height={260}
                strokeWidth={16}
                radius={36}
                chartConfig={{
                    backgroundColor: theme["color-primary-500"],
                    backgroundGradientFrom: '#fff',
                    backgroundGradientTo: '#fff',
                    color: (opacity = 1) => `rgba(${51}, ${102}, ${255}, ${opacity})`,
                    labelColor: (opacity = 1) => theme["color-font-300"],
                    propsForLabels: {
                        fontFamily: iOSUIKit.footnote.fontFamily,
                        fontSize: iOSUIKit.footnote.fontSize,
                        fontWeight: iOSUIKit.footnote.fontWeight,


                    },


                }}

                style={{
                    marginVertical: 8,
                    borderRadius: 16
                }}

            />

            <View style={{ flexDirection: 'row', marginTop: DEFAULT_MARGIN / 2, paddingLeft: DEFAULT_MARGIN, paddingRight: DEFAULT_MARGIN, paddingBottom: DEFAULT_MARGIN, justifyContent: 'center' }}>
                {labels.map((label, idx) => {
                    return (
                        <View style={{ marginLeft: DEFAULT_MARGIN / 2, marginRight: DEFAULT_MARGIN / 2, flexDirection: 'row', alignItems: 'center' }}>
                            <DotCircleIcon style={{ marginRight: DEFAULT_MARGIN / 4, fontSize: 18, color: colors[idx] }}></DotCircleIcon>
                            <TextMuted text={label} style></TextMuted>
                        </View>
                    )
                })}
            </View>
            <View style={{ padding: CONTAINER_PADDING }}>
                <CheckList items={items}></CheckList>
            </View>
        </View >
    )
}