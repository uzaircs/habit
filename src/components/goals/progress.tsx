import { Layout } from '@ui-kitten/components'
import React, { Component } from 'react'
import { InteractionManager, ScrollView, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import { GetGoalTasksWithSubtasks } from '../../models/api'
import { BackNav } from '../../menu/header'
import { GoalStatistics } from '../reports/reports'
import { CenterLoading } from '../activity.page'
import { UppercaseHeading } from '../entry/past'
import { H2, TextMuted } from '../../styles/text'
import { DEFAULT_MARGIN } from '../../common/constants'
import { Col, Grid, Row } from 'react-native-easy-grid'
import moment from 'moment'
import { RateIndicator } from './item'
import { CloseIcon, QuestionIcon } from '../task/list'
import { default as theme } from '../../styles/app-theme.json'
import { GoalProgressChart, LineChartView } from './charts'

const styles = StyleSheet.create({
    progressCard: {
        margin: DEFAULT_MARGIN / 2,
        backgroundColor: '#fff',
        borderRadius: 9,
        padding: DEFAULT_MARGIN / 2,
        minHeight: 84
    }
})
export const ProgressItem = ({ heading, value = null, unit = null, children = null, showHint = false }) => {
    if (children) {
        return (
            <View style={styles.progressCard}>
                <UppercaseHeading heading={heading} />
                <Row style={{ alignItems: 'center' }}>
                    {children()}
                    <View style={{ position: 'absolute', right: DEFAULT_MARGIN / 2 }}>
                        {showHint && <QuestionIcon width={20} height={20} fill={theme["color-font-300"]}></QuestionIcon>}
                    </View>
                </Row>
            </View >
        )
    }
    return (
        <View style={styles.progressCard}>
            <UppercaseHeading heading={heading} />
            <Row style={{ alignItems: 'center' }}>
                <H2 style={{ color: theme["color-font-500"] }} text={value} />
                <TextMuted style={{ marginLeft: DEFAULT_MARGIN / 4 }} text={unit} />
            </Row>
        </View >
    )
}

export class GoalProgressScreen extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {}

    }
    componentDidMount() {
        try {

            setTimeout(() => {
                InteractionManager.runAfterInteractions(() => {
                    try {
                        const { id } = this.props.route.params;
                        let goal = this.props.goals.filter(g => g.id === id);
                        GetGoalTasksWithSubtasks(id).then(tasks => {
                            if (goal && goal.length) {
                                GoalStatistics(id).then((stats) => {
                                
                                    this.setState({
                                        goal: goal[0], ready: true,
                                        tasks, statistics: stats
                                    })
                                })
                            }
                        })

                    } catch (error) {

                    }
                })
            }, 300)
        } catch (error) {

        }
    }
    render() {
        if (!this.state.ready) return <CenterLoading></CenterLoading>
        return (
            <Layout style={{ flex: 1 }} level="4">
                <BackNav title="Goal Progress" />
                <ScrollView>
                    <Grid>
                        <Row size={12}>
                            <Col size={6}>
                                <ProgressItem heading="Avg time per task" value={this.state.statistics.AverageTimePerTask} unit="min" />
                            </Col>
                            <Col size={6}>
                                <ProgressItem heading="Total Estimated time" value={this.state.statistics.TotalEstimatedTime} unit="min" />
                            </Col>
                        </Row>
                        <Row size={12}>
                            <Col size={6}>
                                <ProgressItem heading="Estimtated Time Remaining" value={this.state.statistics.EstimatedTimeRemaining} unit="min" />
                            </Col>
                            <Col size={6}>
                                <ProgressItem showHint heading="Progress Rate" children={props => <RateIndicator rate={(this.state.statistics.PercentChange * 100).toFixed(0)} />} />
                            </Col>
                        </Row>
                        <Row size={12}>
                            <Col size={6}>
                                <ProgressItem heading="Started On" value={moment(this.state.statistics.StartedOn).format('MMM Do, YYYY')} />
                            </Col>
                            <Col size={6}>
                                <ProgressItem heading="Total Tasks" value={this.state.statistics.TotalTasks} unit="tasks" />
                            </Col>
                        </Row>
                        <Row size={12}>
                            <Col size={6}>
                                <ProgressItem heading="Tasks Completed / Day" value={this.state.statistics.TasksCompletedPerDay} unit="tasks" />
                            </Col>
                            <Col size={6}>
                                <ProgressItem heading="Tasks Added / Day" value={this.state.statistics.TaskAddedPerDay} unit="tasks" />
                            </Col>
                        </Row>
                        <Row size={12}>
                            <Col size={6}>
                                <ProgressItem heading="Total Days" value={moment().diff(moment(this.state.statistics.StartedOn), 'd')} unit="days" />
                            </Col>
                            <Col size={6}>
                                <ProgressItem heading="Tasks Added / Day" value={this.state.statistics.TaskAddedPerDay} unit="tasks" />
                            </Col>
                        </Row>
                        <View style={{ padding: DEFAULT_MARGIN / 4, marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                            <UppercaseHeading heading="Progress Chart"></UppercaseHeading>
                            <LineChartView data={this.state.statistics.lineChart}></LineChartView>
                        </View>
                        <View style={{ padding: DEFAULT_MARGIN / 4, marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                            <UppercaseHeading heading="Progress Chart"></UppercaseHeading>
                            <GoalProgressChart data={this.state.statistics.progressRing}></GoalProgressChart>
                        </View>
                    </Grid>
                </ScrollView>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks,
    goals: state.goals.goals
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(GoalProgressScreen)
