import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Input, Select, SelectItem, IndexPath, Button } from '@ui-kitten/components'
import { BackNav } from '../../menu/header'
import { View } from 'native-base'
import { InputLabel } from '../forms/category'
import { TouchableNativeFeedback } from 'react-native'
import { default as theme } from '../../styles/app-theme.json'
import { CalendarIcon } from '../task/add'
import { TextMuted, DefaultText } from '../../styles/text'
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment'
import { useNavigation } from '@react-navigation/native';
import { AddMissionAsync } from '../../actions/mission.action'
export const AddMission = ({ add, missions, ...props }) => {
    const navigation = useNavigation();
    const [name, setName] = React.useState('');
    const [priority, setPriority] = React.useState(2);
    const [dueDate, setDueDate] = React.useState(null);
    const [selectedDate, setSelectedDate] = React.useState(moment().toDate());
    let priorities = ['High', 'Normal', 'Low'];
    const AddMission = () => {
        let mission = {
            name, priority, DueDate: dueDate, status: 0
        }
        console.log(mission);
    }
    return (
        <Layout style={{ flex: 1 }}>
            <BackNav title="Add Mission" navigation={navigation}></BackNav>
            <View style={{ padding: 8, flex: 1 }}>
                <View style={{ marginTop: 8 }}>
                    <View style={{ padding: 8 }}>
                        <Input value={name} onChangeText={text => setName(text)} caption="Enter a name for this goal, example : Loose 10 KG" label={() => <InputLabel label="Goal Name"></InputLabel>}></Input>
                    </View>

                </View>
                <View style={{ marginTop: 8 }}>
                    <View style={{ padding: 8 }}>
                        <Select onSelect={(indexPath: IndexPath) => setPriority(indexPath.row)} value={() => <DefaultText text={priorities[priority - 1]}></DefaultText>} selectedIndex={new IndexPath(priority - 1)} label={() => <InputLabel label="Priority"></InputLabel>}>
                            <SelectItem title="High"></SelectItem>
                            <SelectItem title="Normal"></SelectItem>
                            <SelectItem title="Low"></SelectItem>

                        </Select>
                    </View>
                </View>
                <View style={{ marginTop: 8 }}>
                    <View >
                        <View style={{ padding: 8 }}>
                            <InputLabel label="Due On" style={{ marginBottom: 8 }}></InputLabel>
                        </View>
                        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-300"])} onPress={() => this.setState({ showDatePicker: true })}>
                            <View style={{ width: '100%', padding: 8, flexDirection: 'row', alignItems: 'center' }}>
                                <View style={{ marginRight: 4 }}>
                                    <CalendarIcon ></CalendarIcon>
                                </View>
                                <TextMuted text={dueDate}></TextMuted>
                            </View>
                        </TouchableNativeFeedback>

                        {this.state.showDatePicker && <DateTimePicker
                            onChange={(e, date) => {
                                setDueDate(moment(date).unix() * 1000)
                                setSelectedDate(date)

                            }}
                            value={selectedDate}
                            mode="date"
                            is24Hour={true}
                            display="default"

                        />}
                    </View>
                </View>

            </View>
            <Button status="primary" onPress={AddMission}></Button>
        </Layout>
    )
}

const mapStateToProps = (state) => ({
    missions: state.missions.missions
})

const mapDispatchToProps = dispatch => {
    add: mission => dispatch(AddMissionAsync(mission))
}

export default connect(mapStateToProps, mapDispatchToProps)(AddMission)
