import React, { useEffect } from 'react'
import { Text, Layout, Button, Card, OverflowMenu, IndexPath, MenuItem, Icon } from '@ui-kitten/components'

import { View, TouchableOpacity, ScrollView, TouchableNativeFeedback, Dimensions } from 'react-native';
import { Emoji } from '../emoji/emoji';
import { getMoods, } from '../models/api';
import { AddEntry } from '../actions/entry.actions';
import { connect } from 'react-redux'
import { RequestCategoryAsync } from '../actions/category.action';
import { PastEntriesList, UppercaseHeading } from './entry/past';
import { useIsFocused } from '@react-navigation/native';
import { iOSUIKit } from 'react-native-typography';
import { default as theme } from '../styles/app-theme.json'
import { DefaultText, H4, TextMuted, TextMutedStrong } from '../styles/text';

import MyDayTasks from './task/task.myday';

import { TopNav } from '../menu/header';
import { CONTAINER_PADDING, CONTENT_START, DEFAULT_MARGIN, FAB_DIM, MENU_HEIGHT_COLLAPSED, MENU_HEIGHT_EXPANED, PADDING } from '../common/constants';
import { useNavigation } from '@react-navigation/native';
import Animated, { Extrapolate } from 'react-native-reanimated';
import { EmptyListPlaceholder } from './day.past';
import { EmojiEmpty } from '../main';
import { GoalCard } from './goals/card';
import GoalProgressList from './goals/progress.list';
import { Col, Row } from 'react-native-easy-grid';
import { ButtonText } from './activity';
import KpiCard from './reports/kpi.card';
import { ReminderIcon } from './task/facts.group';

export const EmojiItem = ({ emoji }) => {
    return (
        <View style={{ justifyContent: 'center' }} >

            <Emoji name={emoji}></Emoji>
            {/*  <Text style={[iOSUIKit.footnoteEmphasized, { textAlign: 'center', marginTop: 8 }]}>{title}</Text> */}
        </View>
    )
}

const _EmojiActions = ({ navigation, addEntry, loadCategory, onSelect = null, ...props }) => {
    let [moods, setMoods] = React.useState([]);
    let [fetched, setFetched] = React.useState(false);
    if ((!moods || !moods.length) && !fetched) {
        getMoods().then((__moods) => {
            setMoods(__moods);

            setFetched(true);
        })
    }
    return (
        <View style={{ flexDirection: 'row', height: 64, justifyContent: 'space-between', alignItems: 'center' }}>
            {moods.map((d, idx) => {
                return (
                    <TouchableOpacity key={`moods-${idx}`} onPress={() => {
                        if (!onSelect) {
                            loadCategory();
                            addEntry(d.id)
                            navigation.navigate('entryPage');
                        } else {
                            if (typeof onSelect === 'function') {
                                onSelect(d.id);
                            }
                        }
                    }} >
                        <EmojiItem emoji={d.emoji} title={d.title} key={idx}></EmojiItem>
                        {(props.count && !isNaN(props.count[idx])) && <TextMutedStrong style={{ textAlign: 'center', marginTop: 4 }} text={props.count[idx]} />}
                    </TouchableOpacity>
                )

            })}
        </View>
    )
}



const mapDispatchToProps = dispatch => {
    return {
        addEntry: mood => dispatch(AddEntry(mood)),
        loadCategory: () => dispatch(RequestCategoryAsync())

    }
}

export const EmojiActions = connect(null, mapDispatchToProps)(_EmojiActions);
export const MinimizeView = ({ isMinimized, children }) => {
    return (
        <View style={{ height: isMinimized ? 0 : 'auto', padding: 8, paddingTop: 0 }}>
            {children}
        </View>
    )
}

export const CardSimpleHeader = (props) => {
    return (
        <View {...props}>
            <H4 text={props.title}></H4>
        </View>
    )
}
export const DayTracker = ({ grouped, ...props }) => {
    const isFocused = useIsFocused();
    const navigation = useNavigation();
    const [visible, setVisible] = React.useState(false);
    const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
    const [myday, hasMyDay] = React.useState(false);
    let scrollY = Animated.useValue(0);
    let scrollRef = null;
    console.log(props.kpi);

    const openModal = () => {
        if (props.onModalOpen && typeof props.onModalOpen === 'function') {
            props.onModalOpen();
        }
    }

    /*  const headerHeight = scrollY.interpolate({
         inputRange: [0, MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED],
         outputRange: [MENU_HEIGHT_EXPANED, MENU_HEIGHT_COLLAPSED],
         extrapolate: Extrapolate.CLAMP
 
     })
 
     const fontSize = scrollY.interpolate({
         inputRange: [0, MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED],
         outputRange: [24, 20],
         extrapolate: Extrapolate.CLAMP
     })
     const opacity = scrollY.interpolate({
         inputRange: [0, (MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED) / 2],
         outputRange: [1, 0],
         extrapolate: Extrapolate.CLAMP
 
     }); */
    const onSelect = (item) => {
        setVisible(false);
    }
    const renderToggleButton = () => (
        <View style={{ justifyContent: 'center', alignItems: 'flex-end', }}>
            <TouchableNativeFeedback onPress={() => setVisible(!visible)} background={TouchableNativeFeedback.Ripple(theme['color-font-300'], false)} >
                <View >
                    <Icon width={24} height={24} fill={theme['color-font-300']} name="more-horizontal-outline"></Icon>
                    {/*   <DefaultText style={{ color: theme['color-primary-500'] }} text="Uzair"></DefaultText> */}
                </View>
            </TouchableNativeFeedback>
        </View>
    );
    const Kpi = ({ items }) => {

        if (items) {
            let keys = Object.keys(items);
            return (
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}
                >
                    <View style={{ flexDirection: 'row' }}>
                        {keys.map(key => {
                            return (
                                <KpiCard value={items[key]} title={key}></KpiCard>
                            )
                        })}
                    </View>
                </ScrollView>
            )
        } else {
            return <View />
        }

    }
    const rightDropdown = () => {
        return (
            <Layout level='1'>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>

                    <OverflowMenu
                        anchor={renderToggleButton}
                        visible={visible}
                        /*  selectedIndex={selectedIndex} */
                        onSelect={onSelect}
                        onBackdropPress={() => setVisible(false)}>
                        <MenuItem title={props => <TextMutedStrong text="Create Account" />} />
                        <MenuItem title={props => <TextMutedStrong text="Help" />} />
                        <MenuItem title={props => <TextMutedStrong text="About" />} />
                        <MenuItem title={props => <TextMutedStrong text="Dailyme Web" />} />
                    </OverflowMenu>
                    <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)}>
                        <View style={{ marginLeft: DEFAULT_MARGIN / 2 }}>
                            <Icon name='bell-outline' fill={theme["color-font-200"]} width={24} height={24} {...props} />
                        </View>
                    </TouchableNativeFeedback>
                </View>
            </Layout>
        )
    }

    let hasGroupItems = (Array.isArray(grouped) && grouped.length > 0);
    return (
        <Layout style={{ flex: 1, }} level="3" >
            <ScrollView keyboardShouldPersistTaps="always" keyboardDismissMode="on-drag"
                scrollEventThrottle={32}


            >
                <TopNav title="Dailyme" navigation={props.navigation} accessoryRight={rightDropdown}>

                </TopNav>
                <View style={{ backgroundColor: 'white', paddingLeft: CONTAINER_PADDING / 2, paddingRight: CONTAINER_PADDING / 2 }}>
                    <EmojiActions navigation={navigation}></EmojiActions>
                </View>
                <View style={{ padding: PADDING / 2 }}>
                    <UppercaseHeading margin={0} heading="Overall Progress"></UppercaseHeading>
                </View>
                <Kpi items={props.kpi}></Kpi>
                <View style={{ paddingLeft: CONTAINER_PADDING / 2, paddingRight: CONTAINER_PADDING / 2 }}>

                    {hasGroupItems && <View style={{ marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                        <Row>
                            <Col>
                                <UppercaseHeading margin={0} heading="Moods Today"></UppercaseHeading>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => navigation.navigate('moodsList')}>
                                    <View>
                                        <UppercaseHeading margin={0} heading="View All"></UppercaseHeading>

                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>
                    </View>}
                    <View >
                        {hasGroupItems && <PastEntriesList viewType={2} navigation={navigation} grouped={grouped}></PastEntriesList>}
                        {/*  {!hasGroupItems && <EmptyListPlaceholder
        icon={<EmojiEmpty style={{ fontSize: 42, color: theme["color-font-light-500"], marginBottom: DEFAULT_MARGIN / 4 }}></EmojiEmpty>}
        text="No mood entry today"
        subtext="Tap on a mood above to add a new entry"
        backgroundColor='#fff' />} */}
                    </View>

                    {/*  {myday && <View style={{ marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                        <UppercaseHeading margin={0} heading="Tasks for today"></UppercaseHeading>
                    </View>}
                    {<MyDayTasks empty={isEmpty => {
                        if (myday != isEmpty) {
                            hasMyDay(isEmpty);
                        }
                    }}></MyDayTasks>} */}
                    <View style={{ marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>
                        <UppercaseHeading margin={0} heading="Goals"></UppercaseHeading>
                    </View>
                    <GoalProgressList></GoalProgressList>


                </View>

            </ScrollView>



        </Layout >

    )
}
