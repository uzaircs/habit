import { getCategories } from "../models/api";
import React from "react";
import { Layout, Button, Text, Card, Icon, Spinner, Input, Divider } from "@ui-kitten/components";
import { ScrollView } from "react-native-gesture-handler";
import { fontFamily, sizes, fontColors } from "../styles/fonts";
import { connect } from 'react-redux'
import { CategoryCardComponent } from "./activity";
import { saveAsync, ClearPendingItems, RequestListAsync } from "../actions/entry.actions";
import { BackNav } from "../menu/header";
import { Row, Col } from "react-native-easy-grid";
import { RequestCategoryAsync } from "../actions/category.action";
import { View } from "react-native";
import LottieView from 'lottie-react-native'
import { InputLabel } from "./forms/category";
import { FontFamilies } from "../styles/text";
import { DEFAULT_MARGIN } from "../common/constants";
import { default as theme } from '../styles/app-theme.json'
const PlusIcon = (props) => (
    <Icon {...props} name='plus-square-outline' />
);
export const NewGroupOrItem = ({ navigation, ...props }) => {
    return (
        <Button accessoryLeft={PlusIcon} size='small' appearance="outline" onPress={() => navigation.navigate('addActivity')}>Create New Activity</Button>
    )
}
export const CenterLoading = () => {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <LottieView style={{ width: 160 }} renderMode="HARDWARE" source={require('../images/loading.json')} autoPlay loop={true} />
        </View>
    )
}

const _ActivityPage = (props) => {
    const [notes, setNotes] = React.useState('');

    if (!props.hasItems) {
        return <CenterLoading></CenterLoading>
    }

    const save = () => {
        props.save(notes);
    }
    if (props.saved) {
        /*      props.request(); */
        props.clear();
        props.navigation.pop();
    }
    if (props.error) {

    }
    return (
        <Layout style={{ flex: 1 }} level="4">
            <BackNav title="Select Activities" navigation={props.navigation}></BackNav>
            <ScrollView style={{ flex: 1 }} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>

                <Layout style={{ padding: 8 }} level="4">
                    <Row style={{ justifyContent: 'center', marginTop: 8 }}>
                        <Text style={[fontFamily.semibold, sizes.medium, fontColors.lightDark]}>What were you upto?</Text>

                    </Row>
                    <Row style={{ justifyContent: 'center', marginTop: 8 }}>
                        <NewGroupOrItem navigation={props.navigation}></NewGroupOrItem>
                    </Row>
                    {props.categories.map((c, idx) => {
                        return (<CategoryCardComponent key={idx} category={c} navigation={props.navigation}></CategoryCardComponent>)
                    })}
                    <Divider style={{ marginTop: DEFAULT_MARGIN, marginBottom: DEFAULT_MARGIN / 2, backgroundColor: theme["color-font-200"] }}></Divider>
                    <Input
                        label={props => <InputLabel label="Notes" style={{ marginTop: 8 }} />}
                        multiline={true}
                        value={notes}
                        onChangeText={(text) => setNotes(text)}
                        numberOfLines={10}
                        textStyle={[{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }, { textAlignVertical: 'top', paddingTop: 8 }]}
                        style={{ textAlignVertical: 'top' }}

                        placeholder='Write some notes if need be...'

                    />
                    <Button status="primary" style={{ marginTop: 16 }} onPress={save}>
                        Save
                        </Button>
                </Layout>
            </ScrollView>
        </Layout>
    )
}
const mapStateToProps = (state) => ({
    saved: state.pending.saved,
    error: state.pending.error,
    categories: state.categories.items,
    hasItems: state.categories.hasItems
})

const mapDispatchToProps = dispatch => {
    return {
        save: notes => dispatch(saveAsync(notes)),
        clear: () => dispatch(ClearPendingItems()),
        request: () => dispatch(RequestListAsync()),

    }
}

export const ActivityPage = connect(mapStateToProps, mapDispatchToProps)(_ActivityPage);