import React from 'react'
import { Layout, Text, Card, CheckBox, Icon, Button, Spinner } from '@ui-kitten/components'
import { fontFamily, sizes } from '../styles/fonts'
import { getCategories, getActivities } from '../models/api'
import { Icon as NBIcon } from 'native-base'
import { View, ScrollView, TouchableOpacity, StyleSheet, TouchableWithoutFeedback, TouchableNativeFeedback } from 'react-native'
import { default as theme } from '../styles/app-theme.json'
import Activity from '../models/activity'
import { connect } from 'react-redux'
import { AddActivity, RemoveActivity } from '../actions/entry.actions'
import { Row, Col } from 'react-native-easy-grid'
import { iOSUIKit } from 'react-native-typography'
import { FontFamilies } from '../styles/text'


export const AutoApplyIcon = ({ item, small = null, defaultColor = theme["color-font-300"] }) => {

    let _activity: Activity = item;
    if (!_activity || !_activity.icon) {
        return <></>
    }
    let style_small = {
        width: 18,
        height: 18
    }
    let style_normal = {
        width: 32, height: 32
    }
    let font_size_small = {
        fontSize: 18
    }
    let font_size_normal = {
        fontSize: 28
    }
    if (_activity.icon_type == 'mci') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="MaterialCommunityIcons" name={_activity.icon} style={[{ color: defaultColor }, small]} />
        )
    }
    if (_activity.icon_type == 'eva') {
        if (!small) {
            small = style_normal;
        } else {
            small = style_small;
        }
        let _comp = <Icon name={_activity.icon} fill={defaultColor} style={small}></Icon>;

        return _comp;
    }
    if (_activity.icon_type == 'fa') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="FontAwesome5" name={_activity.icon} style={[{ color: defaultColor }, small]} />
        )
    }
    if (_activity.icon_type == 'ionic') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="Ionicons" name={_activity.icon} style={[{ color: defaultColor }, small]} />
        )
    }
}

export const ActivityIcon = ({ isActive, activity, small = null, defaultColor = theme["color-font-300"] }) => {

    if (!defaultColor) {
        defaultColor = theme["color-font-300"];
    }
    let _activity: Activity = activity;
    if (!_activity || !_activity.icon) {
        return <></>
    }
    let style_small = {
        width: 18,
        height: 18
    }
    let style_normal = {
        width: 32, height: 32
    }
    let font_size_small = {
        fontSize: 18
    }
    let font_size_normal = {
        fontSize: 28
    }
    if (_activity.icon_type == 'mci') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="MaterialCommunityIcons" name={_activity.icon} style={[{ color: isActive ? theme["color-font-light-100"] : defaultColor }, small]} />
        )
    }
    if (_activity.icon_type == 'eva') {
        if (!small) {
            small = style_normal;
        } else {
            small = style_small;
        }
        let _comp = <Icon name={_activity.icon} fill={isActive ? theme["color-font-light-100"] : defaultColor} style={small}></Icon>;

        return _comp;
    }
    if (_activity.icon_type == 'fa') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="FontAwesome5" name={_activity.icon} style={[{ color: isActive ? theme["color-font-light-100"] : defaultColor }, small]} />
        )
    }
    if (_activity.icon_type == 'ionic') {
        if (!small) {
            small = font_size_normal
        } else {
            small = font_size_small;
        }
        return (
            <NBIcon type="Ionicons" name={_activity.icon} style={[{ color: isActive ? theme["color-font-light-100"] : defaultColor }, small]} />
        )
    }

}
const styles = StyleSheet.create({
    iconBorder: {
        borderColor: theme["color-font-200"],
        borderWidth: 1
    },
    iconContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 42,
        height: 42,
        marginBottom: 4,
        /*   backgroundColor: theme["color-primary-500"], */
        borderRadius: 21
    },
    containerActive: {
        backgroundColor: theme["color-primary-700"]
    }
})
const _ActivityItem = ({ activity, addActivity, removeActivity, active }) => {
    const [localActive, setLocalActive] = React.useState(false);
    const _styles: any[] = [styles.iconContainer];
    if (localActive) {
        _styles.push(styles.containerActive);
    } else {
        _styles.push(styles.iconBorder);
    }
    const toggleActivitity = () => {
        if (localActive) {
            setLocalActive(false)
            removeActivity(activity.id);
        } else {
            setLocalActive(true);
            addActivity(activity.id);


        }
    }
    return (
        <Layout style={{ alignItems: 'center' }}>
            <TouchableWithoutFeedback onPress={toggleActivitity} >
                <View style={_styles}>
                    <ActivityIcon isActive={localActive} activity={activity}></ActivityIcon>
                </View>

            </TouchableWithoutFeedback>
            <Text category="label" style={[{ fontFamily: FontFamilies.quicksandbold.fontFamily }, { textAlign: 'center', marginBottom: 8, color: theme["color-font-400"] }]}>{activity.name}</Text>
        </Layout>

    )
}
const mapStateToProps = (state, props) => ({
    active: state.pending.activity.findIndex(a => a == props.activity.id) > -1
})

const mapDispatchToProps = dispatch => {
    return {
        addActivity: (activity) => dispatch(AddActivity(activity)),
        removeActivity: (activity) => dispatch(RemoveActivity(activity))
    }
}
export const ButtonText = ({ text, ...props }) => {
    return (
        <Text {...props} style={[FontFamilies.quicksandbold, { color: props.style.color, fontSize: props.style.fontSize, lineHeight: props.style.lineHeight }, { textTransform: 'uppercase' }]}>{text}</Text>
    )
}
export const ButtonTextIOS = ({ text, ...props }) => {
    return (
        <Text {...props} style={[iOSUIKit.largeTitleEmphasized, { color: props.style.color, fontSize: props.style.fontSize, lineHeight: 20 }, { textTransform: 'uppercase' }]}>{text}</Text>
    )
}
export const PlusOutlineIcon = (props) => {
    return (
        <Icon name="plus-outline" fill={theme["color-primary-400"]} {...props}></Icon>
    )
}
export const RightArrowIcon = (props) => {
    return (
        <Icon name="chevron-right-outline" fill={theme["color-primary-400"]} {...props}></Icon>
    )
}
export const AttachClipIcon = (props) => {
    return (
        <Icon name="attach-outline" fill={theme["color-primary-400"]} {...props}></Icon>
    )
}

export const ImageIcon = (props) => {
    return (
        <Icon name="image-outline" fill={theme["color-primary-400"]} {...props}></Icon>
    )
}
export const EditIcon = (props) => {
    return (
        <Icon name="edit-2-outline" fill={theme["color-primary-500"]} {...props}></Icon>
    )
}
export const MinusOutlineIcon = (props) => {
    return (
        <Icon name="minus-outline" fill={theme["color-primary-400"]} {...props}></Icon>
    )
}
export const ActivityItem = connect(mapStateToProps, mapDispatchToProps)(_ActivityItem);
const _CategoryCardComponent = ({ category, hasItems, ...otherProps }) => {
    const { id } = category;
    const header = props => {

        return (
            <View style={{ paddingLeft: 16, paddingTop: 4 }}>
                <Row size={12} style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Col size={9}>
                        <Text category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{category.name}</Text>
                    </Col>
                    <Col size={4}>
                        <Button onPress={() => otherProps.navigation.navigate('addActivity', { id: id })} accessoryLeft={PlusOutlineIcon} children={(props) => <ButtonText text="Add New" {...props}></ButtonText>} appearance="ghost" size="small" />
                    </Col>
                </Row>
            </View>
        )
    }
    if (!hasItems) {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Spinner></Spinner>
            </View>
        )
    }

    return (
        <Card disabled header={header} appearance="filled" style={{ elevation: 4, marginTop: 16 }}>
            <Layout style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', flex: 1 }}>
                {category.activities.map((a, idx) => {
                    return (
                        <Layout key={idx} style={{ width: '25%', marginBottom: 8 }}>
                            <ActivityItem activity={a}></ActivityItem>
                        </Layout>
                    )
                })}
            </Layout>
        </Card>
    )
}


const mapStateToPropsForCategoryCard = (state, ownProps) => ({

    hasItems: state.categories.hasItems
})

export const CategoryCardComponent = connect(mapStateToPropsForCategoryCard, null)(_CategoryCardComponent);
