import { Button, Divider, Input, Layout } from '@ui-kitten/components'
import moment from 'moment';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ClearAllSessions, DateToDbUnix } from '../models/api';
import { AddSessionEventAction, AddSessionTaskAction, PopulateSession, SaveSessionAction } from '../actions/session.action'
import { Alert, ScrollView } from 'react-native';
import { SessionEventType } from './timer/session';

export class Playground extends Component<any, any>{

    constructor(props) {
        super(props);
        this.state = {
            status: '0',
            time: '25',
            longBreakAfter: '2',
            shortBreak: '5',
            longBreak: '15',
            name: 'Some name',
            limit: '4',
            taskId: '3rczxyhqvbdw8go7',
            session_id: 'p2y1cxieutybn3w0',
            event: {
                event_type: SessionEventType.POMODORO,
                /* startAt :  */
            }
        }
    }
    populatedAndBack = () => {
        this.props.navigation.navigate('Home')
    }
    save = () => {
        if (this.props.save && typeof this.props.save === 'function') {
            let { longBreak, shortBreak, longBreakAfter, status, time, limit } = this.state;
            this.props.save({
                longBreakAfter: +longBreakAfter,
                longBreak: +longBreak,
                shortBreak: +shortBreak,
                status: +status,
                time: +time,
                limit: +limit,

            });
        }
    }
    componentDidMount() {
        if (this.props.load && typeof this.props.load === 'function') {
            this.props.load();
        }
    }
    clear = () => {
        ClearAllSessions().then(() => {
            Alert.alert('Cleared');
        });
    }
    SaveEvents = () => {
        this.props.add_event(this.state.session_id, {
            eventType: SessionEventType.POMODORO,
            startAt: DateToDbUnix(new Date()),
            endAt: DateToDbUnix(new Date()),
        });
    }
    addTask = () => {
        this.props.add_task(this.state.session_id, [this.state.taskId]);
    }
    render() {

        return (
            <Layout style={{ flex: 1, padding: 16 }}>
                <ScrollView>
                    <Button onPress={this.clear}>Clear</Button>
                    <Button onPress={this.populatedAndBack}>Populate DB</Button>
                    <Button onPress={() => this.props.load}>Populate Session</Button>
                    <Input label="time" value={this.state.time}></Input>
                    <Input label="short break" value={this.state.shortBreak}></Input>
                    <Input label="long break" value={this.state.longBreak}></Input>
                    <Input label="long break after" value={this.state.longBreakAfter}></Input>
                    <Input label="limit" value={this.state.limit}></Input>
                    <Input label="name" value={this.state.name}></Input>

                    {/*    <Input label="time" value={'4'}></Input> */}
                    <Button onPress={this.save}>Save</Button>
                    <Divider></Divider>
                    <Input label="session_id" value={this.state.session_id}></Input>
                    <Input label="Task id" value={this.state.taskId}></Input>
                    <Button onPress={this.addTask}>Add Task</Button>
                    <Button onPress={this.SaveEvents}>Add Event</Button>
                </ScrollView>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    sessions: state.sessions
})

const mapDispatchToProps = dispatch => {
    return {
        save: session => dispatch(SaveSessionAction(session)),
        load: () => dispatch(PopulateSession()),
        add_task: (id, task) => dispatch(AddSessionTaskAction(id, task)),
        add_event: (id, event) => dispatch(AddSessionEventAction(id, event))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Playground)
