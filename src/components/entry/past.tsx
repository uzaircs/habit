import { EmptyListPlaceholder } from "../day.past"
import { View, Text, TouchableOpacity } from "react-native"
import { fontFamily, sizes, fontColors } from "../../styles/fonts"
import React from "react"
import { Divider, Icon, OverflowMenu, MenuItem, Modal, Card, Button } from "@ui-kitten/components"
import { UserEntryActivity, UserEntry } from "../../models/activity.entries"
import moment from "moment"
import { Emoji } from "../../emoji/emoji"
import { ActivityIcon, ButtonText } from "../activity"
import { default as theme } from '../../styles/app-theme.json'
import { ReqeustInfluenceAsync } from "../../actions/influence.actions"
import { connect } from 'react-redux'
import { Row, Grid, Col } from "react-native-easy-grid"
import { iOSUIKit } from "react-native-typography"
import { DEFAULT_MARGIN, EMOJI_DIM, TASK_CARD_HEIGHT } from "../../common/constants"
import { MenuIcon } from "../task/list"
import { FontFamilies } from "../../styles/text"

/* item = UserEntry */
export const _ActivityItem = ({ item, navigation, request, showFullDate = false }) => {
    const [selectedIndex, setSelectedIndex] = React.useState(null);
    const [visible, setVisible] = React.useState(false);
    const [modalVisibility, setModalVisibility] = React.useState(false);
    const requestInfluence = () => {
        request(item.mood_id);

        navigation.navigate('itemDetail', { entry: item, navigation, emoji: item.emoji, title: item.mood, mood_id: item.mood_id })
    }
    const IconCustom = ({ name, color = theme["color-font-300"], ...props }) => {
        return <Icon {...props} fill={color} name={name} />
    };
    const actions = [
        {
            title: 'View',
            icon: 'chevron-right-outline'
        },
        {
            title: 'Edit Mood',
            icon: 'chevron-right-outline'

        },
        {
            title: 'Add activities',
            icon: 'chevron-right-outline'
        },
        {
            title: 'Delete',
            icon: 'trash-2-outline',
            color: theme["color-danger-500"]
        },
    ];

    const handleAction = index => {
        setVisible(false);
        switch (index) {
            case 0:
                requestInfluence();
                break;
            case 3:
                setModalVisibility(true);
                break;
            default:

                break;
        }

    }

    const RenderTitle = (title: string, color?: any) => {
        return (
            <Text style={[fontFamily.semibold, sizes.normal, color ? { color: color } : fontColors.mediumDark]}>{title}</Text>
        )
    }
    const renderItem = () => (
        <TouchableOpacity >
            <Grid>
                <Row size={12}>

                    <Col size={10}>
                        <Text style={[iOSUIKit.caption2Emphasized, fontColors.muted]}>{showFullDate ? (moment(item.created_at).format("Do MMM h:mm A")) : moment(item.created_at).format("h:mm A")}</Text>
                        {/*  <Text style={[iOSUIKit.bodyEmphasizedObject, { color: item.color }]}>{item.mood}</Text> */}
                        <View style={{ marginTop: 4, flexDirection: 'row', flexWrap: 'wrap' }}>
                            {
                                item.activity.map((activity: UserEntryActivity, index) => {
                                    if (index == 3) {
                                        return (
                                            <View style={{ marginRight: 10, marginBottom: 8, flexDirection: 'column', alignItems: 'center' }}>
                                                <View style={{ marginRight: 4 }}>
                                                    <Text style={[iOSUIKit.caption2Emphasized, fontColors.muted, { marginBottom: 6 }]}>. . .</Text>
                                                </View>
                                                <Text style={[iOSUIKit.caption2Emphasized, fontColors.muted]}>{(item.activity.length - 3) + ' More'}</Text>
                                            </View>
                                        )
                                    }
                                    if (index > 3) {
                                        return <></>;
                                    }
                                    return (
                                        <View style={{ marginRight: 10, marginBottom: 8, flexDirection: 'column', alignItems: 'center' }}>
                                            <View style={{ marginRight: 4 }}>
                                                <ActivityIcon isActive={false} activity={activity} small={true} defaultColor={theme["color-font-300"]} />
                                            </View>
                                            <Text style={[iOSUIKit.caption2Emphasized, fontColors.muted]}>{activity.title}</Text>
                                        </View>
                                    )
                                })
                            }

                        </View>
                    </Col>
                    <Col size={2} style={{ alignItems: 'flex-end' }}>
                        <Emoji gif={true} name={item.emoji} ></Emoji>
                    </Col>
                </Row>

            </Grid>

        </TouchableOpacity >
    )
    return (
        <View>
            <Modal
                visible={modalVisibility}
                backdropStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}
                onBackdropPress={() => setModalVisibility(false)}>
                <Card disabled={true}>
                    <Text style={[fontFamily.semibold, sizes.normal, { color: theme["color-font-400"], marginBottom: 16 }]}>Are you sure you want to delete this entry?</Text>
                    <Row size={12}>
                        <Col size={6} style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                            <Button children={props => <ButtonText {...props} text="Keep" />} accessoryLeft={(props) => <Icon {...props} name="chevron-left-outline"></Icon>} appearance="ghost" status="primary" onPress={() => setModalVisibility(false)} />

                        </Col>
                        <Col size={6} style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                            <Button children={props => <ButtonText {...props} text="Delete" />} size="small" accessoryLeft={(props) => <Icon {...props} name="trash-2-outline"></Icon>} appearance="outline" status="danger" onPress={() => setModalVisibility(false)} />


                        </Col>
                    </Row>
                </Card>
            </Modal>
            <OverflowMenu
                anchor={renderItem}
                visible={visible}
                selectedIndex={selectedIndex}

                placement="bottom end"
                backdropStyle={{
                    backgroundColor: 'rgba(0, 0, 0, 0.1)',
                }}
                onBackdropPress={() => setVisible(false)}>
                {actions.map((action, idx) => {
                    return (
                        <MenuItem onPress={() => handleAction(idx)} title={(props) => RenderTitle(action.title, action.color)} accessoryRight={(props) => <IconCustom {...props} name={action.icon} color={action.color} ></IconCustom>}></MenuItem>
                    )
                })}
            </OverflowMenu>
        </View>



    )
};


const mapDispatchToProps = dispatch => {
    return {
        request: mood_id => dispatch(ReqeustInfluenceAsync(mood_id))
    }
}

export const ActivityItem = connect(null, mapDispatchToProps)(_ActivityItem)

export type GroupedUserEntry = {
    date: moment.Moment,
    items: UserEntry[]
}
export const GroupUserEntries = (items: UserEntry[]): GroupedUserEntry[] => {
    if (items && items.length) {
        let grouped: GroupedUserEntry[] = [];
        items.forEach((item: UserEntry) => {
            let __date = moment(item.created_at);
            let __exists = grouped.findIndex(g => g.date.isSame(__date, 'day'));
            if (__exists > -1) {
                grouped[__exists].items.push(item);
            } else {
                // push new entry
                let newGroup: GroupedUserEntry = {
                    date: __date,
                    items: [item]
                }
                grouped.push(newGroup);
            }
        })
        return grouped;
    } else {
        return [];
    }
}
export const UppercaseHeading = ({ heading, margin = 8 }) => {
    return (
        <Text style={[iOSUIKit.caption2EmphasizedObject, fontColors.muted, FontFamilies.quicksandbold, { marginBottom: margin, marginTop: margin, textTransform: 'uppercase' }]}>{heading}</Text>
    )
}
export const PastEntriesList = ({ navigation, grouped, viewType = 0, viewLabel }) => {
    return (

        grouped.map((group, index) => {
            return <View key={index} >
                {viewType <= 1 && <View style={{ flexDirection: 'row', justifyContent: "center", flex: 1, alignItems: 'center', padding: 4, paddingRight: 16, borderTopStartRadius: 4, borderTopEndRadius: 4, }}>
                    {/*   <Icon name="calendar-outline" fill={theme["color-font-400"]} style={{ width: 18, height: 18, marginRight: 8 }}></Icon> */}
                    {viewType == 0 && <Text style={[iOSUIKit.callout, fontColors.muted, { paddingBottom: 8, paddingLeft: 12, textAlign: "right", textTransform: "uppercase" }]}>{moment(group.date).format('dddd, Do MMM ')}</Text>}
                    {viewType == 1 && <Text style={[iOSUIKit.callout, fontColors.muted, { paddingBottom: 8, paddingLeft: 12, textAlign: "right", textTransform: "uppercase" }]}>{viewLabel}</Text>}
                </View>}
                {group.items.map((item, key) => {
                    return (
                        <Card onPress={() => {
                            navigation.navigate('itemDetail', { entry: item, navigation, emoji: item.emoji, title: item.mood, mood_id: item.mood_id })
                        }} appearance="filled" key={key} style={{ height: TASK_CARD_HEIGHT, marginBottom: DEFAULT_MARGIN / 4 }} >
                            <ActivityItem key={key} item={item} navigation={navigation}></ActivityItem>
                        </Card>
                    )
                })}
            </View>
        })
    )

}
export const PastList = ({ items = [], navigation }) => {

    if (!items || !items.length) {
        return (
            <EmptyListPlaceholder></EmptyListPlaceholder>
        )
    } else {
        /*   let grouped = GroupUserEntries(items); */
        {/* <PastEntriesList grouped={grouped}></PastEntriesList> */ }
        return (
            <View style={{ marginTop: 24 }}>

                <Row size={12} style={{ marginBottom: 16 }}>
                    <Col style={{ justifyContent: 'center' }} size={4}><UppercaseHeading margin={0} heading="Recent Entries"></UppercaseHeading></Col>
                    <Col size={8} style={{ justifyContent: 'center' }}>
                        <Divider style={{ width: '100%', backgroundColor: theme["color-font-200"] }}></Divider>
                    </Col>
                </Row>

            </View>

        )
    }
}


/* export const PastList = connect(mapStateToProps, null)(_PastList); */