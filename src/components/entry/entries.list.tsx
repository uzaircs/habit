import { NavigationContext } from '@react-navigation/native';
import { Card, Layout } from '@ui-kitten/components';
import React, { Component } from 'react'
import { InteractionManager, ScrollView, View } from 'react-native';
import { connect } from 'react-redux'
import { BackNav } from '../../menu/header';
import { DEFAULT_MARGIN, TASK_CARD_HEIGHT } from '../../common/constants';

import { CenterLoading } from '../activity.page';
import { ActivityItem } from './past';

export class EntriesList extends Component<any, any>{
    static contextType = NavigationContext;
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            ready: false
        }
    }
    componentDidMount() {
        InteractionManager.runAfterInteractions(() => {
            this.setState(() => { return { ready: true } })

        })
    }
    render() {
        if (!this.state.ready) return <CenterLoading></CenterLoading>;
        const navigation = this.context;
        return (
            <View style={{ flex: 1 }}>
                <BackNav title="Mood Entries"></BackNav>
                <Layout style={{ flex: 1, padding: DEFAULT_MARGIN / 2 }} level="3">
                    <ScrollView>
                        {this.props.entries.map((entry) => {
                            return (
                                <View>
                                    <Card onPress={() => {
                                        navigation.navigate('itemDetail', { entry: entry, navigation, emoji: entry.emoji, title: entry.mood, mood_id: entry.mood_id })
                                    }} appearance="filled" key={entry.id} style={{ height: TASK_CARD_HEIGHT, marginBottom: DEFAULT_MARGIN / 4 }} >
                                        <ActivityItem showFullDate={true} key={entry.id} item={entry} navigation={navigation}></ActivityItem>
                                    </Card>
                                </View>
                            )
                        })}
                    </ScrollView>
                </Layout>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    entries: state.entryStorage.entries
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(EntriesList)
