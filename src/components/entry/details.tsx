import React, { Component } from 'react'
import { Layout, Divider, Card, Text, Button, ListItem } from '@ui-kitten/components'
import { Emoji } from '../../emoji/emoji'
import { Col, Grid, Row } from 'react-native-easy-grid'
import { MoodInfluencer, GetMoodCount } from '../reports/reports'
import { connect } from 'react-redux'
import { fontFamily, sizes, fontColors } from '../../styles/fonts'
import { View, Badge, Toast } from 'native-base'
import { ActivityIcon, EditIcon } from '../activity'
import { default as theme } from '../../styles/app-theme.json'
import { ScrollView, StyleSheet, TouchableNativeFeedback, TouchableOpacity } from 'react-native'
import { UppercaseHeading } from './past'
import { UserEntryActivity } from '../../models/activity.entries'
import { BackNav } from '../../menu/header'
import moment from 'moment'
import { ReqeustInfluenceAsync } from '../../actions/influence.actions'
import { iOSUIKit } from 'react-native-typography'
import { DEFAULT_MARGIN } from '../../common/constants'
import { ClockIcon, TrashIcon } from '../task/details'

import Modal from 'react-native-modal';
import { DefaultText, FontFamilies, H4, TextMuted, TextSubtitle } from '../../styles/text'
import { MoodIcon } from '../day/day.tabs'
import { EmojiActions } from '../day'
import { MoodChanged, RemoveEntry } from '../../actions/entry.actions'
import { getMoods } from '../../models/api'
import { CenterLoading } from '../activity.page'

const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    card: {

        margin: 2,
    },
    footerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    footerControl: {
        marginHorizontal: 2,
    },
});
export const InfluenceScoreItem = ({ title, score, activity, iconType, icon, first = false }) => {
    let _activity = {
        icon: icon,
        icon_type: iconType,
        id: activity,
        name: title
    }
    return (
        <View style={{ flexDirection: 'column', marginRight: 8, marginBottom: 8 }}>
            <Badge style={{ backgroundColor: first ? theme["color-primary-400"] : 'rgba(0,0,0,0)', borderColor: first ? theme["color-primary-400"] : theme["color-font-200"], borderWidth: 1, }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <View style={{ marginRight: 4 }}>
                        <ActivityIcon small={true} activity={_activity} isActive={false} defaultColor={first ? fontColors.light.color : theme["color-primary-300"]}></ActivityIcon>
                    </View>
                    <Text style={[iOSUIKit.caption2Emphasized, first ? fontColors.light : fontColors.lightDark, FontFamilies.quicksandbold]}>{title}</Text>
                    <Text style={[iOSUIKit.footnoteEmphasized, FontFamilies.quicksandbold]}> {(+score * 100).toFixed(0)}%</Text>
                </View>
            </Badge>
        </View>
    )
}
export const ActivityEntriesCard = ({ activities, title, subtitle, ...props }) => {
    return (
        <Card>
            <Row size={12}>
                <Col size={props.ac_right ? 8 : 12}>
                    <Text style={[iOSUIKit.subheadEmphasized, FontFamilies.quicksandbold, fontColors.lightDark]}>{title}</Text>
                    <Text style={[iOSUIKit.caption2, fontColors.lightDark, FontFamilies.quicksandsemibold]}>{subtitle}</Text>
                </Col>
                {props.ac_right && <Col size={4}>
                    {props.ac_right && props.ac_right()}
                </Col>}
            </Row>


            <View style={{ marginTop: 9, flexDirection: 'row', flexWrap: 'wrap' }}>
                {activities.map((activity: UserEntryActivity, index) => {
                    return (
                        <View style={{ marginRight: 10, marginBottom: 8, flexDirection: 'column', alignItems: 'center' }}>
                            <View style={{ marginRight: 4 }}>
                                <ActivityIcon isActive={false} activity={activity} small={true} defaultColor={theme["color-font-300"]} />
                            </View>
                            <Text style={[iOSUIKit.caption2Emphasized, fontColors.muted, FontFamilies.quicksandbold]}>{activity.title}</Text>
                        </View>
                        /*   <View style={{ marginRight: 10, marginBottom: 8, flexDirection: 'row' }}>
                              <View style={{ marginRight: 4 }}>
                                  <ActivityIcon isActive={false} activity={activity} small={true} defaultColor={theme["color-primary-200"]} />
                              </View>
                              <Text style={[fontFamily.regular, fontColors.muted, sizes.normal]}>{activity.title}</Text>
                          </View> */
                    )
                })
                }
            </View>
            <View style={{ flexDirection: 'row', marginTop: 8, justifyContent: 'flex-end' }}>
                {props.ac_bottom && props.ac_bottom()}
            </View>
        </Card>
    )
}


export const MoodCountIndicator = ({ mood }) => {
    let [count, setCount] = React.useState(null);
    let [fetch, setFetched] = React.useState(false);
    if (!fetch) {
        setFetched(true);
        GetMoodCount(mood).then((result: any) => {

            setCount(result.count);
            setFetched(true);
        })
    }
    if (count) {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={[fontFamily.regular, fontColors.lightDark, { marginRight: 4 }]}>Total entries</Text>
                <Text style={[fontFamily.semibold, sizes.medium]}>x{count}</Text>
            </View>
        )

    }
    else {
        return <></>
    }

}
export const VerticalDivider = () => {
    return (
        <View style={{ borderRightWidth: 1, borderColor: theme["color-font-200"], width: 1, height: '100%', marginLeft: 8, marginRight: 8 }}>

        </View>
    )
}
export const SummaryItem = ({ itemLabel, itemCount }) => {
    return (
        <View style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
            <Text style={[iOSUIKit.caption2EmphasizedObject, fontColors.lightDark, { textTransform: 'uppercase' }]}>{itemLabel}</Text>
            <Text style={[iOSUIKit.title3Emphasized]}>{itemCount}</Text>

        </View>
    )
}
/* { today, yesterday, last7, emoji } */
export const MoodSummary = ({ today, yesterday, last7, emoji, created_at, onEdit }) => {
    return (
        <Card>
            <Row size={12}>
                <Col size={2} >
                    <TouchableOpacity onPress={() => onEdit()}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Emoji name={emoji}></Emoji>
                            <View style={{ marginTop: 4 }}>
                                <Text style={[iOSUIKit.subheadEmphasized, fontColors.primary, { textTransform: "uppercase" }]}>Edit</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                </Col>
                <Col size={10}>
                    <Row>
                        <Col size={2}>
                            <Row style={{ justifyContent: 'flex-end' }}>
                                <SummaryItem itemCount={today} itemLabel="Today"></SummaryItem>

                            </Row>
                        </Col>
                        <Col size={4}>
                            <Row style={{ justifyContent: 'flex-end' }}>
                                <SummaryItem itemCount={yesterday} itemLabel="Yesterday"></SummaryItem>

                            </Row>
                        </Col>
                        <Col size={4}>
                            <Row style={{ justifyContent: 'flex-end' }}>
                                <SummaryItem itemCount={last7} itemLabel="Last 7 days"></SummaryItem>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <AccessoryRightDate selectedDate={created_at}></AccessoryRightDate>
                        </Col>
                    </Row>
                </Col>
            </Row>


        </Card>
    )
}
export const AccessoryRightDate = ({ selectedDate, ...props }) => {
    return (
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
            <ClockIcon width={16} height={16} style={{ marginRight: DEFAULT_MARGIN / 4 }}></ClockIcon>
            <Text style={[iOSUIKit.footnoteEmphasized, fontColors.lightDark, { lineHeight: 30 }]}>{moment(selectedDate).fromNow()}</Text>
        </View>
    )
}
class _details extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            entry: null
        }
    }
    fromProps = () => {
        if (this.props.entries) {
            this.setState((state, props) => {
                return {
                    entry: (() => {
                        try {
                            const { entry } = this.props.route.params;

                            if (!entry) throw new Error('Entry not found');
                            return entry;
                        } catch (error) {
                            return null;
                        }
                    })()
                }
            })

        }
    }
    componentDidMount() {
        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request(this.props.route.params.entry.mood_id);
            this.fromProps();
        }
    }
    save = () => {
        this.setState(() => {
            return {
                isVisible: false,
                moodVisible: false,
                activitiesVisible: false,
                deleteVisible: false
            }
        })

    }
    deleteModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { deleteVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>
                <Button
                    onPress={() => {
                        if (this.props.deleteEntry && typeof this.props.deleteEntry === 'function') {
                            this.props.deleteEntry(this.state.entry.id)
                        }
                        Toast.show({
                            text: 'Entry deleted',
                            textStyle: iOSUIKit.subheadShortWhite,
                            duration: 2000,
                            type: 'success'
                        })
                        this.setState(() => { return { deleteVisible: false }; })
                        setTimeout(() => {
                            if (this.props.navigation) {
                                this.props.navigation.pop();
                            }
                        })
                    }}
                    style={styles.footerControl}
                    size='small'
                    status='danger'>
                    DELETE
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Confirm Delete"></H4>
                <DefaultText text="Are you sure you want to delete this entry? This action cannot be undone"></DefaultText>
            </Card>
        )
    }
    editModal = () => {
        const Footer = (props) => (
            <View {...props} style={[props.style, styles.footerContainer]}>
                <Button
                    onPress={() => this.setState(() => { return { isVisible: false }; })}
                    style={styles.footerControl}
                    size='small'
                    status='basic'>
                    CANCEL
              </Button>

            </View>
        );

        return (
            <Card style={styles.card} footer={Footer} disabled={true}>
                <H4 style={{ marginBottom: DEFAULT_MARGIN }} text="Edit Mood Entry"></H4>
                <ListItem onPress={() => {
                    this.setState(() => {
                        return {
                            moodVisible: true,
                            isVisible: false,
                        }
                    })

                }}>
                    <View style={{ marginRight: DEFAULT_MARGIN / 2 }}>
                        <MoodIcon style={{ tintColor: theme["color-primary-500"] }}></MoodIcon>
                    </View>
                    <DefaultText style={{ color: theme["color-primary-500"] }} text="Change Mood"></DefaultText>
                </ListItem>
                <ListItem>
                    <View style={{ marginRight: DEFAULT_MARGIN / 2 }}>
                        <EditIcon width={24} height={24} />
                    </View>
                    <DefaultText style={{ color: theme["color-primary-500"] }} text="Edit Activities"></DefaultText>
                </ListItem>
                <ListItem onPress={() => {
                    this.setState(() => {
                        return {
                            isVisible: false,
                            deleteVisible: true,
                            moodVisible: false
                        }
                    })

                }}>
                    <View style={{ marginRight: DEFAULT_MARGIN / 2 }}>
                        <TrashIcon color={theme["color-danger-500"]}></TrashIcon>
                    </View>
                    <DefaultText style={{ color: theme["color-danger-500"] }} text="Delete Entry"></DefaultText>
                </ListItem>
            </Card>
        )
    }
    moodChanged = (id) => {
        getMoods().then(moods => {
            if (moods && Array.isArray(moods) && moods.length) {
                let mood = moods.filter(m => m.id == id)[0];
                if (mood) {
                    this.setState(() => {
                        return {
                            emoji: mood.emoji
                        }
                    })
                    if (this.props.request && typeof this.props.request === 'function') {
                        this.props.request(mood.id);
                    }
                }
            }
        })
    }
    /**
     * @description
     * Count total number of entries on specified date
     * @param day Date of entry
     */
    CountEntriesByDate = day => {
        if (day && moment(day).isValid()) {
            if (this.props.entries && Array.isArray(this.props.entries) && this.props.entries.length) {
                let entries = this.props.entries.filter(e => moment(e.created_at).startOf('day').isSame(moment(day).startOf('day'))).filter(e => e.mood_id == this.state.entry.mood_id);
                if (entries && entries.length) {
                    return entries.length;
                }
            }
        } return 0;
    }
    CountEntriesByDateRange = (start, end) => {
        let total = 0;
        if (moment(start).isValid() && moment(end).isValid()) {
            let totalDays = moment(start).startOf('day').diff(moment(end).startOf('day'), 'd');

            for (let i = 0; i < totalDays; i++) {

                total += this.CountEntriesByDate(moment(start).subtract(i, 'd'));
            }
        }
        return total;
    }
    render() {
        if (!this.state.entry) return <CenterLoading></CenterLoading>
        const { entry } = this.props.route.params; //activities
        let { emoji } = this.props.route.params;
        let today = this.CountEntriesByDate(moment());
        let yesterday = this.CountEntriesByDate(moment().subtract(1, 'day'));
        let last7 = this.CountEntriesByDateRange(moment(), moment().subtract('7', 'd'));
        if (this.state.emoji) {
            emoji = this.state.emoji;
        }
        /*       const { title } = this.props.route.params;
              const { mood_id } = this.props.route.params; */

        return (
            <View style={{ flex: 1 }}>
                <Modal isVisible={this.state.isVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.editModal></this.editModal>
                </Modal>
                <Modal isVisible={this.state.moodVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <Card>
                        <EmojiActions onSelect={(mood_id) => {

                            if (this.props.change_mood && typeof this.props.change_mood === 'function') {
                                if (this.state.entry?.id) {
                                    this.props.change_mood(this.state.entry.id, mood_id)
                                    this.moodChanged(mood_id);
                                }
                            }
                            this.setState((state, props) => {
                                return {
                                    moodVisible: false
                                }
                            })

                        }}></EmojiActions>
                    </Card>
                </Modal>
                <Modal isVisible={this.state.activitiesVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.editModal></this.editModal>
                </Modal>
                <Modal isVisible={this.state.deleteVisible} animationIn="slideInLeft" useNativeDriver={true} animationOut="bounceOutRight">
                    <this.deleteModal></this.deleteModal>
                </Modal>
                <Layout style={{ flex: 1 }} level="4">
                    <BackNav navigation={this.props.navigation} title="Mood details"></BackNav>
                    <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                        <View style={{ padding: 8 }}>
                            <View style={{ marginBottom: 16 }}>
                                <MoodSummary onEdit={() => this.setState(() => { return { isVisible: true }; })
                                } created_at={entry.created_at} emoji={emoji} today={today} yesterday={yesterday} last7={last7}></MoodSummary>
                            </View>

                            <ActivityEntriesCard activities={entry.activity} title="Activities in this entry" subtitle="Activities associated with this mood " />
                            {/*   <View style={{ marginTop: 16 }}>
                                <ActivityEntriesCard activities={entry.activity} title="Uncommon Activities" subtitle="Activities that are not associated with this mood so far" />
                            </View> */}
                            {(entry.notes && entry.notes.length) && <View>
                                <View style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                                    <UppercaseHeading heading="Notes"></UppercaseHeading>
                                </View>
                                <Card appearance="filled" style={{ flex: 1 }} >
                                    <Text category="p1" style={{ fontFamily: FontFamilies.quicksandsemibold.fontFamily }}>{entry.notes}</Text>
                                </Card>
                            </View>}
                            <View style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                                <UppercaseHeading heading="Other information for this mood"></UppercaseHeading>
                            </View>
                            <Card appearance="filled" style={{ flex: 1 }} >
                                <View style={{ marginBottom: 8 }} >
                                    <Text style={[iOSUIKit.subheadEmphasized, fontColors.lightDark, FontFamilies.quicksandbold]}>Related Activities</Text>
                                    <Text style={[iOSUIKit.caption2, fontColors.lightDark, FontFamilies.quicksandsemibold]}>Common activities with their impact on this mood</Text>

                                </View>
                                <Grid>
                                    <Row size={12} style={{ flexWrap: 'wrap' }} >
                                        {
                                            this.props.influence && this.props.influence.influencers && this.props.influence.influencers.map((influence: MoodInfluencer, idx: number) => {
                                                return (
                                                    <InfluenceScoreItem title={influence.activity} activity={influence.activity_id} icon={influence.icon} iconType={influence.icon_type} score={influence.score}></InfluenceScoreItem>
                                                )
                                            })
                                        }
                                    </Row>

                                </Grid>
                            </Card>
                        </View>
                    </ScrollView>
                </Layout >
            </View>
        )
    }
}

const mapStateToProps = (state) => ({
    influence: state.influenceCalculator,
    entries: state.entryStorage.entries
})
const mapDispatchToProps = dispatch => {
    return {
        request: mood_id => dispatch(ReqeustInfluenceAsync(mood_id)),
        change_mood: (e, m) => dispatch(MoodChanged(e, m)),
        deleteEntry: id => dispatch(RemoveEntry(id))
    }
}
export const details = connect(mapStateToProps, mapDispatchToProps)(_details)