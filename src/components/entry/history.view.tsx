import { Button, ButtonGroup, Card, Divider, Input, Layout, RangeDatepicker, Select, SelectItem, Text } from '@ui-kitten/components'
import { View } from 'native-base';
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { BORDER_RADIUS, CONTAINER_PADDING, DEFAULT_MARGIN, PADDING } from '../../common/constants';
import { TopNav } from '../../menu/header'
import { EmojiActions } from '../day';
import { CloseIcon, FilterButton, SortButton } from '../task/list';
/* import Animated from 'react-native-reanimated'; */
import BottomSheet from 'reanimated-bottom-sheet';
import { DefaultText, FontFamilies, H1, H2, H4, TextMutedStrong } from '../../styles/text';
import { Dimensions, InteractionManager, ScrollView, TouchableOpacity, Animated } from 'react-native';
import { GetMoodBreakdown } from '../reports/reports';
import { default as Moods } from '../../models/initial.json'
import { CenterLoading } from '../activity.page';
import { UppercaseHeading } from './past';
import { Col, Row } from 'react-native-easy-grid';
import { Emoji } from '../../emoji/emoji';

import { default as theme } from '../../styles/app-theme.json'
import { ModalHandleBar } from '../day/day.tabs';
import { ButtonText, PlusOutlineIcon, RightArrowIcon } from '../activity';
import { EntryFilter } from '../../models/api';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { BarChart, PieChart } from 'react-native-chart-kit';
import { default as randomColor } from 'randomcolor'
export const ActivityCount = ({ count }) => {
    return (
        <View style={{ minWidth: 24, height: 24, borderRadius: 10, backgroundColor: theme['color-primary-100'], justifyContent: 'center', alignItems: 'center' }}>
            <DefaultText style={{ color: theme['color-primary-500'] }} text={count}></DefaultText>
            {/* <H1 text={'x' + count} style={[{ color: theme['color-font-300'] }]}></H1> */}
        </View>

    )
}
export const ChartHeader = ({ text, subtext = null }) => {
    return (
        <View style={{ padding: PADDING }}>
            <Text category="h6" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{text}</Text>
            {subtext && <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} appearance="hint" category="c1">{subtext}</Text>}
            <Divider style={{ marginBottom: DEFAULT_MARGIN / 2, marginTop: DEFAULT_MARGIN / 2 }}></Divider>
        </View>
    )
}
export const ActivityPieChart = ({ activities, max = 10 }) => {
    const data = [];

    let sortedActivities = activities.sort((a, b) => b.count - a.count).slice(0, max).map((item, idx, arr) => {


        var color = randomColor({
            hue: theme['color-primary-500'],
            seed: `item-${item.id}`,

        });
        return {
            name: item.hobby,
            population: item.count,
            color: color,
            legendFontColor: theme['color-font-400'],
            legendFontSize: 13,
            legendFontFamily: FontFamilies.quicksandbold.fontFamily
        }
    });

    /*   {
          name: "Seoul",
          population: 21500000,
          color: "rgba(131, 167, 234, 1)",
          legendFontColor: "#7F7F7F",
          legendFontSize: 15
        }, */
    const chartConfig = {
      
        backgroundGradientFrom: "#fff",
        backgroundGradientFromOpacity: 0,
        backgroundGradientTo: "#fff",
        backgroundGradientToOpacity: 0.5,
        backgroundColor: '#fff',
        color: (opacity = 1) => `rgba(51, 102, 255, ${opacity})`,
        strokeWidth: 4, // optional, default 3

    };
    const screenWidth = Dimensions.get("window").width;
    let subtitle = "Your top 10 activities"
    return (
        <View style={{ backgroundColor: '#fff', borderRadius: 9, elevation: 1 }}>
            <ChartHeader subtext={subtitle} text="Activities Chart"></ChartHeader>
            <PieChart
                data={sortedActivities}
                width={screenWidth}
                height={220}
                chartConfig={chartConfig}
                accessor={"population"}
                backgroundColor={"transparent"}
                paddingLeft={"15"}

            />
        </View>
    )
}
export const MoodsBarChart = ({ count }) => {

    const chartConfig = {
        propsForLabels: {
            fontFamily: FontFamilies.quicksandbold.fontFamily,
            color: theme['color-font-300']
        },
        formatYLabel: value => {
            if (!isNaN(value)) {
                return (+value).toFixed(0)
            } return value;
        },
        backgroundGradientFrom: "#fff",
        backgroundGradientFromOpacity: 0,
        backgroundGradientTo: "#fff",
        backgroundGradientToOpacity: 0.5,
        backgroundColor: '#fff',
        color: (opacity = 1) => `rgba(51, 102, 255, ${opacity})`,
        strokeWidth: 4, // optional, default 3
        barPercentage: 1,

        useShadowColorFromDataset: false // optional
    };
    const screenWidth = Dimensions.get("window").width;
    const data = {
        labels: ["Awesome", "Good", "Meh", "Bad", "Awful"],
        datasets: [
            {
                data: [...count]
            }
        ]
    };
    let subtitle = "Following chart displays your moods distribution for the selected date range"
    return (
        <View style={{ backgroundColor: '#fff', borderRadius: 9, elevation: 1 }}>
            <ChartHeader subtext={subtitle} text="Moods Chart"></ChartHeader>
            <BarChart
                style={{
                    marginVertical: 8,
                    marginHorizontal: 0,
                    left: -16,
                    borderRadius: 16
                }}
                withInnerLines={false}

                yAxisLabel=""
                yAxisSuffix=""
                data={data}
                width={screenWidth - (PADDING)}
                height={220}
                chartConfig={chartConfig}
                verticalLabelRotation={0}
            />
        </View>
    )
}
export const MoodBadge = ({ mood, count = 0 }) => {

    return (
        <View style={{ borderRadius: BORDER_RADIUS, width: 64, padding: PADDING / 4, flexDirection: 'row', alignItems: 'center' }}>
            <Emoji size={{ width: 24, height: 24 }} name={mood}></Emoji>
            <TextMutedStrong text={count} style={{ marginLeft: DEFAULT_MARGIN / 4 }} />
        </View>
    )
}
const GetMoodsCount = (moods) => {
    let counted = []; //{mood,count,id}
    moods.forEach((mood) => {

        if (mood.mood_id) {
            let index = counted.findIndex(i => i.id === mood.mood_id);
            if (index > -1) {
                counted[index].count = counted[index].count + 1;
            } else {
                counted.push({
                    id: mood.mood_id, count: 1, mood: mood.mood
                })
            }
        }
    })
    return counted;
}
export const ActivityTable = ({ activities }) => {

    if (!activities) {
        return <View />;
    }

    let activityContext = [];

    // count , hobby (mood title), mood id,moods array (name,id)
    activities.forEach((activity, index, array) => {
        activityContext.push({
            hobby: activity.hobby,
            count: activity.count,
            moods: []
        })
        if (activity.moods && Array.isArray(activity.moods) && activity.moods.length) {
            let countedMoods = GetMoodsCount(activity.moods);
            activityContext[index].moods = [...countedMoods];
        }
    })
    return (
        <View>
            {activityContext.map((activity) => {
                return (
                    <Card style={{ marginBottom: DEFAULT_MARGIN / 2 }}>
                        <Row>
                            <Col>
                                <H4 text={activity.hobby}></H4>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <ActivityCount count={activity.count}></ActivityCount>
                            </Col>
                        </Row>
                        <Row>
                            {activity.moods.map((mood, index, arr) => {
                                return (
                                    <View style={{ marginRight: DEFAULT_MARGIN / 4, marginTop: DEFAULT_MARGIN / 2 }}>
                                        <MoodBadge count={mood.count} mood={mood.mood}></MoodBadge>
                                    </View>
                                )
                            })}
                        </Row>
                    </Card>
                )
            })}

        </View>
    )
}
export const DateRangePicker = ({ onSelect, caption, label }) => {
    let datePickerRef = React.useRef(null);
    const [range, setRange] = React.useState({});
    const CaptionText = ({ text }) => {
        return (
            <TextMutedStrong style={{ fontSize: 12 }} text={text} />
        )
    }
    const clear = () => {
        datePickerRef.current.clear();
        datePickerRef.current.hide();
    }
    const ClearRange = () => {
        return (
            <TouchableNativeFeedback onPress={() => clear()}>
                <View>
                    <CloseIcon fill={theme['color-font-500']} width={24} height={24}></CloseIcon>
                </View>
            </TouchableNativeFeedback>
        )
    }
    const LabelText = ({ text }) => {
        return (
            <TextMutedStrong style={{ marginBottom: DEFAULT_MARGIN / 4 }} text={text} />
        )
    }
    return (
        <RangeDatepicker
            placeholder="Filter by date range"
            ref={datePickerRef}
            accessoryRight={ClearRange}
            range={range}
            label={p => <LabelText text={label}></LabelText>}
            caption={p => <CaptionText text={caption}></CaptionText>}
            placement="top start"
            onSelect={nextRange => {
                setRange(nextRange);
                if (onSelect && typeof onSelect === "function") {
                    onSelect(nextRange);
                }
            }}
        />
    );
};
export class MoodsHistoryView extends Component<any, any> {
    /**
     *
     */
    sheetRef: any;
    sortSheetRef: any;
    window = Dimensions.get('window');
    constructor(props) {
        super(props);
        this.state = {
            count: [0, 0, 0, 0, 0],
            selectedMoods: [],
            selectedMoodsValue: null,
            selectedMoodsIds: [],
            opacity: new Animated.Value(0),
        };
        this.sheetRef = React.createRef();
        this.sortSheetRef = React.createRef();
    }
    componentDidUpdate() {
        console.log(this.state);
    }
    renderBackDrop = () => (
        <Animated.View
            style={{
                opacity: this.state.opacity,
                backgroundColor: '#000',
                position: 'absolute',
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                /* width: this.window.width,
                height: this.window.height, */
            }}>
            <TouchableOpacity
                style={{
                    width: this.window.width,
                    height: this.window.height,
                    backgroundColor: 'transparent',
                }}
                activeOpacity={1}
                onPress={this.onClose}
            />
        </Animated.View>
    );
    findMoodId = text => {
        if (this.state.allMoods && Array.isArray(this.state.allMoods)) {
            let index = this.state.allMoods.findIndex(mood => mood.title === text);
            if (index > -1) {
                return this.state.allMoods[index].id
            }
        }
        return 0;
    }
    ApplySort = () => {
        this.onClose(1);
    }
    renderSortContent = () => {
        return (
            <View
                style={{
                    backgroundColor: 'white',
                    padding: 16,
                    height: 450,
                    elevation: 6
                }}
            >
                <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                    <ModalHandleBar></ModalHandleBar>
                </View>

                <View style={{ paddingTop: PADDING, position: "relative", flex: 1 }}>
                    <Text category="h5" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-400'] }}>Sort Entries</Text>
                    <Text category="c1" appearance="hint" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-400'] }}>Sort your activity and mood entries</Text>
                    <Divider style={{ marginTop: DEFAULT_MARGIN, marginBottom: DEFAULT_MARGIN }}></Divider>
                    <View>
                        <Text category="s1" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-400'] }}>Total Activity Count</Text>
                        <View style={{ padding: PADDING / 2, alignItems: 'center' }}>
                            <ButtonGroup status="basic">
                                <Button style={{ backgroundColor: theme['color-primary-500'] }} children={props => <Text category="p2" style={{ color: '#fff', fontFamily: FontFamilies.quicksandbold.fontFamily }}>Highest First</Text>} />
                                <Button children={props => <Text category="p2" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>Lowest First</Text>} />
                            </ButtonGroup>
                        </View>
                    </View>
                    <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, width: '100%' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                            <Button
                                onPress={() => this.ApplySort()}
                                accessoryRight={props => <View style={{ position: 'relative', top: 1 }}><RightArrowIcon {...props} /></View>} appearance="ghost" children={props => <ButtonText text="Apply Sort" {...props} />}></Button>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    renderContent = () => (
        <View
            style={{
                backgroundColor: 'white',
                padding: 16,
                height: 450,
                elevation: 6
            }}
        >
            <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                <ModalHandleBar></ModalHandleBar>
            </View>
            <View style={{ paddingTop: PADDING, position: "relative", flex: 1 }}>
                <Text category="h5" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily, color: theme['color-font-400'] }}>Filter Entries</Text>
                <View style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                    <DateRangePicker label="Date range" caption="Filter your mood entries by a date range" onSelect={range => this.setState((state, props) => { return { range } })
                    }></DateRangePicker>
                </View>
                <View style={{ marginTop: DEFAULT_MARGIN / 2 }}>
                    <Select
                        label={props => <TextMutedStrong style={{ marginBottom: DEFAULT_MARGIN / 4, marginTop: DEFAULT_MARGIN / 2 }} text="Moods" />}
                        multiSelect={true}
                        value={this.state.selectedMoodsValue}
                        selectedIndex={this.state.selectedMoods}
                        onSelect={index => {
                            let value = [];
                            let selectedMoodsIds = [];
                            if (Array.isArray(index)) {
                                index.forEach(item => {
                                    let row = item.row;
                                    switch (row) {
                                        case 0:
                                            value.push('Happy');
                                            if (this.findMoodId('Happy')) {
                                                selectedMoodsIds.push(this.findMoodId('Happy'));
                                            }
                                            break;
                                        case 1:
                                            value.push('Good');
                                            if (this.findMoodId('Good')) {
                                                selectedMoodsIds.push(this.findMoodId('Good'));
                                            }
                                            break;
                                        case 2:
                                            value.push('Meh');
                                            if (this.findMoodId('Meh')) {
                                                selectedMoodsIds.push(this.findMoodId('Meh'));
                                            }
                                            break;
                                        case 3:
                                            value.push('Bad');
                                            if (this.findMoodId('Bad')) {
                                                selectedMoodsIds.push(this.findMoodId('Bad'));
                                            }
                                            break;
                                        case 4:
                                            value.push('Awful');
                                            if (this.findMoodId('Awful')) {
                                                selectedMoodsIds.push(this.findMoodId('Awful'));
                                            }
                                            break;

                                        default:
                                            break;
                                    }
                                })
                            }
                            this.setState(() => { return { selectedMoods: index, selectedMoodsValue: value.join(', '), selectedMoodsIds } })
                        }
                        }>
                        <SelectItem title='Happy' />
                        <SelectItem title='Good' />
                        <SelectItem title='Meh' />
                        <SelectItem title='Bad' />
                        <SelectItem title='Awful' />
                    </Select>
                </View>
                <View style={{ position: 'absolute', bottom: 0, left: 0, right: 0, width: '100%' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                        <Button
                            onPress={() => this.ApplyFilter()}
                            accessoryRight={props => <View style={{ position: 'relative', top: 1 }}><RightArrowIcon {...props} /></View>} appearance="ghost" children={props => <ButtonText text="Apply filters" {...props} />}></Button>
                    </View>
                </View>
            </View>
        </View>
    );
    ClearFilters = () => {
        this.setState((state, props) => { return { filter: null, hasFilter: false } })

    }
    ApplyFilter = () => {
        try {
            let start = this.state.range?.startDate
            let end = this.state.range?.endDate;
            let moods = this.state.selectedMoodsIds;
            let filter: EntryFilter = { start, end, moods };

            this.setState((state, props) => { return { filter, hasFilter: true, } }, () => {
                this.onClose();
                this.loadData();
            })

        } catch (error) {
            console.log(error);
        }

    }
    onClose = (type = null) => {
        Animated.timing(this.state.opacity, {
            toValue: 0,
            duration: 350,
            useNativeDriver: true,
        }).start();
        this.sheetRef?.current.snapTo(2)
        this.sortSheetRef?.current.snapTo(2);

        setTimeout(() => {
            this.setState({ modalOpen: false });
        }, 50);
    };
    loadData = () => {
        InteractionManager.runAfterInteractions(() => {
            if (this.state.hasFilter) {

                let { start, end, moods } = this.state.filter;
                GetMoodBreakdown(start, end, moods).then((breakdown) => {

                    let moods = Moods.moods.map((mood) => mood.title).map(m => {

                        if (Array.isArray(breakdown.moods)) {
                            let index = breakdown.moods.findIndex(i => i.mood === m);
                            if (index > -1) {
                                return breakdown.moods[index].count;
                            } else return 0;
                        }
                    });
                    this.setState((state, props) => { return { count: moods, ready: true, activites: breakdown.activities, allMoods: breakdown.allMoods } })
                })
            } else {
                GetMoodBreakdown().then((breakdown) => {

                    let moods = Moods.moods.map((mood) => mood.title).map(m => {

                        if (Array.isArray(breakdown.moods)) {
                            let index = breakdown.moods.findIndex(i => i.mood === m);
                            if (index > -1) {
                                return breakdown.moods[index].count;
                            } else return 0;
                        }
                    });
                    this.setState((state, props) => { return { count: moods, ready: true, activites: breakdown.activities, allMoods: breakdown.allMoods } })
                })
            }
        })
    }
    onOpen = (type = null) => {

        this.setState({ modalOpen: true });
        if (!type) {
            this.sheetRef?.current.snapTo(1)
        } else {
            this.sortSheetRef?.current.snapTo(1);
        }
        Animated.timing(this.state.opacity, {
            toValue: 0.7,
            duration: 300,
            useNativeDriver: true,
        }).start();
    }
    headerRight = () => {
        return (
            <View style={{ backgroundColor: 'white', justifyContent: 'flex-end', alignItems: 'flex-start', flexDirection: 'row', flex: 1 }}>
                <SortButton status={this.props.hasSort ? "primary" : "basic"} onPress={() => this.onOpen(1)}></SortButton>
                <FilterButton onPress={() => this.onOpen()}></FilterButton>
                {/*  {this.OverflowMenu()} */}
            </View>
        )
    }
    componentDidMount() {
        this.loadData();
    }
    handleEmojiSelect = id => {

    }
    modalCloseStart = () => {
        this.setState((state, props) => { return { modalOpen: false } })

    }
    modalOpenCallback = () => {
        this.setState((state, props) => { return { modalOpen: true } })

    }


    render() {
        if (!this.state.ready) {
            return <CenterLoading></CenterLoading>;
        }
        return (

            <View style={{ flex: 1 }}>




                <Layout style={{ flex: 1, elevation: 0 }} level="2">

                    {/*   {this.state.modalOpen && this.backDrop()} */}
                    <TopNav height={96} subheading="December 2020" accessoryRight={this.headerRight} title="Moods History" navigation={this.props.navigation}></TopNav>
                    <View style={{ backgroundColor: 'white', paddingLeft: CONTAINER_PADDING / 2, paddingRight: CONTAINER_PADDING / 2 }}>
                        <EmojiActions count={this.state.count} onSelect={this.handleEmojiSelect} navigation={this.props.navigation}></EmojiActions>
                    </View>
                    <View style={{ flex: 1, padding: PADDING / 2 }}>
                        <ScrollView>
                            <UppercaseHeading heading="Infographics"></UppercaseHeading>
                            <MoodsBarChart count={this.state.count}></MoodsBarChart>
                            <View style={{ marginTop: DEFAULT_MARGIN / 2 }}></View>
                            <ActivityPieChart activities={this.state.activites}></ActivityPieChart>
                            <UppercaseHeading heading="Activities"></UppercaseHeading>
                            <ActivityTable activities={this.state.activites}></ActivityTable>
                        </ScrollView>

                    </View>
                </Layout>
                <BottomSheet
                    onCloseEnd={this.onClose}
                    ref={this.sheetRef}
                    snapPoints={[450, 450, 0]}
                    initialSnap={2}
                    borderRadius={10}
                    renderContent={this.renderContent}
                />
                <BottomSheet
                    onCloseEnd={() => this.onClose(1)}
                    ref={this.sortSheetRef}
                    snapPoints={[450, 450, 0]}
                    initialSnap={2}
                    borderRadius={10}
                    renderContent={this.renderSortContent}
                />

                {this.state.modalOpen && this.renderBackDrop()}
            </View>

        )
    }
}

const mapStateToProps = (state) => ({
    entries: state.entryStorage.entries
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(MoodsHistoryView)
