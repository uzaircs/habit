## Agenda grouping
Items have created date
should be grouped by day
Each group should be displayed in a card

Get all items  
Get dates from items  (created_at)
add dates to array  

```javascript

let grouped = [
    {
        "date": "date",
        "items": [
            {
                mood?: string;
                color?: string;
                emoji?: string;
                activity?: UserEntryActivity[];
                created_at?: Date;
                updated_at?: Date;
            }
            .....
        ]
    },
   
]

```

add respective item to dates  

display date cards
