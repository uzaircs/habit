// import the component
import ReactSpeedometer from "react-d3-speedometer"
// and just use it
export const Speedometer = () => {
    <ReactSpeedometer
        width={500}
        needleHeightRatio={0.7}
        value={777}
        currentValueText="Happiness Level"
        customSegmentLabels={[
            {
                text: 'Very Bad',

                color: '#555',
            },
            {
                text: 'Bad',

                color: '#555',
            },
            {
                text: 'Ok',

                color: '#555',
                fontSize: '19px',
            },
            {
                text: 'Good',

                color: '#555',
            },
            {
                text: 'Very Good',

                color: '#555',
            },
        ]}
        ringWidth={47}
        needleTransitionDuration={3333}
        needleColor={'#90f2ff'}
        textColor={'#d8dee9'}
    />

}