# Recommended tasks per day


### Eligibilty 
Should have `DueDate` and some tasks in `goal`  

Total number of tasks for example = 50;
days elapsed = 35;
days remaining = 15;
tasks remaining = 25;

Tasks per day = how many tasks should be completed each day in next 15 days to achieve total completed tasks of 25;

t * d  = c;
t = tasks per day
c = remaining tasks
d = remaining days;

t * 15 = 25;
t = 25/15;

Tasks per day recommendation = remaining tasks / remaining days;