import { Card, Text } from '@ui-kitten/components'
import React from 'react'
import { StyleSheet, View } from 'react-native'
import { BORDER_RADIUS, DEFAULT_MARGIN } from '../../common/constants'
import { FontFamilies } from '../../styles/text'
import { PlusOutlineIcon } from '../activity'
import { GetKPILabel } from './reports'

const styles = StyleSheet.create({
    kpiCard: {
        borderRadius: BORDER_RADIUS,
        width: 160,
        height: 84,
        margin: DEFAULT_MARGIN / 2,
        position: 'relative'

    },
    kpiCardIconContainer: {
        position: 'absolute',
        right: 0,
        bottom: 0
    }
})

export default function KpiCard({ title, value, ...props }) {
    let smallText = (isNaN(value) && value.length > 5);
    if (typeof value !== 'object') {
        return (
            <Card appearance="filled" style={styles.kpiCard}>
                <Text appearance="hint" category="label" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{GetKPILabel(title)}</Text>
                <Text category={smallText ? 'p1' : 'h4'} style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{value}</Text>
                {/*  <View style={styles.kpiCardIconContainer}>
                    <PlusOutlineIcon></PlusOutlineIcon>
                </View> */}
            </Card>
        )
    } else return <View />

}
