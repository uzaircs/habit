import moment from "moment"
import { DEFAULT_CHART_DATE_FORMAT, DEFAULT_DATE_FORMAT } from "../../common/constants"
import { TaskSortPipe } from "../../reducers/task.reducer"
import { getEntriesByDay, getEntries, getMoods, GetGoal, GetGoalTasks, GetSubtask, DateToDbUnix, DbUnixToDate, GetEntriesWithFilter, GetTasks, GetGoals, GetDiaryEntries } from "../../models/api"
import { SortOptions } from "../task/list"
import { FormatNumber } from "../../common/formatters"
import { isToday } from "../task/task"
const DUE_ONLY_AFTER = new Date(2020, 1, 1, 1, 1, 1);
export const GetKPILabel = item => {
    switch (item) {
        case 'totalTasks':
            return 'Tasks'

        case 'tasksCompleted':
            return 'Completed tasks'
        case 'totalGoals':
            return 'Goals'
        case 'completedGoals':
            return 'Goals Completed'
        case 'totalMoodEntries':
            return 'Total Mood Entries'
        case 'lastMood':
            return 'Last Mood Entry'
        case 'isLastMoodToday':
            return 'Mood Today'
        case 'diaryEntries':
            return 'Diary entries'
        case ' timerSessions':
            return 'Timer sessions'
        default: break
    }
}
export type GlobalKPI = {
    totalTasks: number;
    tasksCompleted?: number;
    totalGoals?: number;
    completedGoals?: number;
    totalMoodEntries?: number;
    lastMood?: any;
    isLastMoodToday?: boolean;
    diaryEntries?: number;
    timerSessions?: number;

}
export const CalculateKPI = async (): Promise<GlobalKPI> => {
    var tasks = await GetTasks();
    var goals = await GetGoals();
    var entries = await getEntries();
    var diary = await GetDiaryEntries();
    const lastMood = entries[entries.length - 1];
    let lastMoodIsToday: boolean = false;
    if (lastMood) {
        lastMoodIsToday = isToday(lastMood.created_at);
    }
    return new Promise((resolve, reject) => {
        let kpi: GlobalKPI = {
            totalTasks: tasks.length,
            tasksCompleted: tasks.filter(t => t.status).length,
            totalMoodEntries: entries.length,
            lastMood: moment(lastMood.created_at).fromNow(),
            /*  isLastMoodToday: lastMoodIsToday, */
            diaryEntries: diary.length,
            totalGoals: goals.length,
            completedGoals: goals.filter(g => g.status == 2).length
        }

        resolve(kpi);
    })

}
export type MoodCount = {
    mood: string;
    count: number;
}
export type MoodInfluencer = {
    activity: string;
    activity_id: string;
    icon?: string;
    icon_type?: string;
    score: number; // 0 - 1
}
export type Influence = {
    mood: string;
    mood_id: string;
    influencers: MoodInfluencer[];

}
export type Hobby = {
    hobby: string;
    count: number;
    id: string;
    moods?: Array<any>;
}
export const GetMoodBreakdown = async (start: Date = null, end: Date = null, mood_ids: Array<string> = []) => {
    let entries = [];
    if (start && end && mood_ids.length) {
        entries = await GetEntriesWithFilter({ start: start, end: end, moods: mood_ids })
    } else if (start && end) {
        entries = await GetEntriesWithFilter({ start, end })
    } else if (start && mood_ids.length) {
        entries = await GetEntriesWithFilter({ start, moods: mood_ids })
    } else if (mood_ids.length) {
        entries = await GetEntriesWithFilter({ moods: mood_ids })
    }
    let allMoods = await getMoods();
    if (entries.length > 0) {
        let moods = await GetMoodCount(null, entries);
        let activities = await GetActivityCounts(null, entries);
        if (activities && Array.isArray(activities)) {

        } else {
            //no activities breakdown
        }
        if (moods && Array.isArray(moods)) {

        } else {
            // no moods breakdown
        }
        return { activities, moods };
    } else {
        let moods = await GetMoodCount();
        let activities = await GetActivityCounts();
        if (activities && Array.isArray(activities)) {

        } else {
            //no activities breakdown
        }
        if (moods && Array.isArray(moods)) {

        } else {
            // no moods breakdown
        }
        return { activities, moods, allMoods };
    }

}
export const GetMoodCount = (mood_id: string = null, entries = null) => {
    return new Promise((resolve, reject) => {
        let count: MoodCount[] = [];
        if (entries && Array.isArray(entries) && entries.length) {
            entries.forEach(_mood => {
                if (mood_id) {
                    if (_mood.mood_id == mood_id) {
                        let __exists = count.findIndex(m => m.mood == _mood.mood);
                        if (__exists > -1) {
                            count[__exists].count++;
                        } else {
                            let c: MoodCount = {
                                count: 1,
                                mood: _mood.mood
                            }
                            count.push(c);
                        }
                    }
                } else {
                    let __exists = count.findIndex(m => m.mood == _mood.mood);
                    if (__exists > -1) {
                        count[__exists].count++;
                    } else {
                        let c: MoodCount = {
                            count: 1,
                            mood: _mood.mood
                        }
                        count.push(c);
                    }
                }
            })
            if (mood_id) {
                resolve(count[0]);
            } else {
                resolve(count);
            }
        } else {

            getEntries().then((moods) => {
                moods.forEach(_mood => {
                    if (mood_id) {
                        if (_mood.mood_id == mood_id) {
                            let __exists = count.findIndex(m => m.mood == _mood.mood);
                            if (__exists > -1) {
                                count[__exists].count++;
                            } else {
                                let c: MoodCount = {
                                    count: 1,
                                    mood: _mood.mood
                                }
                                count.push(c);
                            }
                        }
                    } else {
                        let __exists = count.findIndex(m => m.mood == _mood.mood);
                        if (__exists > -1) {
                            count[__exists].count++;
                        } else {
                            let c: MoodCount = {
                                count: 1,
                                mood: _mood.mood
                            }
                            count.push(c);
                        }
                    }
                })
                if (mood_id) {
                    resolve(count[0]);
                } else {
                    resolve(count);
                }
            }).catch((err) => {
                reject(err);
            })
        }
    })
}
export const GetMoodCountDay = (day: Date): Promise<MoodCount[]> => {
    return new Promise((resolve, reject) => {
        let count: MoodCount[] = [];
        getEntriesByDay(day).then((moods) => {
            moods.forEach(_mood => {
                let __exists = count.findIndex(m => m.mood == _mood.mood);
                if (__exists > -1) {
                    count[__exists].count++;
                } else {
                    let c: MoodCount = {
                        count: 1,
                        mood: _mood.mood
                    }
                    count.push(c);
                }
            })
            resolve(count);
        }).catch((err) => {
            reject(err);
        })
    })

}
export const TotalEstimatedTime = (tasks: any[]) => {
    let estimatedTime = 0;
    if (tasks && Array.isArray(tasks) && tasks.length) {
        tasks.forEach(t => {
            if (t.estimatedTime) {
                estimatedTime += t.estimatedTime;
            }
        })
    }
    return estimatedTime;
}
export const RemainingEstimatedTime = (tasks: any[]) => {
    let estimatedTime = 0;
    if (tasks && Array.isArray(tasks) && tasks.length) {
        tasks.filter(t => t.status).forEach(t => {
            if (t.estimatedTime) {
                estimatedTime += t.estimatedTime;
            }
        })
    }
    return estimatedTime;
}
export interface GoalProgressRing {
    totalTasks: number;
    tasksPerDay: number;
    tasksPerDayRecommended: number;
    totalDays: number;
    daysElapsed: number;
    completedTasks: number;
    hasDueDate;
}
export interface GoalStats {
    TotalEstimatedTime;
    TotalTasks;
    PercentChange; // Increasing progress or decreasing progress
    AverageTimePerTask; // Average time per task
    TaskAddedPerDay;
    TasksCompletedPerDay;
    StartedOn;
    EstimatedTimeRemaining;
    lineChart?;
    progressRing?: GoalProgressRing;

}
export const DATE_KEY_FORMAT = 'DMMYYYY';
export const TaskAddedPerDay = (tasks: any[]) => {
    if (tasks && Array.isArray(tasks) && tasks.length) {
        let first;
        try {
            first = tasks.filter(t => t.status).sort((a, b) => {
                let a_completedAt = DateToDbUnix(a.completedAt);
                let b_completedAt = DateToDbUnix(b.completedAt);
                if (!a_completedAt && !b_completedAt) return 0;
                if (!a_completedAt) return 1;
                if (!b_completedAt) return -1;
                return a_completedAt < b_completedAt ? -1 : 1;
            })[0].completedAt
        } catch (error) {

        }

        let days = [];
        tasks.forEach(t => {
            if (first && moment(t.createdAt).isAfter(moment(first).add('6', 'h'))) {
                let key = moment(t.createdAt).format(DATE_KEY_FORMAT);
                let existing = days.findIndex(d => d.key === key);
                if (existing !== -1) {
                    days[existing].total++;
                } else {
                    days.push({
                        key, total: 1
                    })
                }
            } else if (!first) {
                let key = moment(t.createdAt).format(DATE_KEY_FORMAT);
                let existing = days.findIndex(d => d.key === key);
                if (existing !== -1) {
                    days[existing].total++;
                } else {
                    days.push({
                        key, total: 1
                    })
                }
            }
        })
        let value;
        try {
            value = (days.map(d => d.total).reduce((p, n) => p + n) / days.length);
        } catch (error) {
            value = 0;
        }
        return value;
    } return 0;
}
export const TasksCompletedPerDay = (tasks: any[]) => {
    if (tasks && Array.isArray(tasks) && tasks.length && tasks.some(t => t.status)) {
        let days = [];

        tasks.filter(t => t.status).forEach(t => {
            let key = moment(t.createdAt).format(DATE_KEY_FORMAT);
            let existing = days.findIndex(d => d.key === key);
            if (existing !== -1) {
                days[existing].total = days[existing].total + 1;
            } else {
                days.push({
                    key, total: 1
                })
            }
        })

        let value;
        try {
            value = (days.map(d => d.total).reduce((p, n) => p + n) / days.length);

        } catch (error) {
            value = 0;
        }
        return value;
    } return 0;
}

export const GetLineChartHorizontalMaximum = (tasks: any[], goalCreatedOn): moment.Moment => {
    if (tasks && Array.isArray(tasks) && tasks.length) {
        if (tasks.some(task => task.status)) {
            let sorted = TaskSortPipe(tasks.filter(t => t.status), SortOptions.DATE_MODIFIED, true)[0]
            if (sorted) {
                return DbUnixToDate(sorted.completedAt);
            } else { return moment(goalCreatedOn).add(1, 'months'); }
        } else {
            let sorted = TaskSortPipe(tasks.filter(t => t.status), SortOptions.DATE_CREATED, true)[0]
            if (sorted) {
                return DbUnixToDate(sorted.createdAt);
            } else {
                let { createdAt } = tasks[0];
                if (createdAt) {
                    return DbUnixToDate(createdAt);
                } else { return moment(goalCreatedOn).add(1, 'months'); }
            }
        }
    } return moment(goalCreatedOn).add(1, 'months');
}
export const GenerateRawDateLabels = (min, max, totalPlotPoints = 5) => {
    let daysDifference = moment(max).diff(moment(min), 'd');
    let interval = Math.round(daysDifference / totalPlotPoints);
    let dateLabels = [];
    for (let i = 0; i < totalPlotPoints; i++) {
        let date = moment(min).add(interval * i, 'days');
        if (date.startOf('d').isAfter(moment(max).startOf('d'))) {
            dateLabels.push(moment(max));
            return dateLabels;
        } else {
            dateLabels.push(date);

        }
    }
    dateLabels.push(moment(max))
    return dateLabels;
}
export const LineChartData = (goal, tasks, dueDate = null) => {
    let max = dueDate;
    let min = DbUnixToDate(goal.createdAt);

    if (!max) {
        max = GetLineChartHorizontalMaximum(tasks, DbUnixToDate(goal.createdAt));
    }
    let dates = GenerateRawDateLabels(min, max);
    let labels = dates.map(date => moment(date).format(DEFAULT_CHART_DATE_FORMAT));
    let data = [];

    dates.forEach(date => {
        if (moment(date).endOf('d').isAfter(moment().endOf('d'))) {
            data.push(0);
        } else {
            data.push(tasks.filter(t => t.completedAt && moment(t.completedAt).endOf('d').isBefore(moment(date).startOf('d'))).length);
        }
    })
    let lineChartData = {
        labels,
        datasets: [
            {
                data
            }
        ]
    }
    return lineChartData;
}

export const GoalStatistics = async (id): Promise<GoalStats> => {
    let goal = await GetGoal(id);

    let startedOn = goal.createdAt;
    let tasks = (await GetGoalTasks(id)).slice();
    let totalTasks = tasks.length;
    let completedTasks = tasks.filter(t => t.status).length;
    let subtasksCollection = [];
    for (let i = 0; i < tasks.length; i++) {
        let subtasks = await GetSubtask(tasks[i].id);
        if (subtasks && subtasks.length) {
            subtasksCollection = [...subtasksCollection, ...subtasks];
        }
    }
    completedTasks += subtasksCollection.filter(s => s.status).length;
    totalTasks += subtasksCollection.length;

    let taskAddedPerDay = TaskAddedPerDay(tasks.concat(subtasksCollection));
    let taskCompletedPerDay = TasksCompletedPerDay(tasks.concat(subtasksCollection));
    let progressRate = (taskCompletedPerDay - taskAddedPerDay) / totalTasks;
    let taskWithTime = tasks.concat(subtasksCollection).filter(t => t.estimatedTime);
    let estimatedTime = TotalEstimatedTime(taskWithTime);
    let remainingTime = RemainingEstimatedTime(taskWithTime);
    let avgRemainingTime = (remainingTime / taskWithTime.length);
    //Tasks per day recommendation = remaining tasks / remaining days;
    let recommendation = null;
    if (goal.DueDate) {
        let remainingDays = moment(goal.DueDate).diff(moment(), 'd');

        recommendation = +FormatNumber((totalTasks - completedTasks) / remainingDays);
    }
    let lineChart = LineChartData(goal, tasks.concat(subtasksCollection), goal.DueDate)
    let progressRing: GoalProgressRing = {
        tasksPerDay: taskCompletedPerDay,
        daysElapsed: moment().diff(moment(startedOn), 'd'),
        totalDays: moment(goal.DueDate).diff(moment(startedOn), 'd'),
        totalTasks: totalTasks,
        tasksPerDayRecommended: recommendation, completedTasks,
        hasDueDate: (() => {
            if (goal.DueDate && moment(goal.DueDate).isValid()) {
                if (moment(goal.DueDate).isAfter(moment(DUE_ONLY_AFTER))) {
                    return goal.DueDate
                } else return false
            } else return false;
        })()

    }
    avgRemainingTime = avgRemainingTime * (totalTasks - completedTasks);
    let avgTime = 0;
    if (estimatedTime) {
        avgTime = (estimatedTime / taskWithTime.length);
    }

    return {
        lineChart,
        progressRing,
        StartedOn: startedOn,
        AverageTimePerTask: +avgTime.toFixed(0),
        PercentChange: progressRate,
        TaskAddedPerDay: FormatNumber(taskAddedPerDay),
        TasksCompletedPerDay: FormatNumber(taskCompletedPerDay),
        TotalEstimatedTime: FormatNumber(avgTime * totalTasks),
        TotalTasks: totalTasks,
        EstimatedTimeRemaining: FormatNumber(avgRemainingTime.toFixed(0))
    }
}
export const GetTopHobbies = async () => {
    let hobbies: Hobby[] = [];
    return new Promise((resolve, reject) => {
        (async () => {
            let entries = await getEntries();
            entries.forEach((entry) => {
                entry.activity.forEach((activity) => {
                    let __exists = hobbies.findIndex(h => h.id == activity.id);
                    if (__exists > -1) {
                        hobbies[__exists].count++;
                    } else {
                        hobbies.push({
                            count: 1,
                            hobby: activity.title,
                            id: activity.id
                        })
                    }
                })
            })
        })().then((hobbies) => {
            resolve(hobbies);
        }).catch((err) => {
            console.warn(err);
            reject(err);
        })
    })

}

export const GetActivityCounts = async (activity_id: string = null, entries = null): Promise<Hobby[]> => {
    let arr: Hobby[] = [];
    if (entries && Array.isArray(entries) && entries.length) {
        entries.forEach((entry) => {
            entry.activity.forEach((activity) => {
                let __exists = arr.findIndex(a => a.id == activity.id);
                if (__exists > -1) {
                    arr[__exists].count++;
                    arr[__exists].moods.push({
                        mood: entry.mood, mood_id: entry.mood_id
                    })
                } else {
                    arr.push({
                        count: 1,
                        hobby: activity.title,
                        id: activity.id, moods: [{
                            mood: entry.mood, mood_id: entry.mood_id
                        }]
                    })
                }
            })
        })
        if (activity_id) {
            return arr.filter(a => a.id == activity_id).slice();
        }
        return arr;
    } else {
        let entriesDirect = await getEntries();

        entriesDirect.forEach((entry) => {
            entry.activity.forEach((activity) => {
                let __exists = arr.findIndex(a => a.id == activity.id);
                if (__exists > -1) {
                    arr[__exists].count++;
                    arr[__exists].moods.push({
                        mood: entry.mood, mood_id: entry.mood_id
                    })
                } else {
                    arr.push({
                        count: 1,
                        hobby: activity.title,
                        id: activity.id, moods: [{
                            mood: entry.mood, mood_id: entry.mood_id
                        }]
                    })
                }
            })
        })
        if (activity_id) {
            return arr.filter(a => a.id == activity_id).slice();
        }
        return arr;
    }


}
const groupInfluences = (influencers: MoodInfluencer[]): MoodInfluencer[] => {
    if (!influencers || influencers.length == 0) {
        return [];
    }
    let grouped: MoodInfluencer[] = [];
    let influencerConstant = influencers[0].score;
    influencers.forEach(influencer => {
        let __exists = grouped.findIndex(x => x.activity_id == influencer.activity_id);
        if (__exists > -1) {
            grouped[__exists].score += influencerConstant;
        } else {
            grouped.push(influencer);
        }
    })
    return grouped;
}
export const CalculateMoodInfluence = async (mood_id: string): Promise<Influence> => {
    const moods = await getMoods();
    let mood = moods.find(mood => mood.id == mood_id);
    if (mood) {
        let influence: Influence = { mood: mood.title, mood_id: mood.id, influencers: [] };
        const entries =
            (await getEntries()).filter(e => e.mood_id == mood_id)
                .map(x => x.activity);
        let totalActivity = entries.map(x => x.length).reduce((a, c) => a + c);
        const influenceConstant = 1 / totalActivity;
        entries.forEach(e => {
            e.forEach(a => {
                influence.influencers.push({
                    activity: a.title, activity_id: a.id, score: influenceConstant, icon: a.icon, icon_type: a.icon_type
                })
            })
        })
        influence.influencers = groupInfluences(influence.influencers);
        influence.influencers = influence.influencers.sort((a, b) => {
            if (a.score < b.score) return 1;
            if (a.score > b.score) return -1;
            return 0;
        });
        return influence;

    } else {
        throw new Error("Something went wrong. You don't have any data for this mood or data could not be retrieved'")
    }


}