import React, { Component } from 'react'
import { BottomNavigation, BottomNavigationTab, Layout, Text, Icon, Card } from '@ui-kitten/components';
import { ScrollView, StyleSheet, TouchableHighlight, TouchableNativeFeedback, TouchableOpacity } from 'react-native';
import { Icon as NBIcon, View } from 'native-base'
import { default as theme } from '../../styles/app-theme.json'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DayTracker, EmojiActions } from '../day';
import DiaryDay from '../diary/day.diary';
import { iOSUIKit } from 'react-native-typography';
import { DefaultText, H2, TextMutedStrong } from '../../styles/text';
import { Col, Row } from 'react-native-easy-grid';
import ModalBox from 'react-native-modalbox';
import { CloseIcon, MenuIconHorizontal, TaskList } from '../task/list';
import { AndroidBackHandler } from "react-navigation-backhandler";
import { BORDER_RADIUS, CONTAINER_PADDING, DEFAULT_MARGIN, FAB_BORDER_WIDTH, FAB_DIM, MENU_HEIGHT_COLLAPSED } from '../../common/constants';
import { PlusOutlineIcon } from '../activity';
import { AddTaskModal, GoalIcon } from '../task/add';
import { TopNav } from '../../menu/header';
import { FancyBox } from '../../common/FancyBox';
import BottomSheet from 'reanimated-bottom-sheet';
import { NavigationContext, useNavigation } from '@react-navigation/native';
import { CalculateKPI } from '../reports/reports';
const Tab = createBottomTabNavigator();
const styles = StyleSheet.create({
    bottomNavigation: {


        shadowColor: "#000",
        shadowOffset: {
            width: 40,
            height: 30,
        },
        height: 56,
        shadowOpacity: 1,
        shadowRadius: 4.65,
        elevation: 10,
        borderTopWidth: 1,
        borderTopColor: theme["color-font-100"]
    },

});
const TaskIcon = (props) => (
    <Icon name='checkmark-square' fill={props.style.tintColor} style={{ width: 24, height: 24 }} />
);
const FacebookIcon = (props) => (
    <Icon name='home-outline' fill={theme["color-primary-500"]} style={{ width: 24, height: 24 }} />
);
export const MoodIcon = (props) => {

    return (
        <Icon name='smiling-face' fill={props.style.tintColor} style={{ width: 24, height: 24 }} />

    )
}
export const DiaryIcon = (props) => {
    return (
        <Icon name='book-outline' fill={props.style.tintColor} style={{ width: 24, height: 24 }} />

    )
}
export const HomeIcon = (props) => {
    return (
        <Icon name='home-outline' fill={props.style.tintColor} style={{ width: 24, height: 24 }} />

    )
}
export const WorkIcon = (props) => {

    return <NBIcon type="MaterialCommunityIcons" name="briefcase-clock-outline" style={{ color: props.style.tintColor, fontSize: 24 }} />
}
const genTitle = (title, props) => {

    return (
        <Text {...props} style={[iOSUIKit.footnoteEmphasized, { color: props.style.color }]}>{title}</Text>
    )
}
export const AddFab = ({ onPress }) => {
    return (

        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-font-100"])} onPress={onPress}>
            <View style={{
                height: FAB_DIM,
                width: FAB_DIM,
                backgroundColor: theme["color-primary-500"],
                borderRadius: FAB_DIM / 2,
                borderWidth: FAB_BORDER_WIDTH,
                borderColor: theme["color-primary-400"],


                justifyContent: "center", alignItems: 'center'
            }}
            >
                <PlusOutlineIcon width={FAB_DIM / 3} height={FAB_DIM / 3} fill="white"></PlusOutlineIcon>
            </View>
        </TouchableNativeFeedback>
    )
}
export const DayBottomTabs = ({ navigation, state, onFabPress }) => {


    /*   navigation.reset({ index: 0, routes: [{ name: state.routeNames[index] }] })} */
    return (
        <React.Fragment >
            <BottomNavigation onSelect={index => navigation.jumpTo(state.routeNames[index])} selectedIndex={state.index} style={styles.bottomNavigation} >


                <BottomNavigationTab title={props => genTitle('Home', props)} icon={HomeIcon} />
                <BottomNavigationTab onPress={() => navigation.navigate('Tasks')} title={props => genTitle('Tasks', props)} icon={TaskIcon} />
                <View style={{ bottom: FAB_DIM - MENU_HEIGHT_COLLAPSED + (DEFAULT_MARGIN), left: 4, right: 0 }}>
                    <AddFab onPress={onFabPress}></AddFab>
                </View>
                <BottomNavigationTab title={props => genTitle('Diary', props)} icon={DiaryIcon} />
                <BottomNavigationTab title={props => genTitle('More', props)} icon={MenuIconHorizontal} />
            </BottomNavigation>
        </React.Fragment>
    );
};

export const MoreTab = (props) => {
    const fancy = [
        { name: 'Moods', route: 'moodsView', subtitle: 'Your mood history' },
        { name: 'Work', route: 'work', subtitle: 'Work timer and pomodoro' },

        /*    { name: 'Statistics', route: null },
           { name: 'Acheivements', route: null },
           { name: 'Missions', route: null }, */
        { name: 'Goals', route: 'goalList', subtitle: 'Add and manage your goals' },
        { name: 'Settings', route: 'PlanPage', subtitle: 'Application settings' },
        /*   { name: 'Timeline', route: 'timelineScreen' }, */
        /*  { name: 'Settings', route: null }, */
    ]
    return (
        <Layout style={{ flex: 1 }} level="2">
            <TopNav navigation={props.navigation} title="Dailyme"></TopNav>
            <ScrollView >
                <View style={{ padding: CONTAINER_PADDING, flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'space-evenly' }}>
                    {fancy.map(f => {
                        return (
                            <View style={{ width: '45%', margin: DEFAULT_MARGIN / 2 }}>
                                <FancyBox onPress={() => {
                                    if (f.route) {
                                        props.navigation.navigate(f.route)
                                    }
                                }} title={f.name} subtitle={f.subtitle}></FancyBox>
                            </View>
                        )
                    })}
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TextMutedStrong text="More features coming soon..." />
                </View>
            </ScrollView>
        </Layout>
    )
}
export const ModalHandleBar = () => {
    return (
        <View style={{ borderRadius: BORDER_RADIUS, backgroundColor: theme['color-font-200'], width: 48, height: 8, marginTop: DEFAULT_MARGIN / 2, marginBottom: DEFAULT_MARGIN / 2 }}>

        </View>
    )
}
export const ModalAddItem = ({ item, icon = null, onPress }) => {
    return (
        <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"], true)} onPress={onPress}>
            <View style={{ width: 40, height: 40, justifyContent: 'center', alignItems: 'center', marginLeft: DEFAULT_MARGIN / 2, marginRight: DEFAULT_MARGIN / 2 }}>
                <View style={{ borderRadius: 20, width: 40, height: 40, borderColor: theme['color-primary-400'], borderWidth: 2, justifyContent: 'center', alignItems: 'center' }}>
                    {icon && icon()}
                </View>
                <DefaultText style={{ color: theme['color-primary-400'], marginTop: DEFAULT_MARGIN / 2 }} text={item}></DefaultText>
            </View>
        </TouchableNativeFeedback>
    )
}
export default class DayContainer extends React.PureComponent<any, any>{
    /**
     *
     */
    static contextType = NavigationContext;
    constructor(props) {
        super(props);
        this.sheetRef = React.createRef();
        this.state = {
            modalOpen: false,
            actionSheetOpen: false
        }
    }
    componentDidMount() {
        CalculateKPI().then(kpi => {
            this.setState((state, props) => { return { kpi } })

        })
        if (this.props.request && typeof this.props.request === 'function') {
            this.props.request();
        }
    }
    modalRef = null;
    sheetRef: any = null;
    onClose = () => {
        this.setState({ modalOpen: false })
        this.modalRef?.close();
    }
    onOpen = () => {

    }
    openModal = () => {
        this.setState({ modalOpen: true });
        this.modalRef?.open();
    }
    onBackButtonPressAndroid = () => {
        if (this.state.modalOpen) {
            this.onClose();
            return true;
        }
        if (this.state.actionSheetOpen) {
            this.sheetRef?.current.snapTo(2);
            return true;
        }
        return false;
    };
    modalCloseStart = () => {
        this.setState((state, props) => { return { actionSheetOpen: false } })

    }
    modalOpenCallback = () => {
        this.setState((state, props) => { return { actionSheetOpen: true } })

    }

    renderContent = () => {
        const navigation = this.context;
        return (
            <View
                style={{
                    backgroundColor: 'white',
                    padding: 16,
                    height: 160,

                }}
            >
                <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                    <ModalHandleBar></ModalHandleBar>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TextMutedStrong text="What would you like to add?" />
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: DEFAULT_MARGIN * 2 }}>
                    <ModalAddItem
                        icon={() => <TaskIcon style={{ tintColor: theme['color-primary-500'] }}></TaskIcon>}
                        item="Task"
                        onPress={() => {
                            this.sheetRef?.current.snapTo(2);
                            this.setState(() => { return { addModalOpen: true, actionSheetOpen: false, } })
                        }}></ModalAddItem>
                    <ModalAddItem icon={() => <GoalIcon fontsize={24} color={theme['color-primary-500']}></GoalIcon>}
                        item="Goal"
                        onPress={() => {
                            this.sheetRef?.current.snapTo(2);
                            this.setState(() => { return { actionSheetOpen: false } })
                            setTimeout(() => {
                                navigation.navigate('addGoal');
                            }, 150);
                        }} />
                    <ModalAddItem
                        icon={() => <DiaryIcon style={{ tintColor: theme['color-primary-500'] }}></DiaryIcon>}
                        item="Diary"
                        onPress={() => {
                            this.sheetRef?.current.snapTo(2);
                            this.setState(() => { return { actionSheetOpen: false } })
                            setTimeout(() => {
                                navigation.navigate('addDiaryEntry');
                            }, 150);
                        }} />
                </View>
            </View>
        )
    }
    openActionSheet = () => {
        /*       () => this.setState((state, props) => { return { actionSheetOpen: !state.actionSheetOpen } }) */
        if (this.sheetRef) {
            this.sheetRef?.current.snapTo(1)
        }

    }
    backDrop = () => (
        <View style={{ backgroundColor: 'rgba(0,0,0,0.4)', height: '100%', width: '100%', position: 'absolute', top: -10, left: 0, elevation: 1 }}>

        </View>
    )
    render() {

        return (
            <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>

                <Layout style={{ flex: 1, elevation: 0 }} level="4">
                    <AddTaskModal visible={this.state.addModalOpen} onToggle={() => this.setState((state) => { return { addModalOpen: !state.addModalOpen } })
                    }></AddTaskModal>
                    <BottomSheet
                        onCloseEnd={this.modalCloseStart}
                        onCloseStart={this.modalCloseStart}
                        onOpenEnd={this.modalOpenCallback}
                        onOpenStart={this.modalOpenCallback}
                        ref={this.sheetRef}

                        snapPoints={[160, 160, 0]}
                        initialSnap={2}
                        borderRadius={10}
                        renderContent={this.renderContent}
                    />

                    <ModalBox
                        style={{ zIndex: 999 }}
                        useNativeDriver={true}
                        ref={ref => { this.modalRef = ref }}
                        swipeToClose={true}
                        onClosed={this.onClose.bind(this)}
                        onOpened={this.onOpen.bind(this)}>
                        <Layout style={{ flex: 1, padding: 16 }} level="3">

                            <Row style={{ maxHeight: 56 }} size={12}>
                                <Col style={{ justifyContent: 'center' }} size={9}>
                                    <Row style={{ alignItems: 'center' }}>
                                        <H2 text="How are you feeling?" style={{ color: theme["color-font-500"], textAlign: 'center' }}></H2>
                                    </Row>
                                </Col>
                                <Col style={{ justifyContent: 'center' }}>
                                    <View style={{ alignItems: 'flex-end' }}>
                                        <TouchableNativeFeedback onPress={this.onClose}>
                                            <CloseIcon width={24} height={24} fill={theme["color-font-300"]} />
                                        </TouchableNativeFeedback>
                                    </View></Col>
                            </Row>
                            <View style={{ flex: 1, justifyContent: 'center' }}>

                                <Card appearance="filled" style={{ elevation: 9 }}>
                                    <EmojiActions navigation={this.props.navigation}></EmojiActions>
                                </Card>
                            </View>
                        </Layout>

                    </ModalBox>

                    <View style={{ flex: 1 }}>
                        <Tab.Navigator tabBar={props => <DayBottomTabs onFabPress={this.openActionSheet}
                            {...props}></DayBottomTabs>} tabBarOptions={{ keyboardHidesTabBar: true }} >
                            <Tab.Screen name="myday" children={() => <DayTracker kpi={this.state.kpi} onModalOpen={this.openModal} grouped={this.props.grouped} navigation={this.props.navigation} {...this.props}></DayTracker>} />
                            <Tab.Screen name="Tasks" component={TaskList} />
                            <Tab.Screen name="placeholder" component={TaskList} />
                            <Tab.Screen name="Diary" component={DiaryDay} />
                            <Tab.Screen name="More" component={MoreTab} />
                        </Tab.Navigator>
                    </View>
                </Layout>
            </AndroidBackHandler>
        )
    }
}
