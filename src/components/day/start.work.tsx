import React from 'react'
import { Layout } from '@ui-kitten/components'
import { PomodoroTimer } from '../timer/pomodoro'
export const StartWork = (props) => {

    return (
        <Layout style={{ flex: 1, }}>
            <PomodoroTimer {...props}></PomodoroTimer>
            
        </Layout>
    )
}
