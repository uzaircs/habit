import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Divider, Card, Button, Text } from '@ui-kitten/components'
import { PomodoroTimer } from '../timer/pomodoro'
import { Box } from '../home/start'
import { View } from 'native-base'
import { TouchableOpacity, TouchableNativeFeedback, ScrollView } from 'react-native'
import { default as theme } from '../../styles/app-theme.json'
import { FontFamilies, H2, TextMuted } from '../../styles/text'
import { RetreiveTimer } from '../../models/api'
import { CenterLoading } from '../activity.page'
import { SessionManager } from '../timer/session'
import SessionCard from '../session/session.card'
import { BackNav, TopNav } from '../../menu/header'
import { DEFAULT_MARGIN, PADDING } from '../../common/constants'
import SessionList from '../session/sessions.list'
class _WorkTab extends Component<any, any>{
    /**
     *
     */
    constructor(props) {
        super(props);
        this.state = {
            ready: false
        }
    }
    componentDidMount() {
        let manager = new SessionManager({
            config: {
                id: 'abc',
                longBreak: 15 * 60,
                longBreakAfter: 4,
                shortBreak: 5 * 60,
                time: 25 * 60,

            },
            limit: 4,
            tasks: []
        });
        if (this.props.tasks) {
            manager.AutoRetrieveFromList(this.props.tasks.filter(t => !t.status), 5).then(v => {
                this.setState({
                    ...this.state, sessionTasks: v
                })
            });
        }
        RetreiveTimer().then(t => {
            if (t) {
                this.props.navigation.navigate('StartWork');

            }
            this.setState({ ready: true })
        })
    }
    SessionTasks = () => {
        if (this.state.sessionTasks) {
            return (
                <View style={{ marginBottom: 16 }}>
                    <SessionCard items={this.state.sessionTasks}></SessionCard>
                </View>
            )
        } else {
            return <View />;
        }
    }
    render() {
        const ac_right = () => {
            return (
                <View>
                    <Button
                        onPress={() => this.props.navigation.navigate('createSession')}
                        appearance="ghost" children={props => <Text style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }} {...props}>Create Session</Text>}></Button>
                </View>
            )
        }
        if (!this.state.ready) { return <CenterLoading></CenterLoading> }
        return (
            <Layout style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} level="4">
                <BackNav ac_right={ac_right} navigation={this.props.navigation} title="Work"></BackNav>
                <ScrollView style={{ width: '100%' }} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false}>
                    <View style={{ padding: PADDING / 2 }}>
                        <SessionList></SessionList>
                        <View style={{ marginTop: DEFAULT_MARGIN, flex: 1 }}>

                            <View style={{ marginBottom: 16, justifyContent: 'center', alignItems: 'center' }}>
                                <H2 text="Choose an option"></H2>
                                <TextMuted text="How would you like to use pomodoro timer?"></TextMuted>
                            </View>
                            <TouchableNativeFeedback background={TouchableNativeFeedback.Ripple(theme["color-primary-100"])} onPress={() => { this.props.navigation.navigate('StartWork') }}>
                                <View style={{ marginBottom: 8, width: '100%', zIndex: 999, backgroundColor: '#fff', }}>
                                    <Box icon="clock-time-two-outline" title="Simple" subtitle="Use pomodoro technique to get work done"></Box>
                                </View>
                            </TouchableNativeFeedback>


                        </View>
                    </View>
                </ScrollView>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks
})

const mapDispatchToProps = dispatch => {
    return {}
}

export const WorkTab = connect(mapStateToProps, mapDispatchToProps)(_WorkTab)
