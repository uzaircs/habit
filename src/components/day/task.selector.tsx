import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Card, ListItem, List, CheckBox } from '@ui-kitten/components/ui'
import { View } from 'native-base'
import { DefaultText } from '../../styles/text'
import { UpdateTaskListAsync } from '../../actions/task.action'
import { Row, Col } from 'react-native-easy-grid'

const RenderItem = ({ item }) => {
  
    return (
        <ListItem>
            <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                <Row style={{ minHeight: 32 }} size={12}>

                    <Col size={11}>
                        <DefaultText text={item.name}></DefaultText>
                    </Col>
                    <Col size={1}>
                        <CheckBox></CheckBox>
                    </Col>
                </Row>
            </View>
        </ListItem>
    )
}
const _TaskSelector = ({ tasks }) => {
    return (
        <Layout style={{ flex: 1, padding: 4 }}>
            <List
                renderItem={RenderItem}
                data={tasks}
            >

            </List>
        </Layout>
    )
}

const mapStateToProps = (state) => ({
    tasks: state.tasks.tasks.filter(t => t.status == 0),
})

const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync())
    }
}

export const TaskSelector = connect(mapStateToProps, mapDispatchToProps)(_TaskSelector)
