import React from 'react'
import { Layout, Input, Button, Card, Icon } from '@ui-kitten/components'
import { UppercaseHeading } from '../entry/past'
import { InputLabel } from '../forms/category'
import { ButtonText, PlusOutlineIcon } from '../activity'
import { View } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler'
import { UpdateTaskListAsync } from '../../actions/task.action'
import { connect } from 'react-redux'
import { Row, Col } from 'react-native-easy-grid'
import Slider from '@react-native-community/slider';
import { default as theme } from '../../styles/app-theme.json'
import { DefaultText, TextMuted } from '../../styles/text'
import { TouchableNativeFeedback } from 'react-native'
import { BackNav } from '../../menu/header'
const _WorkSession = ({ navigation, request }) => {
    let [longAfter, setLongAfter] = React.useState(4);
    let [shortTime, setShortTime] = React.useState(5);
    let [longTime, setLongTime] = React.useState(15)
    let [pomodoro, setPomodoro] = React.useState(25);
    let [total, setTotal] = React.useState(4);
    return (
        <View style={{ flex: 1 }}>
            <BackNav subtitle="Create a custom session" navigation={navigation} title="Start Working"></BackNav>
            <Layout style={{ flex: 1, padding: 8, paddingTop: 16 }} level="2">
                <ScrollView>
                    <Card style={{ justifyContent: 'center', alignItems: 'center', marginBottom: 16 }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <UppercaseHeading heading="Tasks"></UppercaseHeading>
                            <Button accessoryLeft={PlusOutlineIcon} onPress={() => { request(); navigation.navigate('TaskSelector') }} appearance="ghost" style={{ width: 164 }} children={(props) => <ButtonText {...props} text="Add Tasks"></ButtonText>}></Button>
                        </View>
                    </Card>

                    <Card disabled={true} >
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <UppercaseHeading heading="Pomodoro Settings"></UppercaseHeading>
                        </View>
                        <Row>
                            <Col>
                                <InputLabel label="Pomodoro Time"></InputLabel>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <InputLabel label={pomodoro + ' Minutes'}></InputLabel>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 16, marginTop: 8 }} size={12}>

                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setPomodoro(pomodoro <= 5 ? 5 : pomodoro - 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="minus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                            <Col size={10}>
                                <Slider
                                    onSlidingComplete={val => setPomodoro(val)}
                                    value={pomodoro}
                                    step={1}
                                    thumbTintColor={theme["color-primary-600"]}
                                    minimumValue={5}
                                    maximumValue={90}
                                    minimumTrackTintColor={theme["color-primary-500"]}
                                    maximumTrackTintColor={theme["color-primary-200"]}
                                />
                            </Col>
                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setPomodoro(pomodoro >= 90 ? 90 : pomodoro + 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="plus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <InputLabel label="Short Break Time"></InputLabel>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <InputLabel label={shortTime + ' Minutes'}></InputLabel>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 16, marginTop: 8 }} size={12}>

                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setShortTime(shortTime <= 1 ? 1 : shortTime - 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="minus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                            <Col size={10}>
                                <Slider
                                    onSlidingComplete={val => setShortTime(val)}
                                    value={shortTime}
                                    step={1}
                                    thumbTintColor={theme["color-primary-600"]}
                                    minimumValue={1}
                                    maximumValue={30}
                                    minimumTrackTintColor={theme["color-primary-500"]}
                                    maximumTrackTintColor={theme["color-primary-200"]}
                                />
                            </Col>
                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setShortTime(shortTime >= 30 ? 30 : shortTime + 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="plus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <InputLabel label="Long Break Time"></InputLabel>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <InputLabel label={longTime + ' Minutes'}></InputLabel>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 16, marginTop: 8 }} size={12}>

                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setLongTime(longTime <= 5 ? 5 : longAfter - 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="minus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                            <Col size={10}>
                                <Slider
                                    onSlidingComplete={val => setLongTime(val)}
                                    value={longTime}
                                    step={1}
                                    thumbTintColor={theme["color-primary-600"]}
                                    minimumValue={5}
                                    maximumValue={90}
                                    minimumTrackTintColor={theme["color-primary-500"]}
                                    maximumTrackTintColor={theme["color-primary-200"]}
                                />
                            </Col>
                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setLongTime(longTime >= 90 ? 90 : longAfter + 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="plus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <InputLabel label="Long Break After"></InputLabel>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <InputLabel label={longAfter + ' Pomodoro'}></InputLabel>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 16, marginTop: 8 }} size={12}>

                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setLongAfter(longAfter <= 1 ? 1 : longAfter - 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="minus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                            <Col size={10}>
                                <Slider
                                    onSlidingComplete={val => setLongAfter(val)}
                                    value={longAfter}
                                    step={1}
                                    thumbTintColor={theme["color-primary-600"]}
                                    minimumValue={1}
                                    maximumValue={10}
                                    minimumTrackTintColor={theme["color-primary-500"]}
                                    maximumTrackTintColor={theme["color-primary-200"]}
                                />
                            </Col>
                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setLongAfter(longAfter >= 10 ? 10 : longAfter + 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="plus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <InputLabel label="Total Pomodoro"></InputLabel>
                            </Col>
                            <Col style={{ alignItems: 'flex-end' }}>
                                <InputLabel label={total + ' Pomodoro'}></InputLabel>
                            </Col>
                        </Row>
                        <Row style={{ marginBottom: 16, marginTop: 8 }} size={12}>

                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setLongAfter(total <= 1 ? 1 : total - 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="minus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                            <Col size={10}>
                                <Slider
                                    onSlidingComplete={val => setTotal(val)}
                                    value={total}
                                    step={1}
                                    thumbTintColor={theme["color-primary-600"]}
                                    minimumValue={1}
                                    maximumValue={10}
                                    minimumTrackTintColor={theme["color-primary-500"]}
                                    maximumTrackTintColor={theme["color-primary-200"]}
                                />
                            </Col>
                            <Col size={1}>
                                <TouchableNativeFeedback onPress={() => {
                                    setTotal(total >= 10 ? 10 : total + 1);
                                }} background={TouchableNativeFeedback.Ripple(theme["color-primary-300"], true)}>
                                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Icon name="plus-circle-outline" style={{ width: 24, height: 24 }} fill={theme["color-primary-300"]}></Icon>
                                    </View>
                                </TouchableNativeFeedback>
                            </Col>
                        </Row>



                    </Card>

                </ScrollView>
            </Layout>
            <Button>Start</Button>
        </View>
    )
}


const mapDispatchToProps = dispatch => {
    return {
        request: () => dispatch(UpdateTaskListAsync())
    }
}

export const WorkSession = connect(null, mapDispatchToProps)(_WorkSession)