import React from 'react'
import { Card, Text, Icon } from '@ui-kitten/components'
import { View } from 'react-native'
import { fontFamily, sizes } from '../styles/fonts'
import { default as theme } from '../styles/app-theme.json'
import { iOSUIKit } from 'react-native-typography'
import { BORDER_RADIUS, DEFAULT_MARGIN } from '../common/constants'

export const DayPastCard = ({ day }) => {
    return (
        <Card>
            <Text></Text>
        </Card>
    )
}
export const EmptyIcon = (props) => {
    return (
        <Icon name='folder-outline' fill="#aaa" style={{ width: 56, height: 56, marginBottom: 8 }} />
    )
}
export const EmptyListPlaceholder = ({ icon, text = "You don't have any past entries", subtext = "Your past entries will be shown here", backgroundColor = null, ...props }) => {
    return (
        <Card style={{backgroundColor: backgroundColor || 'transparent', }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
                {icon || <EmptyIcon></EmptyIcon>}
                <Text style={[iOSUIKit.title3Emphasized, { color: theme["color-font-light-600"] }]}>{text}</Text>
                <Text style={[iOSUIKit.subheadEmphasized, { color: theme["color-font-light-500"], marginTop: DEFAULT_MARGIN / 4 }]}>{subtext}</Text>
                {props.children}
            </View>

        </Card>
    )
}