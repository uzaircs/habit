import { StyleSheet } from "react-native";
import { default as theme } from './app-theme.json'
export const fontFamily = StyleSheet.create({
    bold: {
        fontFamily: 'Graphie-Bold'
    },
    regular: {
        fontFamily: 'Graphie-Regular'
    },
    black: {
        fontFamily: 'Graphie-ExtraBold'
    },
    light: {
        fontFamily: 'Graphie-Light'
    },
    semibold: {
        fontFamily: 'Graphie-SemiBold'
    }
})
export const fontColors = StyleSheet.create({
    muted: {
        color: theme["color-font-300"]
    },
    warning: {
        color: theme["color-warning-600"]
    },
    danger: {
        color: theme["color-danger-500"]
    },
    mediumDark: {
        color: theme["color-font-500"]
    },
    lightDark: {
        color: theme["color-font-400"]
    },
    primary: {
        color: theme["color-primary-500"]
    },
    dark: {
        color: theme["color-font-700"]
    },
    light: {
        color: theme["color-font-light-300"]
    },
    success: {
        color: theme["color-success-500"]
    },
    black: {
        color: theme["color-font-900"]
    },
    white: {
        color: theme["color-font-light-100"]
    }
})
export const sizes = StyleSheet.create({
    small: {
        fontSize: 14
    },
    normal: {
        fontSize: 16,
    },
    medium: {
        fontSize: 20,
    },
    large: {
        fontSize: 24
    },
    huge: {
        fontSize: 32
    }
})