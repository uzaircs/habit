import { fontFamily, sizes, fontColors } from "./fonts"
import { Text } from '@ui-kitten/components';
import React from 'react'
import { iOSUIKit } from 'react-native-typography'
import { default as theme } from '../styles/app-theme.json'
import { StyleSheet } from "react-native";
export const FontFamilies = StyleSheet.create({
    quicksand: {
        fontFamily: 'Quicksand-Regular',
    },
    quicksandbold: {
        fontFamily: 'Quicksand-Bold'
    },
    quicksandlight: {
        fontFamily: 'Quicksand-Light'
    },
    quicksandsemibold: {
        fontFamily: 'Quicksand-SemiBold'
    },
    quicksandmedium: {
        fontFamily: 'Quicksand-Medium'
    }
})
export const DefaultText = ({ text, ...props }) => {
    return (
        <Text style={[iOSUIKit.subheadShort, FontFamilies.quicksandsemibold, { ...props.style }]}>{text}</Text>
    )
}

export const H1Large = ({ text, ...props }) => {

    if (Array.isArray(props.style)) {
        return (
            <Text style={[ FontFamilies.quicksandbold, { fontSize: 28 }, ...props.style]} >{text}</Text>
        )
    } else {
        return (
            <Text style={[ FontFamilies.quicksandbold, { fontSize: 28, color: '#002251' }]} >{text}</Text>
        )
    }
}
export const H1 = ({ text, ...props }) => {

    if (Array.isArray(props.style)) {
        return (
            <Text style={[iOSUIKit.title3Emphasized, FontFamilies.quicksandbold, ...props.style]}>{text}</Text>
        )
    } else {
        return (
            <Text style={[iOSUIKit.title3Emphasized, FontFamilies.quicksandbold]} {...props}>{text}</Text>
        )
    }
}
export const H4 = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.subheadEmphasized, FontFamilies.quicksandsemibold, { ...props.style }]}> { text}</Text >
    )
}
export const H2 = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.title3Emphasized, FontFamilies.quicksandbold, { ...props.style }]}>{text}</Text>
    )
}
export const TextDanger = ({ text, ...props }) => {
    return (
        <Text style={[fontFamily.semibold, sizes.normal, fontColors.danger, FontFamilies.quicksand, { ...props.style }]}>{text}</Text>
    )
}
export const TextSuccess = ({ text, ...props }) => {
    return (
        <Text style={[fontFamily.semibold, sizes.normal, FontFamilies.quicksand, fontColors.success, { ...props.style }]}>{text}</Text>
    )
}
export const TextWarning = ({ text, ...props }) => {
    return (
        <Text style={[fontFamily.semibold, sizes.normal, FontFamilies.quicksand, fontColors.warning, { ...props.style }]}>{text}</Text>
    )
}
export const TextMuted = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.subhead, FontFamilies.quicksandmedium, fontColors.muted, { ...props.style }]}>{text}</Text>
    )
}
export const TextMutedStrong = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.subheadEmphasized, FontFamilies.quicksandbold, fontColors.muted, { ...props.style }]}>{text}</Text>
    )
}


export const TextSubtitle = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.footnote, FontFamilies.quicksand, fontColors.muted]}>{text}</Text>
    )
}
export const TextSmall = ({ text, ...props }) => {

    return (
        <Text style={[iOSUIKit.footnoteEmphasized, FontFamilies.quicksandsemibold, sizes.small, fontColors.muted, { ...props.style }]}>{text}</Text>
    )
}
