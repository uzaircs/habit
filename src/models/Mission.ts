import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy } from '@nozbe/watermelondb/decorators'

export default class Mission extends Model {
    static table = 'missions'
    @field('name') name;
    @field('due_date') DueDate;
    @field('priority') priority;
    @field('status') status;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}