import { Database } from '@nozbe/watermelondb'
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite'
import { default as _seed } from './initial.json'
import { schema } from './schema'
import Moods from './moods'
import Category from './category'

import { getCategories, getActivities, AddTaskListItem } from './api'
import Activity from './activity'
import Entries from './entries'
import ActivityEntries from './activity.entries'
import Tasks from './Tasks'
import Goals from './Goals'
import { migrations } from './migrations'
import { Toast } from 'native-base'
import ListModel from './List'
import DiaryEntry from './Diary'
import TimelineLogs from './timelinelogs'
import Mission from './Mission'
import Reminder from './Reminder'
import { synchronize } from '@nozbe/watermelondb/sync'
import item from 'src/components/goals/item'
import { SessionEvents, Sessions, SessionsTasks } from './Session'
// Then, make a Watermelon database from it!
// First, create the adapter to the underlying database:


const adapter = new SQLiteAdapter({
    schema,
    migrations
})

const database = new Database({
    adapter,
    modelClasses: [
        Moods,
        Category,
        Activity,
        ActivityEntries,
        Entries,
        Tasks,
        Goals,
        ListModel,
        DiaryEntry,
        TimelineLogs,
        Mission,
        Reminder,
        Sessions,
        SessionsTasks,
        SessionEvents


    ],
    actionsEnabled: true,

})


async function mySync() {
    await synchronize({
        database,
        //@ts-expect-error
        pullChanges: async ({ lastPulledAt, schemaVersion, migration }) => {
            const response = await fetch(`https://my.backend/sync`, {
                body: JSON.stringify({ lastPulledAt, schemaVersion, migration })
            })
            if (!response.ok) {
                throw new Error(await response.text())
            }

            const { changes, timestamp } = await response.json()
            return { changes, timestamp }
        },
        pushChanges: async ({ changes, lastPulledAt }) => {
            const response = await fetch(`https://my.backend/sync?last_pulled_at=${lastPulledAt}`, {
                method: 'POST',
                body: JSON.stringify(changes)
            })
            if (!response.ok) {
                throw new Error(await response.text())
            }
        },
        //@ts-expect-error
        migrationsEnabledAtVersion: 1,
    })
}




const checkSeed = async () => {
    const is_seeded = await database.adapter.getLocal("seeded")
    return is_seeded === "true";
}
const updateActivity = async (activity: Activity, category: Category) => {
    await database.action(async () => {
        await activity.update(() => {

            activity.category.id = category.id;
        })
    })
}
const updateIds = async () => {
    const is_seeded = await database.adapter.getLocal("seeded");
    if (is_seeded) {
        let categories = await getCategories();

        let activities = await getActivities();
        for (let i = 0; i < activities.length; i++) {
            let _item = activities[i];
            let local_id = _item.category_local

            let ___index = categories.findIndex(c => c.local_id == local_id)
            if (___index >= 0) {
                await updateActivity(_item, categories[___index]);
            }
        }
        return true;
    } else {
        return false;
    }
}
const reseed = async () => {
    await database.adapter.setLocal("seeded", "true");
    const categoryCollection = await database.collections.get('categories');
    const activityCollection = await database.collections.get('activities');
    const moodsCollection = await database.collections.get<Moods>('moods');

    for (let m = 0; m < _seed.lists.length; m++) {
        try {
            const listItem = _seed.lists[m];

            await AddTaskListItem(listItem);

        } catch (error) {

        }

    }
    await database.action(async () => {
        for (let i = 0; i < _seed.categories.length - 1; i++) {
            const item = _seed.categories[i];
            await categoryCollection.create((c: Category) => {
                c.local_id = item.id.toString();
                c.name = item.name;
                c.icon = item.icon;
                c.icon_type = item.icon_type;
            })
        }

        for (let j = 0; j < _seed.activities.length; j++) {
            const item = _seed.activities[j];
            await activityCollection.create((c: Activity) => {
                c.icon = item.icon;
                c.icon_type = item.icon_type;
                c.name = item.name;
                c.category_local = item.category_id;
            })
        }
        for (let k = 0; k < _seed.moods.length; k++) {
            let _item = _seed.moods[k];
            await moodsCollection.create((c: Moods) => {
                c.emoji = _item.emoji;
                c.title = _item.title;;
                c.subtitle = _item.subtitle;
                c.color = _item.color;
            })
        }

    })
    await updateIds();
    return true;
}
export const ParseSyncChanges = (changes: any[]) => {
    if (Array.isArray(changes) && changes.length) {
        let parsedChanges = {};
        /*     Changes = {
                 [table_name: string]: {
                   created: RawRecord[],
                   updated: RawRecord[],
                   deleted: string[],
                 }
               } */
        changes.forEach((change) => {
            let table = change.record.table;
            let changeType = change.type;
            let raw = change.record._raw;
            if (changeType === 'destroyed') {
                if (parsedChanges[table]) {
                    if (parsedChanges[table]['deleted'] && Array.isArray(parsedChanges[table]['deleted'])) {
                        let existing = parsedChanges[table]['deleted'].findIndex(t => t.id === raw.id);
                        if (existing < 0) {
                            parsedChanges[table]['deleted'].push(raw.id);
                        }
                    } else {
                        parsedChanges[table]['deleted'] = [{ ...raw }];
                    }
                } else {
                    parsedChanges[table] = {};
                    parsedChanges[table]['deleted'] = [{ ...raw }];

                }
            } else {
                if (parsedChanges[table]) {
                    if (parsedChanges[table][changeType] && Array.isArray(parsedChanges[table][changeType])) {
                        let existing = parsedChanges[table][changeType].findIndex(t => t.id === raw.id);
                        if (existing < 0) {
                            parsedChanges[table][changeType].push(raw);
                        }
                    } else {
                        parsedChanges[table][changeType] = [raw];
                    }
                } else {
                    parsedChanges[table] = {};
                    parsedChanges[table][changeType] = [raw];

                }
            }

        })
        return parsedChanges;
    }
}
export const QUEUE_TOKEN = '@changes';
export const LAST_PULLED_AT = '@lastSync'
export const MergeChanges = async (incoming, current) => {
    if (incoming) {

        let merged = incoming;
        let tables = Object.keys(incoming).concat(Object.keys(current));
        if (tables && tables.length) {
            for (let i = 0; i < tables.length; i++) {
                const table = tables[i];
                if (current.hasOwnProperty(table) && current[table]) {
                    let { created, updated, deleted } = current[table];

                    if (created && Array.isArray(created) && created.length) {

                        if (merged[table] && merged[table].created && Array.isArray(merged[table].created) && merged[table].created.length) {

                            created.forEach(c => {
                                let createdEntryExistingIndex = merged[table].created.findIndex(i => i.id === c.id);
                                if (createdEntryExistingIndex < 0) {
                                    merged[table].created.push(c);
                                }
                            })
                        } else {
                            if (!merged[table]) {
                                merged[table] = {};
                                merged[table].created = [];
                                created.forEach(c => {
                                    let createdEntryExistingIndex = merged[table].created.findIndex(i => i.id === c.id);
                                    if (createdEntryExistingIndex < 0) {
                                        merged[table].created.push(c);
                                    }
                                })
                            } else {
                                merged[table].created = created;
                            }
                        }
                    }
                    if (updated && Array.isArray(updated) && updated.length) {
                        if (merged[table] && merged[table].updated && Array.isArray(merged[table].updated) && merged[table].updated.length) {
                            updated.forEach(c => {
                                let updatedEntryExistingIndex = merged[table].updated.findIndex(i => i.id === c.id);
                                if (updatedEntryExistingIndex < 0) {
                                    merged[table].updated.push(c);
                                }
                            })
                        }
                        else {
                            if (!merged[table]) {
                                merged[table] = {};
                                merged[table].updated = [];
                                updated.forEach(c => {
                                    let updatedEntryExistingIndex = merged[table].updated.findIndex(i => i.id === c.id);
                                    if (updatedEntryExistingIndex < 0) {
                                        merged[table].updated.push(c);
                                    }
                                })
                            }
                            else {
                                merged[table].updated = updated;
                            }
                        }
                    }
                    if (deleted && Array.isArray(deleted) && deleted.length) {
                        if (merged[table] && merged[table].deleted && Array.isArray(merged[table].deleted) && merged[table].deleted.length) {
                            deleted.forEach(c => {
                                let deletedEntryExistingIndex = merged[table].deleted.findIndex(i => i === c.id);
                                if (deletedEntryExistingIndex < 0) {
                                    merged[table].deleted.push(c.id);
                                }
                            })
                        }
                        else {
                            if (!merged[table]) {
                                merged[table] = {};
                                merged[table].deleted = [];
                                deleted.forEach(c => {
                                    let deletedEntryExistingIndex = merged[table].deleted.findIndex(i => i === c.id);
                                    if (deletedEntryExistingIndex < 0) {
                                        merged[table].deleted.push(c.id);
                                    }
                                })
                            }
                            else {
                                merged[table].deleted = deleted;
                            }
                        }
                    }
                } else {
                    if (!current[table]) {
                        console.log(`no table ${table} in current`);

                    }
                }

            }
        }
        return merged;
    } else {
        return current;
    }
}
export const QueueChanges = async (changes: any) => {
    try {
        let existing = await database.adapter.getLocal(QUEUE_TOKEN);
        if (existing) {
            existing = JSON.parse(existing);
            let merged = await MergeChanges(changes, existing)
            await database.adapter.setLocal(QUEUE_TOKEN, JSON.stringify(merged));

        } else {
            await database.adapter.setLocal(QUEUE_TOKEN, JSON.stringify(changes));
        }
    } catch (error) {
        console.log(error);
    }
    await database.adapter.setLocal(QUEUE_TOKEN, JSON.stringify(changes));
}
export const seed = async () => {
    const is_seeded = await checkSeed();
    database.withChangesForTables(['tasks', 'activity_entries', 'goals', 'diary_entry']).subscribe(changes => {
        if (changes && Array.isArray(changes) && changes.length) {
            QueueChanges(ParseSyncChanges(changes));
        }
    })
    if (is_seeded) {
        return true;
    } else {
        await reseed();
        return true;
    }

}

export default database;