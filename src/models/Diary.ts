import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy } from '@nozbe/watermelondb/decorators'

export default class DiaryEntry extends Model {
    static table = 'diary_entry'
    @field('title') title;
    @field('text') text;
    @relation('moods', 'mood_id') mood;
    @relation('lists', 'list_id') list;
    @field('status') status;
    @field('tags') tags;
    @field('initiated_at') initiatedAt;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}