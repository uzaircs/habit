
import { Q } from '@nozbe/watermelondb'
import Category from './category';
import Activity from './activity';
import Mood from './moods'
import Entries from './entries';
import ActivityEntries, { UserEntry, UserEntryActivity } from './activity.entries';
import Moods from './moods';
import moment from 'moment'
import database from './database';
import Tasks, { TaskItem } from './Tasks';
import { Session, SessionEvent } from '../components/timer/session';
import { Ticker } from '../components/timer/ticker';
import Goals from './Goals';
import ListModel, { TaskListItem } from './List';
import { sub } from 'react-native-reanimated';
import DiaryEntry from './Diary';
import TimelineLogs from './timelinelogs';
import Mission from './Mission';
import Reminder, { ReminderItem } from './Reminder';
import { NotificationService } from '../modules/NotificationManager';
import { SessionEvents, Sessions, SessionsTasks } from './Session';


// TODO Add custom moods

export const TICKER_TOKEN = '@ticker';
export const SESSION_TOKEN = '@session';
export const EVENTS_TOKEN = '@events';
export const ChangeTaskName = async (id, name) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(id);
            if (task) {
                task.update(t => {
                    t.name = name;
                })
                return true;
            }
            return false;
        })
    } catch (error) {
        return false;
    }
}
export const SessionMapper = session => {
    return {
        id: session.id,
        time: session.time,
        shortBreak: session.shortBreak,
        longBreak: session.longBreak,
        status: session.status,
        limit: session.limit,
        name: session.name,
        createdAt: session.createdAt,
        updatedAt: session.updatedAt,
        tasks: [],
        events: [],
    }
}
export const AddSession = async (session: Sessions) => {
    try {
        const collections = await database.collections.get<Sessions>('sessions');
        let result = await database.action(async () => {
            return await collections.create(c => {
                c.limit = session.limit;
                c.longBreak = session.longBreak;
                c.longBreakAfter = session.longBreakAfter;
                c.name = session.name;
                c.shortBreak = session.shortBreak;
                c.time = session.time;

            })
        })
        return SessionMapper(result);
    } catch (error) {

    }
}
export const GetSessionEvent = async id => {
    const eventCollection = await database.collections.get<SessionEvents>('sessions_events');
    let eventItem = await eventCollection.find(id);
    return eventItem;
}
export const UpdateSessionEventStatus = async (event_id: string, status) => {
    try {
        const eventCollection = await database.collections.get<SessionEvents>('sessions_events');
        let eventItem = await eventCollection.find(event_id);
        let result = await database.action(async () => {
            await eventItem.update(item => {
                item.status = status;
                /*  item.session.id = event.session.id;
                 item.eventType = event.eventType;
                 item.status = event.status;
                 item.endAt = event.endAt;
                 item.endedAt = event.endedAt;
                 item.startAt = event.startAt;
                 item.startedAt = event.startedAt; */
            })
        })
        return result;
    } catch (error) {
        return null;
    }
}
export const ClearAllSessions = async () => {
    try {
        const collections = await database.collections.get<Sessions>('sessions');

        let items = await collections.query().fetch();
        for (let i = 0; i < items.length; i++) {
            const element = items[i];
            await RemoveSession(element.id)
        }

    } catch (error) {
        console.log(error);
    }
}
export const AddSessionEvent = async (session_id: string, event: SessionEvents) => {
    try {
        const eventCollection = await database.collections.get<SessionEvents>('sessions_events');
        let result = await database.action(async () => {
            return await eventCollection.create(item => {
                item.session.id = session_id;
                item.eventType = event.eventType;
                item.endAt = DateToDbUnix(event.endAt);
                item.endedAt = DateToDbUnix(event.endedAt);
                item.startAt = DateToDbUnix(event.startAt);
                item.startedAt = DateToDbUnix(event.startedAt);
                item.status = event.status;
            })
        })
        return SessionEventMapper(result);
    } catch (error) {
        console.log(error);
        return null;
    }
}
export const GetSingleSession = async id => {
    try {
        const collections = await database.collections.get<Sessions>('sessions');
        let item = await collections.find(id);
        let itemMapped = SessionMapper(item);
        if (item) {
            let tasks = await GetSessionTasks(item.id);
            let events = await GetSessionEvents(item.id);
            itemMapped.tasks = tasks;
            itemMapped.events = events;
        }
        return itemMapped;
    } catch (error) {
        return null;
    }

}
export const AddSessionTask = async (session_id: string, ids: string[]) => {
    try {
        const collections = await database.collections.get<SessionsTasks>('sessions_tasks');
        let result = await database.action(async () => {
            for (let i = 0; i < ids.length; i++) {
                const element = ids[i];
                await collections.create(item => {
                    item.session.id = session_id;
                    item.task.id = element;
                })

            }
        })
        return result;
    } catch (error) {
        console.log(error);
        return null;
    }
}
export const SessionEventMapper = (event) => {
    return {
        status: event.status,
        start_at: event.startAt,
        end_at: event.endAt,
        created_at: event.createdAt,
        session_id: event.session.id,
        session: { id: event.session.id }

    }
}
export const GetSessionEvents = async (session_id = null) => {
    try {
        const collections = await database.collections.get<SessionEvents>('sessions_events');
        let result = [];
        if (session_id) {
            result = await collections.query(Q.where('session_id', session_id)).fetch()
        } else {
            result = await collections.query().fetch();
        }
        return result.map(SessionEventMapper)

    } catch (error) {
        return [];
    }
}
export const GetSessionTasks = async session_id => {
    try {
        const collections = await database.collections.get<SessionsTasks>('sessions_tasks');
        let result = await collections.query(Q.where('session_id', session_id)).fetch();
        for (let i = 0; i < result.length; i++) {
            const element = result[i];
            element.task.fetch();
            /*   element.session.fetch(); */
        }
        return result;

    } catch (error) {
        return [];
    }
}
/**
 * Statuses of sessions  
 * > `Created` (not touched just created) = 1  
 * `Active` session is active and has timer running (2)  
 * Session was `completed` (3)  
 * Session was created and was active once but `not active` anymore  
 */
export enum SessionsStatus {
    CREATED = 1,
    ACTIVE = 2,
    COMPLETED = 3,
    INACTIVE = 4
}
export const UpdateSessionStatus = async (session_id, status: number) => {
    try {
        const collections = await database.collections.get<Sessions>('sessions');
        let item = await collections.find(session_id);
        if (item) {
            return await database.action(async () => {
                return await item.update(i => {
                    i.status = status;
                })
            })
        }
    } catch (error) {

    }
}
export const RemoveSession = async (session_id) => {
    try {
        return await database.action(async () => {
            const collections = await database.collections.get<Sessions>('sessions');

            const collections_tasks = await database.collections.get<SessionsTasks>('sessions_tasks');
            const collections_events = await database.collections.get<SessionEvents>('sessions_events');
            let item = await collections.find(session_id);
            let task_items = await collections_tasks.query(Q.where('session_id', session_id)).fetch();
            let event_items = await collections_events.query(Q.where('session_id', session_id)).fetch();
            for (let i = 0; i < event_items.length; i++) {
                const element = event_items[i];
                await element.destroyPermanently();

            }

            try {
                for (let i = 0; i < task_items.length; i++) {
                    const element = task_items[i];
                    console.log(`clearing`);
                    console.log(element);
                    await element.destroyPermanently();

                }
            } catch (error) {

            }
            await item.destroyPermanently();
        })

    } catch (error) {
        return [];
    }
}
export const GetSessions = async () => {
    try {
        const collections = await database.collections.get<Sessions>('sessions');
        let items = await collections.query().fetch();
        if (items) {

            let mappedItems = items.map(SessionMapper)
            for (let i = 0; i < mappedItems.length; i++) {
                const element = mappedItems[i];
                if (element) {

                    let tasks = await GetSessionTasks(element.id);
                    let events = await GetSessionEvents(element.id);

                    if (tasks && Array.isArray(tasks) && tasks.length) {
                        element.tasks = tasks.map(t => t.task.id);
                    }
                    element.events = events;
                }

            }
            return mappedItems;
        } else {
            return [];
        }
    } catch (error) {
        console.log(error);
        return [];
    }
}
export const CreateCategory = async ({ name, icon, icon_type }) => {
    if (!name || !icon || !icon_type) {
        throw new Error('Something went wrong, please make sure you have selected Icon and entered a name for this category');
    }
    let existing_category = await CategoryExistsByName(name);
    if (existing_category) {
        throw new Error('A category with this name already exists');
    }
    const categoryCollection = await database.collections.get('categories');
    return await database.action(async () => {
        return await categoryCollection.create((c: Category) => {
            c.name = name;
            c.icon = icon;
            c.icon_type = icon_type;
        })
    })
}
export const CategoryExistsByName = async (name: string) => {
    const categoryCollection = await database.collections.get('categories');
    let item = await categoryCollection.query(Q.where('name', name)).fetch();
    if (item && item.length > 0) {
        return true;
    } else {
        return false;
    }


}
export const GetGoal = async (id) => {
    try {
        const goalsCollection = await database.collections.get<Goals>('goals');
        let g = await goalsCollection.find(id);
        return {
            name: g.name,
            id: g.id,
            priority: g.priority,
            status: g.status,
            createdAt: g.createdAt,
            updatedAt: g.updatedAt,
            DueDate: (() => {
                try {
                    return DbUnixToDate(+g.DueDate)
                } catch (error) {
                    return null;
                }
            })()
        }
    } catch (error) {

    }

}

export const GetGoals = async () => {
    const goalsCollection = await database.collections.get<Goals>('goals');
    return (await goalsCollection.query().fetch()).map(g => {
        return {
            name: g.name,
            id: g.id,
            priority: g.priority,
            status: g.status,
            createdAt: g.createdAt,
            updatedAt: g.updatedAt,
            DueDate: (() => {
                try {
                    return DbUnixToDate(+g.DueDate)
                } catch (error) {
                    return null;
                }
            })()
        }
    });
}
export const GetMissions = async () => {
    const missionCollection = await database.collections.get<Mission>('missions');
    return (await missionCollection.query().fetch()).map(g => {
        return {
            name: g.name,
            id: g.id,
            priority: g.priority,
            status: g.status,
            createdAt: g.createdAt,
            updatedAt: g.updatedAt
        }
    });
}
export const AddGoal = async (goal) => {
    return await database.action(async () => {
        const goalsCollection = await database.collections.get<Goals>('goals');
        return await goalsCollection.create(g => {
            if (goal.DueDate) {
                g.DueDate = goal.DueDate;
            } else g.DueDate = null;
            g.priority = goal.priority;
            g.status = goal.status;
            g.name = goal.name;
        });
    })
}
export const AddMission = async (mission) => {
    return await database.action(async () => {
        const missionCollection = await database.collections.get<Mission>('missions');
        return await missionCollection.create(m => {
            m.DueDate = mission.DueDate;
            m.priority = mission.priority;
            m.status = mission.status;
            m.name = mission.name;

        });
    })
}
export enum GOAL_STATUS {
    ACTIVE = 1,
    COMPLETED = 2
}
export const ChangeGoalStatus = async (id, status) => {
    return await database.action(async () => {
        const goalsCollection = await database.collections.get<Goals>('goals');
        let goal = await goalsCollection.find(id);
        await goal.update(g => {
            g.status = status;
        })
        return {
            name: goal.name,
            id: goal.id,
            priority: goal.priority,
            status: goal.status,
            createdAt: goal.createdAt,
            updatedAt: goal.updatedAt,
            DueDate: (() => {
                try {
                    return DbUnixToDate(+goal.DueDate)
                } catch (error) {
                    return null;
                }
            })()
        }

    })
}
export const UpdateGoal = async (id, newGoal: Goals) => {
    return await database.action(async () => {
        const goalsCollection = await database.collections.get<Goals>('goals');
        let goal = await goalsCollection.find(id);
        return await goal.update(g => {
            g.status = newGoal.status;
            g.name = newGoal.name;
            g.priority = newGoal.priority;
            g.DueDate = newGoal.DueDate;
        })
    })
}
export const UpdateMission = async (id, newMission: Mission) => {
    return await database.action(async () => {
        const missionCollection = await database.collections.get<Mission>('missions');
        let goal = await missionCollection.find(id);
        return await goal.update(m => {
            m.status = newMission.status;
            m.name = newMission.name;
            m.priority = newMission.priority;
            m.DueDate = newMission.DueDate;
        })
    })
}

export const AddMyDay = async (id) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(id);
            if (task) {
                task.update(t => {
                    t.myDay = moment().unix() * 1000;
                })
                return true;
            }
            return false;
        })
    } catch (error) {
        return false;
    }
}
export const RemoveMyDay = async (id) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(id);
            if (task) {
                task.update(t => {
                    t.myDay = null;
                })
                return true;
            }
            return false;
        })
    } catch (error) {
        return false;
    }
}
export const DeleteGoal = async (id, newGoalId = null) => {
    try {
        let result = await database.action(async () => {
            const goalsCollection = await database.collections.get<Goals>('goals');
            let goal = await goalsCollection.find(id);
            goal.destroyPermanently();
            return true;

        })
        if (result) {
            let taskCollection = await database.collections.get<Tasks>('tasks')
            let tasks = await taskCollection.query(Q.where('goal_id', id)).fetch();
            if (tasks.length) {
                for (let i = 0; i < tasks.length; i++) {
                    await database.action(async () => {
                        await tasks[i].update(context => {
                            context.goal.id = null;
                        })
                    })

                }
            }
            return true;
        }
        return false;
    } catch (error) {

        return false;
    }
}
export const DeleteMission = async (id) => {
    try {
        return await database.action(async () => {
            const missionCollection = await database.collections.get<Mission>('missions');
            let mission = await missionCollection.find(id);
            mission.destroyPermanently();
            return true;
        })
    } catch (error) {
        return false;
    }
}
const ListMapper = l => {

    return {
        name: l.name,
        id: l.id,
        color: l.color,
        icon: l.icon,
        icon_type: l.icon_type,
    }
}
const TaskMapper = (t) => {

    return {
        name: t.name,
        id: t.id,
        notes: t.notes,
        repeat: t.repeat,
        generatedBy: t.generatedBy,
        status: t.status,
        priority: t.priority,
        parent: {
            id: t.parent?.id,

        },
        myDay: t.myDay,
        goal: {
            id: t.goal?.id,
            name: t.goal?.name
        },
        estimatedTime: t.estimatedTime,
        list: {
            id: t.list?.id,
            name: t.list?.name
        },
        reminders: [],
        DueDate: t.DueDate,
        createdAt: t.createdAt,
        updatedAt: t.updatedAt,
        completedAt: (t.status == 1 && t.completedAt) ? DbUnixToDate(t.completedAt) : null
    }

}
export const GetTasks = async () => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    let taskItems = [];
    let queryResult = await taskCollection.query(Q.where('is_emitter', Q.notEq(true))).fetch();

    for (let i = 0; i < queryResult.length; i++) {
        let item = queryResult[i];
        let taskItem = TaskMapper(item);
        if (item.list && item.list.id) {
            let l = await item.list.fetch();
            let listItem = ListMapper(l);
            taskItem.list = listItem;
        }
        let reminder = await GetReminderByItemId(item.id);
        if (Array.isArray(reminder) && reminder.length > 0) {
            taskItem.reminders = [...reminder];
        }
        taskItems.push(taskItem);
    }
    return taskItems;
}
export const GetRepeatTasks = async () => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    let taskItems = [];
    let queryResult = await taskCollection.query(Q.where('is_emitter', true)).fetch();
    for (let i = 0; i < queryResult.length; i++) {
        let item = queryResult[i];
        let taskItem = TaskMapper(item);
        if (item.list && item.list.id) {
            let l = await item.list.fetch();
            let listItem = ListMapper(l);
            taskItem.list = listItem;
        }
        let reminder = await GetReminderByItemId(item.id);
        if (Array.isArray(reminder) && reminder.length > 0) {
            taskItem.reminders = [...reminder];
        }
        taskItems.push(taskItem);
    }
    return taskItems;
}
export const GetGoalTasks = async (id) => {
    try {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        const goalCollection = await database.collections.get<Goals>('goals');
        let goal = await goalCollection.find(id);
        if (goal) {
            let taskItems = [];
            let queryResult = await taskCollection.query(Q.where('goal_id', goal.id)).fetch();
            for (let i = 0; i < queryResult.length; i++) {
                let item = queryResult[i];

                let taskItem = TaskMapper(item);

                if (item.list && item.list.id) {
                    let l = await item.list.fetch();
                    let listItem = ListMapper(l);
                    taskItem.list = listItem;
                }
                let reminder = await GetReminderByItemId(item.id);
                if (Array.isArray(reminder) && reminder.length > 0) {
                    taskItem.reminders = [...reminder];
                }
                taskItems.push(taskItem);
            }
            return taskItems;
        }
    } catch (error) {

    }
}
export const RemoveDuplicateTasks = tasks => {
    return tasks.filter(function (item, pos, self) {
        return self.findIndex(i => i.id === item.id) == pos
    })
}
export const GetGoalTasksWithSubtasks = async (id) => {
    try {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        const goalCollection = await database.collections.get<Goals>('goals');
        let goal = await goalCollection.find(id);
        if (goal) {
            let taskItems = [];
            let queryResult = await taskCollection.query(Q.where('goal_id', goal.id)).fetch();
            for (let i = 0; i < queryResult.length; i++) {
                let item = queryResult[i];

                let taskItem = TaskMapper(item);
                let subtasks = await GetSubtask(taskItem.id);
                if (item.list && item.list.id) {
                    let l = await item.list.fetch();
                    let listItem = ListMapper(l);
                    taskItem.list = listItem;
                }
                let reminder = await GetReminderByItemId(item.id);
                if (Array.isArray(reminder) && reminder.length > 0) {
                    taskItem.reminders = [...reminder];
                }
                taskItems.push(taskItem);
                if (subtasks && subtasks.length) {
                    subtasks.forEach(t => taskItems.push(t))
                }
            }

            return RemoveDuplicateTasks(taskItems);
        }
    } catch (error) {
        console.log(error);
    }
}
export const SetTaskEstimatedTime = async (id, est) => {
    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        try {
            let task = await taskCollection.find(id);
            task.update(t => {
                t.estimatedTime = est;
            })
            return true;
        } catch (error) {
            return false;
        }
    })
}
export const GetTask = async (id: any) => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    let task = await taskCollection.find(id);
    let list = await task.list.fetch();
    let reminder = await GetReminderByItemId(id);
    let mapped = TaskMapper(task);
    if (Array.isArray(reminder) && reminder.length > 0) {
        mapped.reminders = [...reminder];
    }
    mapped.list = list;
    return mapped;

}

export const GetTasksByDate = async (start: Date, end: Date) => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    let startOfDay = moment(start).startOf('day').unix() * 1000;
    let endOfDay = moment(end).endOf('day').unix() * 1000;
    return await (await taskCollection.query(Q.where('created_at', Q.between(startOfDay, endOfDay))).fetch()).map(TaskMapper);
}
export const GetTaskByStatus = async (status) => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    return await (await taskCollection.query(Q.where('status', status)).fetch()).map(TaskMapper);
}
export const GetRecentTasks = async (limit: number = 10) => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    return await (await taskCollection.query().fetch()).reverse().slice(0, limit).map(TaskMapper);
}
export const GetTasksByList = async (list: number) => {
    const taskCollection = await database.collections.get<Tasks>('tasks');
    return await (await taskCollection.query(Q.where('list', list)).fetch()).map(TaskMapper);
}
export const AddTask = async (item: TaskItem) => {

    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let rawTask = await taskCollection.create(task => {
            task.name = item.name;
            task.priority = item.priority;
            task.DueDate = item.DueDate;
            if (item.goal_id) {
                task.goal.id = item.goal_id;
            }
            if (item.parent_id) {
                task.parent.id = item.parent_id;
            }
            if (item.list_id) {
                task.list.id = item.list_id;
            }
            task.status = 0;

        })
        let addedTask = await GetTask(rawTask.id);
        return (addedTask)
    })
}
export const ChangeTaskDueDate = async (id: string, DueDate: string) => {
    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        try {
            let task = await taskCollection.find(id);
            task.update(t => {
                t.DueDate = DueDate;
            })
            return true;
        } catch (error) {
            return false;
        }
    })
}

export const ChangeTaskStatus = async (id: string, status: number) => {
    let completedAt;
    if (status == 1) {
        completedAt = DateToDbUnix(moment());
    }
    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        try {
            let task = await taskCollection.find(id);
            task.update(t => {
                t.status = status;
                t.completedAt = completedAt;
            })
            return true;
        } catch (error) {
            return false;
        }
    })
}
//TODO Repeat update for task
/* export const ChangeTaskRepeatSettings = async (id: string, DueDate: Date) => {
    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        try {
            let task = await taskCollection.find(id);
            task.update(t => {
                t.DueDate = DueDate;
            })
            return true;
        } catch (error) {
            return false;
        }
    })
} */
export const CategoryExists = async (id: string) => {
    const categoryCollection = await database.collections.get('categories');
    let item = await categoryCollection.query(Q.where('id', id)).fetch();
    if (item && item.length > 0) {
        return true;

    } else {
        return false;
    }

}

export const CreateActivity = async ({ name, icon, icon_type, category_id }) => {
    if (!name || !name.length || !icon || !icon.length || !icon_type || !category_id) {
        throw new Error('Something went wrong. Please try later\nSome values are invalid or missing');
    }
    const activityCollection = await database.collections.get<Activity>('activities');
    return await database.action(async () => {
        return await activityCollection.create(a => {
            a.name = name;
            a.icon = icon;
            a.icon_type = icon_type;
            a.category.id = category_id;
        })
    })
}
export const CreateMood = async ({ title, emoji, color, subtitle }) => {
    if (!title || !title.length || !emoji || !emoji.length || !color) {
        throw new Error('Something went wrong. Please try later\nSome values are invalid or missing');
    }
    const moodsCollection = await database.collections.get<Mood>('moods');
    return database.action(async () => {
        return await moodsCollection.create(m => {
            m.title = title;
            m.subtitle = subtitle;
            m.emoji = emoji;
            m.color = color;

        })
    })
}
export const getEntriesByDay = async (day: Date): Promise<UserEntry[]> => {
    let startOfDay = moment(day).startOf('day').unix() * 1000;
    let endOfDay = moment(day).endOf('day').unix() * 1000;
    const userEntries = await database.collections.get<Entries>('entries').query(Q.where('created_at', Q.between(startOfDay, endOfDay))).fetch();
    return await getEntries(userEntries);
}
export const GetEntry = async id => {
    try {
        const userEntries = await database.collections.get<Entries>('entries')
        let result = await userEntries.find(id);
        const activityEntries = await database.collections.get<ActivityEntries>('activity_entries').query(Q.where('entry_id', result.id)).fetch();
        let _mood: Moods = await result.mood.fetch();
        let entry: UserEntry = { id: result.id };
        entry.mood = _mood.title;
        entry.created_at = result.createdAt;
        entry.updated_at = result.updatedAt;
        entry.mood_id = _mood.id;
        entry.color = _mood.color;
        entry.emoji = _mood.emoji;
        entry.activity = [];
        for (let j = 0; j < activityEntries.length; j++) {
            const _activity_entry = activityEntries[j];
            let activity: Activity = await _activity_entry.activity.fetch();
            if (activity && activity.icon) {
                let _activity: UserEntryActivity = {
                    icon: activity.icon,
                    icon_type: activity.icon_type,
                    title: activity.name,
                    id: activity.id
                }
                entry.activity.push(_activity);
            }

        }
        return entry;
    } catch (error) {
        return null;
    }
}
export const DeleteEntry = async id => {
    try {
        return await database.action(async () => {
            const userEntries = await database.collections.get<Entries>('entries')
            let result = await userEntries.find(id);
            await result.destroyPermanently();
            return true;
        })
    } catch (error) {
        return false;
    }
}
export const ChangeMood = async (entry_id, mood_id) => {
    try {
        return await database.action(async () => {
            const userEntries = await database.collections.get<Entries>('entries')
            let result = await userEntries.find(entry_id);
            await result.update((e) => {
                e.mood.id = mood_id;
            })
            return true;
        })
    } catch (error) {

        return null;
    }
}
export const getActivitiesByDay = async (day: Date, mood_id: string = null) => {
    let entries = await getEntriesByDay(day);
    let activities: UserEntryActivity[] = [];
    if (mood_id !== null) {
        entries = entries.filter((entry) => entry.mood_id == mood_id);
    }
    for (let i = 0; i < entries.length; i++) {
        const entry = entries[i];
        activities.push(...entry.activity);
    }
    return activities;
}
export const ConfigureRepeat = async (task, repeatConfig) => {
    // TODO
    if (!task.id) return false;
    try {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let dbTask = await taskCollection.find(task.id);
        let duplicateTask = await AddTask(dbTask);
        return await database.action(async () => {
            await dbTask.goal.fetch();
            await dbTask.list.fetch();
            await dbTask.parent.fetch();
            if (repeatConfig.mode) {
                let duplicateTaskContext = await taskCollection.find(duplicateTask.id);
                await duplicateTaskContext.update(context => {
                    context.repeat = repeatConfig;
                    context.isEmitter = true;
                    context.DueDate = dbTask.DueDate;
                    context.estimatedTime = dbTask.estimatedTime;
                    context.goal.id = dbTask.goal.id;
                    context.list.id = dbTask.list.id;
                    context.notes = dbTask.notes;
                    context.parent.id = dbTask.parent.id;


                })
                await dbTask.update(context => {
                    context.generatedBy = duplicateTaskContext.id;
                })

                /*   await dbTask.update(updateTaskContext => {
                      updateTaskContext.repeat = JSON.stringify(repeatConfig);
                  }) */
                return duplicateTaskContext.id;
            }
            return false;
        })
    } catch (error) {
        return false;
    }
}
export interface EntryFilter {
    start?: any;
    end?: any;
    moods?: any;
}
export const GetEntriesWithFilter = async (filter: EntryFilter) => {
    let userEntries = [];
    let query = [];
    if (filter.start && filter.end) {
        let startUnix = DateToDbUnix(filter.start);
        let endUnix = DateToDbUnix(filter.end);
        let dateQuery = Q.and(Q.where('created_at', Q.gte(startUnix)), Q.where('created_at', Q.lte(endUnix)));
        query.push(dateQuery);
    } else if (filter.start) {
        let startUnix = DateToDbUnix(filter.start);
        let dateQuery = Q.where('created_at', Q.gte(startUnix));
        query.push(dateQuery);
    }
    if (filter.moods && Array.isArray(filter.moods)) {
        let moodQuery = Q.where('mood_id', Q.oneOf(filter.moods));
        query.push(moodQuery);
    }

    userEntries = await database.collections.get<Entries>('entries').query(...query).fetch();
    return getEntries(userEntries);
}
export const getEntries = async (entries = null): Promise<UserEntry[]> => {

    let userEntries = [];
    if (!entries) {
        userEntries = await database.collections.get<Entries>('entries').query().fetch();

    } else {
        userEntries = entries;
    }

    const _entries: UserEntry[] = [];

    for (let i = 0; i < userEntries.length; i++) {
        const element = userEntries[i];

        const activityEntries = await database.collections.get<ActivityEntries>('activity_entries').query(Q.where('entry_id', element.id)).fetch();
        let _mood: Moods = await element.mood.fetch();
        let entry: UserEntry = { id: element.id };
        entry.mood = _mood.title;
        entry.created_at = element.createdAt;
        entry.updated_at = element.updatedAt;
        entry.notes = element.notes;
        entry.mood_id = _mood.id;
        entry.color = _mood.color;
        entry.emoji = _mood.emoji;
        entry.activity = [];
        for (let j = 0; j < activityEntries.length; j++) {
            const _activity_entry = activityEntries[j];
            let activity: Activity = await _activity_entry.activity.fetch();
            if (activity && activity.icon) {
                let _activity: UserEntryActivity = {
                    icon: activity.icon,
                    icon_type: activity.icon_type,
                    title: activity.name,
                    id: activity.id
                }
                entry.activity.push(_activity);
            }

        }
        _entries.push(entry);

    }

    return _entries;

}

export const getMoods = async () => {
    const moodsCollection = await database.collections.get<Mood>('moods');
    const allMoods = await (await moodsCollection.query().fetch()).map(m => {
        return {
            id: m.id,
            title: m.title,
            emoji: m.emoji,
            color: m.color,
            subtitle: m.subtitle,
            createdAt: m.createdAt,
            updatedAt: m.updatedAt
        }
    });
    return allMoods;
}

export const CreateEntry = async (mood: string, notes: string = '') => {
    const entryCollection = await database.collections.get<Entries>('entries');
    return await database.action(async () => {
        let newEntry = await entryCollection.create(e => {
            e.mood.id = mood;

            e.notes = notes

        })
        return newEntry.id;
    })


}
export const AttachActivity = async (entry_id: any, activities: string[]) => {
    await database.action(async () => {
        const userActivityCollection = await database.collections.get<ActivityEntries>('activity_entries');
        for (let i = 0; i < activities.length; i++) {
            let activityItem = activities[i];
            /* const newActivity = */
            await userActivityCollection.create(_a => {
                _a.entry.id = entry_id;
                _a.activity.id = activityItem;
            })
        }

    })
}
/* Add new user entry */
export const SaveEntry = async (mood: string, activities: string[], options = { notes: '' }) => {

    let newEntry = await CreateEntry(mood, options.notes);
    await AttachActivity(newEntry, activities);
    return newEntry;

}

export const getCategories = async () => {
    const categoryCollection = database.collections.get<Category>('categories')
    const allCategories = await categoryCollection.query().fetch();
    return allCategories.map(c => {
        return {
            id: c.id,
            name: c.name,
            local_id: c.local_id,
            icon_type: c.icon_type,
            icon: c.icon
        }
    });
}

const ActivityMapper = (a) => {

    return {
        name: a.name,
        icon: a.icon,
        icon_type: a.icon_type,
        id: a.id,
        category_id: a.category.id,
        category_local: a.category_local,
        category: {
            id: a.category.id,
            name: a.category.name,
            icon: a.category.icon, icon_type: a.category.icon_type
        },

    }

}

export const getActivities = async (category_id = null) => {
    const activityCollection = database.collections.get<Activity>('activities')

    if (!category_id) {
        const allActivities = await activityCollection.query().fetch();

        return allActivities;
    } else {

        const activities = await activityCollection.query(Q.where('category_id', category_id)).fetch();
        return activities;
    }
}
export const ClearTicker = async () => {
    console.log(`ticker cleared`);
    return await database.adapter.removeLocal(TICKER_TOKEN);
}

/**
 * @description
 * Retrieves any timer (instanceof Ticker) stored previously. If none is stored, returns null. 
 * @returns {Promise<Ticker>}
 */
export const RetreiveTimer = async (): Promise<Ticker | null> => {
    try {
        let tickerString = await database.adapter.getLocal(TICKER_TOKEN);
        console.log(`got ticker string: ${tickerString}`);
        if (!tickerString) return null;
        let tickerJson: Ticker = JSON.parse(tickerString);

        let ticker = new Ticker(tickerJson.event);
        ticker.isPaused = tickerJson.isPaused
        ticker.timeKeeper = new Date(ticker.timeKeeper);
        if (tickerJson.pausedAt) {
            ticker.pausedAt = new Date(tickerJson.pausedAt);
        }
        ticker.endAt = new Date(tickerJson.endAt);
        ticker.timeDifference = tickerJson.timeDifference;
        return ticker;
    } catch (error) {
        return null;
    }

}
export const SaveTimer = async (ticker: Ticker) => {
    let tickerString = JSON.stringify(ticker);
    return await database.adapter.setLocal(TICKER_TOKEN, tickerString);
}
/**
 * Save session
 * @param session instance of Session
 */
export const SaveSession = async (session: Session) => {
    let sessionString = JSON.stringify(session);
    return await database.adapter.setLocal(SESSION_TOKEN, sessionString);
}
export const RetrieveSession = async (): Promise<Session> => {
    let sessionString = await database.adapter.getLocal(SESSION_TOKEN);
    if (sessionString) {
        return JSON.parse(sessionString);
    } else return null;
}
export const SaveEvents = async (events: SessionEvent) => {
    let eventsString = JSON.stringify(events);
    return await database.adapter.setLocal(EVENTS_TOKEN, eventsString);
}
export const DeleteTasks = async (ids) => {
    if (Array.isArray(ids)) {
        for (let i = 0; i < ids.length; i++) {
            const id = ids[i];
            try {
                await DeleteTask(id);
            } catch (error) {

            }
        }
        return true;
    } else {
        return false;
    }
}
export const DeleteTask = async (id) => {

    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let task = await taskCollection.find(id);
        let subtasks = await taskCollection.query(Q.where('parent_id', task.id)).fetch();
        for (let i = 0; i < subtasks.length; i++) {
            const subtask = subtasks[i];
            await subtask.destroyPermanently();

        }
        if (task) {
            await task.destroyPermanently();
            return true;
        } else {
            return false;
        }
    })
}
export const GetSubtask = async (id) => {
    try {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let task = await taskCollection.find(id);
        let subtasksCollection = [];
        let subtasks: any = await taskCollection.query(Q.where('parent_id', task.id)).fetch();
        for (let i = 0; i < subtasks.length; i++) {
            let item = subtasks[i];

            let taskItem = TaskMapper(item);

            if (item.list && item.list.id) {
                let l = await item.list.fetch();
                let listItem = ListMapper(l);
                taskItem.list = listItem;
            }
            let reminder = await GetReminderByItemId(item.id);
            if (Array.isArray(reminder) && reminder.length > 0) {
                taskItem.reminders = [...reminder];
            }
            subtasksCollection.push(taskItem);
        }

        if (subtasksCollection.length) {
            for (let i = 0; i < subtasksCollection.length; i++) {
                let innerTasks = await GetSubtask(subtasksCollection[i].id);
                subtasksCollection = [...subtasksCollection, ...innerTasks];
            }
        }
        return subtasksCollection;
    } catch (error) {
        return [];
    }
}
export const GetSubtaskCount = async (id) => {
    try {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let task = await taskCollection.find(id);
        let subtasks = await taskCollection.query(Q.where('parent_id', task.id)).fetch();
        return subtasks.length;
    } catch (error) {
        return 0;
    }
}
export const GroupTask = async (ids, name) => {
    return await database.action(async () => {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        if (ids && Array.isArray(ids) && ids.length > 0) {
            let task = await taskCollection.create(task => {
                task.name = name;
                task.priority = 0;
                task.status = 0;


            })
            for (let i = 0; i < ids.length; i++) {
                const subtask = ids[i];
                let subTask = await taskCollection.find(subtask);
                if (subTask) {
                    await subTask.update(t => {
                        t.parent.id = task.id;
                    })
                }

            }
            return true;
        } else {
            return false;
        }
    })
}
export const GetListItems = async () => {
    const listCollection = await database.collections.get<ListModel>('lists');
    return await (await listCollection.query().fetch()).map(item => {
        return {
            name: item.name,
            icon: item.icon,
            icon_type: item.icon_type,
            color: item.color,
            id: item.id,
        }
    });
}
export const GetListItem = async (id) => {
    const listCollection = await database.collections.get<ListModel>('lists');
    try {
        return await [(await listCollection.find(id))].map(item => {
            return {
                name: item.name,
                icon: item.icon,
                icon_type: item.icon_type,
                color: item.color,
                id: item.id,
            }
        })[0];
    } catch (error) {
        return false;
    }
}
export const AttachTaskToList = async (taskid, listid) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(taskid);
            if (task) {
                await task.update(t => {

                    t.list.id = listid;
                })

                return true;
            } else {
                return false;
            }

        })
    } catch (error) {

        return false;
    }
}
export const AddTaskListItem = async (listItem) => {
    return await database.action(async () => {
        const listCollection = await database.collections.get<ListModel>('lists');
        return await listCollection.create((item: any) => {
            item.name = listItem.name;
            item.icon = listItem.icon;
            item.icon_type = listItem.icon_type;
            item.color = listItem.color;
        })

    })
}
export const DeleteTaskListItem = async (id) => {
    try {
        return await database.action(async () => {
            const listCollection = await database.collections.get<ListModel>('lists');
            let listItem = await listCollection.find(id);
            if (listItem) {
                await listItem.destroyPermanently();
                return true;
            } else {
                return false;
            }

        })
    } catch (error) {
        return false;
    }
}
export const RemoveTaskList = async (taskid) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(taskid);
            if (task) {
                await task.update(t => {
                    t.list.id = null;
                })

                return true;
            } else {
                return false;
            }

        })
    } catch (error) {
        return false;
    }
}
export const AddTaskNotes = async (id, notes) => {
    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(id);
            if (task) {
                await task.update(t => {
                    t.notes = notes;
                })
                return true;
            } else {
                return false;
            }
        })
    } catch (error) {
        return false;
    }
}
export const AttachTaskToGoal = async (taskid, goalid) => {

    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(taskid);
            if (task) {
                await task.update(t => {
                    t.goal.id = goalid;
                })

                return true;
            } else {
                return false;
            }

        })
    } catch (error) {
        return false;
    }
}
export const RemoveTaskToGoal = async (taskid) => {

    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(taskid);
            if (task) {
                await task.update(t => {
                    t.goal.id = null;
                })

                return true;
            } else {
                return false;
            }

        })
    } catch (error) {
        return false;
    }
}
export const BindTaskToParent = async (taskid, parentid) => {

    try {
        return await database.action(async () => {
            const taskCollection = await database.collections.get<Tasks>('tasks');
            let task = await taskCollection.find(taskid);
            if (task) {
                await task.update(t => {
                    t.parent.id = parentid;
                })

                return true;
            } else {
                return false;
            }

        })
    } catch (error) {
        return false;
    }
}


//#region Diary API
const diaryEntryMapper = (item) => {

}

export const RemoveDiaryEntry = async (id) => {
    return await database.action(async () => {
        const diaryCollection = await database.collections.get<DiaryEntry>('diary_entry');
        try {
            let entry = await diaryCollection.find(id);
            if (entry) {
                await entry.destroyPermanently();
                return true;
            }
        } catch (error) {
            return false;
        }
    })
}
export const DiaryMapper = entry => {
    return {
        id: entry.id,
        title: entry.title,
        text: entry.text,
        status: entry.status,
        initiatedAt: entry.initiatedAt,
        tags: entry.tags,
        createdAt: entry.createdAt,
        updatedAt: entry.updatedAt,
        list: {
            id: entry.list?.id,
        },
        mood: {
            id: entry.mood?.id,
            emoji: entry.mood?.emoji,
            color: entry.mood?.color,
            createdAt: entry.mood?.createdAt,
            updatedAt: entry.mood?.updatedAt,
        }
    }
}
export const GetDiaryEntries = async () => {
    const diaryCollection = await database.collections.get<DiaryEntry>('diary_entry');
    let entries = await diaryCollection.query().fetch();
    for (let i = 0; i < entries.length; i++) {
        const entry = entries[i];
        await entry.mood.fetch();
        await entry.list.fetch();
    }
    return entries.map(DiaryMapper);
}
export const AddDiaryEntry = async (entry: any) => {
    return await database.action(async () => {
        const diaryCollection = await database.collections.get<DiaryEntry>('diary_entry');
        try {
            let e = await diaryCollection.create(newEntry => {
                newEntry.title = entry.title;
                newEntry.text = entry.text;
                newEntry.tags = entry.tags;
                newEntry.status = 1;
                if (entry.initiatedAt && moment(entry.initiatedAt).isValid()) {
                    let unix = moment(entry.initiatedAt).unix() * 1000;
                    newEntry.initiatedAt = unix;

                }
                if (entry.mood && entry.mood.id) {
                    newEntry.mood.id = entry.mood.id;
                }
                if (entry.list && entry.list.id) {
                    newEntry.list.id = entry.list.id;
                }
            })
            return DiaryMapper(e);
        } catch (error) {
            return false;
        }
    });
}
export const UpdateDiaryEntry = async (id, entry) => {
    return await database.action(async () => {
        try {
            let entryCollection = await database.collections.get<DiaryEntry>('diary_entry');
            let item = await entryCollection.find(id);
            if (item) {
                await item.update(updated => {
                    updated.title = entry.title;
                    updated.text = entry.text;
                    updated.status = entry.status;
                    if (entry.mood && entry.mood.id) {
                        updated.mood.id = entry.mood.id;
                    }
                    if (entry.list && entry.list.id) {
                        updated.list.id = entry.list.id;
                    }
                })
                return DiaryMapper(item);
            } else return false;
        } catch (error) {
            return false;
        }
    })
}
//#endregion Diary API


//#region Timeline Logs API
export const CreateTimelineLog = async (newLog: TimelineLogs) => {
    return await database.action(async () => {
        let logCollection = await database.collections.get<TimelineLogs>('timeline_logs');
        return logCollection.create(log => {
            log.subject = newLog.subject;
            log.subject_type = newLog.subject_type;
            log.action = newLog.action;
            log.actionDescription = newLog.actionDescription;

        })
    })
}
export const GetTimelineLogs = async () => {
    let logCollection = await database.collections.get<TimelineLogs>('timeline_logs');
    let logs = await logCollection.query().fetch();
    return logs.map(log => {
        return {
            subject_type: log.subject_type, subject: log.subject, action: log.action, actionDescription: log.actionDescription, createdAt: log.createdAt
        }
    })
}
export const isDbUnix = date => {
    return (date && (!isNaN(date) || !isNaN(parseInt(date)) && moment.unix(+date / 1000).isValid()));
}
export const DbUnixToDate = (date: number | any) => {
    if (!isNaN(date) || !isNaN(parseInt(date))) {
        return moment.unix(+date / 1000)
    } else return;
}
export const DateToDbUnix = (date: any) => {
    if ((date instanceof Date) || moment(date).isValid()) {
        return moment(date).unix() * 1000
    }
    return null;
}
//#endregion


//#region START REMINDER API
export const GetReminderByItemId = async (itemId: string) => {
    try {
        let reminders = await database.collections.get<Reminder>(`reminders`);
        let result = await reminders.query(Q.where('item_id', itemId)).fetch();
        return result.map(reminder => {
            return {
                notificationId: reminder.notificationId,
                createdAt: reminder.createdAt,
                itemId: reminder.itemId, date: reminder.date
            }
        })
    } catch (error) {
        return [];
    }
}
export const GetReminderById = async (id) => {
    try {
        let reminders = await database.collections.get<Reminder>(`reminders`);
        return await reminders.query(Q.where('id', id)).fetch();
    } catch (error) {
        return [];
    }
}
export const AddReminder = async (reminder: ReminderItem) => {


    if (!reminder.itemType) {
        const taskCollection = await database.collections.get<Tasks>('tasks');
        let task = await taskCollection.find(reminder.itemId);
        NotificationService.schedule(task.name, "Task Reminder", task.name, reminder.date, reminder.notificationId);
    }

    return await database.action(async () => {
        const reminderCollection = await database.collections.get<Reminder>('reminders');
        return await reminderCollection.create(r => {
            r.itemId = reminder.itemId;
            r.notificationId = reminder.notificationId.toString();
            r.itemType = reminder.itemType;
            r.date = DateToDbUnix(reminder.date);
        })
    })
}
export const RemoveReminder = async (itemId: string) => {
    try {

        return await database.action(async () => {
            const reminderCollection = await database.collections.get<Reminder>('reminders');
            let reminders = await reminderCollection.query(Q.where('itemId', itemId)).fetch();
            if (Array.isArray(reminders) && reminders.length == 1) {
                let reminder = reminders[0];
                NotificationService.cancelScheduled(reminder.notificationId);
                await reminder.destroyPermanently();
                return true;
            } else if (reminders.length > 1) {
                // some issue, delete all
                for (let i = 0; i < reminders.length; i++) {
                    const r = reminders[i];
                    NotificationService.cancelScheduled(r.notificationId);
                    await r.destroyPermanently();

                }
                return true;
            } else return false;
        })
    } catch (error) {
        return false;
    }
}
//#endregion