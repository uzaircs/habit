import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy } from '@nozbe/watermelondb/decorators'
export type ReminderItem = {
    notificationId: number | string;
    itemId: string;
    itemType?: number;
    date: any;
    createdAt?: Date;
    updatedAt?: Date;

}


export default class Reminder extends Model {
    static table = 'reminders'
    @field('item_id') itemId;
    @field('notification_id') notificationId;
    @field('item_type') itemType;
    @date('date') date;

    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}