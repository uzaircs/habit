import { appSchema, tableSchema } from '@nozbe/watermelondb';

export const schema = appSchema({
    version: 18,
    tables: [
        tableSchema(
            {
                name: 'diary_entry',
                columns: [
                    { name: 'title', type: 'string', isOptional: true },
                    { name: 'text', type: 'string', isOptional: true },
                    { name: 'mood_id', type: 'string', isOptional: true },
                    { name: 'list_id', type: 'string', isOptional: true },
                    { name: 'status', type: 'string', isOptional: true },
                    { name: 'tags', type: 'string', isOptional: true },
                    { name: 'status', type: 'number', isOptional: true },
                    { name: 'initiated_at', type: 'number', isOptional: true },
                    { name: 'created_at', type: 'number' },
                    { name: 'updated_at', type: 'number' },

                ]
            }),
        tableSchema(
            {
                name: 'moods',
                columns: [
                    { name: 'color', type: 'string', isOptional: true },
                    { name: 'title', type: 'string' },
                    { name: 'emoji', type: 'string' },
                    { name: 'subtitle', type: 'string', isOptional: true },

                ]
            }),
        tableSchema(
            {
                name: 'reminders',
                columns: [
                    { name: 'item_id', type: 'string' },
                    { name: 'notification_id', type: 'string' },
                    { name: 'date', type: 'number' },
                    { name: 'item_type', type: 'number', isOptional: true },
                    { name: 'created_at', type: 'number' },
                    { name: 'updated_at', type: 'number' },

                ]
            }),
        tableSchema(
            {
                name: 'timeline_logs',
                columns: [
                    { name: 'subject_id', type: 'string', isOptional: true },
                    { name: 'action_type', type: 'string', isOptional: true },
                    { name: 'subject_type', type: 'string', isOptional: true },
                    { name: 'created_at', type: 'number' },
                    { name: 'action_description', type: 'string', isOptional: true },

                ]
            }),
        tableSchema({
            name: 'emoji',
            columns: [
                { name: 'name', type: 'string' },

            ]
        }),
        tableSchema({
            name: 'categories',
            columns: [
                { name: 'name', type: 'string' },
                { name: 'icon', type: 'string', isOptional: true },
                { name: 'local_id', type: 'string', isOptional: true },
                { name: 'icon_type', type: 'string', isOptional: true },

            ]
        }),
        tableSchema({
            name: 'activities',
            columns: [
                { name: 'name', type: 'string' },

                { name: 'category_id', type: 'string', isIndexed: true, isOptional: true },
                { name: 'category_local', type: 'number', isIndexed: true, isOptional: true },
                { name: 'icon_type', type: 'string', isOptional: true },
                { name: 'icon', type: 'string', isIndexed: true },
            ]
        }),
        tableSchema({
            name: 'entries',
            columns: [
                { name: 'mood_id', type: 'string' },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
                { name: 'notes', type: 'string', isOptional: true },

            ]
        }),
        tableSchema({
            name: 'activity_entries',
            columns: [
                { name: 'entry_id', type: 'string' },
                { name: 'activity_id', type: 'string' }
            ]
        }),
        tableSchema({
            name: 'goals',
            columns: [
                { name: 'name', type: 'string' },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
                { name: 'due_date', type: 'string', isOptional: true },
                { name: 'priority', type: 'string', isOptional: true },
                { name: 'status', type: 'number', isOptional: true },
            ]
        }),
        tableSchema({
            name: 'missions',
            columns: [
                { name: 'name', type: 'string' },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
                { name: 'due_date', type: 'string', isOptional: true },
                { name: 'priority', type: 'string', isOptional: true },
                { name: 'status', type: 'number', isOptional: true },
            ]
        }),
        tableSchema({
            name: 'lists',
            columns: [
                { name: 'name', type: 'string' },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
                { name: 'icon_type', type: 'string', isOptional: true },
                { name: 'icon', type: 'string', isOptional: true },
                { name: 'color', type: 'string', isOptional: true },

            ]
        }),
        tableSchema({
            name: 'sessions',
            columns: [
                { name: 'status', type: 'number', isOptional: true },
                { name: 'name', type: 'string' },
                { name: 'time', type: 'number', isOptional: true },
                { name: 'shortBreak', type: 'number', isOptional: true },
                { name: 'longBreak', type: 'number', isOptional: true },
                { name: 'longBreakAfter', type: 'number', isOptional: true },
                { name: 'limit', type: 'number', isOptional: true },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },

            ]
        }),
        tableSchema({
            name: 'sessions_tasks',
            columns: [
                { name: 'task_id', type: 'string' },
                { name: 'session_id', type: 'string' },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
            ]
        }),
        tableSchema({
            name: 'sessions_events',
            columns: [
                { name: 'session_id', type: 'string' },
                { name: 'event_type', type: 'number', isOptional: true },
                { name: 'start_at', type: 'number', isOptional: true },
                { name: 'status', type: 'number', isOptional: true },
                { name: 'started_at', type: 'number', isOptional: true },
                { name: 'end_at', type: 'number', isOptional: true },
                { name: 'ended_at', type: 'number', isOptional: true },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
            ]
        }),
        tableSchema({
            name: 'tasks',
            columns: [
                { name: 'name', type: 'string' },
                { name: 'notes', type: 'string', isOptional: true },
                { name: 'list_id', type: 'string', isOptional: true },
                { name: 'repeat', type: 'string', isOptional: true },
                { name: 'is_emitter', type: 'boolean', isOptional: true },
                { name: 'generated_by', type: 'string', isOptional: true },
                { name: 'created_at', type: 'number' },
                { name: 'updated_at', type: 'number' },
                { name: 'completed_at', type: 'number', isOptional: true },
                { name: 'estimated_time', type: 'number', isOptional: true },
                { name: 'start_time', type: 'number', isOptional: true },
                { name: 'my_day', type: 'number', isOptional: true },
                { name: 'parent_id', type: 'string', isOptional: true },
                { name: 'due_date', type: 'string', isOptional: true },
                { name: 'priority', type: 'string', isOptional: true },
                { name: 'status', type: 'number', isOptional: true },
                { name: 'goal_id', type: 'string', isOptional: true },
            ]
        }),
    ]
})