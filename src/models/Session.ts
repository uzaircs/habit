import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy, json } from '@nozbe/watermelondb/decorators'

export class Sessions extends Model {
    static table = 'sessions'
    @field('status') status;
    @field('time') time;
    @field('shortBreak') shortBreak;
    @field('longBreak') longBreak;
    @field('longBreakAfter') longBreakAfter;
    @field('limit') limit;
    @field('name') name;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}
export class SessionsTasks extends Model {
    static table = 'sessions_tasks'
    @relation('tasks', 'task_id') task;
    @relation('sessions', 'session_id') session;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}
export class SessionEvents extends Model {
    static table = 'sessions_events'
    @relation('sessions', 'session_id') session;
    @field('event_type') eventType;
    @field('status') status;
    @date('start_at') startAt;
    @date('end_at') endAt;
    @date('started_at') startedAt;
    @date('ended_at') endedAt;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt

}