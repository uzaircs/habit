
import { Model } from '@nozbe/watermelondb'
import { field } from '@nozbe/watermelondb/decorators'

export default class Category extends Model {
    static table = 'categories'
    @field('name') name;
    @field('local_id') local_id;
    @field('icon') icon;
    @field('icon_type') icon_type;


}