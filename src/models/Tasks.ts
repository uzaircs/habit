import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy, json } from '@nozbe/watermelondb/decorators'
import { distinctUntilChanged, map as map$, distinctUntilKeyChanged, switchMap, repeat } from 'rxjs/operators'

import moment from 'moment'
export type TaskItem = {
    name: string;
    DueDate?: string;
    priority?: number;
    status?: number;
    list_id?: number;
    repeat?: string;
    generated_by?: string; // Repeating emmiter parent id
    is_emitter?: boolean;
    notes?: string;
    my_day?: number;
    goal_id?: string;
    parent_id?: string;
    estimatedTime?: number;
    createdAt?: number;
    updatedAt?: number;
    completedAt: number;
    reminder?: any;
}
export enum RepeatModes {
    DAILY = 1,
    WEEKLY = 2,
    MONTHLY = 3,
    ANNUAL = 4
}
export interface Repeat {
    mode: RepeatModes;
    days?: any[];
    endsBefore?: Date;
}
export const ParseRepeat = (repeat): Repeat => {
    if (!repeat || !repeat.mode) {
        return null;
    }
    let endsBefore = repeat.endsBefore
    if (endsBefore) {
        endsBefore = moment(endsBefore).toDate();
    }
    return {
        mode: repeat.mode,
        days: repeat.days,
        endsBefore
    }
}
export const sanitizeRepeat = repeat => {
    if (repeat?.mode) {
        return repeat
    }
    return {};
}
export default class Tasks extends Model {
    static table = 'tasks'
    @field('name') name;
    @field('due_date') DueDate;
    @field('notes') notes;
    @field('priority') priority;
    @field('generated_by') generatedBy;
    @field('is_emitter') isEmitter;
    @field('status') status;
    @json('repeat', sanitizeRepeat) repeat;
    @field('my_day') myDay;
    @field('estimated_time') estimatedTime; //minutes
    @relation('goals', 'goal_id') goal;
    @relation('tasks', 'parent_id') parent;
    @relation('lists', 'list_id') list;
    @readonly @date('created_at') createdAt; //unix timestamp * 1000
    @readonly @date('updated_at') updatedAt
    @date('completed_at') completedAt;
}