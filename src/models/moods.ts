
import { Model } from '@nozbe/watermelondb'
import { field, date, readonly } from '@nozbe/watermelondb/decorators'

export default class Moods extends Model {
    static table = 'moods'
    @field('title') title;
    @field('emoji') emoji;
    @field('color') color;
    @field('subtitle') subtitle;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}