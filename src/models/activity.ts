
import { Model } from '@nozbe/watermelondb'
import { field, relation } from '@nozbe/watermelondb/decorators'

export default class Activity extends Model {
    static table = 'activities'
    @field('name') name;
    @field('icon') icon;
    @field('icon_type') icon_type;
    @field('category_local') category_local;

    @relation('categories', 'category_id') category
}