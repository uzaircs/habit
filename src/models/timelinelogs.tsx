
import { Model } from '@nozbe/watermelondb'
import { field, readonly, date } from '@nozbe/watermelondb/decorators'
export enum TimelineActionTypes {
    TASK_CREATED = '@timeline-action/task-created',
    TASK_COMPLETED = '@timeline-action/task-completed',
    TASK_INCOMPLETED = '@timeline-action/task-incompleted',
    TASK_DELETE = '@timeline-action/task-deleted',
    MOOD_ADDED = '@timeline-action/mood-added',
    POMODORO_STARTED = '@timeline-action/pomodoro-started',
    POMODORO_STOPPED = '@timeline-action/pomodoro-stopped',
    DIARY_ENTRY = '@timeline-action/diary-entry',
    DIARY_ENTRY_UPDATED = '@timeline-action/diary-updated',

}
export enum TimelineActionObjectType {
    TASK = '@timeline-action/task',
    MOOD = '@timeline-action/mood',
    POMODORO = '@timeline-action/pomodoro',
    DIARY = '@timeline-action/diary',
    GENERAL = '@timeline-action/general'
}
export const ParseAction = (action) => {
    switch (action) {
        case TimelineActionTypes.DIARY_ENTRY:
            return "Diary Entry"
        case TimelineActionTypes.DIARY_ENTRY_UPDATED:
            return "Diary Updated"
        case TimelineActionTypes.MOOD_ADDED:
            return "Mood Check in"
        case TimelineActionTypes.POMODORO_STARTED:
            return "Pomodoro Started"
        case TimelineActionTypes.POMODORO_STOPPED:
            return "Pomodoro Stopped"
        case TimelineActionTypes.TASK_COMPLETED:
            return "Task Completed"
        case TimelineActionTypes.TASK_CREATED:
            return "Task Added"
        case TimelineActionTypes.TASK_DELETE:
            return "Task Deleted"
        case TimelineActionTypes.TASK_INCOMPLETED:
            return "Task Unchecked"

    }
}
export type LogContext = {
    contextType: TimelineActionObjectType;
    context: any;
}
export const GetLogContext = async (log): Promise<LogContext> => {
    let action = GetTimelineActionType(log.title);
    if (action == TimelineActionObjectType.TASK) {

        return {
            context: null,
            contextType: TimelineActionObjectType.TASK
        }
    }
}
export const GetTimelineActionType = (action) => {
    if (action == TimelineActionTypes.TASK_COMPLETED || action == TimelineActionTypes.TASK_CREATED || action == TimelineActionTypes.TASK_DELETE || action == TimelineActionTypes.TASK_INCOMPLETED) {
        return TimelineActionObjectType.TASK;
    }
    if (action == TimelineActionTypes.MOOD_ADDED) {
        return TimelineActionObjectType.MOOD
    }
    if (action == TimelineActionTypes.POMODORO_STOPPED || action == TimelineActionTypes.POMODORO_STARTED) {
        return TimelineActionObjectType.POMODORO
    }
    if (action == TimelineActionTypes.DIARY_ENTRY || action == TimelineActionTypes.DIARY_ENTRY_UPDATED) {
        return TimelineActionObjectType.DIARY
    }
    return TimelineActionObjectType.GENERAL
}
export default class TimelineLogs extends Model {
    static table = 'timeline_logs'

    @field('action_type') action;
    @field('subject_id') subject;
    @field('subject_type') subject_type;
    @field('action_description') actionDescription;
    @readonly @date('created_at') createdAt;



}