
import { Model } from '@nozbe/watermelondb'
import { field, relation, date, readonly } from '@nozbe/watermelondb/decorators'

export type UserEntryActivity = {
    title?: string;
    icon?: string;
    icon_type?: string;
    id: string;
}
export type UserEntry = {
    mood?: string;
    mood_id?: string;
    color?: string;
    id: string;
    notes?: string;
    emoji?: string;
    activity?: UserEntryActivity[];
    created_at?: Date;
    updated_at?: Date;
}

export default class ActivityEntries extends Model {
    static table = 'activity_entries'
    @relation('entries', 'entry_id') entry;
    @relation('activities', 'activity_id') activity;

}