
import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy } from '@nozbe/watermelondb/decorators'

export default class Entries extends Model {
    static table = 'entries'
    static associations: any = {
        activity_entries: { type: 'has_many', foreignKey: 'entry_id' },
    }
    @field('notes') notes;
    @relation('moods', 'mood_id') mood;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
    @lazy
    activities = this.collections
        .get('activities')
        .query(Q.on('activity_entries', 'entry_id', this.id));
}