import { schemaMigrations, addColumns, createTable } from '@nozbe/watermelondb/Schema/migrations'


export const migrations = schemaMigrations({
    migrations: [
        {
            toVersion: 18,
            steps: [
                addColumns({
                    table: 'sessions_events',
                    columns: [
                        { name: 'status', type: 'number', isOptional: true },
                    ]
                }),

            ]
        },
        {
            toVersion: 17,
            steps: [
                createTable({
                    name: 'sessions',
                    columns: [
                        { name: 'status', type: 'number', isOptional: true },
                        { name: 'name', type: 'string' },
                        { name: 'time', type: 'number', isOptional: true },
                        { name: 'shortBreak', type: 'number', isOptional: true },
                        { name: 'longBreak', type: 'number', isOptional: true },
                        { name: 'longBreakAfter', type: 'number', isOptional: true },
                        { name: 'limit', type: 'number', isOptional: true },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },

                    ]
                }),
                createTable({
                    name: 'sessions_tasks',
                    columns: [
                        { name: 'task_id', type: 'string' },
                        { name: 'session_id', type: 'string' },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },
                    ]
                }),
                createTable({
                    name: 'sessions_events',
                    columns: [
                        { name: 'session_id', type: 'string' },
                        { name: 'event_type', type: 'number', isOptional: true },
                        { name: 'start_at', type: 'number', isOptional: true },
                        { name: 'started_at', type: 'number', isOptional: true },
                        { name: 'end_at', type: 'number', isOptional: true },
                        { name: 'ended_at', type: 'number', isOptional: true },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },
                    ]
                }),

            ]
        },
        {
            toVersion: 16,
            steps: [
                addColumns({
                    table: 'diary_entry',
                    columns: [
                        { name: 'list_id', type: 'string', isOptional: true },
                    ]
                }),

            ]
        },
        {
            toVersion: 15,
            steps: [
                addColumns({
                    table: 'entries',
                    columns: [
                        { name: 'notes', type: 'string', isOptional: true },
                    ]
                }),
                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'generated_by', type: 'string', isOptional: true },
                        { name: 'is_emitter', type: 'boolean', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 14,
            steps: [
                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'notes', type: 'string', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 13,
            steps: [
                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'completed_at', type: 'number', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 12,
            steps: [
                createTable({
                    name: 'reminders',
                    columns: [
                        { name: 'item_id', type: 'string' },
                        { name: 'notification_id', type: 'string' },
                        { name: 'date', type: 'string' },
                        { name: 'item_type', type: 'number', isOptional: true },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },

                    ]
                })
            ]
        },
        {
            toVersion: 11,
            steps: [
                createTable({
                    name: 'missions',
                    columns: [
                        { name: 'name', type: 'string' },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },
                        { name: 'due_date', type: 'string', isOptional: true },
                        { name: 'priority', type: 'string', isOptional: true },
                        { name: 'status', type: 'number', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 10,
            steps: [
                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'estimated_time', type: 'number', isOptional: true },
                        { name: 'start_time', type: 'number', isOptional: true },


                    ]
                })
            ]
        },
        {
            toVersion: 9,
            steps: [
                createTable({
                    name: 'timeline_logs',
                    columns: [
                        { name: 'subject_id', type: 'string', isOptional: true },
                        { name: 'action_type', type: 'string', isOptional: true },
                        { name: 'subject_type', type: 'string', isOptional: true },
                        { name: 'created_at', type: 'number' },
                        { name: 'action_description', type: 'string', isOptional: true },

                    ]
                })
            ]
        },
        {
            toVersion: 8,
            steps: [
                addColumns({
                    table: 'diary_entry',
                    columns: [
                        { name: 'initiated_at', type: 'number', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 7,
            steps: [
                createTable({
                    name: 'diary_entry',
                    columns: [
                        { name: 'title', type: 'string', isOptional: true },
                        { name: 'text', type: 'string', isOptional: true },
                        { name: 'mood_id', type: 'string', isOptional: true },
                        { name: 'status', type: 'string', isOptional: true },
                        { name: 'tags', type: 'string', isOptional: true },
                        { name: 'status', type: 'string', isOptional: true },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },

                    ]
                })
            ]
        },
        {
            toVersion: 6,
            steps: [
                addColumns({
                    table: 'lists',
                    columns: [
                        { name: 'color', type: 'string', isOptional: true },
                    ]
                })
            ]
        },
        {
            toVersion: 5,
            steps: [
                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'list_id', type: 'string', isOptional: true },
                    ]
                }),
                createTable({
                    name: 'lists',
                    columns: [
                        { name: 'name', type: 'string' },
                        { name: 'created_at', type: 'number' },
                        { name: 'updated_at', type: 'number' },
                        { name: 'icon_type', type: 'string', isOptional: true },
                        { name: 'icon', type: 'string', isIndexed: true },

                    ],
                }),
            ],
        },
        {
            toVersion: 4,
            steps: [

                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'parent_id', type: 'string', isOptional: true },

                    ],
                }),
            ],
        },
        {
            toVersion: 3,
            steps: [

                addColumns({
                    table: 'tasks',
                    columns: [
                        { name: 'my_day', type: 'number', isOptional: true },

                    ],
                }),
            ],
        },

    ],
})