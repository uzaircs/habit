import { Model, Q } from '@nozbe/watermelondb'
import { field, relation, date, readonly, lazy } from '@nozbe/watermelondb/decorators'

export default class Goals extends Model {
    static table = 'goals'
    @field('name') name;
    @field('due_date') DueDate;
    @field('priority') priority;
    @field('status') status;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}