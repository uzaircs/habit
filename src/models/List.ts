import { Model } from "@nozbe/watermelondb";
import { field, date, readonly } from '@nozbe/watermelondb/decorators'
export type TaskListItem = {
    name?: string;
    id?: string;
    icon?: string;
    icon_type?: string;
    color?: string;
}
export default class ListModel extends Model {
    static table = 'lists';
    @field('name') name;
    @field('icon') icon;
    @field('icon_type') icon_type;
    @field('color') color;
    @readonly @date('created_at') createdAt
    @readonly @date('updated_at') updatedAt
}