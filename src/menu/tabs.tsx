import React from 'react'
import { BottomNavigation, BottomNavigationTab, Icon } from '@ui-kitten/components'
import { StyleSheet, Text } from 'react-native';
import { fontFamily } from '../styles/fonts';


const HomeIcon = (props) => (
    <Icon {...props} name='home-outline' />
);

const CalendarIcon = (props) => (
    <Icon {...props} name='calendar-outline' />
);
const useBottomNavigationState = (navigation, initialState = 0) => {
    const [selectedIndex, setSelectedIndex] = React.useState(initialState);
    const navToRoute = index => {
        let route = routes[index];
        if (route) {

            navigation.reset({
                index: 1,
                routes: [{ name: 'Dashboard' }, { name: route }],
            });
        }
        setSelectedIndex(index);
    }
    return { selectedIndex, onSelect: navToRoute };
};
const routes = [
    'Dashboard',
    'calendarView'
]
const SettingsIcon = (props) => (
    <Icon {...props} name='settings-outline' />
);
const genTitle = (title, props) => {
    return (
        <Text {...props} style={[fontFamily.regular, { color: '#aaa' }]}>{title}</Text>
    )
}
export const BottomTabs = (props) => {


    const bottomState = useBottomNavigationState(props.navigation);
    return (
        <BottomNavigation appearance="noIndicator"  {...bottomState}>
            <BottomNavigationTab icon={HomeIcon} title={props => genTitle('Home', props)} />
            <BottomNavigationTab icon={CalendarIcon} title={props => genTitle('Calendar', props)} />
            <BottomNavigationTab icon={SettingsIcon} title={props => genTitle('Settings', props)} />
        </BottomNavigation>
    )
}

