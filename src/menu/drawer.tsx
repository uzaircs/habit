import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Drawer, DrawerItem, Layout, Text, IndexPath, Icon, Divider } from '@ui-kitten/components';
import { sizes, fontFamily, fontColors } from '../styles/fonts';
import { default as theme } from '../styles/app-theme.json'
import { ImageBackground, StyleSheet } from 'react-native';
const { Navigator, Screen } = createDrawerNavigator();

const PlaceholderIcon = (props) => (
    <Icon name='home-outline' fill={theme["color-primary-500"]} style={{ width: 24, height: 24 }} />
);
const styles = StyleSheet.create({
    header: {
        height: 128,
        flexDirection: 'row',
        alignItems: 'center',
    },
})
const Header = (props) => (
    <React.Fragment>
        <ImageBackground
            style={[props.style, styles.header]}
            source={require('../images/r.png')}
        />
        <Divider />
    </React.Fragment>
);
const DrawerText = ({ text, ...props }) => {
   
    return (
        <Text style={[sizes.normal, fontFamily.semibold, { color: props.style[0].color, textAlign: 'left', flex: 1 }, { marginLeft: 8 }]}>{text}</Text>
    )
}

const DrawerContent = ({ navigation, state }) => (
    <Drawer
        header={Header}
        selectedIndex={new IndexPath(state.index)}
        onSelect={index => navigation.navigate(state.routeNames[index.row])}>
        <DrawerItem title={(props) => DrawerText({ text: "Home", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Account", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Export", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Import", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Help", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Feedback", ...props })} accessoryLeft={PlaceholderIcon} />
        <DrawerItem title={(props) => DrawerText({ text: "Privacy Policy", ...props })} accessoryLeft={PlaceholderIcon} />

    </Drawer>
);

export const DrawerNavigator = ({ stack }) => (
    <Navigator drawerContent={props => <DrawerContent {...props} />}>
        <Screen component={stack} name="MainScreen"></Screen>
    </Navigator>
);
