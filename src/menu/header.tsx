import React from 'react';
import { Icon, Text, TopNavigation, TopNavigationAction, Layout, OverflowMenu, MenuItem, Avatar } from '@ui-kitten/components';
import { fontFamily, sizes, fontColors } from '../styles/fonts';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { View } from 'native-base';
import { iOSUIKit } from 'react-native-typography';
import { CONTAINER_PADDING, MENU_HEIGHT_COLLAPSED, MENU_HEIGHT_EXPANED } from '../common/constants';
import { TextMuted, H1, TextMutedStrong, FontFamilies } from '../styles/text';
import moment from 'moment';
import Animated from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';
export const styles = StyleSheet.create({
    menuShadow: {
      
        elevation: 2,
    }
})
export const genTitle = (title, props) => {
    return (
        <Text {...props} style={[fontFamily.regular, sizes.normal, { width: '100%' }]}>{title}</Text>
    )
}
const MenuIcon = (props) => (
    <Icon {...props} name='menu' />

);
const BackIcon = (props) => (
    <Icon {...props} name='arrow-back-outline' />

);

const BackAction = ({ navigation }) => (
    <TopNavigationAction onPress={() => navigation.toggleDrawer()} icon={MenuIcon} />
);
export const BackButton = ({ navigation }) => (
    <TopNavigationAction onPress={() => navigation.pop()} icon={BackIcon} />
);
export const NavTitle = (props) => (
    <View>
        <Text category="h6" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily}}>{props.title ? props.title : 'Daily Me'}</Text>
        {props.subtitle && <Text style={[fontFamily.regular, sizes.small, fontColors.muted]}>{props.subtitle}</Text>}
    </View>
)

const StarIcon = (props) => (
    <Icon {...props} name='star' />
);
export const AvatarMenu = () => {
    const [selectedIndex, setSelectedIndex] = React.useState(null);
    const [visible, setVisible] = React.useState(false);
    const renderAvatarToggle = () => (
        <TouchableOpacity onPress={() => setVisible(true)}>
            <Avatar source={require('../images/r.png')} />
        </TouchableOpacity>
    );

    const dismiss = () => {
        setVisible(false);
    }

    return (
        <OverflowMenu
            anchor={renderAvatarToggle}
            visible={visible}
            selectedIndex={selectedIndex}
            onSelect={() => { }}
            onBackdropPress={() => setVisible(false)}>
            <MenuItem title={props => genTitle('Login', props)} accessoryLeft={StarIcon} onPress={dismiss} />
            <MenuItem title={props => genTitle('Account', props)} accessoryLeft={StarIcon} onPress={dismiss} />
            <MenuItem title={props => genTitle('Settings', props)} accessoryLeft={StarIcon} onPress={dismiss} />
        </OverflowMenu>
    )
}
export const TopNav = ({ title = "Welcome", navigation, height = MENU_HEIGHT_COLLAPSED, showDate = true, ...props }) => {

    if (props.animated) {
        return (<Animated.View style={{ backgroundColor: 'white', height: props.animated, width: '100%', elevation: 1, position: 'absolute', left: 0, top: 0 }}>
            <Animated.View style={{ width: '100%', padding: CONTAINER_PADDING, flexDirection: 'row' }}>
                <View style={{ width: '70%' }}>
                    {height > MENU_HEIGHT_COLLAPSED && <TextMuted text={moment().format('dddd D MMM').toUpperCase()}></TextMuted>}
                    <Animated.Text numberOfLines={1} style={[iOSUIKit.title3Emphasized, { fontSize: props.fontSize }]}>{title}</Animated.Text>

                    <Animated.View style={{ opacity: props.opacity }}>
                        <TextMutedStrong text={props.subheading}></TextMutedStrong>
                    </Animated.View>
                </View>
                <View style={{ width: '30%' }}>
                    {props.accessoryRight && props.accessoryRight()}
                </View>
            </Animated.View>
            <Animated.View style={{ width: '100%', padding: CONTAINER_PADDING, opacity: props.opacity }}>
                {props.children}
            </Animated.View>
        </Animated.View>)
    }
    return (
        <View style={{ backgroundColor: 'white', height, width: '100%', }} {...props}>
            <View style={{ width: '100%', height: height + (props.subheading ? 20 : 0), padding: CONTAINER_PADDING, flexDirection: 'row' }}>
                <View style={{ width: '70%', justifyContent: 'center', top: 8, }}>
                    {/*    {height > MENU_HEIGHT_COLLAPSED && <TextMuted text={moment().format('dddd D MMM').toUpperCase()}></TextMuted>} */}
                    <Text numberOfLines={1} category="h5" style={{ fontFamily: FontFamilies.quicksandbold.fontFamily }}>{title}</Text>
                    <TextMutedStrong text={props.subheading}></TextMutedStrong>
                </View>
                <View style={{ width: '30%' }}>
                    {props.accessoryRight && props.accessoryRight()}
                </View>
            </View>
            <View style={{ width: '100%', height: MENU_HEIGHT_EXPANED - MENU_HEIGHT_COLLAPSED, padding: CONTAINER_PADDING }}>
                {props.children}
            </View>
        </View>
    )
};
export const BackNav = ({ title, subtitle = false, ...props }) => {
    const navigation = useNavigation();
    if (props.ac_right) {
        return (
            <TopNavigation
                style={styles.menuShadow}
                accessoryRight={props.ac_right}
                accessoryLeft={() => <BackButton navigation={navigation}></BackButton>}
                title={(props) => <NavTitle {...props} title={title} subtitle={subtitle}></NavTitle>}
            />
        )
    } else {
        return (
            <TopNavigation
                style={styles.menuShadow}
                accessoryLeft={() => <BackButton navigation={navigation}></BackButton>}
                title={(props) => <NavTitle {...props} title={title} subtitle={subtitle}></NavTitle>}
            />
        )
    }
};





