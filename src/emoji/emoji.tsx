import React from 'react'
import { Image } from 'react-native'
export const EmojiSet = [
    {
        name: 'smiling',

        gif: require('../emoji/smiling_a.gif'),
        png: require('../emoji/smiling.png'),
        tags: ['smile', 'laugh', 'happy']
    },
    {
        name: 'good',

        /*     gif: require('../emoji/good.gif'), */
        png: require('../emoji/good.png'),
        tags: ['smile', 'laugh', 'happy']
    },
    {
        name: 'meh',
        /*     gif: require('../emoji/meh.gif'), */
        png: require('../emoji/meh.png'),
        tags: ['smile', 'laugh', 'happy']
    },
    {
        name: 'bad',
        /*   gif: require('../emoji/bad.gif'), */
        png: require('../emoji/bad.png'),
        tags: ['smile', 'laugh', 'happy']
    },
    {
        name: 'awful',
        /*   gif: require('../emoji/awful.gif'), */
        png: require('../emoji/awful.png'),
        tags: ['smile', 'laugh', 'happy']
    },
]
export const Emoji = ({ name, gif = false, size = { width: 36, height: 36 } }) => {

    let _emoji = null;
    let _item = EmojiSet.find(x => x.name === name.toLowerCase());
    if (!_item) {
        return <></>;
    } else {
        _emoji = _item.png;
    }
    if (gif) {

        if (_item.gif) {
            _emoji = _item.gif;
        }
    }
    return (
        <Image source={_emoji} style={size} />
    )
}
