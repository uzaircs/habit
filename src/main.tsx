import React from 'react'
import { connect } from 'react-redux'
import { Text } from 'react-native'
import { Icon as NBIcon, Toast } from 'native-base'
import { Layout, Icon, Button } from '@ui-kitten/components'
import { default as theme } from './styles/app-theme.json'
import { fontFamily, sizes } from './styles/fonts'

import { CardStyleInterpolators, createStackNavigator } from '@react-navigation/stack';
import { DrawerNavigator } from './menu/drawer'

import CustomCalendar from './calendar/calendar'


import Plan from './components/plan/plan'
import moment from 'moment'
import { ActivityPage } from './components/activity.page'
import { GroupUserEntries } from './components/entry/past'
import { PopulateEntries, RequestListAsync } from './actions/entry.actions'
import { details } from './components/entry/details'
import { IconSelectionNav } from './components/forms/icon.selector'
import { CategoryForm } from './components/forms/category'
import { CreateActivityForm } from './components/forms/activity.form'

import { TaskDetails } from './components/task/details'
import DayContainer from './components/day/day.tabs'
import { StartWork } from './components/day/start.work'
import { WorkSession } from './components/day/work.session'
import { TaskSelector } from './components/day/task.selector'
import GoalsList from './components/goals/list'
import AddGoal from './components/goals/add'
import GoalItem from './components/goals/item'
import { Scan } from './auth/scan'
import AddDiaryEntry from './components/diary/diary.add'
import DiaryDay from './components/diary/day.diary'
import TimelineList from './components/timeline/timeline.list'
import { Populate } from './actions/task.action'
import { PopulateGoals } from './actions/goal.action'
import { RequestDiaryListAsync } from './actions/diary.action'
import TaskDeadline from './components/task/task.deadlines'
import { WorkTab } from './components/day/work'
import EntriesList from './components/entry/entries.list'
import GoalProgressScreen from './components/goals/progress'
import { AuthScreen } from './auth/auth.screen'
import { PopulateMoods } from './actions/mood.action'
import RepeatModal from './components/task/repeat.modal'
import { PopulateRepeatItems } from './actions/task.repeat.action'
import MoodsHistoryView from './components/entry/history.view'
import DiaryView from './components/diary/diary.view'
import { UpdateListAsync } from './actions/list.action'
import { RequestLogs } from './actions/timeline.action'
import Playground from './components/playground'
import { PopulateSession, PopulateSessionEvents } from './actions/session.action'
import CreateSession from './components/session/session.create'
import TimerV2 from './components/timer/timer.v2'

const Stack = createStackNavigator();
const FacebookIcon = (props) => (
    <Icon name='home-outline' fill={theme["color-primary-500"]} style={{ width: 24, height: 24 }} />
);
export const PlayIcon = (props) => (
    <Icon name='play-circle-outline' {...props} />
);
const NBFontAwesomeIcon = (props) => {
    return (
        <NBIcon type="FontAwesome" name="home" />
    )
}
export const EmojiEmpty = (props) => {
    return (
        <NBIcon type="FontAwesome5" name="meh-blank" style={{ fontSize: 24, color: theme["color-font-300"] }} {...props} />
    )
}
const NBMaterialIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="home" />
    )
}
export const DotIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="checkbox-blank-circle-outline" {...props} />
    )
}
export const DotFilledIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="checkbox-blank-circle" {...props} />
    )
}
export const DotCircleIcon = (props) => {
    return (
        <NBIcon type="MaterialCommunityIcons" name="checkbox-blank-circle" {...props} />
    )
}
const _Dashboard = ({ navigation, items, requestItems, ready, isLoading, error, ...props }) => {
    let grouped = GroupUserEntries(items);
    return (
        <Layout style={{ flex: 1 }} level="1">

            <DayContainer request={requestItems} grouped={grouped} navigation={navigation} {...props}></DayContainer>
        </Layout>
    )
}
const mapStateToProps = (state) => ({
    items: state.entryStorage.entries.filter(entry => moment.unix(+entry.created_at / 1000).isSame(moment(), 'day')),
    isLoading: state.entryStorage.loading,
    ready: state.entryStorage.ready,
    error: state.entryStorage.error
})

const mapDispatchToProps = dispatch => {
    return {
        requestItems: () => {
            dispatch(PopulateEntries());
            dispatch(Populate());
            dispatch(PopulateRepeatItems());
            dispatch(PopulateMoods());
            dispatch(PopulateGoals());
            dispatch(RequestDiaryListAsync());
            dispatch(UpdateListAsync());
            dispatch(RequestLogs());
            dispatch(PopulateSession());
            dispatch(PopulateSessionEvents())
        }
    }
}
export const Dashboard = connect(mapStateToProps, mapDispatchToProps)(_Dashboard);
export const CalendarView = (props) => {
    return (
        <CustomCalendar></CustomCalendar>
    )
}
export const HomePage = (props) => {

    return (
        <Layout style={{ flex: 1 }}>

            <Dashboard></Dashboard>
        </Layout>
    )
}


export const AnotherComponent = ({ navigation }) => {
    return (
        <Layout style={{ flex: 1 }}>
            <Text style={[fontFamily.bold, { color: 'white' }, sizes.large]}>Hello World</Text>

            <Layout
                style={{ flexDirection: 'row', flex: 1, }}

            >
                <CustomCalendar></CustomCalendar>
            </Layout>
            <Button onPress={() => navigation.pop()}>Go Back</Button>
        </Layout>
    )
}
export const StackNav = ({ navigation }) => (
    <Stack.Navigator screenOptions={{
        headerShown: false,
        gestureEnabled: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS

    }}>
        {/*   <Stack.Screen name="Login" component={AuthScreen} /> */}
        {/* for debugging */}
        {/*   <Stack.Screen name="Playground" component={Playground} /> */}

        <Stack.Screen name="Home" component={HomePage} />
        <Stack.Screen name="Dashboard" component={Dashboard} />
        <Stack.Screen name="entryPage" component={ActivityPage} />
        <Stack.Screen name="itemDetail" component={details}></Stack.Screen>
        <Stack.Screen name="moodsList" component={EntriesList}></Stack.Screen>
        <Stack.Screen name="moodsView" component={MoodsHistoryView}></Stack.Screen>
        <Stack.Screen name="taskDetail" component={TaskDetails}></Stack.Screen>
        <Stack.Screen name="goalList" component={GoalsList}></Stack.Screen>
        <Stack.Screen name="goalProgress" component={GoalProgressScreen}></Stack.Screen>
        <Stack.Screen name="StartWork" component={StartWork} />
        <Stack.Screen name="addCategory" component={CategoryForm} />
        <Stack.Screen name="addGoal" component={AddGoal} />
        <Stack.Screen name="RepeatModal" component={RepeatModal} />
        <Stack.Screen name="work" component={WorkTab} />
        <Stack.Screen name="scan" component={Scan} />
        <Stack.Screen name="addDiaryEntry" component={AddDiaryEntry} />
        <Stack.Screen name="diaryView" component={DiaryView} />
        <Stack.Screen name="diaryList" component={DiaryDay} />
        <Stack.Screen name="timelineScreen" component={TimelineList} />
        <Stack.Screen name="TimerV2" component={TimerV2} />
        <Stack.Screen name="deadlineScreen" component={TaskDeadline} />
        <Stack.Screen name="goalItem" component={GoalItem} />
        <Stack.Screen name="addActivity" component={CreateActivityForm} />
        <Stack.Screen name="IconSelectionNav" component={IconSelectionNav} />
        <Stack.Screen name="PlanPage" component={Plan} />
        <Stack.Screen name="createSession" component={CreateSession} />
        <Stack.Screen name="Session" children={() => <WorkSession navigation={navigation}></WorkSession>} />
        <Stack.Screen name="TaskSelector" children={() => <TaskSelector navigation={navigation}></TaskSelector>} />

    </Stack.Navigator>
)

// TODO: Add state management
export const Walkthrough = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false,
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid

        }}>
            <Stack.Screen name="drawerRoot" component={DrawerRoot} />
            {/*   <Stack.Screen name="onboarding" children={(props) => <OnBoarding ondone={() => Done(props.navigation)} onskip={() => Done(props.navigation)} {...props}></OnBoarding>} /> */}

        </Stack.Navigator>
    )
}
export const DrawerRoot = () => {

    return (
        <DrawerNavigator stack={StackNav}></DrawerNavigator>
    )
}
