import Carousel from 'react-native-snap-carousel';
import React, { Component } from 'react';
import { View, Text, Dimensions } from 'react-native';
import { Card } from '@ui-kitten/components';
import { fontFamily, sizes } from '../styles/fonts';
import { default as theme } from '../styles/app-theme.json'
export class SimpleCarousel extends Component<any, any> {

    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                width: Dimensions.get('window').width,
                height: Dimensions.get('window').height
            },
            entries: [
                {
                    title: 'Item 1'
                },
                {
                    title: 'Item 2'
                },
                {
                    title: 'Item 3'
                },
                {
                    title: 'Item 4'
                }
            ]
        }

    }

    _renderItem = ({ item, index }) => {
        return (
            <Card status="primary" appearance="filled" style={{ elevation: 2, minHeight: 160 }}>
                <Text style={[fontFamily.semibold, sizes.large, { color: '#fff' }]}>{item.title}</Text>
            </Card>
        );
    }

    render() {
        let { layout = 'default' } = this.props;
        return (
            <Carousel
                data={this.state.entries}
                renderItem={this._renderItem}

                itemWidth={this.state.viewport.width - 20}
                layout={layout}
                sliderWidth={this.state.viewport.width}
            />
        );
    }
}