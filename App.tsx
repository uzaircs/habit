/* import './track'; */
import React, { Component } from 'react';
import { Root, Toast } from "native-base";

// Ui kitten wrapper for User interface
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';

import { ApplicationProvider, IconRegistry, Layout } from '@ui-kitten/components';
// End UI Kitten imports

if (__DEV__) {
  import('./reactotron.config').then(() => console.log('Reactotron Configured'))

}
import { Provider } from 'react-redux'
import store from './src/store/store'
import { Walkthrough, StackNav } from './src/main';
import { default as theme } from './src/styles/app-theme.json'
import { NavigationContainer, NavigationContext } from '@react-navigation/native';
import { StatusBar } from 'react-native';
import SplashScreen from 'react-native-splash-screen'
import { seed } from './src/models/database';
import { CenterLoading } from './src/components/activity.page';
import { activateKeepAwake } from 'expo-keep-awake';

// TODO Icons search
// TODO add calendar
// TODO map data to calendar


export default class App extends Component<any, any>{
  /**
   *
   */
  static contextType = NavigationContext;
  constructor(props) {
    super(props);
    StatusBar.setBackgroundColor(theme["color-primary-300"]);
    StatusBar.setHidden(false);
    this.state = {
      ready: false
    }
  }
  componentDidMount() {
    if (__DEV__) {
      activateKeepAwake();
    }
    seed().then(() => {
      this.setState({ ready: true });
    }).catch((err) => {


    })
    setTimeout(() => {
      SplashScreen.hide();


    }, 500)
  }
  render() {
    const navigation = this.context;

    return (
      /* 
         <EntryComponent></EntryComponent>
      */
      /* Native base root component */
      <Provider store={store}>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider {...eva} theme={{ ...eva.light, ...theme }}>
          <Root>
            <NavigationContainer>
              {/* Eva design / UI Kitten ApplicationProvider */}


              {/* EntryComponent (can be reaplaced with your component) */}

              {/*  <Walkthrough></Walkthrough> */}
              {this.state.ready && <StackNav navigation={navigation}></StackNav>}
              {!this.state.ready && <CenterLoading></CenterLoading>}
              {/*     <Storybook></Storybook> */}
            </NavigationContainer>
          </Root>
        </ApplicationProvider>
      </Provider>
    )
  }
}
